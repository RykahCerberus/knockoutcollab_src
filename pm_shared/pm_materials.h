/***
*
*	Copyright (c) 1996-2002, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/
#if !defined( PM_MATERIALSH )
#define PM_MATERIALSH
#pragma once

#define CBTEXTURENAMEMAX	13			// only load first n chars of name

// texture types
#define CHAR_TEX_CONCRETE	'C'			
#define CHAR_TEX_METAL		'M'
#define CHAR_TEX_DIRT		'D'
#define CHAR_TEX_VENT		'V'
#define CHAR_TEX_GRATE		'G'
#define CHAR_TEX_TILE		'T'
#define CHAR_TEX_SLOSH		'S'
#define CHAR_TEX_WOOD		'W'
#define CHAR_TEX_COMPUTER	'P'
#define CHAR_TEX_GLASS		'Y'
#define CHAR_TEX_FLESH		'F'

#define CHAR_TEX_LADDER		'L'
#define CHAR_TEX_WADE		'Q'

#define CHAR_TEX_SNOW		'O'
#define CHAR_TEX_ORGAN		'A'
#define CHAR_TEX_GRASS		'X'
#define CHAR_TEX_CARPET		'Z'
#define CHAR_TEX_MUD		'E'
#define CHAR_TEX_TIN		'I'
#define CHAR_TEX_CHAIN		'K'
#define CHAR_TEX_TOXIC		'J'
#define CHAR_TEX_CARDB		'N'
#define CHAR_TEX_GRAVEL		'R'

#define CHAR_TEX_NOTHING	'U'


#define STEP_CONCRETE	0		// default step sound
#define STEP_METAL		1		// metal floor
#define STEP_DIRT		2		// dirt, sand, rock
#define STEP_VENT		3		// ventillation duct
#define STEP_GRATE		4		// metal grating
#define STEP_TILE		5		// floor tiles
#define STEP_SLOSH		6		// shallow liquid puddle
#define STEP_WADE		7		// wading in liquid
#define STEP_LADDER		8		// climbing ladder
#define STEP_FLESH		19		// flesh
#define STEP_WOOD		23		// wood
#define STEP_GLASS		24		// glass
#define STEP_COMPUTER	25		// computer

#define STEP_SNOW		9		// czds snow
#define STEP_ORGAN		11		// op4 organic
#define STEP_GRASS		12		// czds grass
#define STEP_CARPET		13		// czds carpet
#define STEP_MUD		14		// czds mud
#define STEP_TIN		15		// czds tin
#define STEP_CHAIN		16		// hl2 chainlink
#define STEP_TOXIC		17		// hl2 mud
#define STEP_CARDB		20		// l4d cardboard
#define STEP_GRAVEL		21		// czds gravel

#define STEP_NOTHING	22		// nothing at all

#endif // !PM_MATERIALSH
