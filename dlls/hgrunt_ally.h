
#ifndef HGRUNT_ALLY_H
#define HGRUNT_ALLY_H

extern int g_fGruntAllyQuestion;				// true if an idle grunt asked a question. Cleared when someone answers.

extern DLL_GLOBAL int		g_iSkillLevel;

//=========================================================
// monster-specific DEFINE's
//=========================================================
#define	GRUNT_MP5_CLIP_SIZE				36 // how many bullets in a clip? - NOTE: 3 round burst sound, so keep as 3 * x!
#define GRUNT_SHOTGUN_CLIP_SIZE			8
#define GRUNT_SAW_CLIP_SIZE				36
#define GRUNT_VOL						0.35		// volume of grunt sounds
#define GRUNT_ATTN						ATTN_NORM	// attenutation of grunt sentences
#define HGRUNT_LIMP_HEALTH				20
#define HGRUNT_DMG_HEADSHOT				( DMG_BULLET | DMG_CLUB )	// damage types that can kill a grunt with a single headshot.
#define HGRUNT_NUM_HEADS				2 // how many grunt heads are there? 
#define HGRUNT_MINIMUM_HEADSHOT_DAMAGE	15 // must do at least this much damage in one shot to head to score a headshot kill
#define	HGRUNT_SENTENCE_VOLUME			(float)0.35 // volume of grunt sentences

namespace HGruntAllyWeaponFlag
{
	enum HGruntAllyWeaponFlag
	{
		MP5 = 1 << 0,
		HandGrenade = 1 << 1,
		GrenadeLauncher = 1 << 2,
		Shotgun = 1 << 3,
		Saw = 1 << 4
	};
}

namespace HGruntAllyBodygroup
{
	enum HGruntAllyBodygroup
	{
		Head = 1,
		Torso = 2,
		Weapons = 3
	};
}

namespace HGruntAllyHead
{
	enum HGruntAllyHead
	{
		Default = -1,
		GasMask = 0,
		BeretWhite,
		OpsMask,
		BandanaWhite,
		BandanaBlack,
		MilitaryPolice,
		Commander,
		BeretBlack,
	};
}

namespace HGruntAllyTorso
{
	enum HGruntAllyTorso
	{
		Normal = 0,
		Saw,
		Nothing,
		Shotgun
	};
}

namespace HGruntAllyWeapon
{
	enum HGruntAllyWeapon
	{
		MP5 = 0,
		Shotgun,
		Saw,
		None
	};
}

//=========================================================
// Monster's Anim Events Go Here
//=========================================================
#define		HGRUNT_AE_RELOAD		( 2 )
#define		HGRUNT_AE_KICK			( 3 )
#define		HGRUNT_AE_BURST1		( 4 )
#define		HGRUNT_AE_BURST2		( 5 ) 
#define		HGRUNT_AE_BURST3		( 6 ) 
#define		HGRUNT_AE_GREN_TOSS		( 7 )
#define		HGRUNT_AE_GREN_LAUNCH	( 8 )
#define		HGRUNT_AE_GREN_DROP		( 9 )
#define		HGRUNT_AE_CAUGHT_ENEMY	( 10) // grunt established sight with an enemy (player only) that had previously eluded the squad.
#define		HGRUNT_AE_DROP_GUN		( 11) // grunt (probably dead) is dropping his mp5.

//=========================================================
// monster-specific schedule types
//=========================================================
enum
{
	SCHED_GRUNT_SUPPRESS = LAST_TALKMONSTER_SCHEDULE + 1,
	SCHED_GRUNT_ESTABLISH_LINE_OF_FIRE,// move to a location to set up an attack against the enemy. (usually when a friendly is in the way).
	SCHED_GRUNT_COVER_AND_RELOAD,
	SCHED_GRUNT_SWEEP,
	SCHED_GRUNT_FOUND_ENEMY,
	SCHED_GRUNT_REPEL,
	SCHED_GRUNT_REPEL_ATTACK,
	SCHED_GRUNT_REPEL_LAND,
	SCHED_GRUNT_WAIT_FACE_ENEMY,
	SCHED_GRUNT_TAKECOVER_FAILED,// special schedule type that forces analysis of conditions and picks the best possible schedule to recover from this type of failure.
	SCHED_GRUNT_ELOF_FAIL,
};

//=========================================================
// monster-specific tasks
//=========================================================
enum
{
	TASK_GRUNT_FACE_TOSS_DIR = LAST_TALKMONSTER_TASK + 1,
	TASK_GRUNT_SPEAK_SENTENCE,
	TASK_GRUNT_CHECK_FIRE,
};

//=========================================================
// monster-specific conditions
//=========================================================
#define bits_COND_GRUNT_NOFIRE	( bits_COND_SPECIAL1 )

class CHGruntAlly : public COFSquadTalkMonster
{
public:
	virtual void Spawn() override;
	virtual void Precache() override;
	void SetYawSpeed() override;
	virtual int  Classify() override;
	int ISoundMask() override;
	virtual void HandleAnimEvent(MonsterEvent_t* pEvent) override;
	BOOL FCanCheckAttacks() override;
	BOOL CheckMeleeAttack1(float flDot, float flDist) override;
	virtual BOOL CheckRangeAttack1(float flDot, float flDist) override;
	virtual BOOL CheckRangeAttack2(float flDot, float flDist) override;
	void CheckAmmo() override;
	virtual void SetActivity(Activity NewActivity) override;
	virtual void StartTask(Task_t* pTask) override;
	virtual void RunTask(Task_t* pTask) override;
	virtual void DeathSound() override;
	virtual void PainSound() override;
	void IdleSound() override;
	Vector GetGunPosition() override;
	virtual void Shoot();
	virtual void Shotgun();
	void PrescheduleThink() override;
	virtual void GibMonster() override;
	void SpeakSentence();

	int	Save(CSave& save) override;
	int Restore(CRestore& restore) override;

	CBaseEntity* Kick();
	virtual Schedule_t* GetSchedule() override;
	virtual Schedule_t* GetScheduleOfType(int Type) override;
	void TraceAttack(entvars_t* pevAttacker, float flDamage, Vector vecDir, TraceResult* ptr, int bitsDamageType) override;
	virtual int TakeDamage(entvars_t* pevInflictor, entvars_t* pevAttacker, float flDamage, int bitsDamageType) override;

	BOOL FOkToSpeak();
	void JustSpoke();

	virtual int ObjectCaps() override;

	void TalkInit();

	void AlertSound() override;

	void DeclineFollowing() override;

	virtual void ShootSaw();

	void KeyValue(KeyValueData* pkvd) override;

	virtual void Killed(entvars_t* pevAttacker, int iGib) override;

	MONSTERSTATE GetIdealState() override
	{
		return COFSquadTalkMonster::GetIdealState();
	}

	CUSTOM_SCHEDULES;
	static TYPEDESCRIPTION m_SaveData[];

	BOOL m_lastAttackCheck;
	float m_flPlayerDamage;

	// checking the feasibility of a grenade toss is kind of costly, so we do it every couple of seconds,
	// not every server frame.
	float m_flNextGrenadeCheck;
	float m_flNextPainTime;
	float m_flLastEnemySightTime;

	Vector m_vecTossVelocity;

	BOOL m_fThrowGrenade;
	BOOL m_fStanding;
	BOOL m_fFirstEncounter;// only put on the handsign show in the squad's first encounter.
	int m_cClipSize;

	int m_iBrassShell;
	int m_iShotgunShell;
	int m_iSawShell;
	int m_iSawLink;

	int m_iSentence;

	int m_iWeaponIdx;
	int m_iGruntHead;
	int m_iGruntTorso;

	static const char* pGruntSentences[];

	BOOL m_bFriendlyToScientists;

	bool FGRUNT_FriendlyToSci() override { return m_bFriendlyToScientists; }
	virtual int IRelationship(CBaseEntity* pTarget) override;

	virtual int MyTalkMonsterType() { return TLKMON_TYPE_ALLY_GRUNT; }
};

#endif
