/***
*
*	Copyright (c) 1996-2001, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/
//
// teamplay_gamerules.cpp
//
#include	"extdll.h"
#include	"util.h"
#include	"cbase.h"
#include	"player.h"
#include	"weapons.h"
#include	"gamerules.h"
#include	"skill.h"
#include	"items.h"
#include	"UserMessages.h"
#include	"hltv.h"

extern DLL_GLOBAL CGameRules	*g_pGameRules;
extern DLL_GLOBAL BOOL	g_fGameOver;

char* g_szDeathType;

//=========================================================
//=========================================================
CHalfLifeRules::CHalfLifeRules()
{
	RefreshSkillData();
}

//=========================================================
//=========================================================
void CHalfLifeRules::Think ()
{
}

//=========================================================
//=========================================================
BOOL CHalfLifeRules::IsMultiplayer()
{
	return FALSE;
}

//=========================================================
//=========================================================
BOOL CHalfLifeRules::IsDeathmatch ()
{
	return FALSE;
}

//=========================================================
//=========================================================
BOOL CHalfLifeRules::IsCoOp()
{
	return FALSE;
}


//=========================================================
//=========================================================
BOOL CHalfLifeRules::FShouldSwitchWeapon( CBasePlayer *pPlayer, CBasePlayerItem *pWeapon )
{
	if ( !pPlayer->m_pActiveItem )
	{
		// player doesn't have an active item!
		return TRUE;
	}

	if ( !pPlayer->m_pActiveItem->CanHolster() )
	{
		return FALSE;
	}

	return TRUE;
}

//=========================================================
//=========================================================
BOOL CHalfLifeRules :: GetNextBestWeapon( CBasePlayer *pPlayer, CBasePlayerItem *pCurrentWeapon )
{
	return FALSE;
}

//=========================================================
//=========================================================
BOOL CHalfLifeRules :: ClientConnected( edict_t *pEntity, const char *pszName, const char *pszAddress, char szRejectReason[ 128 ] )
{
	return TRUE;
}

void CHalfLifeRules :: InitHUD( CBasePlayer *pl )
{
}

//=========================================================
//=========================================================
void CHalfLifeRules :: ClientDisconnected( edict_t *pClient )
{
}

//=========================================================
//=========================================================
float CHalfLifeRules::FlPlayerFallDamage( CBasePlayer *pPlayer )
{
	// subtract off the speed at which a player is allowed to fall without being hurt,
	// so damage will be based on speed beyond that, not the entire fall
	pPlayer->m_flFallVelocity -= PLAYER_MAX_SAFE_FALL_SPEED;
	return pPlayer->m_flFallVelocity * DAMAGE_FOR_FALL_SPEED;
}

//=========================================================
//=========================================================
void CHalfLifeRules :: PlayerSpawn( CBasePlayer *pPlayer )
{
	CBaseEntity	*pWeaponEntity = NULL;

	//LRC- support the new "start with HEV" flag...
	if (g_startSuit){
		pPlayer->pev->weapons |= (1<<WEAPON_SUIT);
	}

// LRC what's wrong with allowing "game_player_equip" entities in single player? (The
// level designer is God: if he wants the player to start with a weapon, we should allow it!)
	while ( pWeaponEntity = UTIL_FindEntityByClassname( pWeaponEntity, "game_player_equip" ))
	{
		pWeaponEntity->Touch( pPlayer );
	}
}

//=========================================================
//=========================================================
BOOL CHalfLifeRules :: AllowAutoTargetCrosshair()
{
	return ( g_iSkillLevel == SKILL_EASY );
}

//=========================================================
//=========================================================
void CHalfLifeRules :: PlayerThink( CBasePlayer *pPlayer )
{
}


//=========================================================
//=========================================================
BOOL CHalfLifeRules :: FPlayerCanRespawn( CBasePlayer *pPlayer )
{
	return TRUE;
}

//=========================================================
//=========================================================
float CHalfLifeRules :: FlPlayerSpawnTime( CBasePlayer *pPlayer )
{
	return gpGlobals->time;//now!
}

//=========================================================
// IPointsForKill - how many points awarded to anyone
// that kills this player?
//=========================================================
int CHalfLifeRules :: IPointsForKill( CBasePlayer *pAttacker, CBasePlayer *pKilled )
{
	return 1;
}

//=========================================================
// PlayerKilled - someone/something killed this player
//=========================================================
void CHalfLifeRules :: PlayerKilled( CBasePlayer *pVictim, entvars_t *pKiller, entvars_t *pInflictor )
{
}

//=========================================================
// Deathnotice
//=========================================================
void CHalfLifeRules::DeathNotice( CBasePlayer *pVictim, entvars_t *pKiller, entvars_t *pevInflictor)
{
	//if (gpgs && gpgs->m_bDeathNotice)
	{
		// Work out what killed the player, and send a message to all clients about it
		CBaseEntity* Killer = CBaseEntity::Instance(pKiller);

		const char* killer_weapon_name = "world";		// by default, the player is killed by the world
		const char* kill_technique = "killed %";	//AJH default is 'killed' (% = victim's name)
		int killer_index = 0;

		// Hack to fix name change
		const char* tau = "tau_cannon";
		const char* gluon = "gluon gun";

		if (g_szDeathType)
		{
			killer_weapon_name = g_szDeathType;
		}
		else
		{
			if (pKiller->flags & FL_CLIENT)
			{
				killer_index = ENTINDEX(ENT(pKiller));

				if (pevInflictor)
				{
					if (pevInflictor == pKiller)
					{
						// If the inflictor is the killer,  then it must be their current weapon doing the damage
						CBasePlayer* pPlayer = (CBasePlayer*)CBaseEntity::Instance(pKiller);

						if (pPlayer->m_pActiveItem)
						{
							killer_weapon_name = pPlayer->m_pActiveItem->pszName();
						}
					}
					else
					{
						killer_weapon_name = STRING(pevInflictor->classname);  // it's just that easy
					}
				}
			}
			else
			{
				killer_weapon_name = STRING(pevInflictor->classname);
			}

			// strip the monster_* or weapon_* from the inflictor's classname
			if (strncmp(killer_weapon_name, "ARgr", 4) == 0)
				killer_weapon_name += 2;
			else if (strncmp(killer_weapon_name, "weapon_", 7) == 0)
				killer_weapon_name += 7;
			else if (strncmp(killer_weapon_name, "monster_", 8) == 0)
				killer_weapon_name += 8;
			else if (strncmp(killer_weapon_name, "func_", 5) == 0)
				killer_weapon_name += 5;


			if (pevInflictor) {
				CBaseEntity* Inflictor = CBaseEntity::Instance(pevInflictor);
				if (Inflictor->killname) {
					killer_weapon_name = STRING(Inflictor->killname);//AJH Custom 'kill' names for entities

				}
				if (Inflictor->killmethod) {
					kill_technique = STRING(Inflictor->killmethod);//AJH Custom 'kill' techniques for entities
				}
			}
		}

		//	if(CBaseEntity* game_deathnotice = UTIL_FindEntityByClassname(NULL,"game_deathnotice")){ //AJH fully custom/random kill strings
				//Figure out how to implement
		//	}

		MESSAGE_BEGIN(MSG_ALL, gmsgDeathMsg);
		WRITE_BYTE(killer_index);						// the killer
		WRITE_BYTE(ENTINDEX(pVictim->edict()));		// the victim
		WRITE_STRING(killer_weapon_name);		// what they were killed by (should this be a string?)
		WRITE_STRING(kill_technique); //AJH How the victim was killed.
		MESSAGE_END();

		//Not done by Op4
		/*
		// replace the code names with the 'real' names
		if ( !strcmp( killer_weapon_name, "egon" ) )
			killer_weapon_name = gluon;
		else if ( !strcmp( killer_weapon_name, "gauss" ) )
			killer_weapon_name = tau;
			*/

		if (pVictim->pev == pKiller)
		{
			// killed self
			UTIL_LogPrintf("\"%s<%i><%s><%i>\" committed suicide with \"%s\"\n",
				STRING(pVictim->pev->netname),
				GETPLAYERUSERID(pVictim->edict()),
				GETPLAYERAUTHID(pVictim->edict()),
				GETPLAYERUSERID(pVictim->edict()),
				killer_weapon_name);
		}
		else if (pKiller->flags & FL_CLIENT)
		{
			UTIL_LogPrintf("\"%s<%i><%s><%i>\" killed \"%s<%i><%s><%i>\" with \"%s\"\n",
				STRING(pKiller->netname),
				GETPLAYERUSERID(ENT(pKiller)),
				GETPLAYERAUTHID(ENT(pKiller)),
				GETPLAYERUSERID(ENT(pKiller)),
				STRING(pVictim->pev->netname),
				GETPLAYERUSERID(pVictim->edict()),
				GETPLAYERAUTHID(pVictim->edict()),
				GETPLAYERUSERID(pVictim->edict()),
				killer_weapon_name);

		}
		else
		{
			// killed by the world
			UTIL_LogPrintf("\"%s<%i><%s><%i>\" committed suicide with \"%s\" (world)\n",
				STRING(pVictim->pev->netname),
				GETPLAYERUSERID(pVictim->edict()),
				GETPLAYERAUTHID(pVictim->edict()),
				GETPLAYERUSERID(pVictim->edict()),
				killer_weapon_name);
		}

		MESSAGE_BEGIN(MSG_SPEC, SVC_DIRECTOR);
		WRITE_BYTE(9);	// command length in bytes
		WRITE_BYTE(DRC_CMD_EVENT);	// player killed
		WRITE_SHORT(ENTINDEX(pVictim->edict()));	// index number of primary entity
		if (pevInflictor)
			WRITE_SHORT(ENTINDEX(ENT(pevInflictor)));	// index number of secondary entity
		else
			WRITE_SHORT(ENTINDEX(ENT(pKiller)));	// index number of secondary entity
		WRITE_LONG(7 | DRC_FLAG_DRAMATIC);   // eventflags (priority and flags)
		MESSAGE_END();

		//  Print a standard message
			// TODO: make this go direct to console
		return; // just remove for now
	}
}

//=========================================================
// PlayerGotWeapon - player has grabbed a weapon that was
// sitting in the world
//=========================================================
void CHalfLifeRules :: PlayerGotWeapon( CBasePlayer *pPlayer, CBasePlayerItem *pWeapon )
{
}

//=========================================================
// FlWeaponRespawnTime - what is the time in the future
// at which this weapon may spawn?
//=========================================================
float CHalfLifeRules :: FlWeaponRespawnTime( CBasePlayerItem *pWeapon )
{
	return -1;
}

//=========================================================
// FlWeaponRespawnTime - Returns 0 if the weapon can respawn 
// now,  otherwise it returns the time at which it can try
// to spawn again.
//=========================================================
float CHalfLifeRules :: FlWeaponTryRespawn( CBasePlayerItem *pWeapon )
{
	return 0;
}

//=========================================================
// VecWeaponRespawnSpot - where should this weapon spawn?
// Some game variations may choose to randomize spawn locations
//=========================================================
Vector CHalfLifeRules :: VecWeaponRespawnSpot( CBasePlayerItem *pWeapon )
{
	return pWeapon->pev->origin;
}

//=========================================================
// WeaponShouldRespawn - any conditions inhibiting the
// respawning of this weapon?
//=========================================================
int CHalfLifeRules :: WeaponShouldRespawn( CBasePlayerItem *pWeapon )
{
	return GR_WEAPON_RESPAWN_NO;
}

//=========================================================
//=========================================================
BOOL CHalfLifeRules::CanHaveItem( CBasePlayer *pPlayer, CItem *pItem )
{
	return TRUE;
}

//=========================================================
//=========================================================
void CHalfLifeRules::PlayerGotItem( CBasePlayer *pPlayer, CItem *pItem )
{
}

//=========================================================
//=========================================================
int CHalfLifeRules::ItemShouldRespawn( CItem *pItem )
{
	return GR_ITEM_RESPAWN_NO;
}


//=========================================================
// At what time in the future may this Item respawn?
//=========================================================
float CHalfLifeRules::FlItemRespawnTime( CItem *pItem )
{
	return -1;
}

//=========================================================
// Where should this item respawn?
// Some game variations may choose to randomize spawn locations
//=========================================================
Vector CHalfLifeRules::VecItemRespawnSpot( CItem *pItem )
{
	return pItem->pev->origin;
}

//=========================================================
//=========================================================
BOOL CHalfLifeRules::IsAllowedToSpawn( CBaseEntity *pEntity )
{
	return TRUE;
}

//=========================================================
//=========================================================
void CHalfLifeRules::PlayerGotAmmo( CBasePlayer *pPlayer, char *szName, int iCount )
{
}

//=========================================================
//=========================================================
int CHalfLifeRules::AmmoShouldRespawn( CBasePlayerAmmo *pAmmo )
{
	return GR_AMMO_RESPAWN_NO;
}

//=========================================================
//=========================================================
float CHalfLifeRules::FlAmmoRespawnTime( CBasePlayerAmmo *pAmmo )
{
	return -1;
}

//=========================================================
//=========================================================
Vector CHalfLifeRules::VecAmmoRespawnSpot( CBasePlayerAmmo *pAmmo )
{
	return pAmmo->pev->origin;
}

//=========================================================
//=========================================================
float CHalfLifeRules::FlHealthChargerRechargeTime()
{
	return 0;// don't recharge
}

//=========================================================
//=========================================================
int CHalfLifeRules::DeadPlayerWeapons( CBasePlayer *pPlayer )
{
	return GR_PLR_DROP_GUN_NO;
}

//=========================================================
//=========================================================
int CHalfLifeRules::DeadPlayerAmmo( CBasePlayer *pPlayer )
{
	return GR_PLR_DROP_AMMO_NO;
}

//=========================================================
//=========================================================
int CHalfLifeRules::PlayerRelationship( CBaseEntity *pPlayer, CBaseEntity *pTarget )
{
	// why would a single player in half life need this? 
	return GR_NOTTEAMMATE;
}

//=========================================================
//=========================================================
BOOL CHalfLifeRules :: FAllowMonsters()
{
	return TRUE;
}
