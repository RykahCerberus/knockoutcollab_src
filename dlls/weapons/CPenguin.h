/***
*
*	Copyright (c) 1996-2001, Valve LLC. All rights reserved.
*
*	This product contains software technology licensed from Id
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc.
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/
#pragma once

class CPenguin : public CBasePlayerWeapon
{
public:
	using BaseClass = CBasePlayerWeapon;

#ifndef CLIENT_DLL
	int Save(CSave& save) override;
	int Restore(CRestore& restore) override;

	static TYPEDESCRIPTION m_SaveData[];
#endif

	void Precache() override;

	void Spawn() override;

	BOOL Deploy() override;

	void Holster(int skiplocal = 0) override;

	void WeaponIdle() override;

	void PrimaryAttack() override;

	void SecondaryAttack() override;

	int iItemSlot() override;

	int GetItemInfo(ItemInfo* p) override;

	BOOL UseDecrement() override
	{
#if defined( CLIENT_WEAPONS )
		return UTIL_DefaultUseDecrement();
#else
		return FALSE;
#endif
	}
	bool	ShouldFlip() override { return CVAR_GET_FLOAT("__flip_penguin"); }

private:
	int m_fJustThrown;
	unsigned short m_usPenguinFire;
};

#ifndef CLIENT_DLL
class CPenguinGrenade : public CGrenade
{
public:
	int Save(CSave& save) override;
	int Restore(CRestore& restore) override;

	static TYPEDESCRIPTION m_SaveData[];

	void Precache() override;
	void GibMonster() override;
	void EXPORT SuperBounceTouch(CBaseEntity* pOther);
	void Spawn() override;

	int Classify() override;
	int IRelationship(CBaseEntity* pTarget) override;
	void Killed(entvars_t* pevAttacker, int iGib) override;
	void EXPORT HuntThink();
	void Smoke();
	int BloodColor() override;

	static float m_flNextBounceSoundTime;

	float m_flDie;
	Vector m_vecTarget;
	float m_flNextHunt;
	float m_flNextHit;
	Vector m_posPrev;
	EHANDLE m_hOwner;
	int m_iMyClass;

	float m_flDamagePop;
	float m_flDamageBite;
};
#endif
