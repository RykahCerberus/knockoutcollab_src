
#include "extdll.h"
#include "util.h"
#include "cbase.h"
#include "weapons.h"
#include "player.h"
#include "skill.h"
#include "items.h"
#include "gamerules.h"
#include "UserMessages.h"
#include "knockout_dll/newitems.h"

bool g_bTouchedCustomAmmo;

class CItemNewArmor : public CItem
{
	void Spawn() override
    { 
		Precache( );

		if (pev->model)
			SET_MODEL(ENT(pev), STRING(pev->model));
		else
			SET_MODEL(ENT(pev), "models/w_armor.mdl");

		CItem::Spawn( );
	}
	void Precache() override
    {
		if (pev->model)
			PRECACHE_MODEL((char*)STRING(pev->model));
		else
			PRECACHE_MODEL ("models/w_armor.mdl");

		if (pev->noise)
			PRECACHE_SOUND( (char*)STRING(pev->noise) );
		else
			PRECACHE_SOUND( "items/gunpickup2.wav" );
	}
	BOOL MyTouch( CBasePlayer *pPlayer ) override
    {
		if (pPlayer->pev->weapons & (1 << WEAPON_SUIT))
		{
			//Player can pickup if:
			//Player has less then max armor
			//This provides a helmet and they don't have one
			//I change max armor
			if ((pPlayer->pev->armorvalue < pPlayer->GetMaxArmor()) || (pev->max_health && (!pPlayer->m_iHasHelmet)) || pev->iuser2)
			{
				int pct;
				char szcharge[64];

				if (pev->armorvalue)
					pPlayer->pev->armorvalue += pev->armorvalue;

				pPlayer->pev->armorvalue = V_min(pPlayer->pev->armorvalue, pPlayer->GetMaxArmor());

				if (pev->iuser2)
				{
					pPlayer->pev->armorvalue = pev->armorvalue;
					pPlayer->m_flMaxArmor = pev->armorvalue;

					if (gpgs)
						gpgs->m_iPlayerMaxArmor = pev->armorvalue;

					CVAR_SET_FLOAT("__hud_max_bat", pev->armorvalue);
				}

				if (pev->noise)
					EMIT_SOUND(pPlayer->edict(), CHAN_ITEM, STRING(pev->noise), 1, ATTN_NORM); //LRC
				else
					EMIT_SOUND(pPlayer->edict(), CHAN_ITEM, "items/gunpickup2.wav", 1, ATTN_NORM);


				MESSAGE_BEGIN(MSG_ONE, gmsgItemPickup, NULL, pPlayer->pev);

				if (pev->netname)
					WRITE_STRING(STRING(pev->netname));
				else
					WRITE_STRING(STRING(pev->classname));

				MESSAGE_END();

				if (pev->max_health == 1 && !(pPlayer->m_iHasHelmet == 2))
					pPlayer->m_iHasHelmet = true;

				if (pev->armortype && pev->armortype < pPlayer->pev->armortype)
					pPlayer->pev->armortype = pev->armortype;

				// Suit reports new power level
				// For some reason this wasn't working in release build -- round it.
				pct = (int)((float)(pPlayer->pev->armorvalue * 100.0) * (1.0 / pPlayer->GetMaxArmor()) + 0.5);
				pct = (pct / 5);

				if (pct >= 20)
					pct = 19;

				if (pct > 0)
					pct--;

				sprintf(szcharge, "_%1dP", pct);

				//EMIT_SOUND_SUIT(ENT(pev), szcharge);
				pPlayer->SetSuitUpdateTweaked(szcharge, FALSE, SUIT_NEXT_IN_30SEC);

				return TRUE;
			}
		}
		return FALSE;
	}
};

LINK_ENTITY_TO_CLASS(item_armor, CItemNewArmor);


void CCustomAmmoClip::KeyValue( KeyValueData *pkvd )
{
	if (FStrEq(pkvd->szKeyName, "ItemTarget"))
	{
		m_strTarget = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else
		CBasePlayerAmmo::KeyValue( pkvd );
}


TYPEDESCRIPTION	CCustomAmmoClip::m_SaveData[] =
{
	DEFINE_FIELD( CCustomAmmoClip, m_strTarget, FIELD_STRING ),

};
IMPLEMENT_SAVERESTORE( CCustomAmmoClip, CBasePlayerAmmo);

void CCustomAmmoClip::Spawn()
{
	m_chzEntToGive = NULL;
	m_iMaxCarry = 0;

	Precache();

	if (pev->model)
		SET_MODEL(ENT(pev), STRING(pev->model));
	else
		SET_MODEL(ENT(pev), "models/w_9mmARclip.mdl");

	CBasePlayerAmmo::Spawn();
}

void CCustomAmmoClip::Precache()
{
	if (pev->model)
		PRECACHE_MODEL((char*)STRING(pev->model));
	else
		PRECACHE_MODEL("models/w_9mmARclip.mdl");

	if (pev->noise)
		PRECACHE_SOUND((char*)STRING(pev->noise));
	else
		PRECACHE_SOUND("items/9mmclip1.wav");
}

const char* CCustomAmmoClip::HandleAmmoType()
{
	const char* chzAmmoType = NULL;

	switch ((int)pev->health)
	{
	case 1: m_iMaxCarry = BOLT_MAX_CARRY; chzAmmoType = "bolts"; break;
	case 2: m_iMaxCarry = URANIUM_MAX_CARRY; chzAmmoType = "uranium"; break;
	case 3: m_iMaxCarry = HANDGRENADE_MAX_CARRY; chzAmmoType = "Hand Grenade"; m_chzEntToGive = "weapon_handgrenade"; break;
	case 4: m_iMaxCarry = HORNET_MAX_CARRY; chzAmmoType = "Hornets"; break;
	case 5: m_iMaxCarry = _357_MAX_CARRY; chzAmmoType = "357"; break;
	case 6: m_iMaxCarry = ROCKET_MAX_CARRY; chzAmmoType = "rockets"; break;
	case 7: m_iMaxCarry = SATCHEL_MAX_CARRY; chzAmmoType = "Satchel Charge"; m_chzEntToGive = "weapon_satchel"; break;
	case 8: m_iMaxCarry = BUCKSHOT_MAX_CARRY; chzAmmoType = "buckshot"; break;
	case 9: m_iMaxCarry = SNARK_MAX_CARRY; chzAmmoType = "Snarks"; m_chzEntToGive = "weapon_snark"; break;
	case 10: m_iMaxCarry = TRIPMINE_MAX_CARRY; chzAmmoType = "Trip Mine"; m_chzEntToGive = "weapon_tripmine"; break;
	case 11: m_iMaxCarry = M249_MAX_CARRY; chzAmmoType = "556"; break;
	case 12: m_iMaxCarry = PENGUIN_MAX_CARRY; chzAmmoType = "Penguins"; m_chzEntToGive = "weapon_penguin"; break;
	case 13: m_iMaxCarry = SHOCKRIFLE_MAX_CLIP; chzAmmoType = "shock"; break;
	case 14: m_iMaxCarry = SNIPERRIFLE_MAX_CARRY; chzAmmoType = "762"; break;
	case 15: m_iMaxCarry = SPORELAUNCHER_MAX_CARRY; chzAmmoType = "spores"; break;
	case 16: m_iMaxCarry = M203_GRENADE_MAX_CARRY; chzAmmoType = "ARgrenades"; break;

	case 17:
	default: 
		m_iMaxCarry = _9MM_MAX_CARRY; 
		chzAmmoType = "9mm"; 
		break;
	}

	if (pev->max_health == 2)
	{
		switch ((int)pev->health)
		{
		case 3: m_chzEntToGive = "weapon_handgrenade_slim"; break;
		case 7: m_chzEntToGive = "weapon_satchel_slim"; break;
		case 9: m_chzEntToGive = "weapon_snark_slim"; break;
		case 10: m_chzEntToGive = "weapon_tripmine_slim"; break;
		case 12: m_chzEntToGive = "weapon_penguin_slim"; break;

		default:
			break;
		}
	}

	return chzAmmoType;
}

extern int gEvilImpulse101;

BOOL CCustomAmmoClip::AddAmmo(CBaseEntity* pOther)
{
	int bResult = (pOther->GiveAmmo(pev->armorvalue, HandleAmmoType(), m_iMaxCarry) != -1);

	if (bResult)
	{
		if (pev->noise)
			EMIT_SOUND(ENT(pev), CHAN_ITEM, STRING(pev->noise), 1, ATTN_NORM);
		else
			EMIT_SOUND(ENT(pev), CHAN_ITEM, "items/9mmclip1.wav", 1, ATTN_NORM);

		CBasePlayer* pPlayer = ToPlayer(pOther);

		if (pPlayer && !(m_chzEntToGive == NULL) && pev->max_health)
		{
			gEvilImpulse101 = TRUE;
			pPlayer->GiveNamedItem(m_chzEntToGive);
			gEvilImpulse101 = false;
		}

		//ALERT(at_console, "CCustomAmmoClip - My Target is: %d\n", STRING(m_strTarget));

		if (m_strTarget)
		{
			//ALERT(at_console, "CCustomAmmoClip - I'm firing my target!\n");
			FireTargets(STRING(m_strTarget), this, this, USE_TOGGLE, 0);
		}
		else
		{
			//ALERT(at_console, "CCustomAmmoClip - Not firing target.\n");
		}

	}

	return bResult;
}

CCustomAmmoClip *CCustomAmmoClip::CreatePickup( CBaseEntity *pVictim, int type, int ammount, char* model, char* sound )
{
	if (!pVictim)
		return NULL; // nothing in it

	CCustomAmmoClip*pBackpack = GetClassPtr((CCustomAmmoClip*)NULL );

	UTIL_SetOrigin( pBackpack->pev, pVictim->pev->origin + Vector( 0, 0, 24 ));

	pBackpack->pev->velocity.x = RANDOM_FLOAT( -100, 100 );
	pBackpack->pev->velocity.y = RANDOM_FLOAT( -100, 100 );
	pBackpack->pev->velocity.z = 100;
	pBackpack->pev->model = ALLOC_STRING(model);
	pBackpack->pev->health = type;
	pBackpack->pev->armorvalue = ammount;
	pBackpack->pev->noise = ALLOC_STRING(sound);
	pBackpack->pev->max_health = 2;
	pBackpack->Spawn();
	pBackpack->pev->classname = MAKE_STRING( "ammo_customizable" );

	return pBackpack;
}

LINK_ENTITY_TO_CLASS( ammo_customizable, CCustomAmmoClip );


class CGenericPickup : public CItem
{
	void Spawn() override
    { 
		Precache( );

		if (pev->model)
			SET_MODEL(ENT(pev), STRING(pev->model));
		else
			SET_MODEL(ENT(pev), "models/gun_mag.mdl");

		CItem::Spawn( );
	}
	void Precache() override
    {
		if (pev->model)
			PRECACHE_MODEL((char*)STRING(pev->model));
		else
			PRECACHE_MODEL ("models/gun_mag.mdl");

		if (pev->noise)
			PRECACHE_SOUND( (char*)STRING(pev->noise) );
		else
			PRECACHE_SOUND( "items/gunpickup2.wav" );
	}
	BOOL MyTouch(CBasePlayer* pPlayer) override
	{
		if (pev->noise)
			EMIT_SOUND(pPlayer->edict(), CHAN_ITEM, STRING(pev->noise), 1, ATTN_NORM); //LRC
		else
			EMIT_SOUND(pPlayer->edict(), CHAN_ITEM, "items/gunpickup2.wav", 1, ATTN_NORM);

		MESSAGE_BEGIN(MSG_ONE, gmsgItemPickup, NULL, pPlayer->pev);

		if (pev->netname)
			WRITE_STRING(STRING(pev->netname));
		else
			WRITE_STRING("item_bomb");

		MESSAGE_END();

		return TRUE;
	}
};

LINK_ENTITY_TO_CLASS(item_scripted, CGenericPickup);



class CFlashlightMod : public CItem
{
	void Spawn() override
    { 
		Precache( );

		if (pev->model)
			SET_MODEL(ENT(pev), STRING(pev->model));
		else
			SET_MODEL(ENT(pev), "models/w_flashlight.mdl");

		CItem::Spawn( );
	}
	void Precache() override
    {
		if (pev->model)
			PRECACHE_MODEL((char*)STRING(pev->model));
		else
			PRECACHE_MODEL ("models/w_flashlight.mdl");

		if (pev->noise)
			PRECACHE_SOUND( (char*)STRING(pev->noise) );
		else
			PRECACHE_SOUND( "items/gunpickup2.wav" );
	}
	BOOL MyTouch(CBasePlayer* pPlayer) override
	{
		if (!gpgs)
			return FALSE;

		if (pev->noise)
			EMIT_SOUND(pPlayer->edict(), CHAN_ITEM, STRING(pev->noise), 1, ATTN_NORM); //LRC
		else
			EMIT_SOUND(pPlayer->edict(), CHAN_ITEM, "items/gunpickup2.wav", 1, ATTN_NORM);

		const char* name;

		pPlayer->FlashlightTurnOff();
		
		switch ((int)pev->max_health)
		{
		case 1: 
			gpgs->m_iFlashlightBehavour = 1;
			CVAR_SET_FLOAT("__hud_flashlight_behavour", 1);
			name = "fl_nv";
			break;

		case 2:
			gpgs->m_iFlashlightBehavour = 2;
			CVAR_SET_FLOAT("__hud_flashlight_behavour", 2);
			name = "fl_off";
			break;

		default:
			gpgs->m_iFlashlightBehavour = 0;
			CVAR_SET_FLOAT("__hud_flashlight_behavour", 0);
			name = "fl_on";
			break;
		}

		if (pev->netname)
			name = STRING(pev->netname);

		MESSAGE_BEGIN(MSG_ONE, gmsgItemPickup, NULL, pPlayer->pev);
			WRITE_STRING(name);
		MESSAGE_END();

		return TRUE;
	}
};

LINK_ENTITY_TO_CLASS(item_flashlightmod, CFlashlightMod);


class CHealthPickup : public CItem
{
	void Spawn() override;
	void Precache() override;
	BOOL MyTouch( CBasePlayer *pPlayer ) override;
};


LINK_ENTITY_TO_CLASS( item_health, CHealthPickup);

void CHealthPickup:: Spawn()
{
	Precache( );

	if (pev->model)
		SET_MODEL(ENT(pev), STRING(pev->model)); //LRC
	else
		SET_MODEL(ENT(pev), "models/healthkit.mdl");

	CItem::Spawn();
}

void CHealthPickup::Precache()
{
	if (pev->model)
		PRECACHE_MODEL((char*)STRING(pev->model)); //LRC
	else
		PRECACHE_MODEL("models/healthkit.mdl");
	
	if (pev->noise)
		PRECACHE_SOUND( (char*)STRING(pev->noise) ); //LRC
	else
		PRECACHE_SOUND("items/smallmedkit1.wav");
}

BOOL CHealthPickup::MyTouch( CBasePlayer *pPlayer )
{
	if ( pPlayer->pev->deadflag != DEAD_NO )
	{
		return FALSE;
	}

	int iHealth = 0;

	if (pev->armorvalue)
		iHealth = pev->armorvalue;

	bool successful = 0;

	if (pev->max_health)
	{
		//We're being told to modify their max health, so we will.
		pPlayer->pev->health = iHealth;
		pPlayer->pev->max_health = iHealth;

		if (gpgs)
			gpgs->m_iPlayerMaxHealth = iHealth;

		CVAR_SET_FLOAT("__hud_max_hp", iHealth);
		successful = true;
	}
	else
	{
		if (pPlayer->TakeHealth(iHealth, DMG_GENERIC))
			successful = true;
	}

	if (successful)
	{
		MESSAGE_BEGIN( MSG_ONE, gmsgItemPickup, NULL, pPlayer->pev );
			if (pev->netname)
				WRITE_STRING( STRING(pev->netname) );
			else
				WRITE_STRING( "item_healthkit" );
		MESSAGE_END();

		if (pev->noise)
			EMIT_SOUND(ENT(pPlayer->pev), CHAN_ITEM, STRING(pev->noise), 1, ATTN_NORM);
		else
			EMIT_SOUND(ENT(pPlayer->pev), CHAN_ITEM, "items/smallmedkit1.wav", 1, ATTN_NORM);

		UTIL_Remove(this);
		return TRUE;
	}

	return FALSE;
}
