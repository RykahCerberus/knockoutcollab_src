/***
*
*	Copyright (c) 1996-2001, Valve LLC. All rights reserved.
*
*	This product contains software technology licensed from Id
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc.
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/
#ifndef KOC_POWERUPS_H
#define KOC_POWERUPS_H


#define POWERUP_INVISIBILITY				(1 << 0)
#define POWERUP_INVULNERABILITY				(1 << 1)
#define POWERUP_ENVSUIT						(1 << 2)
#define POWERUP_QUADDMG						(1 << 3)

#define POWERUP_HEVREGEN					(1 << 4)
#define POWERUP_HEALTHREGEN					(1 << 5)
#define POWERUP_AMMOREGEN					(1 << 6)
#define POWERUP_LONGJUMP					(1 << 7)
#define POWERUP_ACCELERATOR					(1 << 8)

#define POWERUP_MARKSMAN					(1 << 9)
#define POWERUP_BERSERK						(1 << 10)

#define	MAX_POWERUPS 11

#ifndef CLIENT_DLL

#include "items.h"
class CQuakeItem : public CItem
{
public:
	void	Spawn();
	void	Precache();

	// Respawning
	void	EXPORT Materialize();
	void	Respawn(float flTime);

	virtual void SetObjectCollisionBox();

	// Touch
	void	EXPORT ItemTouch(CBaseEntity* pOther);
	virtual	BOOL MyTouch(CBasePlayer* pOther) { return FALSE; };

	float	m_flRespawnTime;

	void	KeyValue(KeyValueData* pkvd) override;
	float	m_flDuration;
	string_t m_chzCustomPickupMessage;
	int		m_iStrength;
	int		m_iDoRespawn;
	int		m_iRespawnTime;

    int		Save( CSave &save ) override;
    int		Restore( CRestore &restore ) override;
	static	TYPEDESCRIPTION m_SaveData[];

	int		m_iPowerupBit;
	float	invincible_finished;
	float	radsuit_finished;
	float	invisible_finished;
	float	super_damage_finished;

	float	op4ammo_finished;
	float	op4armor_finished;
	float	op4health_finished;
	float	op4accel_finished;
	float	op4jump_finished;

	float	marksman_finished;
	float	berserk_finished;
};

class CItemPowerup : public CQuakeItem
{
public:
	BOOL	MyTouch(CBasePlayer* pPlayer);
};

#endif

#endif
