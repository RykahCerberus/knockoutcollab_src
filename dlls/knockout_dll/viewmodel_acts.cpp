
#include "extdll.h"
#include "util.h"
#include "cbase.h"
#include "knockout_dll\viewmodel_acts.h"

#if 0

int AnimIdle( int Group);
int AnimAttack( int Group);
int AnimAttackAlt( int Group);
int AnimSwing( int Group);
int AnimSwingMiss( int Group);
int AnimReload( int Group);
int AnimReloadStart( int Group);
int AnimReloadLoop( int Group);
int AnimReloadEnd( int Group);
int AnimDraw( int Group);
int AnimHolster( int Group);
int AnimSpecial( int Group);

int GetViewmodelAnim(int Anim, int Group)
{
	switch (Anim)
	{
		case ACT_VM_IDLE: AnimIdle(Group); break;
		case ACT_VM_ATTACK: AnimAttack(Group); break;
		case ACT_VM_ATTACK_ALT: AnimAttackAlt(Group); break;
		case ACT_VM_SWING: AnimSwing(Group); break;
		case ACT_VM_SWING_MISS: AnimSwingMiss(Group); break;
		case ACT_VM_RELOAD: AnimReload(Group); break;
		case ACT_VM_RELOAD_START: AnimReloadStart(Group); break;
		case ACT_VM_RELOAD_LOOP: AnimReloadLoop(Group); break;
		case ACT_VM_RELOAD_END: AnimReloadEnd(Group); break;
		case ACT_VM_DRAW: AnimDraw(Group); break;
		case ACT_VM_HOLSTER: AnimHolster(Group); break;
		case ACT_VM_SPECIAL: AnimSpecial(Group); break;
	}

	return Group;
}

int AnimIdle( int Group) 
{
	switch (Group)
	{
	case VM_GROUP_GLOCK:
	}

	return Group;
}


int AnimAttack( int Group);
int AnimAttackAlt( int Group);
int AnimSwing( int Group);
int AnimSwingMiss( int Group);
int AnimReload( int Group);
int AnimReloadStart( int Group);
int AnimReloadLoop( int Group);
int AnimReloadEnd( int Group);
int AnimDraw( int Group);
int AnimHolster( int Group);
int AnimSpecial( int Group);

#endif
