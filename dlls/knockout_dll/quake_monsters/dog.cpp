/***
*
*	Copyright (c) 1996-2002, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   This source code contains proprietary and confidential information of
*   Valve LLC and its suppliers.  Access to this code is restricted to
*   persons who have executed a written SDK license with Valve.  Any access,
*   use or distribution of this code by or to any unlicensed person is illegal.
*
****/

#include	"extdll.h"
#include	"util.h"
#include	"cbase.h"
#include	"quake_monster.h"
#include  "animation.h"
#include	"weapons.h"
#include	"skill.h"
#include	"player.h"
#include  "gamerules.h"
#include  "decals.h"

//=========================================================
// Monster's Anim Events Go Here
//=========================================================
#define DOG_IDLE_SOUND	1
#define DOG_LEAP		2
#define DOG_ATTACK		3

class CDog : public CQuakeMonster
{
public:
	void Spawn( void );
	void Precache( void );
	BOOL MonsterHasMeleeAttack( void ) { return TRUE; }
	BOOL MonsterHasMissileAttack( void ) { return TRUE; }
	void MonsterMeleeAttack( void );
	void MonsterMissileAttack( void );
	void HandleAnimEvent( MonsterEvent_t *pEvent );

	void MonsterAttack( void );
	BOOL MonsterCheckAttack( void );
	void MonsterKilled( entvars_t *pevAttacker, int iGib );
	void MonsterPain( CBaseEntity *pAttacker, float flDamage );
	int BloodColor( void ) { return BLOOD_COLOR_RED; }

	void EXPORT JumpTouch( CBaseEntity *pOther );
	BOOL CheckMelee( void );
	BOOL CheckJump( void );

	void MonsterSight( void );
	void MonsterIdle( void );
	void MonsterWalk( void );	
	void MonsterRun( void );
	void MonsterLeap( void );
	void MonsterBite( void );
};

LINK_ENTITY_TO_CLASS( monster_dog, CDog );

void CDog :: MonsterSight( void )
{
	if (!m_bMonstBeQuiet)
		EMIT_SOUND( edict(), CHAN_VOICE, "quake/dog/dsight.wav", 1.0, ATTN_NORM );
}

void CDog :: MonsterIdle( void )
{
	m_iAIState = STATE_IDLE;
	SetActivity( ACT_IDLE );
	m_flMonsterSpeed = 0;
}

void CDog :: MonsterWalk( void )
{
	m_iAIState = STATE_WALK;
	SetActivity( ACT_WALK );
	m_flMonsterSpeed = 7;
}

void CDog :: MonsterRun( void )
{
	m_iAIState = STATE_RUN;
	SetActivity( ACT_RUN );
	m_flMonsterSpeed = 32;
}

void CDog :: MonsterPain( CBaseEntity *pAttacker, float flDamage )
{
	m_iAIState = STATE_PAIN;
	SetActivity( ACT_BIG_FLINCH );

	if (!m_bMonstBeQuiet)
		EMIT_SOUND( edict(), CHAN_VOICE, "quake/dog/dpain1.wav", 1.0, ATTN_NORM );

	AI_Pain( 4 );
}

void CDog :: MonsterAttack( void )
{
	if( pev->impulse == ATTACK_MELEE )
	{
		if( !m_pfnTouch )
			AI_Face();
	}
	else if( pev->impulse == ATTACK_MISSILE )
	{
		AI_Charge( 10 );
	}

	if( m_iAIState == STATE_ATTACK && m_fSequenceFinished )
	{
		pev->impulse = ATTACK_NONE;	// reset shadow of attack state
		MonsterRun();
	}
}

void CDog :: JumpTouch( CBaseEntity *pOther )
{
	if (pev->health <= 0)
		return;
		
	if (pOther->pev->takedamage)
	{
		if ( pev->velocity.Length() > 300 )
		{
			float ldmg = gsd.dogDmgJump + RANDOM_FLOAT( 0.0f, gsd.dogDmgJumpBonus );
			pOther->TakeDamage (pev, pev, ldmg, DMG_GENERIC);	
		}
	}

	if (!ENT_IS_ON_FLOOR( edict() ))
	{
		if (pev->flags & FL_ONGROUND)
		{
			// jump randomly to not get hung up
			SetTouch( NULL );
			m_iAIState = STATE_ATTACK;
			SetActivity( ACT_LEAP );
		}
		return;	// not on ground yet
	}

	SetTouch( NULL );
	MonsterRun();
}

BOOL CDog::CheckMelee( void )
{
	if (m_iEnemyRange == RANGE_MELEE)
	{	
		// FIXME: check canreach
		m_iAttackState = ATTACK_MELEE;
		return TRUE;
	}

	return FALSE;
}

BOOL CDog::CheckJump( void )
{
	if (pev->origin.z + pev->mins.z > m_hQEnemy->pev->origin.z + m_hQEnemy->pev->mins.z + 0.75f * m_hQEnemy->pev->size.z)
		return FALSE;
		
	if (pev->origin.z + pev->maxs.z < m_hQEnemy->pev->origin.z + m_hQEnemy->pev->mins.z + 0.25f * m_hQEnemy->pev->size.z)
		return FALSE;
		
	Vector dist = m_hQEnemy->pev->origin - pev->origin;
	dist.z = 0;
	
	float d = dist.Length();
	
	if (d < 80)
		return FALSE;
		
	if (d > 150)
		return FALSE;
		
	return TRUE;
}

BOOL CDog::MonsterCheckAttack( void )
{
	// if close enough for slashing, go for it
	if (CheckMelee ())
	{
		m_iAttackState = ATTACK_MELEE;
		pev->impulse = ATTACK_MELEE;
		return TRUE;
	}
	
	if (CheckJump ())
	{
		m_iAttackState = ATTACK_MISSILE;
		pev->impulse = ATTACK_MISSILE;
		return TRUE;
	}
	
	return FALSE;
}

void CDog :: MonsterKilled( entvars_t *pevAttacker, int iGib )
{
	if (m_chzEntToDrop && !m_bDroppedMyEnt)
	{
		m_bDroppedMyEnt = true;
		DropItem(STRING(m_chzEntToDrop), pev->origin, pev->angles);
	}

	if( ShouldGibMonster( iGib ))
	{
		if (!m_bMonstBeQuiet)
			EMIT_SOUND( edict(), CHAN_VOICE, "quake/player/udeath.wav", 1.0, ATTN_NORM );

		if (m_bUseHLGibs)
		{
			CGib::ThrowHead("models/gib_b_gib.mdl", pev);
			CGib::ThrowGib("models/gib_b_gib.mdl", pev);
			CGib::ThrowGib("models/gib_b_gib.mdl", pev);
			CGib::ThrowGib("models/gib_b_gib.mdl", pev);
		}
		else
		{
			CGib::ThrowHead ("models/quake/h_dog.mdl", pev);
			CGib::ThrowGib ("models/quake/gib3.mdl", pev);
			CGib::ThrowGib ("models/quake/gib3.mdl", pev);
			CGib::ThrowGib ("models/quake/gib3.mdl", pev);
		}

		UTIL_Remove( this );
		return;
	}

	// regular death
	if (!m_bMonstBeQuiet)
		EMIT_SOUND( edict(), CHAN_VOICE, "quake/dog/ddeath.wav", 1.0, ATTN_NORM );
}

void CDog :: MonsterLeap( void )
{
	AI_Face();
	
	SetTouch( &CDog::JumpTouch );
	UTIL_MakeVectors( pev->angles );

	pev->origin.z++;
	pev->velocity = gpGlobals->v_forward * 300 + Vector( 0, 0, 200 );
	pev->flags &= ~FL_ONGROUND;
}

void CDog :: MonsterBite( void )
{
	if (!m_bMonstBeQuiet)
		EMIT_SOUND( edict(), CHAN_VOICE, "quake/dog/dattack1.wav", 1.0, ATTN_NORM );

	if (m_hQEnemy == NULL)
		return;

	AI_Charge(10);

	if (!Q_CanDamage( m_hQEnemy, this ))
		return;

	Vector delta = m_hQEnemy->pev->origin - pev->origin;

	if (delta.Length() > 100)
		return;
		
	float ldmg = (RANDOM_FLOAT(0, gsd.dogDmgBiteBase) + RANDOM_FLOAT(0, gsd.dogDmgBiteBase) + RANDOM_FLOAT(0, gsd.dogDmgBiteBase)) * gsd.dogDmgBiteMultiplier;

	if (dog_unfucked_logic.value)
		ldmg = gsd.dogDmgBiteBase;

	m_hQEnemy->TakeDamage (pev, pev, ldmg, DMG_GENERIC);	
}

void CDog :: MonsterMissileAttack( void )
{
	m_iAIState = STATE_ATTACK;
	SetActivity( ACT_LEAP );
}

void CDog :: MonsterMeleeAttack( void )
{
	m_iAIState = STATE_ATTACK;
	SetActivity( ACT_MELEE_ATTACK1 );
}

//=========================================================
// Spawn
//=========================================================
void CDog :: Spawn( void )
{
	if( !g_pGameRules->FAllowMonsters( ))
	{
		REMOVE_ENTITY( ENT(pev) );
		return;
	}

	Precache( );

	if (pev->model)
		SET_MODEL(ENT(pev), STRING(pev->model));
	else if (gpgs && gpgs->m_iszDogModel)
		SET_MODEL(ENT(pev), STRING(gpgs->m_iszDogModel));
	else
		SET_MODEL(ENT(pev), "models/quake/dog.mdl");

	UTIL_SetSize( pev, Vector( -32, -32, -36 ), Vector( 32, 32, 40 ));

	pev->solid	= SOLID_SLIDEBOX;
	pev->movetype	= MOVETYPE_STEP;

	if (pev->health == 0)
		pev->health	= gsd.dogHealth;

	WalkMonsterInit ();
}

//=========================================================
// Precache - precaches all resources this monster needs
//=========================================================
void CDog :: Precache()
{
	if (pev->model)
		PRECACHE_MODEL(STRING(pev->model));
	else
		PRECACHE_MODEL( "models/quake/dog.mdl" );

	PRECACHE_MODEL( "models/quake/h_dog.mdl" );

	PRECACHE_SOUND( "quake/dog/dattack1.wav" );
	PRECACHE_SOUND( "quake/dog/ddeath.wav" );
	PRECACHE_SOUND( "quake/dog/dpain1.wav" );
	PRECACHE_SOUND( "quake/dog/dsight.wav" );
	PRECACHE_SOUND( "quake/dog/idle.wav" );

	QuakePrecache(m_bUseHLGibs);
}


//=========================================================
// HandleAnimEvent - catches the monster-specific messages
// that occur when tagged animation frames are played.
//=========================================================
void CDog :: HandleAnimEvent( MonsterEvent_t *pEvent )
{
	switch( pEvent->event )
	{
	case DOG_IDLE_SOUND:
		if( RANDOM_FLOAT( 0.0f, 1.0f ) < 0.05f )
			if (!m_bMonstBeQuiet)
				EMIT_SOUND( edict(), CHAN_VOICE, "quake/dog/idle.wav", 1.0, ATTN_IDLE );
		break;
	case DOG_LEAP:
		MonsterLeap();
		break;
	case DOG_ATTACK:
		MonsterBite();
		break;
	default:
		CQuakeMonster::HandleAnimEvent( pEvent );
		break;
	}
}