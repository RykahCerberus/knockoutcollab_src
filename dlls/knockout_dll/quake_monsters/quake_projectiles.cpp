/***
*
*	Copyright (c) 1996-2002, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/
/*

===== weapons.cpp ========================================================

  Quake weaponry

*/

#include "extdll.h"
#include "util.h"
#include "cbase.h"
#include "player.h"
#include "quake_projectiles.h"
#include "quake_monster.h"
#include "gamerules.h"
#include "decals.h"
#include "items.h"
#include "skill.h"
#include "weapons.h"
#include "UserMessages.h"

DLL_GLOBAL short	g_sModelIndexBubbles;// holds the index for the bubbles model
DLL_GLOBAL short	g_sModelIndexBloodDrop;// holds the sprite index for the initial blood
DLL_GLOBAL short	g_sModelIndexBloodSpray;// holds the sprite index for splattered blood

BOOL IsSkySurface( CBaseEntity *pEnt, const Vector &point, const Vector &vecDir )
{
	Vector vecSrc = point + vecDir * -2.0f;
	Vector vecEnd = point + vecDir * 2.0f;

	const char *pTex = TRACE_TEXTURE( pEnt->edict(), vecSrc, vecEnd );

	if( pTex != NULL && !strnicmp( pTex, "sky", 3 ))
		return TRUE;
	return FALSE;
}

LINK_ENTITY_TO_CLASS( spike, CNail );

//=========================================================
CNail *CNail::CreateNail( Vector vecOrigin, Vector vecDir, CBaseEntity *pOwner )
{
	CNail *pNail = GetClassPtr((CNail *)NULL );

	UTIL_SetOrigin( pNail->pev, vecOrigin );

	pNail->pev->velocity = vecDir * 1000;
	pNail->pev->angles = UTIL_VecToAngles( vecDir );
	pNail->pev->owner = pOwner->edict();
	pNail->Spawn();
	pNail->pev->classname = MAKE_STRING( "spike" );
	pNail->pev->animtime = gpGlobals->time;
	pNail->pev->framerate = 1.0;

	return pNail;
}

CNail *CNail::CreateSuperNail( Vector vecOrigin, Vector vecDir, CBaseEntity *pOwner )
{
	CNail *pNail = CreateNail( vecOrigin, vecDir, pOwner );

	// super nails simply do more damage
	pNail->pev->dmg = gsd.MonDmgQuakeSuperNail;
	pNail->pev->skin = 1;
	return pNail;
}

CNail *CNail::CreateKnightSpike( Vector vecOrigin, Vector vecDir, CBaseEntity *pOwner )
{
	CNail *pNail = CreateNail( vecOrigin, vecDir, pOwner );

	SET_MODEL( ENT(pNail->pev), "models/quake/k_spike.mdl" );
	pNail->pev->velocity = vecDir * 300; // knightspikes goes slow
	//pNail->pev->effects |= EF_FULLBRIGHT;

	return pNail;
}

//=========================================================
void CNail::Spawn( void )
{
	Precache();

	// Setup
	pev->movetype = MOVETYPE_FLYMISSILE;
	pev->solid = SOLID_BBOX;
	
	// Safety removal
	pev->nextthink = gpGlobals->time + 6;
	SetThink( &CBaseEntity::SUB_Remove );
	
	// Touch
	SetTouch( &CNail::NailTouch );

	// Model
	SET_MODEL( ENT(pev), "models/quake/spike.mdl" );
	UTIL_SetSize(pev, Vector( 0, 0, 0), Vector(0, 0, 0));
	UTIL_SetOrigin( pev, pev->origin );

	// Damage
	pev->dmg = gsd.MonDmgQuakeNail;
}

void CNail :: Precache( void )
{
	PRECACHE_MODEL( "models/quake/spike.mdl" );
}

//=========================================================
void CNail::NailTouch( CBaseEntity *pOther )
{
	if (pOther->pev->solid == SOLID_TRIGGER)
		return;

	TraceResult tr = UTIL_GetGlobalTrace();

	// Remove if we've hit skybrush
	if ( IsSkySurface( pOther, tr.vecEndPos, pev->velocity.Normalize( ) ) )
	{
		UTIL_Remove( this );
		return;
	}

	// Hit something that bleeds
	if (pOther->pev->takedamage)
	{
		CBaseEntity *pOwner = CBaseEntity::Instance(pev->owner);
		if( !pOwner ) pOwner = g_pWorld;

		if ( g_pGameRules->PlayerRelationship( pOther, pOwner ) != GR_TEAMMATE )
			SpawnBlood( pev->origin, pOther->BloodColor(), pev->dmg );

		if( !FNullEnt( pOwner ))
			pOther->TakeDamage( pev, pOwner->pev, pev->dmg, DMG_GENERIC );
		else 
			pOther->TakeDamage( pev, pev, pev->dmg, DMG_GENERIC );
	}
	else
	{
		if ( pOther->pev->solid == SOLID_BSP || pOther->pev->movetype == MOVETYPE_PUSHSTEP )
		{
			Vector vecDir = pev->velocity.Normalize( );

			CBaseEntity *pEntity = NULL;
			Vector point = pev->origin - vecDir * 3;

			MESSAGE_BEGIN( MSG_BROADCAST, gmsgTempEntity );
				if( FStrEq( STRING (pev->model), "models/quake/w_spike.mdl" ))
					WRITE_BYTE( TE_WIZSPIKE );
				else if( FStrEq( STRING (pev->model), "models/quake/k_spike.mdl" ))
					WRITE_BYTE( TE_KNIGHTSPIKE );
				else
					WRITE_BYTE( pev->skin ? TE_SUPERSPIKE : TE_SPIKE );

				WRITE_COORD( point.x );
				WRITE_COORD( point.y );
				WRITE_COORD( point.z );
			MESSAGE_END();

			if ( strcmp( "models/spike.mdl", STRING(pev->model)) )
			{
				// wizard and knight spikes aren't stuck in the walls
				UTIL_Remove( this );
				return;
			}

			// too many overlapped spikes looks ugly
			while ((pEntity = UTIL_FindEntityInSphere( pEntity, point, 1 )) != NULL)
			{
				if( FClassnameIs( pEntity->pev, "spike" ) && pEntity != this )
				{
					UTIL_Remove( this );
					return;
				}
                              }

			UTIL_SetOrigin( pev, point );
			pev->angles = UTIL_VecToAngles( vecDir );
			pev->solid = SOLID_NOT;
			pev->velocity = Vector( 0, 0, 0 );
			pev->angles.z = RANDOM_LONG(0, 360);

			SetTouch( NULL );
			return;	// stay in brush
		}
	}

	UTIL_Remove( this );
}

void CNail::ExplodeTouch( CBaseEntity *pOther )
{
	if( pOther->edict() == pev->owner )
		return; // don't explode on owner

	TraceResult tr = UTIL_GetGlobalTrace();

	// Remove if we've hit skybrush
	if ( IsSkySurface( pOther, tr.vecEndPos, pev->velocity.Normalize( ) ) )
	{
		UTIL_Remove( this );
		return;
	}

	UTIL_ScreenShake( pev->origin, 20.0f, 4.0f, 0.7f, 350.0f );

	float dmg = gsd.MonDmgQuakeNailExplode + RANDOM_FLOAT( 0.0f, gsd.MonDmgQuakeNailExplodeBonus);
	CBaseEntity *pOwner = CBaseEntity::Instance(pev->owner);
	if( !pOwner ) pOwner = g_pWorld;

	if( pOther->pev->health && pOther->pev->takedamage )
	{
		if( FClassnameIs( pOther->pev, "monster_shambler" ))
			dmg *= 0.5f; // mostly immune
		pOther->TakeDamage( pev, pOwner->pev, dmg, DMG_BLAST );
	}

	Q_RadiusDamage( this, pOwner, 120, pOther );

	pev->origin = pev->origin - 8 * pev->velocity.Normalize();

	MESSAGE_BEGIN( MSG_BROADCAST, gmsgTempEntity );
		WRITE_BYTE( TE_EXPLOSION );
		WRITE_COORD( pev->origin.x );
		WRITE_COORD( pev->origin.y );
		WRITE_COORD( pev->origin.z );
	MESSAGE_END();

	UTIL_DecalTrace( &tr, DECAL_SCORCH1 + RANDOM_LONG( 0, 1 ));

	UTIL_Remove( this );
	SetTouch( NULL );
}

LINK_ENTITY_TO_CLASS( laser, CProjectileLaser );

//=========================================================
CProjectileLaser *CProjectileLaser::LaunchLaser( Vector vecOrigin, Vector vecAngles, CBaseEntity *pOwner )
{
	CProjectileLaser *pLaser = GetClassPtr((CProjectileLaser *)NULL );

	UTIL_SetOrigin( pLaser->pev, vecOrigin );

	pLaser->pev->velocity = vecAngles * 600;
	pLaser->pev->angles = UTIL_VecToAngles( vecAngles );
	pLaser->pev->owner = pOwner->edict();
	pLaser->Spawn();
	pLaser->pev->classname = MAKE_STRING( "laser" );

	return pLaser;
}

//=========================================================
void CProjectileLaser::Spawn( void )
{
	Precache();

	// Setup
	pev->movetype = MOVETYPE_FLY;
	pev->solid = SOLID_BBOX;
	pev->effects = EF_DIMLIGHT;
	
	// Safety removal
	pev->nextthink = gpGlobals->time + 5;
	SetThink( &CBaseEntity::SUB_Remove );
	
	// Touch
	SetTouch( &CProjectileLaser::LaserTouch );

	// Model
	SET_MODEL( ENT(pev), "models/quake/laser.mdl" );
	UTIL_SetSize(pev, Vector( 0, 0, 0), Vector(0, 0, 0));
	UTIL_SetOrigin( pev, pev->origin );

	// Damage
	pev->dmg = gsd.MonDmgQuakeLaser;
}

//=========================================================
void CProjectileLaser::LaserTouch( CBaseEntity *pOther )
{
	if (pOther->pev->solid == SOLID_TRIGGER)
		return;

	TraceResult tr = UTIL_GetGlobalTrace();

	// Remove if we've hit skybrush
	if ( IsSkySurface( pOther, tr.vecEndPos, pev->velocity.Normalize( ) ) )
	{
		UTIL_Remove( this );
		return;
	}

	EMIT_SOUND(ENT(pOther->pev), CHAN_WEAPON, "quake/enforcer/enfstop.wav", 1, ATTN_NORM);

	// Hit something that bleeds
	if (pOther->pev->takedamage)
	{
		CBaseEntity *pOwner = CBaseEntity::Instance(pev->owner);
		if( !pOwner ) pOwner = g_pWorld;

		if ( g_pGameRules->PlayerRelationship( pOther, pOwner ) != GR_TEAMMATE )
			SpawnBlood( pev->origin, pOther->BloodColor(), pev->dmg );

		pOther->TakeDamage( pev, pOwner->pev, pev->dmg, DMG_GENERIC );
	}
	else
	{
		if ( pOther->pev->solid == SOLID_BSP || pOther->pev->movetype == MOVETYPE_PUSHSTEP )
		{
			Vector vecDir = pev->velocity.Normalize( );
			Vector point = pev->origin - vecDir * 8;

			MESSAGE_BEGIN( MSG_BROADCAST, gmsgTempEntity );
				WRITE_BYTE( TE_GUNSHOT );
				WRITE_COORD( point.x );
				WRITE_COORD( point.y );
				WRITE_COORD( point.z );
			MESSAGE_END();
		}
	}

	UTIL_Remove( this );
}

LINK_ENTITY_TO_CLASS( qgrenade, CRocket );
LINK_ENTITY_TO_CLASS( rocket, CRocket );

//=========================================================
CRocket *CRocket::CreateRocket( Vector vecOrigin, Vector vecAngles, CBaseEntity *pOwner )
{
	CRocket *pRocket = GetClassPtr( (CRocket *)NULL );
	
	UTIL_SetOrigin( pRocket->pev, vecOrigin );
	SET_MODEL(ENT(pRocket->pev), "models/quake/missile.mdl");
	pRocket->Spawn();
	pRocket->pev->classname = MAKE_STRING("missile");
	pRocket->pev->owner = pOwner->edict();

	// Setup
	pRocket->pev->movetype = MOVETYPE_FLYMISSILE;
	pRocket->pev->solid = SOLID_BBOX;
		
	// Velocity
	pRocket->pev->velocity = vecAngles * 1000;
	pRocket->pev->angles = UTIL_VecToAngles( vecAngles );
	
	// Touch
	pRocket->SetTouch( &CRocket::RocketTouch );

	// Safety Remove
	pRocket->pev->nextthink = gpGlobals->time + 5;
	pRocket->SetThink( &CBaseEntity::SUB_Remove );

	// Effects
//	pRocket->pev->effects |= EF_LIGHT;

	return pRocket;
} 

//=========================================================
CRocket *CRocket::CreateGrenade( Vector vecOrigin, Vector vecVelocity, CBaseEntity *pOwner )
{
	CRocket *pRocket = GetClassPtr( (CRocket *)NULL );

	UTIL_SetOrigin( pRocket->pev, vecOrigin );
	SET_MODEL(ENT(pRocket->pev), "models/quake/grenade.mdl");
	pRocket->Spawn();
	pRocket->pev->classname = MAKE_STRING("qgrenade");
	pRocket->pev->owner = pOwner->edict();

	// Setup
	pRocket->pev->movetype = MOVETYPE_BOUNCE;
	pRocket->pev->solid = SOLID_BBOX;

	pRocket->pev->avelocity = Vector(300,300,300);
	
	// Velocity
	pRocket->pev->velocity = vecVelocity;
	pRocket->pev->angles = UTIL_VecToAngles(vecVelocity);
	pRocket->pev->friction = 0.5;

	// Touch
	pRocket->SetTouch( &CRocket::GrenadeTouch );

	if (FClassnameIs( pOwner->pev, "monster_ogre"))
		pRocket->pev->dmg = gsd.MonDmgQuakeGrenadeOgre;
	else 
		pRocket->pev->dmg = gsd.MonDmgQuakeGrenade;

	// set newmis duration
	if ( gpGlobals->deathmatch == 4 )
	{
		pRocket->m_flAttackFinished = gpGlobals->time + 1.1;	// What's this used for?
		if (pOwner)
			pOwner->TakeDamage( pOwner->pev, pOwner->pev, 10, DMG_BLAST );
	}

	pRocket->pev->nextthink = gpGlobals->time + 2.5;
	pRocket->SetThink( &CRocket::GrenadeExplode );

	return pRocket;
}

//=========================================================
void CRocket::Spawn( void )
{
	Precache();

	UTIL_SetSize(pev, Vector( 0, 0, 0), Vector(0, 0, 0));
	UTIL_SetOrigin( pev, pev->origin );
}

void CRocket :: Precache( void )
{
	PRECACHE_MODEL( "models/quake/grenade.mdl" );
	PRECACHE_SOUND( "quake/weapons/grenade.wav" );
	PRECACHE_SOUND( "quake/weapons/bounce.wav" );
}

//=========================================================
void CRocket::RocketTouch ( CBaseEntity *pOther )
{
	TraceResult tr = UTIL_GetGlobalTrace();

	// Remove if we've hit skybrush
	if ( IsSkySurface( pOther, tr.vecEndPos, pev->velocity.Normalize( ) ) )
	{
		UTIL_Remove( this );
		return;
	}

	// Do touch damage
	//float flDmg = RANDOM_FLOAT( 100, 120 );
	//Honestly, fuck this random damage in particular.

	CBaseEntity *pOwner = CBaseEntity::Instance(pev->owner);
	if( !pOwner ) pOwner = g_pWorld;

	if (pOther->pev->health && pOther->pev->takedamage)
	{
		//if (FClassnameIs( pOther->pev, "monster_shambler"))
		//	flDmg *= 0.5f; // mostly immune

		pOther->TakeDamage( pev, pOwner->pev, gsd.MonDmgQuakeRocket, DMG_BLAST );
	}

	// Don't do radius damage to the other, because all the damage was done in the impact
	Q_RadiusDamage(this, pOwner, gsd.MonDmgQuakeRocket, pOther);

	// Finish and remove
	Explode();
}

//=========================================================
void CRocket::GrenadeTouch( CBaseEntity *pOther )
{
	if (pOther->pev->takedamage == DAMAGE_AIM)
	{
		GrenadeExplode();
		return;
	}

	if (m_flBounceTime < gpGlobals->time)
	{
		//Don't make this bouncing sound more then once every half a second (stops a horrible buzzing noise when the grenade slides on the ground)
		m_flBounceTime = gpGlobals->time + 0.5;
		EMIT_SOUND(ENT(pev), CHAN_WEAPON, "quake/weapons/bounce.wav", 1, ATTN_NORM);
	}

	if (pev->velocity == g_vecZero)
		pev->avelocity = g_vecZero;
}

//=========================================================
void CRocket::GrenadeExplode()
{
	CBaseEntity *pOwner = CBaseEntity::Instance(pev->owner);
	if( !pOwner ) pOwner = g_pWorld;

	Q_RadiusDamage(this, pOwner, pev->dmg, NULL);

	// Finish and remove
	Explode();
}

//=========================================================
void CRocket::Explode()
{
	pev->origin = pev->origin - 8 * pev->velocity.Normalize();

	UTIL_ScreenShake( pev->origin, 16.0f, 2.0f, 0.5f, 250.0f );

	MESSAGE_BEGIN( MSG_BROADCAST, gmsgTempEntity );
		WRITE_BYTE( TE_EXPLOSION );
		WRITE_COORD( pev->origin.x );
		WRITE_COORD( pev->origin.y );
		WRITE_COORD( pev->origin.z );
	MESSAGE_END();

	UTIL_Remove( this );
}

LINK_ENTITY_TO_CLASS( zombie_missile, CZombieMissile );

CZombieMissile *CZombieMissile :: CreateMissile( Vector vecOrigin, Vector vecOffset, Vector vecAngles, CBaseEntity *pOwner )
{
	if( !pOwner ) //|| pOwner->m_hEnemy == NULL )
		return NULL;

	CQuakeMonster *pMonst = (CQuakeMonster*)pOwner;
	if (!pMonst)
		return NULL;

	if (pMonst->m_hQEnemy == NULL)
		return NULL;

	UTIL_MakeVectors(vecAngles);

	// calc org
	Vector org = vecOrigin + vecOffset.x * gpGlobals->v_forward + vecOffset.y * gpGlobals->v_right + (vecOffset.z - 24) * gpGlobals->v_up;

	CZombieMissile *pMeat = GetClassPtr((CZombieMissile *)NULL );

	UTIL_SetOrigin( pMeat->pev, org );

	pMeat->pev->classname = MAKE_STRING( "zombie_missile" );
	pMeat->pev->velocity = (pMonst->m_hQEnemy->pev->origin - org).Normalize() * 600;
	pMeat->pev->velocity.z = 200;
	pMeat->pev->avelocity = Vector( 3000, 1000, 2000 );
	pMeat->pev->owner = pMonst->edict();
	pMeat->pev->solid = SOLID_BBOX;
	pMeat->Spawn();
	pMeat->SetTouch( &CZombieMissile::MeatTouch );

	// Safety removal
	pMeat->pev->nextthink = gpGlobals->time + 2.5f;
	pMeat->SetThink( &CBaseEntity::SUB_Remove );

	// done
	return pMeat;
}

CZombieMissile *CZombieMissile :: CreateSpray( Vector vecOrigin, Vector vecVelocity )
{
	CZombieMissile *pMeat = GetClassPtr((CZombieMissile *)NULL );

	UTIL_SetOrigin( pMeat->pev, vecOrigin );
	pMeat->pev->classname = MAKE_STRING( "zombie_missile" );
	pMeat->pev->velocity = vecVelocity;
	pMeat->pev->velocity.z += 250 + RANDOM_FLOAT( 0.0f, 50.0f );
	pMeat->pev->avelocity = Vector( 3000, 1000, 2000 );
	pMeat->pev->solid = SOLID_NOT;
	pMeat->Spawn();

	// Safety removal
	pMeat->pev->nextthink = gpGlobals->time + 1.0f;
	pMeat->SetThink( &CBaseEntity::SUB_Remove );

	// done
	return pMeat;
}

void CZombieMissile :: Spawn( void )
{
	Precache();

	// Setup
	pev->movetype = MOVETYPE_BOUNCE;
	pev->dmg = gsd.zombieqDmgThrow;

	SET_MODEL( ENT(pev), "models/quake/zom_gib.mdl" );
	UTIL_SetSize(pev, Vector( 0, 0, 0), Vector(0, 0, 0));
	UTIL_SetOrigin( pev, pev->origin );
}

void CZombieMissile :: Precache( void )
{
	PRECACHE_MODEL( "models/quake/zom_gib.mdl" );

	PRECACHE_SOUND( "quake/zombie/z_hit.wav" );
	PRECACHE_SOUND( "quake/zombie/z_miss.wav" );
}

void CZombieMissile :: MeatTouch( CBaseEntity *pOther )
{
	if( pOther->edict() == pev->owner )
		return; // don't explode on owner

	if( pOther->pev->takedamage )
	{
		CBaseEntity *pOwner = CBaseEntity::Instance(pev->owner);

		if( !FNullEnt( pOwner ))
			pOther->TakeDamage( pev, pOwner->pev, pev->dmg, DMG_GENERIC );
		else 
			pOther->TakeDamage( pev, pev, pev->dmg, DMG_GENERIC );

		EMIT_SOUND( edict(), CHAN_WEAPON, "quake/zombie/z_hit.wav", 1.0, ATTN_NORM );
		UTIL_Remove( this );
		return;
	}

	EMIT_SOUND( edict(), CHAN_WEAPON, "quake/zombie/z_miss.wav", 1.0, ATTN_NORM );	// bounce sound

	TraceResult tr = UTIL_GetGlobalTrace();
	UTIL_DecalTrace( &tr, DECAL_BLOOD1 + RANDOM_LONG( 0, 5 ));

	pev->velocity = g_vecZero;
	pev->avelocity = g_vecZero;

	SetTouch( NULL );
	UTIL_Remove( this );
}

LINK_ENTITY_TO_CLASS( shal_missile, CShalMissile );

CShalMissile *CShalMissile :: CreateMissile( Vector vecOrigin, Vector vecVelocity )
{
	CShalMissile *pMiss = GetClassPtr((CShalMissile *)NULL );

	UTIL_SetOrigin( pMiss->pev, vecOrigin );
	pMiss->pev->classname = MAKE_STRING( "shal_missile" );
	pMiss->pev->avelocity = Vector( 300, 300, 300 );
	pMiss->pev->velocity = vecVelocity;
	pMiss->Spawn();

	// done
	return pMiss;
}

void CShalMissile :: Spawn( void )
{
	Precache();

	// Setup
	pev->solid = SOLID_BBOX;
	pev->movetype = MOVETYPE_FLYMISSILE;

	SET_MODEL( ENT(pev), "models/quake/v_spike.mdl" );
	UTIL_SetSize(pev, Vector( -1, -1, -1 ), Vector( 1, 1, 1 ));	// allow to explode with himself
	UTIL_SetOrigin( pev, pev->origin );

	SetTouch( &CShalMissile::ShalTouch );
	SetThink( &CShalMissile::ShalHome );
}

void CShalMissile :: Precache( void )
{
	PRECACHE_MODEL( "models/quake/v_spike.mdl" );
}

void CShalMissile :: ShalHome( void )
{
	if( !pev->enemy || pev->enemy->v.health <= 0.0f )
	{
		CBaseEntity *pOwner = CBaseEntity::Instance(pev->owner);
		if( pOwner ) pOwner->pev->impulse--; // decrease missiles		
		REMOVE_ENTITY( edict() );
		return;
	}

	CBaseEntity *pEnemy = CBaseEntity::Instance(pev->enemy);
	Vector vecTmp = pEnemy->pev->origin + Vector( 0, 0, 10 );
	Vector vecDir = ( vecTmp - pev->origin ).Normalize();

	if( g_iSkillLevel == SKILL_NIGHTMARE )
		pev->velocity = vecDir * 350.0f;
	else 
		pev->velocity = vecDir * 250.0f;

	pev->nextthink = gpGlobals->time + 0.2f;
}

void CShalMissile :: ShalTouch( CBaseEntity *pOther )
{
	if( pOther->edict() == pev->owner )
		return; // don't explode on owner

	CBaseEntity *pOwner = CBaseEntity::Instance(pev->owner);

	if( !pOwner ) 
		pOwner = g_pWorld; // shalrath is gibbed
	else 
		pOwner->pev->impulse--; // decrease missiles

	if( FClassnameIs( pOther->pev, "monster_zombie_quake" ))
		pOther->TakeDamage( pev, pev, (gsd.zombieqHealth * 2), DMG_ALWAYSGIB );

	Q_RadiusDamage( this, pOwner, gsd.shalDmgThrow, g_pWorld );

	MESSAGE_BEGIN( MSG_BROADCAST, gmsgTempEntity );
		WRITE_BYTE( TE_EXPLOSION );
		WRITE_COORD( pev->origin.x );
		WRITE_COORD( pev->origin.y );
		WRITE_COORD( pev->origin.z );
	MESSAGE_END();

	pev->velocity = g_vecZero;
	SetThink( NULL );
	SetTouch( NULL );

	UTIL_Remove( this );
}

//================================================================================================
// WEAPON FUNCTIONS
//================================================================================================
// Returns true if the inflictor can directly damage the target.  Used for explosions and melee attacks.
float Q_CanDamage(CBaseEntity *pTarget, CBaseEntity *pInflictor) 
{
	TraceResult trace;

	// bmodels need special checking because their origin is 0,0,0
	if (pTarget->pev->movetype == MOVETYPE_PUSH)
	{
		UTIL_TraceLine( pInflictor->pev->origin, 0.5 * (pTarget->pev->absmin + pTarget->pev->absmax), ignore_monsters, NULL, &trace );
		if (trace.flFraction == 1)
			return TRUE;
		CBaseEntity *pEntity = CBaseEntity::Instance(trace.pHit);
		if (pEntity == pTarget)
			return TRUE;
		return FALSE;
	}
	
	UTIL_TraceLine( pInflictor->pev->origin, pTarget->pev->origin, ignore_monsters, NULL, &trace );
	if (trace.flFraction == 1)
		return TRUE;
	UTIL_TraceLine( pInflictor->pev->origin, pTarget->pev->origin + Vector(15,15,0), ignore_monsters, NULL, &trace );
	if (trace.flFraction == 1)
		return TRUE;
	UTIL_TraceLine( pInflictor->pev->origin, pTarget->pev->origin + Vector(-15,-15,0), ignore_monsters, NULL, &trace );
	if (trace.flFraction == 1)
		return TRUE;
	UTIL_TraceLine( pInflictor->pev->origin, pTarget->pev->origin + Vector(-15,15,0), ignore_monsters, NULL, &trace );
	if (trace.flFraction == 1)
		return TRUE;
	UTIL_TraceLine( pInflictor->pev->origin, pTarget->pev->origin + Vector(15,-15,0), ignore_monsters, NULL, &trace );
	if (trace.flFraction == 1)
		return TRUE;

	return FALSE;
}

// Quake Radius damage
void Q_RadiusDamage( CBaseEntity *pInflictor, CBaseEntity *pAttacker, float flDamage, CBaseEntity *pIgnore )
{
	CBaseEntity *pEnt = NULL;

	while ( (pEnt = UTIL_FindEntityInSphere( pEnt, pInflictor->pev->origin, flDamage+40 )) != NULL )
	{
		if (pEnt != pIgnore)
		{
			if (pEnt->pev->takedamage)
			{
				Vector vecOrg = pEnt->pev->origin + ((pEnt->pev->mins + pEnt->pev->maxs) * 0.5);
				float flPoints = 0.5 * (pInflictor->pev->origin - vecOrg).Length();
				if (flPoints < 0)
					flPoints = 0;
				flPoints = flDamage - flPoints;
				
				if (pEnt == pAttacker)
					flPoints = flPoints * 0.5;
				if (flPoints > 0)
				{
					if ( Q_CanDamage( pEnt, pInflictor ) )
					{
						// shambler takes half damage from all explosions
						if (FClassnameIs( pEnt->pev, "monster_shambler"))
							pEnt->TakeDamage( pInflictor->pev, pAttacker->pev, flPoints * 0.5f, DMG_GENERIC );
						else
							pEnt->TakeDamage( pInflictor->pev, pAttacker->pev, flPoints, DMG_GENERIC );
					}
				}
			}
		}
	}
}

extern void LightningHit(CBaseEntity* pTarget, CBaseEntity* pAttacker, Vector vecHitPos, float flDamage, TraceResult* ptr, Vector vecDir);

void SpawnMeatSpray( Vector vecOrigin, Vector vecVelocity )
{
	CZombieMissile :: CreateSpray( vecOrigin, vecVelocity );
}

extern Vector g_vecAttackDir;
