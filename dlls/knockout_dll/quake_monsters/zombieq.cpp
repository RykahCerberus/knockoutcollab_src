/***
*
*	Copyright (c) 1996-2002, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   This source code contains proprietary and confidential information of
*   Valve LLC and its suppliers.  Access to this code is restricted to
*   persons who have executed a written SDK license with Valve.  Any access,
*   use or distribution of this code by or to any unlicensed person is illegal.
*
****/

#include	"extdll.h"
#include	"util.h"
#include	"cbase.h"
#include	"quake_monster.h"
#include  "animation.h"
#include	"weapons.h"
#include	"skill.h"
#include	"player.h"
#include  "gamerules.h"
#include  "decals.h"
#include  "zombieq.h"

extern int gmsgTempEntity;

//=========================================================
// Monster's Anim Events Go Here
//=========================================================
#define ZOMBIE_CURCIFIED_IDLE_SOUND	1
#define ZOMBIE_WALK_IDLE_SOUND	2
#define ZOMBIE_RUN_IDLE_SOUND		3
#define ZOMBIE_RIGHTHAND_ATTACK	4
#define ZOMBIE_LEFTHAND_ATTACK	5
#define ZOMBIE_FALL_SOUND		6
#define ZOMBIE_TEMPORARY_DEAD		7

LINK_ENTITY_TO_CLASS( monster_zombie_quake, CZombieQ );



TYPEDESCRIPTION CZombieQ::m_SaveData[] =
{
	DEFINE_FIELD(CZombieQ, m_flMaxHealth, FIELD_FLOAT),
	DEFINE_FIELD(CZombieQ, m_bFellDown, FIELD_BOOLEAN),
}; 
IMPLEMENT_SAVERESTORE(CZombieQ, CQuakeMonster);

const char *CZombieQ::pIdleSounds[] = 
{
	"quake/zombie/z_idle.wav",
	"quake/zombie/z_idle1.wav",
};

const char *CZombieQ::pPainSounds[] = 
{
	"quake/zombie/z_pain.wav",
	"quake/zombie/z_pain1.wav",
};

void CZombieQ :: AI_Idle( void )
{
	if( FBitSet( pev->spawnflags, SF_QMONSTER_CRUCIFIED ))
		return;	// stay idle

	if (FindTarget ())
		return;
	
	if (gpGlobals->time > m_flPauseTime)
	{
		MonsterWalk();
		return;
	}

	// change angle slightly
}

void CZombieQ :: MonsterIdle( void )
{
	m_iAIState = STATE_IDLE;
	SetActivity( ACT_IDLE );
	m_flMonsterSpeed = 0;
}

void CZombieQ :: MonsterWalk( void )
{
	m_iAIState = STATE_WALK;
	SetActivity( ACT_WALK );
	m_flMonsterSpeed = 1;
}

void CZombieQ :: MonsterRun( void )
{
	m_iAIState = STATE_RUN;
	SetActivity( ACT_RUN );
	m_flMonsterSpeed = 4;
	pev->impulse = 0;	// not in pain
	pev->frags = 0;	// not dead
}

void CZombieQ :: MonsterMissileAttack( void )
{
	m_iAIState = STATE_ATTACK;
	SetActivity( ACT_MELEE_ATTACK1 );
}

void CZombieQ :: MonsterSight( void )
{
	if (!m_bMonstBeQuiet)
		EMIT_SOUND( edict(), CHAN_VOICE, "quake/zombie/z_idle.wav", 1.0, ATTN_NORM );
}

void CZombieQ :: ThrowMeat( int iAttachment, Vector vecOffset )
{
	Vector vecOrigin;
#if 0
	GetAttachment( iAttachment, vecOrigin, vecTemp );
	vecOffset = g_vecZero;
#else
	vecOrigin = pev->origin;
#endif
	EMIT_SOUND( edict(), CHAN_WEAPON, "quake/zombie/z_shot1.wav", 1.0, ATTN_NORM );
	CZombieMissile :: CreateMissile( vecOrigin, vecOffset, pev->angles, this );

	MonsterRun ();
}

void CZombieQ :: MonsterPain( CBaseEntity *pAttacker, float flDamage )
{
	pev->health = m_flMaxHealth;		// allways reset health

	if( flDamage < 9 )
		return;		// totally ignore

	if( m_iAIState != STATE_PAIN )
	{
		// play pain sounds if not in pain
		if (!m_bMonstBeQuiet)
			EMIT_SOUND_ARRAY_DYNATTN( CHAN_VOICE, pPainSounds, ATTN_NORM );
	}

	m_iAIState = STATE_PAIN;

	if( pev->impulse == 2 )
		return; // down on ground, so don't reset any counters

	// go down immediately if a big enough hit
	if( flDamage >= 25 )
	{
		SetActivity( ACT_BIG_FLINCH );
		pev->impulse = 2;
		AI_Pain( 2 );
		return;
	}
	
	if( pev->impulse )
	{
		// if hit again in next gre seconds while not in pain frames, definately drop
		pev->pain_finished = gpGlobals->time + 3;
		return; // currently going through an animation, don't change
	}
	
	if( pev->pain_finished > gpGlobals->time )
	{
		// hit again, so drop down
		SetActivity( ACT_BIG_FLINCH );
		pev->impulse = 2;
		AI_Pain( 2 );
		return;
	}

	// go into one of the fast pain animations	
	pev->impulse = 1;

	SetActivity( ACT_SMALL_FLINCH );
	AI_PainForward( 3 );
}

void CZombieQ :: Killed( entvars_t *pevAttacker, int iGib )
{
	if( m_hQEnemy == NULL )
		m_hQEnemy = CBaseEntity::Instance( pevAttacker );
	MonsterDeathUse( m_hQEnemy, this, USE_TOGGLE, 0.0f );
	
	if (m_chzEntToDrop && !m_bDroppedMyEnt)
	{
		m_bDroppedMyEnt = true;
		DropItem(STRING(m_chzEntToDrop), pev->origin, pev->angles);
	}

	if (!m_bMonstBeQuiet)
		EMIT_SOUND( edict(), CHAN_VOICE, "quake/zombie/z_gib.wav", 1.0, ATTN_NORM );

	if (m_bUseHLGibs)
	{
		CGib::ThrowHead("models/gib_skull.mdl", pev);
		CGib::ThrowGib("models/gib_b_gib.mdl", pev);
		CGib::ThrowGib("models/gib_b_gib.mdl", pev);
		CGib::ThrowGib("models/gib_b_gib.mdl", pev);
	}
	else
	{
		CGib::ThrowHead("models/quake/h_zombie.mdl", pev);
		CGib::ThrowGib("models/quake/gib1.mdl", pev);
		CGib::ThrowGib("models/quake/gib2.mdl", pev);
		CGib::ThrowGib("models/quake/gib3.mdl", pev);
	}

	UTIL_Remove( this );
}

//=========================================================
// Spawn
//=========================================================
void CZombieQ :: Spawn( void )
{
	if( !g_pGameRules->FAllowMonsters( ))
	{
		REMOVE_ENTITY( ENT(pev) );
		return;
	}

	Precache( );
	
	if (pev->model)
		SET_MODEL(ENT(pev), STRING(pev->model));
	else if (gpgs && gpgs->m_iszQZombieModel)
		SET_MODEL(ENT(pev), STRING(gpgs->m_iszQZombieModel));
	else
		SET_MODEL(ENT(pev), "models/quake/zombie.mdl");

	UTIL_SetSize( pev, Vector( -16, -16, -36 ), Vector( 16, 16, 40 ));

	pev->solid	= SOLID_SLIDEBOX;
	pev->movetype	= MOVETYPE_STEP;

	if (pev->health)
		m_flMaxHealth = pev->health;
	else
		m_flMaxHealth = gsd.zombieqHealth;

	pev->health	= m_flMaxHealth;

	if( FBitSet( pev->spawnflags, SF_QMONSTER_CRUCIFIED ))
	{
		pev->movetype	= MOVETYPE_NONE;
		pev->takedamage	= DAMAGE_NO;

		// static monster as furniture
		m_iAIState = STATE_IDLE;
		SetActivity( ACT_SLEEP );

		SetThink( &CQuakeMonster::MonsterThink );
		pev->nextthink = gpGlobals->time + (RANDOM_LONG( 1, 10 ) * 0.1f);
	}
	else
	{
		WalkMonsterInit ();
	}

	m_bFellDown = false;
}

//=========================================================
// Precache - precaches all resources this monster needs
//=========================================================
void CZombieQ :: Precache()
{
	if (pev->model)
		PRECACHE_MODEL(STRING(pev->model));
	else
		PRECACHE_MODEL( "models/quake/zombie.mdl" );

	PRECACHE_MODEL( "models/quake/h_zombie.mdl" );
	PRECACHE_MODEL( "models/quake/zom_gib.mdl" );

	PRECACHE_SOUND_ARRAY( pIdleSounds );
	PRECACHE_SOUND_ARRAY( pPainSounds );

	PRECACHE_SOUND( "quake/zombie/z_fall.wav" );
	PRECACHE_SOUND( "quake/zombie/z_miss.wav" );
	PRECACHE_SOUND( "quake/zombie/z_hit.wav" );
	PRECACHE_SOUND( "quake/zombie/z_shot1.wav" );
	PRECACHE_SOUND( "quake/zombie/z_gib.wav" );
	PRECACHE_SOUND( "quake/zombie/idle_w2.wav" );	// crucified

	QuakePrecache(m_bUseHLGibs);

	//UTIL_PrecacheOther("zombie_missile");
}

void CZombieQ :: ZombieDefeated( void )
{
	if (!m_bMonstBeQuiet)
		EMIT_SOUND( edict(), CHAN_VOICE, "quake/zombie/z_idle.wav", 1.0, ATTN_IDLE );

	pev->health = m_flMaxHealth;
	pev->solid = SOLID_SLIDEBOX;
	m_bFellDown = false;

	if( !WALK_MOVE( ENT(pev), 0, 0, WALKMOVE_NORMAL ))
	{
		// no space to standing up (e.g. player blocked)
		pev->solid = SOLID_NOT;
		pev->nextthink = gpGlobals->time + 5.0f;
	}
	else
	{
		ResetSequenceInfo( );
		SetThink( &CQuakeMonster::MonsterThink );
		pev->nextthink = gpGlobals->time + 0.1f;
	}
}

//=========================================================
// HandleAnimEvent - catches the monster-specific messages
// that occur when tagged animation frames are played.
//=========================================================
void CZombieQ :: HandleAnimEvent( MonsterEvent_t *pEvent )
{
	switch( pEvent->event )
	{
	case ZOMBIE_CURCIFIED_IDLE_SOUND:
		if (!m_bMonstBeQuiet)
			if( RANDOM_FLOAT( 0.0f, 1.0f ) < 0.05f )
				EMIT_SOUND( edict(), CHAN_VOICE, "quake/zombie/idle_w2.wav", 1.0, ATTN_STATIC );
		pev->framerate = RANDOM_FLOAT( 0.5f, 1.1f ); // randomize animation speed
		break;
	case ZOMBIE_WALK_IDLE_SOUND:
		if (!m_bMonstBeQuiet)
			if( RANDOM_FLOAT( 0.0f, 1.0f ) < 0.05f )
				EMIT_SOUND( edict(), CHAN_VOICE, "quake/zombie/z_idle.wav", 1.0, ATTN_IDLE );
		break;
	case ZOMBIE_RUN_IDLE_SOUND:
		if( RANDOM_FLOAT( 0.0f, 1.0f ) < 0.05f )
		{
			if (!m_bMonstBeQuiet)
				EMIT_SOUND_ARRAY_DYNATTN( CHAN_VOICE, pIdleSounds, ATTN_IDLE );
		}
		break;
	case ZOMBIE_RIGHTHAND_ATTACK:
		ThrowMeat( 1, Vector( -10, 22, 30 ));
		break;
	case ZOMBIE_LEFTHAND_ATTACK:
		ThrowMeat( 2, Vector( -10, -24, 29 ));
		break;
	case ZOMBIE_FALL_SOUND:
		if( !pev->frags )
			EMIT_SOUND( edict(), CHAN_VOICE, "quake/zombie/z_fall.wav", 1.0, ATTN_NORM );
		break;
	case ZOMBIE_TEMPORARY_DEAD:
		if( !pev->frags )
		{
			SetThink( &CZombieQ::ZombieDefeated );
			pev->nextthink = gpGlobals->time + 5.0f;
			StopAnimation(); // stop the animation!
			pev->solid = SOLID_NOT;
			pev->frags = 1.0f;

			m_bFellDown = true;
		}
		break;
	default:
		CQuakeMonster::HandleAnimEvent( pEvent );
		break;
	}
}