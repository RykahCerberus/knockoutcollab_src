/***
*
*	Copyright (c) 1996-2002, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   This source code contains proprietary and confidential information of
*   Valve LLC and its suppliers.  Access to this code is restricted to
*   persons who have executed a written SDK license with Valve.  Any access,
*   use or distribution of this code by or to any unlicensed person is illegal.
*
****/

#include	"extdll.h"
#include	"util.h"
#include	"cbase.h"
#include	"quake_monster.h"
#include  "animation.h"
#include	"items.h"
#include	"skill.h"
#include	"player.h"
#include  "gamerules.h"
#include  "decals.h"
#include  "weapons.h"
#include  "knockout_dll/newitems.h"

//=========================================================
// Monster's Anim Events Go Here
//=========================================================
#define ENF_END_ATTACK	1
#define ENF_DROP_BACKPACK	2
#define ENF_SHOOT		3
#define ENF_IDLE_SOUND	4

class CEnforcer : public CQuakeMonster
{
public:
	void Spawn( void );
	void Precache( void );
	BOOL MonsterHasMissileAttack( void ) { return TRUE; }
	void MonsterMissileAttack( void );
	void HandleAnimEvent( MonsterEvent_t *pEvent );

	void MonsterAttack( void );
	void MonsterKilled( entvars_t *pevAttacker, int iGib );
	void MonsterPain( CBaseEntity *pAttacker, float flDamage );
	int BloodColor( void ) { return BLOOD_COLOR_RED; }

	void MonsterSight( void );
	void MonsterIdle( void );
	void MonsterWalk( void );	
	void MonsterRun( void );

	void MonsterFire( void );
	void PainSound( void );
	void IdleSound( void );
	void DeathSound( void );

	static const char *pSightSounds[];
	static const char *pPainSounds[];

	virtual int Save( CSave &save );
	virtual int Restore( CRestore &restore );
	static TYPEDESCRIPTION m_SaveData[];

	int m_fAttackFinished;
	int m_fInAttack;
};

LINK_ENTITY_TO_CLASS( monster_enforcer, CEnforcer );

TYPEDESCRIPTION CEnforcer :: m_SaveData[] = 
{
	DEFINE_FIELD( CEnforcer, m_fInAttack, FIELD_CHARACTER ),
	DEFINE_FIELD( CEnforcer, m_fAttackFinished, FIELD_BOOLEAN ),
}; IMPLEMENT_SAVERESTORE( CEnforcer, CQuakeMonster );

const char *CEnforcer::pPainSounds[] = 
{
	"quake/enforcer/pain1.wav",
	"quake/enforcer/pain2.wav",
};

const char *CEnforcer::pSightSounds[] = 
{
	"quake/enforcer/sight1.wav",
	"quake/enforcer/sight2.wav",
	"quake/enforcer/sight3.wav",
	"quake/enforcer/sight4.wav",
};

void CEnforcer :: MonsterSight( void )
{
	if (!m_bMonstBeQuiet)
		EMIT_SOUND_ARRAY_DYNATTN( CHAN_VOICE, pSightSounds, ATTN_NORM );
}

void CEnforcer :: MonsterIdle( void )
{
	m_iAIState = STATE_IDLE;
	SetActivity( ACT_IDLE );
	m_flMonsterSpeed = 0;
}

void CEnforcer :: MonsterWalk( void )
{
	m_iAIState = STATE_WALK;
	SetActivity( ACT_WALK );
	m_flMonsterSpeed = 3;
}

void CEnforcer :: MonsterRun( void )
{
	m_iAIState = STATE_RUN;
	SetActivity( ACT_RUN );
	m_flMonsterSpeed = 12;
}

void CEnforcer :: MonsterMissileAttack( void )
{
	m_iAIState = STATE_ATTACK;
	SetActivity( ACT_RANGE_ATTACK1 );
}

void CEnforcer :: MonsterPain( CBaseEntity *pAttacker, float flDamage )
{
	if( pev->pain_finished > gpGlobals->time )
		return;

	if (!m_bMonstBeQuiet)
		EMIT_SOUND_ARRAY_DYNATTN( CHAN_VOICE, pPainSounds, ATTN_NORM );

	m_iAIState = STATE_PAIN;
	SetActivity( ACT_BIG_FLINCH );
	m_flMonsterSpeed = 0;

	pev->pain_finished = gpGlobals->time + (SequenceDuration() - 0.3f);
}

void CEnforcer :: MonsterKilled( entvars_t *pevAttacker, int iGib )
{
	if (m_chzEntToDrop && !m_bDroppedMyEnt)
	{
		m_bDroppedMyEnt = true;
		DropItem(STRING(m_chzEntToDrop), pev->origin, pev->angles);
	}

	CCustomAmmoClip::CreatePickup(this, 2, 5, "models/quake/backpack.mdl", "quake/weapons/pkup.wav");

	if( ShouldGibMonster( iGib ))
	{
		if (!m_bMonstBeQuiet)
			EMIT_SOUND( edict(), CHAN_VOICE, "quake/player/udeath.wav", 1.0, ATTN_NORM );

		if (m_bUseHLGibs)
		{
			CGib::ThrowHead("models/gib_skull.mdl", pev);
			CGib::ThrowGib("models/gib_b_gib.mdl", pev);
			CGib::ThrowGib("models/gib_b_gib.mdl", pev);
			CGib::ThrowGib("models/gib_b_gib.mdl", pev);
		}
		else
		{
			CGib::ThrowHead ("models/quake/h_mega.mdl", pev);
			CGib::ThrowGib ("models/quake/gib1.mdl", pev);
			CGib::ThrowGib ("models/quake/gib2.mdl", pev);
			CGib::ThrowGib ("models/quake/gib3.mdl", pev);
		}

		UTIL_Remove( this );
		return;
	}

	// regular death
	if (!m_bMonstBeQuiet)
		EMIT_SOUND( edict(), CHAN_VOICE, "quake/enforcer/death1.wav", 1.0, ATTN_NORM );
}

void CEnforcer :: MonsterAttack( void )
{
	if( m_fAttackFinished )
	{
		m_fAttackFinished = FALSE;
		m_fInAttack = FALSE;

		if( CheckRefire())
		{
			MonsterMissileAttack ();
		}
		else
		{
			MonsterRun();
		}
	}

	AI_Face();
}

void CEnforcer :: MonsterFire( void )
{
	if( m_fInAttack > 1 ) return;

	AI_Face();

	UTIL_MakeVectors(pev->angles);

	Vector vecOrg = pev->origin + gpGlobals->v_forward * 30.0f + gpGlobals->v_right * 8.5 + Vector( 0, 0, 16 );
	Vector vecDir;
	
	if (m_hQEnemy)
		vecDir = (m_hQEnemy->pev->origin - pev->origin).Normalize();
	else
		return;

	EMIT_SOUND(edict(), CHAN_VOICE, "quake/enforcer/enfire.wav", 1.0, ATTN_NORM);

	CProjectileLaser::LaunchLaser( vecOrg, vecDir, this );

	pev->effects |= EF_MUZZLEFLASH;
	m_fInAttack++;
}

//=========================================================
// Spawn
//=========================================================
void CEnforcer :: Spawn( void )
{
	if( !g_pGameRules->FAllowMonsters( ) )
	{
		REMOVE_ENTITY( ENT(pev) );
		return;
	}

	Precache( );
	
	if (pev->model)
		SET_MODEL(ENT(pev), STRING(pev->model));
	else if (gpgs && gpgs->m_iszEnforcerModel)
		SET_MODEL(ENT(pev), STRING(gpgs->m_iszEnforcerModel));
	else
		SET_MODEL(ENT(pev), "models/quake/enforcer.mdl");

	UTIL_SetSize( pev, Vector( -16, -16, -36 ), Vector( 16, 16, 40 ));

	pev->solid	= SOLID_SLIDEBOX;
	pev->movetype	= MOVETYPE_STEP;

	if (pev->health == 0)
		pev->health	= gsd.enforcerHealth;

	WalkMonsterInit ();
}

//=========================================================
// Precache - precaches all resources this monster needs
//=========================================================
void CEnforcer :: Precache()
{
	if (pev->model)
		PRECACHE_MODEL(STRING(pev->model));
	else
		PRECACHE_MODEL( "models/quake/enforcer.mdl" );

	PRECACHE_MODEL( "models/quake/h_mega.mdl" );
	PRECACHE_MODEL( "models/quake/laser.mdl" );

	PRECACHE_SOUND_ARRAY( pSightSounds );
	PRECACHE_SOUND_ARRAY( pPainSounds );

	PRECACHE_SOUND( "quake/enforcer/death1.wav" );
	PRECACHE_SOUND( "quake/enforcer/enfire.wav" );
	PRECACHE_SOUND( "quake/enforcer/enfstop.wav" );
	PRECACHE_SOUND( "quake/enforcer/idle1.wav" );

	QuakePrecache(m_bUseHLGibs);
}

//=========================================================
// HandleAnimEvent - catches the monster-specific messages
// that occur when tagged animation frames are played.
//=========================================================
void CEnforcer :: HandleAnimEvent( MonsterEvent_t *pEvent )
{
	switch( pEvent->event )
	{
	case ENF_SHOOT:
		MonsterFire();
		break;
	case ENF_END_ATTACK:
		m_fInAttack = 2;
		m_fAttackFinished = TRUE;
		break;
	case ENF_DROP_BACKPACK:
		break;
	case ENF_IDLE_SOUND:
		if( RANDOM_FLOAT( 0.0f, 1.0f ) < 0.2f )
			if (!m_bMonstBeQuiet)
				EMIT_SOUND( edict(), CHAN_VOICE, "quake/enforcer/idle1.wav", 1.0, ATTN_IDLE );
		break;
	default:
		CQuakeMonster::HandleAnimEvent( pEvent );
		break;
	}
}