/***
*
*	Copyright (c) 1996-2002, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   This source code contains proprietary and confidential information of
*   Valve LLC and its suppliers.  Access to this code is restricted to
*   persons who have executed a written SDK license with Valve.  Any access,
*   use or distribution of this code by or to any unlicensed person is illegal.
*
****/
#ifndef QUAKE_MONSTER_H
#define QUAKE_MONSTER_H

#include "monsters.h"
#include	"UserMessages.h"
#include	"game.h"

#define SF_QMONSTER_AMBUSH		32
#define SF_QMONSTER_CRUCIFIED	1024

typedef enum
{
	ATTACK_NONE = 0,
	ATTACK_STRAIGHT,
	ATTACK_SLIDING,
	ATTACK_MELEE,
	ATTACK_MISSILE
} ATTACKSTATE;

typedef enum
{
	STATE_IDLE = 0,
	STATE_WALK,
	STATE_RUN,
	STATE_ATTACK,
	STATE_PAIN,
	STATE_DEAD
} AISTATE;

// distance ranges
typedef enum
{
	RANGE_MELEE = 0,
	RANGE_NEAR,
	RANGE_MID,
	RANGE_FAR
} RANGETYPE;

//
// generic Monster
//
class CQuakeMonster : public CBaseAnimating
{
public:
	float		m_flSearchTime;
	float		m_flPauseTime;

	AISTATE		m_iAIState;
	ATTACKSTATE	m_iAttackState;

	Activity		m_Activity;	// what the monster is doing (animation)
	Activity		m_IdealActivity;	// monster should switch to this activity

	float		m_flMonsterSpeed;
	float		m_flMoveDistance;	// member laste move distance. Used for strafe
	BOOL		m_fLeftY;

	float		m_flSightTime;
	EHANDLE		m_hSightEntity;
	int		m_iRefireCount;

	float		m_flEnemyYaw;
	RANGETYPE		m_iEnemyRange;
	BOOL		m_fEnemyInFront;
	BOOL		m_fEnemyVisible;

	virtual CQuakeMonster *GetMonster( void ) { return this; }

	// overloaded monster functions (same as th_callbacks in quake)
	virtual void	MonsterIdle( void ) {}
	virtual void	MonsterWalk( void ) {}
	virtual void	MonsterRun( void ) {}
	virtual void	MonsterMeleeAttack( void ) {}
	virtual void	MonsterMissileAttack( void ) {}
	virtual void	MonsterPain( CBaseEntity *pAttacker, float flDamage );
	virtual void	MonsterKilled( entvars_t *pevAttacker, int iGib ) {}
	virtual void	MonsterSight( void );
	virtual void	MonsterAttack( void );
	virtual void	CornerReached( void ) {}	// called while path_corner is reached

	virtual BOOL	MonsterCheckAnyAttack( void );
	virtual BOOL	MonsterCheckAttack( void );
	virtual BOOL	MonsterHasMeleeAttack( void ) { return FALSE; }
	virtual BOOL	MonsterHasMissileAttack( void ) { return FALSE; }
	virtual BOOL	MonsterHasPain( void ) { return TRUE; }	// tarbaby feels no pain

	void EXPORT	MonsterThink( void );
	void EXPORT	MonsterUse( CBaseEntity *pActivator, CBaseEntity *pCaller, USE_TYPE useType, float value );
	void EXPORT	MonsterDeathUse( CBaseEntity *pActivator, CBaseEntity *pCaller, USE_TYPE useType, float value );

	virtual void	TraceAttack( entvars_t *pevAttacker, float flDamage, Vector vecDir, TraceResult *ptr, int bitsDamageType );
	virtual int		TakeDamage( entvars_t* pevInflictor, entvars_t* pevAttacker, float flDamage, int bitsDamageType );
	virtual void	Killed( entvars_t *pevAttacker, int iGib );
		
	// common utility functions
	void		SetEyePosition( void );
	void		SetActivity ( Activity NewActivity );
	inline void	StopAnimation( void ) { pev->framerate = 0; }
	void		AttackFinished( float flFinishTime );
	RANGETYPE		TargetRange( CBaseEntity *pTarg );
	BOOL		TargetVisible( CBaseEntity *pTarg );
	BOOL		InFront( CBaseEntity *pTarg );
	BOOL		FindTarget( void );
	BOOL		FacingIdeal( void );
	void EXPORT	FoundTarget( void );
	void		HuntTarget ( void );
	BOOL		ShouldGibMonster( int iGib );
	BOOL		CheckRefire( void );
	void		ReportAIState( void ); // debug

	// MoveExecute functions
	BOOL		CloseEnough( float flDist );
	BOOL		WalkMove( float flYaw, float flDist );
	void		MoveToGoal( float flDist );

	virtual void	AI_Forward( float flDist );
	virtual void	AI_Backward( float flDist );
	virtual void	AI_Pain( float flDist );
	virtual void	AI_PainForward( float flDist );
	virtual void	AI_Walk( float flDist );
	virtual void	AI_Run( float flDist );
	virtual void	AI_Idle( void );
	virtual void	AI_Turn( void );
	virtual void	AI_Run_Melee( void );
	virtual void	AI_Run_Missile( void );
	virtual void	AI_Run_Slide( void );
	virtual void	AI_Charge( float flDist );
	virtual void	AI_Charge_Side( void );
	virtual void	AI_Face( void );
	virtual void	AI_Melee( void );
	virtual void	AI_Melee_Side( void );

	virtual int Save( CSave &save );
	virtual int Restore( CRestore &restore );
	static TYPEDESCRIPTION m_SaveData[];

	// monsters init
	void EXPORT	FlyMonsterInitThink( void );
	void		FlyMonsterInit( void );

	void EXPORT	WalkMonsterInitThink( void );
	void		WalkMonsterInit( void );

	void EXPORT	SwimMonsterInitThink( void );
	void		SwimMonsterInit( void );

	int			Classify() override { return CLASS_QUAKE_MONSTER; }

	EHANDLE			m_hMoveTarget;

	EHANDLE			m_hQEnemy;
	EHANDLE			m_hQOldEnemy;	// mad at this player before taking damage
	
	static void FireQuakeBullets( entvars_t *pev, int iShots, Vector vecDir, Vector vecSpread );

	
	void    KeyValue(KeyValueData* pkvd) override;
	BOOL	m_bUseHLGibs;

	virtual bool	IsQuakeMonster() override { return true; }
	virtual bool	IsHellKnight() { return false; }
};

extern void QuakePrecache(bool HLGib = false);

#endif