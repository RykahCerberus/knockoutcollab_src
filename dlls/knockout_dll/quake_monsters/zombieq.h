/***
*
*	Copyright (c) 1996-2002, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   This source code contains proprietary and confidential information of
*   Valve LLC and its suppliers.  Access to this code is restricted to
*   persons who have executed a written SDK license with Valve.  Any access,
*   use or distribution of this code by or to any unlicensed person is illegal.
*
****/
#ifndef QUAKE_ZOMBIE_H
#define QUAKE_ZOMBIE_H

#include "quake_monster.h"

class CZombieQ : public CQuakeMonster
{
public:
	void Spawn( void );
	void Precache( void );
	BOOL MonsterHasMissileAttack( void ) { return TRUE; }
	void MonsterMissileAttack( void );
	void HandleAnimEvent( MonsterEvent_t *pEvent );

	void MonsterPain( CBaseEntity *pAttacker, float flDamage );
	int BloodColor( void ) { return BLOOD_COLOR_RED; }

	void MonsterSight( void );
	void MonsterIdle( void );
	void MonsterWalk( void );	
	void MonsterRun( void );

	virtual void AI_Idle( void );
	void ThrowMeat( int iAttachment, Vector vecOffset );

	static const char *pIdleSounds[];
	static const char *pPainSounds[];
	static const char *pDeathSounds[];

	void EXPORT ZombieDefeated( void );

	void Killed( entvars_t *pevAttacker, int iGib );

	virtual int Save(CSave& save);
	virtual int Restore(CRestore& restore);
	static TYPEDESCRIPTION m_SaveData[];
	float	m_flMaxHealth;

	BOOL	m_bFellDown;
};

#endif