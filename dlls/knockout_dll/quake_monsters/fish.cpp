/***
*
*	Copyright (c) 1996-2002, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   This source code contains proprietary and confidential information of
*   Valve LLC and its suppliers.  Access to this code is restricted to
*   persons who have executed a written SDK license with Valve.  Any access,
*   use or distribution of this code by or to any unlicensed person is illegal.
*
****/

#include	"extdll.h"
#include	"util.h"
#include	"cbase.h"
#include	"quake_monster.h"
#include  "animation.h"
#include	"items.h"
#include	"skill.h"
#include	"player.h"
#include  "gamerules.h"
#include  "decals.h"

//=========================================================
// Monster's Anim Events Go Here
//=========================================================
#define FISH_IDLE_SOUND		1
#define FISH_MELEE_ATTACK		2

class CFish : public CQuakeMonster
{
public:
	void Spawn( void );
	void Precache( void );
	BOOL MonsterHasMeleeAttack( void ) { return TRUE; }
	void MonsterMeleeAttack( void );
	void HandleAnimEvent( MonsterEvent_t *pEvent );
	int BloodColor( void ) { return BLOOD_COLOR_RED; }

	void MonsterPain( CBaseEntity *pAttacker, float flDamage );
	void MonsterKilled( entvars_t *pevAttacker, int iGib );

	void MonsterIdle( void );
	void MonsterWalk( void );	
	void MonsterRun( void );
	void MonsterAttack( void );
	void FishMelee( void );
};

LINK_ENTITY_TO_CLASS( monster_fish, CFish );

void CFish :: MonsterIdle( void )
{
	m_iAIState = STATE_IDLE;
	SetActivity( ACT_SWIM );
	m_flMonsterSpeed = 0;
}

void CFish :: MonsterWalk( void )
{
	m_iAIState = STATE_WALK;
	SetActivity( ACT_SWIM );
	m_flMonsterSpeed = 8;
}

void CFish :: MonsterRun( void )
{
	m_iAIState = STATE_RUN;
	SetActivity( ACT_SWIM );
	m_flMonsterSpeed = 12;
}

void CFish :: MonsterPain( CBaseEntity *pAttacker, float flDamage )
{
	m_iAIState = STATE_PAIN;
	SetActivity( ACT_BIG_FLINCH );

	AI_Pain( 6 );
}

void CFish :: MonsterMeleeAttack( void )
{
	m_iAIState = STATE_ATTACK;
	SetActivity( ACT_MELEE_ATTACK1 );
}

void CFish :: MonsterAttack( void )
{
	AI_Charge( 10 );

	if( m_iAIState == STATE_ATTACK && m_fSequenceFinished )
		MonsterRun();
}

void CFish :: FishMelee( void )
{
	if (m_hQEnemy == NULL)
		return; // removed before stroke
		
	Vector delta = m_hQEnemy->pev->origin - pev->origin;

	//Was 60 but this kept the fish from being able to attack an HL sized player.
	if (delta.Length() > 135)
		return;

	if (!m_bMonstBeQuiet)
		EMIT_SOUND( edict(), CHAN_VOICE, "quake/fish/bite.wav", 1.0, ATTN_NORM );		

	//Tweaked the logic a bit, but still the same end result
	//float ldmg = (RANDOM_FLOAT( 0.0f, 1.0f ) + RANDOM_FLOAT( 0.0f, 1.0f )) * 3;

	float ldmg = gsd.fishDmgBite + RANDOM_FLOAT(0.0f, gsd.fishDmgBiteBonus);
	m_hQEnemy->TakeDamage (pev, pev, ldmg, DMG_GENERIC);	
}

void CFish :: MonsterKilled( entvars_t *pevAttacker, int iGib )
{
	if (m_chzEntToDrop && !m_bDroppedMyEnt)
	{
		m_bDroppedMyEnt = true;
		DropItem(STRING(m_chzEntToDrop), pev->origin, pev->angles);
	}

	if (iGib == GIB_ALWAYS)
	{
		if (!m_bMonstBeQuiet)
			EMIT_SOUND(edict(), CHAN_VOICE, "quake/fish/death.wav", 1.0, ATTN_NORM);

		if (m_bUseHLGibs)
			CGib::ThrowGib("models/gib_b_gib.mdl", pev);
		else
			CGib::ThrowGib("models/quake/gib1.mdl", pev);

		UTIL_Remove(this);
		return;
	}

	if (!m_bMonstBeQuiet)
		EMIT_SOUND( edict(), CHAN_VOICE, "quake/fish/death.wav", 1.0, ATTN_NORM );

	pev->flags &= ~(FL_SWIM);
	pev->gravity = 0.08f; // underwater gravity

	// a bit of a hack. If a corpses' bbox is positioned such that being left solid so that
	// it can be attacked will block the player on a slope or stairs, the corpse is made nonsolid. 
	UTIL_SetSize ( pev, Vector ( -16, -16, -36 ), Vector ( 16, 16, 16 ) );
}

//=========================================================
// Spawn
//=========================================================
void CFish :: Spawn( void )
{
	if( !g_pGameRules->FAllowMonsters( ) )
	{
		REMOVE_ENTITY( ENT(pev) );
		return;
	}

	Precache( );

	if (pev->model)
		SET_MODEL(ENT(pev), STRING(pev->model));
	else if (gpgs && gpgs->m_iszRotfishModel)
		SET_MODEL(ENT(pev), STRING(gpgs->m_iszRotfishModel));
	else
		SET_MODEL(ENT(pev), "models/quake/fish.mdl");

	UTIL_SetSize( pev, Vector( -16, -16, -24 ), Vector( 16, 16, 24 ));

	pev->solid	= SOLID_SLIDEBOX;
	pev->movetype	= MOVETYPE_STEP;

	if (pev->health == 0)
		pev->health	= gsd.fishHealth;

	SwimMonsterInit();
}

//=========================================================
// Precache - precaches all resources this monster needs
//=========================================================
void CFish :: Precache()
{
	if (pev->model)
		PRECACHE_MODEL(STRING(pev->model));
	else
		PRECACHE_MODEL( "models/quake/fish.mdl" );

	PRECACHE_SOUND( "quake/fish/death.wav" );
	PRECACHE_SOUND( "quake/fish/bite.wav" );
	PRECACHE_SOUND( "quake/fish/idle.wav" );

	QuakePrecache(m_bUseHLGibs);
}

//=========================================================
// HandleAnimEvent - catches the monster-specific messages
// that occur when tagged animation frames are played.
//=========================================================
void CFish :: HandleAnimEvent( MonsterEvent_t *pEvent )
{
	switch( pEvent->event )
	{
	case FISH_IDLE_SOUND:
		if( m_iAIState == STATE_RUN && RANDOM_FLOAT( 0.0f, 1.0f ) < 0.5f )
			if (!m_bMonstBeQuiet)
				EMIT_SOUND( edict(), CHAN_VOICE, "quake/fish/idle.wav", 1.0, ATTN_NORM );
		break;
	case FISH_MELEE_ATTACK:
		FishMelee ();
		break;
	default:
		CQuakeMonster::HandleAnimEvent( pEvent );
		break;
	}
}