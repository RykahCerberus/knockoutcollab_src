
#include	"extdll.h"
#include	"util.h"
#include	"cbase.h"
#include	"monsters.h"
#include	"talkmonster.h"
#include	"schedule.h"
#include	"defaultai.h"
#include	"scripted.h"
#include	"weapons.h"
#include	"soundent.h"
#include	"knockout_dll\game_settings.h"
#include	"barney.h"
#include	"animation.h"

enum BarnTypes
{
	BARNEY_TYPE_RANDOM_SCI = -4,
	BARNEY_TYPE_RANDOM_OTIS,
	BARNEY_TYPE_RANDOM_BAR,
	BARNEY_TYPE_RANDOM_ALL,

	BARNEY_TYPE_INVAILD = 0,

	BARNEY_TYPE_REGULAR,
	BARNEY_TYPE_NOVEST,
	BARNEY_TYPE_OTIS_LONG,
	BARNEY_TYPE_OTIS_SHORT,
	BARNEY_TYPE_SCIENTIST,
	BARNEY_TYPE_CLEANSUIT,
	BARNEY_TYPE_HEV,
	BARNEY_TYPE_GMAN,
	BARNEY_TYPE_CIVILIAN,
	BARNEY_TYPE_CUSTOM,
};


enum BarnWeapon
{
	BARNEY_WEAPON_RAND = -1,
	BARNEY_WEAPON_GLOCK = 0,
	BARNEY_WEAPON_357,
	BARNEY_WEAPON_EAGLE,
	BARNEY_WEAPON_MP5,
	BARNEY_WEAPON_SHOTGUN,
};

enum BarnBodyGroups
{
	BARNEY_BODYGROUP_HEAD = 0,
	BARNEY_BODYGROUP_GUN,
	BARNEY_BODYGROUP_HELMET,
};

enum BarnBodyWeapon
{
	BARNEY_WEAPONBODY_NONE = 0,
	BARNEY_WEAPONBODY_GLOCK,
	BARNEY_WEAPONBODY_GLOCK_HOLSTER,
	BARNEY_WEAPONBODY_EAGLE,
	BARNEY_WEAPONBODY_EAGLE_HOLSTER,
	BARNEY_WEAPONBODY_357,
	BARNEY_WEAPONBODY_357_HOLSTER,
	BARNEY_WEAPONBODY_MP5,
	BARNEY_WEAPONBODY_SHOTGUN,
};

enum BarnBodyHead
{
	BARNEY_HEADRAND_SCI = -4,
	BARNEY_HEADRAND_OTIS = -3,
	BARNEY_HEADRAND_ALL = -2,
	BARNEY_HEADRAND_MATCH = -1,
	BARNEY_HEADBODY_BARNEY = 0,
	BARNEY_HEADBODY_OTIS,
	BARNEY_HEADBODY_OTIS2,
	BARNEY_HEADBODY_OTISBLK,
	BARNEY_HEADBODY_OTISMP,
	BARNEY_HEADBODY_SCIWHT,
	BARNEY_HEADBODY_SCIBLK,
	BARNEY_HEADBODY_SCINERD,
	BARNEY_HEADBODY_NONE,
};

enum BarnBodyHelmet
{
	BARNEY_HELMETBODY_OFF = 0,
	BARNEY_HELMETBODY_ON,
	BARNEY_HELMETBODY_SCION,
};

enum BarnVoice
{
	BARNEY_VOICE_RANDOM = -2,
	BARNEY_VOICE_MATCHHEAD = -1,
	BARNEY_VOICE_BARNEY = 0,
	BARNEY_VOICE_OTIS,
	BARNEY_VOICE_SCIENTIST,
};

enum BarnHelmet
{
	BARNEY_HELMET_MATCHOUTFIT = -3,
	BARNEY_HELMET_RANDOM = -2,
	BARNEY_HELMET_MATCH = -1,
	BARNEY_HELMET_ON = 0,
	BARNEY_HELMET_OFF,
};

enum BarnStrengthKevlar
{
	BARNEY_KEVLAR_STRENGTH_MATCH = -1,
	BARNEY_KEVLAR_STRENGTH_DEFAULT = 0,
	BARNEY_KEVLAR_STRENGTH_USELESS,
	BARNEY_KEVLAR_STRENGTH_SUPER,
};

enum BarnStrengthHelmet
{
	BARNEY_HELMET_STRENGTH_DEFAULT = 0,
	BARNEY_HELMET_STRENGTH_STRONGER,
	BARNEY_HELMET_STRENGTH_SUPER,
	BARNEY_HELMET_STRENGTH_USELESS,
};

class CBarneyExtended : public CBarney
{
public:
	void Spawn() override;
	void Precache() override;
	void KeyValue(KeyValueData* pkvd) override;
	void HandleAnimEvent(MonsterEvent_t* pEvent) override;
	void BarneyFirePistol() override;
	void Killed(entvars_t* pevAttacker, int iGib) override;
	void SetActivity(Activity NewActivity) override;
	void TraceAttack(entvars_t* pevAttacker, float flDamage, Vector vecDir, TraceResult* ptr, int bitsDamageType) override;
	void DeathSound() override;
	void PainSound() override;

	void SetupWeapons();
	void SetupVoice();
	void SetupHelmet();
	void PreSetup();
	void SetupOutfit();

	int m_iHelmet;
	int m_iHelmetStrength;
	int m_iArmorStrength;
	int m_iHead;
	int m_iVoicePreset;
	bool m_bCanHolster;
	int m_iMagSize;
	int m_iType;

	int m_bBodyGroup_Gun;
	int m_bBodyGroup_Holster;
	string_t m_chzGunSound;
	string_t m_chzWeaponClassname;

	bool m_bDroppedAlready;
	bool m_bHasCustomSpeak;
	bool m_bPlayerBullets;

	int	Save(CSave& save) override;
	int Restore(CRestore& restore) override;
	static TYPEDESCRIPTION m_SaveData[];
};

TYPEDESCRIPTION	CBarneyExtended::m_SaveData[] =
{
	DEFINE_FIELD( CBarneyExtended, m_iHelmet, FIELD_INTEGER ),
	DEFINE_FIELD( CBarneyExtended, m_iHelmetStrength, FIELD_INTEGER ),
	DEFINE_FIELD( CBarneyExtended, m_iArmorStrength, FIELD_INTEGER ),
	DEFINE_FIELD( CBarneyExtended, m_iVoicePreset, FIELD_INTEGER ),
	DEFINE_FIELD( CBarneyExtended, m_bCanHolster, FIELD_BOOLEAN ),
	DEFINE_FIELD( CBarneyExtended, m_iMagSize, FIELD_INTEGER ),
	DEFINE_FIELD( CBarneyExtended, m_iType, FIELD_INTEGER ),
	
	DEFINE_FIELD( CBarneyExtended, m_bBodyGroup_Gun, FIELD_INTEGER ),
	DEFINE_FIELD( CBarneyExtended, m_bBodyGroup_Holster, FIELD_INTEGER ),
	DEFINE_FIELD( CBarneyExtended, m_chzGunSound, FIELD_STRING ),
	DEFINE_FIELD( CBarneyExtended, m_chzWeaponClassname, FIELD_STRING ),
	
	DEFINE_FIELD( CBarneyExtended, m_bDroppedAlready, FIELD_BOOLEAN ),
	DEFINE_FIELD( CBarneyExtended, m_bHasCustomSpeak, FIELD_BOOLEAN ),
	DEFINE_FIELD( CBarneyExtended, m_bPlayerBullets, FIELD_BOOLEAN ),
};

IMPLEMENT_SAVERESTORE( CBarneyExtended, CBarney );

LINK_ENTITY_TO_CLASS(monster_barney_extended, CBarneyExtended);

void CBarneyExtended::KeyValue(KeyValueData* pkvd)
{
	if (FStrEq(pkvd->szKeyName, "HasHelmet"))
	{
		m_iHelmet = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "HelmetStrength"))
	{
		m_iHelmetStrength = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "PlayerBullets"))
	{
		m_bPlayerBullets = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "VoicePreset"))
	{
		m_iVoicePreset = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else
		CTalkMonster::KeyValue(pkvd);
}

//=========================================================
// Precache - precaches all resources this monster needs
//=========================================================
void CBarneyExtended:: Precache()
{
	PRECACHE_MODEL((char*)STRING(pev->model));

	PRECACHE_SOUND("barney/ba_attack1.wav" );
	PRECACHE_SOUND("barney/ba_attack2.wav" );

	PRECACHE_SOUND("barney/ba_pain1.wav");
	PRECACHE_SOUND("barney/ba_pain2.wav");
	PRECACHE_SOUND("barney/ba_pain3.wav");

	PRECACHE_SOUND("barney/ba_die1.wav");
	PRECACHE_SOUND("barney/ba_die2.wav");
	PRECACHE_SOUND("barney/ba_die3.wav");
	
	PRECACHE_SOUND("scientist/sci_pain1.wav");
	PRECACHE_SOUND("scientist/sci_pain2.wav");
	PRECACHE_SOUND("scientist/sci_pain3.wav");
	PRECACHE_SOUND("scientist/sci_pain4.wav");
	PRECACHE_SOUND("scientist/sci_pain5.wav");

	PRECACHE_SOUND("player/bhit_helmet-1.wav");
	PRECACHE_SOUND("player/bhit_kevlar-1.wav");
	
	// every new barney must call this, otherwise
	// when a level is loaded, nobody will talk (time is reset to 0)
	TalkInit();
	CTalkMonster::Precache();
}	

void CBarneyExtended::Spawn()
{
	if (!pev->model)
	{
		ALERT(at_console, "EXTENDED BARNEY SPAWNED WITHOUT A MODEL!!!\n");
		m_iType = BARNEY_TYPE_RANDOM_ALL;
	}

	SetupOutfit();

	m_bBodyGroup_Holster = BARNEY_WEAPONBODY_NONE;
	m_bBodyGroup_Gun = BARNEY_WEAPONBODY_NONE;

	PreSetup();

	if (m_iszSpeakAs)
		m_bHasCustomSpeak = true;
	else
		m_bHasCustomSpeak = false;

	SetupVoice();

	Precache();

	SET_MODEL(ENT(pev), STRING(pev->model));

	UTIL_SetSize(pev, VEC_HUMAN_HULL_MIN, VEC_HUMAN_HULL_MAX);

	pev->solid			= SOLID_SLIDEBOX;
	pev->movetype		= MOVETYPE_STEP;
	m_bloodColor		= BLOOD_COLOR_RED;

	if (pev->health == 0) //LRC
		pev->health			= gSkillData.barneyHealth;

	pev->view_ofs		= Vector ( 0, 0, 50 );// position of the eyes relative to monster's origin.
	m_flFieldOfView		= VIEW_FIELD_WIDE; // NOTE: we need a wide field of view so npc will notice player and say hello
	m_MonsterState		= MONSTERSTATE_NONE;

	m_afCapability		= bits_CAP_HEAR | bits_CAP_TURN_HEAD | bits_CAP_DOORS_GROUP;

	MonsterInit();
	SetUse( &CBarney::FollowerUse );

	SetupWeapons();

	SetBodygroup(BARNEY_BODYGROUP_HEAD, m_iHead);

	if ((m_iHead == BARNEY_HEADBODY_OTISBLK) || (m_iHead == BARNEY_HEADBODY_SCIBLK))
		pev->skin = 1;
	else
		pev->skin = 0;

	m_bDroppedAlready = false;

	if (m_bCanHolster)
	{
		SetBodygroup(BARNEY_BODYGROUP_GUN, m_bBodyGroup_Holster);
		m_fGunDrawn = FALSE;
	}
	else
	{
		SetBodygroup(BARNEY_BODYGROUP_GUN, m_bBodyGroup_Gun);
		m_fGunDrawn = TRUE;
	}

	SetupHelmet();

	if (m_iVoicePreset == BARNEY_VOICE_SCIENTIST)
	{
		if (!m_bHasCustomSpeak)
			m_szGrp[TLK_HELLO] =	"SC_HELLOB";
	}
}

void CBarneyExtended::PreSetup()
{
	if ( m_iHead == BARNEY_HEADRAND_MATCH )
	{
		switch (m_iType)
		{
			case BARNEY_TYPE_REGULAR: m_iHead = BARNEY_HEADBODY_BARNEY; break;
			case BARNEY_TYPE_NOVEST: m_iHead = BARNEY_HEADBODY_BARNEY; break;
			case BARNEY_TYPE_OTIS_LONG: m_iHead = BARNEY_HEADRAND_OTIS; break;
			case BARNEY_TYPE_OTIS_SHORT: m_iHead = BARNEY_HEADRAND_OTIS; break;
			case BARNEY_TYPE_SCIENTIST: m_iHead = BARNEY_HEADRAND_SCI; break;
			case BARNEY_TYPE_CLEANSUIT: m_iHead = BARNEY_HEADRAND_SCI; break;
			case BARNEY_TYPE_HEV: m_iHead = BARNEY_HEADRAND_SCI; break;
			case BARNEY_TYPE_GMAN: m_iHead = BARNEY_HEADRAND_SCI; break;
			case BARNEY_TYPE_CIVILIAN: m_iHead = BARNEY_HEADRAND_OTIS; break;
		}
	}

	if (m_iHead == BARNEY_HEADRAND_ALL)
	{
		//Give regular barney a bit extra chance
		int j = RANDOM_LONG(0, 7);
		if (j == 7)
			m_iHead = BARNEY_HEADBODY_BARNEY;
		else
			m_iHead = RANDOM_LONG(BARNEY_HEADBODY_BARNEY, BARNEY_HEADBODY_SCINERD);
	}
	else if (m_iHead == BARNEY_HEADRAND_OTIS)
	{
		m_iHead = RANDOM_LONG(BARNEY_HEADBODY_OTIS, BARNEY_HEADBODY_OTISMP);
	}
	else if (m_iHead == BARNEY_HEADRAND_SCI)
	{
		m_iHead = RANDOM_LONG(BARNEY_HEADBODY_SCIWHT, BARNEY_HEADBODY_SCINERD);
	}
}

void CBarneyExtended::SetupVoice()
{
	const char* voset;

	if (m_iVoicePreset == BARNEY_VOICE_RANDOM)
		m_iVoicePreset = RANDOM_LONG(BARNEY_VOICE_BARNEY, BARNEY_VOICE_SCIENTIST);

	if (m_iVoicePreset == BARNEY_VOICE_MATCHHEAD)
	{
		switch (m_iHead)
		{
			case BARNEY_HEADBODY_OTIS: m_iVoicePreset = BARNEY_VOICE_OTIS; break;
			case BARNEY_HEADBODY_OTIS2: m_iVoicePreset = BARNEY_VOICE_OTIS; break;
			case BARNEY_HEADBODY_OTISBLK: m_iVoicePreset = BARNEY_VOICE_OTIS; break;
			case BARNEY_HEADBODY_OTISMP: m_iVoicePreset = BARNEY_VOICE_OTIS; break;
			case BARNEY_HEADBODY_SCIWHT: m_iVoicePreset = BARNEY_VOICE_SCIENTIST; break;
			case BARNEY_HEADBODY_SCIBLK: m_iVoicePreset = BARNEY_VOICE_SCIENTIST; break;
			case BARNEY_HEADBODY_SCINERD: m_iVoicePreset = BARNEY_VOICE_SCIENTIST; break;
			default: m_iVoicePreset = BARNEY_VOICE_BARNEY; break;
		}
	}

	switch (m_iVoicePreset)
	{
		case BARNEY_VOICE_OTIS: voset = "OT"; break;
		case BARNEY_VOICE_SCIENTIST: voset = "SC"; break;
		default: voset = "BA"; break;
	}

	if (!m_iszSpeakAs)
		m_iszSpeakAs = ALLOC_STRING(voset);

	if (!m_voicePitch)
	{
		// get voice for head
		switch (pev->body % 3)
		{
			case BARNEY_HEADBODY_SCINERD:	m_voicePitch = 105; break;
			case BARNEY_HEADBODY_OTIS2:	m_voicePitch = 105; break;
			case BARNEY_HEADBODY_SCIBLK:	m_voicePitch = 95;  break;
			case BARNEY_HEADBODY_OTISBLK:	m_voicePitch = 95;  break;
			default: m_voicePitch = 100; break;
		}
	}
}

void CBarneyExtended::SetupOutfit()
{
	if (strncmp (STRING(pev->model),"models/editormodels/_random_all.mdl", 36) == 0)
		m_iType = BARNEY_TYPE_RANDOM_ALL;
	else if (strncmp (STRING(pev->model), "models/editormodels/_random_bar.mdl", 36) == 0)
		m_iType = BARNEY_TYPE_RANDOM_BAR;
	else if (strncmp (STRING(pev->model), "models/editormodels/_random_otis.mdl", 37) == 0)
		m_iType = BARNEY_TYPE_RANDOM_OTIS;
	else if (strncmp (STRING(pev->model), "models/editormodels/_random_sci.mdl", 36) == 0)
		m_iType = BARNEY_TYPE_RANDOM_SCI;

	if (m_iType == BARNEY_TYPE_RANDOM_ALL)
	{
		int i = RANDOM_LONG(0, 7);

		switch (i)
		{
		case 0:
		case 1:
			m_iType = BARNEY_TYPE_RANDOM_OTIS; 
			break;
		case 2:
		case 3:
			m_iType = BARNEY_TYPE_RANDOM_SCI; 
			break;
		case 4:
		case 5:
			m_iType = BARNEY_TYPE_RANDOM_BAR; 
			break;
		case 6:
			pev->model = ALLOC_STRING("models/barney2/civilian.mdl");
			m_iType = BARNEY_TYPE_CIVILIAN; 
			break;
		case 7:
			pev->model = ALLOC_STRING("models/barney2/gman.mdl");
			m_iType = BARNEY_TYPE_GMAN; 
			break;
		}
	}

	if (m_iType == BARNEY_TYPE_RANDOM_BAR)
	{
		int i = RANDOM_LONG(0, 1);

		switch (i)
		{
		case 0:	pev->model = ALLOC_STRING("models/barney2/barney.mdl"); break;
		case 1:	pev->model = ALLOC_STRING("models/barney2/barney2.mdl"); break;
		}
	}

	if (m_iType == BARNEY_TYPE_RANDOM_SCI)
	{
		int i = RANDOM_LONG(0, 2);

		switch (i)
		{
		case 0:	pev->model = ALLOC_STRING("models/barney2/cl_suit.mdl"); break;
		case 1:	pev->model = ALLOC_STRING("models/barney2/scientist.mdl"); break;
		case 2:	pev->model = ALLOC_STRING("models/barney2/hev.mdl"); break;
		}
	}

	if (m_iType == BARNEY_TYPE_RANDOM_OTIS)
	{
		int i = RANDOM_LONG(0, 1);

		switch (i)
		{
		case 0:	pev->model = ALLOC_STRING("models/barney2/otis.mdl"); break;
		case 1:	pev->model = ALLOC_STRING("models/barney2/otis2.mdl"); break;
		}
	}

	if (strncmp (STRING(pev->model), "models/barney2/cl_suit.mdl", 27) == 0)
		m_iType = BARNEY_TYPE_CLEANSUIT;
	else if (strncmp (STRING(pev->model), "models/barney2/scientist.mdl", 29) == 0)
		m_iType = BARNEY_TYPE_SCIENTIST;
	else if (strncmp (STRING(pev->model), "models/barney2/hev.mdl", 23) == 0)
		m_iType = BARNEY_TYPE_HEV;
	else if (strncmp (STRING(pev->model), "models/barney2/gman.mdl", 24) == 0)
		m_iType = BARNEY_TYPE_GMAN;
	else if (strncmp (STRING(pev->model), "models/barney2/otis.mdl", 24) == 0)
		m_iType = BARNEY_TYPE_OTIS_LONG;
	else if (strncmp (STRING(pev->model), "models/barney2/otis2.mdl", 25) == 0)
		m_iType = BARNEY_TYPE_OTIS_SHORT;
	else if (strncmp (STRING(pev->model), "models/barney2/civilian.mdl", 28) == 0)
		m_iType = BARNEY_TYPE_CIVILIAN;
	else if (strncmp (STRING(pev->model), "models/barney2/barney2.mdl", 27) == 0)
		m_iType = BARNEY_TYPE_NOVEST;
	else if (strncmp (STRING(pev->model), "models/barney2/barney.mdl", 26) == 0)
		m_iType = BARNEY_TYPE_REGULAR;
	else
		m_iType = BARNEY_TYPE_CUSTOM;

	m_iHead = pev->body;
	pev->body = 0;

	if ((m_iType == BARNEY_TYPE_INVAILD) ||
		(strncmp(STRING(pev->model), "models/editormodels/_random_all.mdl", 36) == 0) ||
		(strncmp(STRING(pev->model), "models/editormodels/_random_sci.mdl", 36) == 0) ||
		(strncmp(STRING(pev->model), "models/editormodels/_random_bar.mdl", 36) == 0) ||
		(strncmp(STRING(pev->model), "models/editormodels/_random_otis.mdl", 37) == 0))
	{
		pev->model = ALLOC_STRING("models/barney2/barney.mdl");
		ALERT(at_console, "CBarneyExtended::Spawn - Model failed to set properly!\n");
	}

	if (m_iArmorStrength == BARNEY_KEVLAR_STRENGTH_MATCH)
	{
		switch (m_iType)
		{
			case BARNEY_TYPE_CIVILIAN:
			case BARNEY_TYPE_CLEANSUIT:
			case BARNEY_TYPE_GMAN:
			case BARNEY_TYPE_NOVEST:
			case BARNEY_TYPE_SCIENTIST:
				m_iArmorStrength = BARNEY_KEVLAR_STRENGTH_USELESS;
				break;
			default: 
				m_iArmorStrength = BARNEY_KEVLAR_STRENGTH_DEFAULT; 
				break;
		}
	}
}

void CBarneyExtended::SetupHelmet()
{
	//Einstein head can't fit the helmet, trying to make it work looked terrible
	if ( m_iHead == BARNEY_HEADBODY_SCIWHT && !(m_iType == BARNEY_TYPE_HEV) )
	{
		m_iHelmet = BARNEY_HELMET_OFF;
	}

	if (m_iHelmet == BARNEY_HELMET_MATCHOUTFIT)
	{
		switch (m_iType)
		{
		case BARNEY_TYPE_CIVILIAN:
		case BARNEY_TYPE_CLEANSUIT:
		case BARNEY_TYPE_GMAN:
		case BARNEY_TYPE_NOVEST:
		case BARNEY_TYPE_SCIENTIST:
			m_iHelmet = BARNEY_HELMET_OFF;
			break;

		case BARNEY_TYPE_OTIS_SHORT:
		case BARNEY_TYPE_OTIS_LONG:
		case BARNEY_TYPE_REGULAR:
			m_iHelmet = BARNEY_HELMET_ON; 
			break;

		case BARNEY_TYPE_HEV: //hevs are random on if they get a helmet or not
		default: //no clue what we are, randomize
			m_iHelmet = BARNEY_HELMET_RANDOM; 
			break;
		}
	}

	if (m_iHelmet == BARNEY_HELMET_RANDOM)
	{
		m_iHelmet = RANDOM_LONG(BARNEY_HELMET_ON, BARNEY_HELMET_OFF);
	}

	if (m_iHelmet == BARNEY_HELMET_MATCH)
	{
		switch (m_iHead)
		{
			case BARNEY_HEADBODY_BARNEY:
			case BARNEY_HEADBODY_OTIS:
			case BARNEY_HEADBODY_OTIS2:
			case BARNEY_HEADBODY_OTISBLK:
			case BARNEY_HEADBODY_OTISMP: 
				m_iHelmet = BARNEY_HELMET_ON; 
				break;

			default: m_iHelmet = BARNEY_HELMET_OFF; break;
		}
	}

	int helmbod;

	if (m_iHelmet == BARNEY_HELMET_ON && m_iType == BARNEY_TYPE_HEV)
	{
		helmbod = BARNEY_HELMETBODY_ON;
		SetBodygroup(BARNEY_BODYGROUP_HEAD, BARNEY_HEADBODY_NONE);
	}

	if (m_iHelmet == BARNEY_HELMET_ON)
	{
		if (m_iHead == BARNEY_HEADBODY_SCIBLK || m_iHead == BARNEY_HEADBODY_SCINERD)
			helmbod = BARNEY_HELMETBODY_SCION;
		else
			helmbod = BARNEY_HELMETBODY_ON;
	}
	else
	{
		helmbod = BARNEY_HELMETBODY_OFF;
	}

	SetBodygroup(BARNEY_BODYGROUP_HELMET, helmbod);

	//Helmet clips with Cleansuit, don't render it
	if (m_iType == BARNEY_TYPE_CLEANSUIT)
	{
		SetBodygroup(BARNEY_BODYGROUP_HELMET, BARNEY_HELMETBODY_OFF);
	}
}

void CBarneyExtended::SetupWeapons()
{
	if (pev->weapons == BARNEY_WEAPON_RAND)
		pev->weapons = RANDOM_LONG(BARNEY_WEAPON_GLOCK, BARNEY_WEAPON_SHOTGUN);

	switch (pev->weapons)
	{
		case BARNEY_WEAPON_EAGLE:
			m_bCanHolster = true;
			m_iMagSize = EAGLE_MAX_CLIP;

			m_bBodyGroup_Gun = BARNEY_WEAPONBODY_EAGLE;
			m_bBodyGroup_Holster = BARNEY_WEAPONBODY_EAGLE_HOLSTER;
			
			if (gpgs && gpgs->m_iszEagleSound)
				m_chzGunSound = (gpgs->m_iszEagleSound);
			else
				m_chzGunSound = ALLOC_STRING("weapons/desert_eagle_fire.wav");

			m_chzWeaponClassname = ALLOC_STRING("weapon_eagle");
			break;

		case BARNEY_WEAPON_357:
			m_bCanHolster = true;
			m_iMagSize = PYTHON_MAX_CLIP;

			m_bBodyGroup_Gun = BARNEY_WEAPONBODY_357;
			m_bBodyGroup_Holster = BARNEY_WEAPONBODY_357_HOLSTER;
			
			if (gpgs && gpgs->m_isz357Sound)
				m_chzGunSound = (gpgs->m_isz357Sound);
			else
				m_chzGunSound = ALLOC_STRING("weapons/357_shot1.wav");

			m_chzWeaponClassname = ALLOC_STRING("weapon_357");
			break;

		case BARNEY_WEAPON_MP5:
			m_bCanHolster = false;
			m_iMagSize = MP5_MAX_CLIP;

			m_bBodyGroup_Gun = BARNEY_WEAPONBODY_MP5;
			m_bBodyGroup_Holster = BARNEY_WEAPONBODY_MP5;

			if (gpgs && gpgs->m_iszMP5Sound)
				m_chzGunSound = (gpgs->m_iszMP5Sound);
			else
				m_chzGunSound = ALLOC_STRING("weapons/hks1.wav");

			m_chzWeaponClassname = ALLOC_STRING("weapon_9mmAR");
			break;

		case BARNEY_WEAPON_SHOTGUN:
			m_bCanHolster = false;
			m_iMagSize = SHOTGUN_MAX_CLIP;

			m_bBodyGroup_Gun = BARNEY_WEAPONBODY_SHOTGUN;
			m_bBodyGroup_Holster = BARNEY_WEAPONBODY_SHOTGUN;

			if (gpgs && gpgs->m_iszShotgunSoundPri)
				m_chzGunSound = (gpgs->m_iszShotgunSoundPri);
			else
				m_chzGunSound = ALLOC_STRING("weapons/sbarrel1.wav");

			m_chzWeaponClassname = ALLOC_STRING("weapon_shotgun");
			break;

		case BARNEY_WEAPON_GLOCK:
		default:
			m_bCanHolster = true;
			m_iMagSize = GLOCK_MAX_CLIP;

			m_bBodyGroup_Gun = BARNEY_WEAPONBODY_GLOCK;
			m_bBodyGroup_Holster = BARNEY_WEAPONBODY_GLOCK_HOLSTER;

			if (gpgs && gpgs->m_iszGlockSound)
				m_chzGunSound = (gpgs->m_iszGlockSound);
			else
				m_chzGunSound = ALLOC_STRING("weapons/pl_gun3.wav");

			m_chzWeaponClassname = ALLOC_STRING("weapon_9mmhandgun");
			break;
	}

	PRECACHE_SOUND(STRING(m_chzGunSound));
}

void CBarneyExtended::Killed( entvars_t *pevAttacker, int iGib )
{
	if (!(pev->spawnflags & SF_MONSTER_NO_WPN_DROP))
	{
		if (!m_bDroppedAlready)
		{
			// drop the gun!
			Vector vecGunPos;
			Vector vecGunAngles;

			m_bDroppedAlready = true;

			SetBodygroup(BARNEY_BODYGROUP_GUN, BARNEY_WEAPONBODY_NONE);

			GetAttachment(0, vecGunPos, vecGunAngles);

			CBaseEntity* pGun;
			pGun = DropItem(STRING(m_chzWeaponClassname), vecGunPos, vecGunAngles);
		}
	}

	SetUse( NULL );	
	CTalkMonster::Killed( pevAttacker, iGib );
}

void CBarneyExtended::HandleAnimEvent(MonsterEvent_t* pEvent)
{
	switch (pEvent->event)
	{
	case BARNEY_AE_SHOOT:
		BarneyFirePistol();
		break;

	case BARNEY_AE_DRAW:
		// barney's bodygroup switches here so he can pull gun from holster
		SetBodygroup(BARNEY_BODYGROUP_GUN, m_bBodyGroup_Gun);

		m_fGunDrawn = TRUE;
		break;

	case BARNEY_AE_HOLSTER:

		if (!m_bCanHolster)
		{
			// don't change group, don't put gun away
			SetBodygroup(BARNEY_BODYGROUP_GUN, m_bBodyGroup_Gun);
			m_fGunDrawn = TRUE;
		}
		else
		{
			// change bodygroup to replace gun in holster
			SetBodygroup(BARNEY_BODYGROUP_GUN, m_bBodyGroup_Holster);
			m_fGunDrawn = FALSE;
		}

		break;

	default:
		CTalkMonster::HandleAnimEvent(pEvent);
	}
}

void CBarneyExtended::BarneyFirePistol()
{
	//if we got here and we still don't have our gun out, do this to fix it
	SetBodygroup(BARNEY_BODYGROUP_GUN, m_bBodyGroup_Gun);

	Vector vecShootOrigin;

	UTIL_MakeVectors(pev->angles);
	vecShootOrigin = pev->origin + Vector(0, 0, 55);
	Vector vecShootDir = ShootAtEnemy(vecShootOrigin);

	Vector angDir = UTIL_VecToAngles(vecShootDir);
	SetBlending(0, angDir.x);
	pev->effects = EF_MUZZLEFLASH;

	int bulletamt = 1;
	int bullettype = BULLET_MONSTER_9MM;
	int bullettypeplayer = BULLET_PLAYER_9MM;
	Vector cone = VECTOR_CONE_2DEGREES;

	switch (pev->weapons)
	{
		case BARNEY_WEAPON_EAGLE: bullettype = BULLET_MONSTER_EAGLE; bullettypeplayer = BULLET_PLAYER_EAGLE; break;
		case BARNEY_WEAPON_357: bullettype = BULLET_MONSTER_357; bullettypeplayer = BULLET_PLAYER_357; break;
		case BARNEY_WEAPON_MP5: bullettype = BULLET_MONSTER_MP5; bullettypeplayer = BULLET_PLAYER_MP5; cone = VECTOR_CONE_10DEGREES; break;
		case BARNEY_WEAPON_SHOTGUN: bullettype = BULLET_MONSTER_BUCKSHOT; bullettypeplayer = BULLET_PLAYER_BUCKSHOT; cone = VECTOR_CONE_15DEGREES; bulletamt = 6; break;
	}

	if (m_bPlayerBullets)
		FireBullets(bulletamt, vecShootOrigin, vecShootDir, cone, 1024, bullettypeplayer);
	else
		FireBullets(bulletamt, vecShootOrigin, vecShootDir, cone, 1024, bullettype);

	int pitchShift = RANDOM_LONG(0, 20);

	// Only shift about half the time
	if (pitchShift > 10)
		pitchShift = 0;
	else
		pitchShift -= 5;

	EMIT_SOUND_DYN(ENT(pev), CHAN_WEAPON, STRING(m_chzGunSound), 1, ATTN_NORM, 0, 100 + pitchShift);

	CSoundEnt::InsertSound(bits_SOUND_COMBAT, pev->origin, 384, 0.3);

	// UNDONE: Reload?
	//m_iMagSize--;// take away a bullet!

	// Teh_Freak: World Lighting!
	MESSAGE_BEGIN(MSG_BROADCAST, SVC_TEMPENTITY);
	WRITE_BYTE(TE_DLIGHT);
	WRITE_COORD(vecShootOrigin.x); // origin
	WRITE_COORD(vecShootOrigin.y);
	WRITE_COORD(vecShootOrigin.z);
	WRITE_BYTE(16);     // radius
	WRITE_BYTE(255);     // R
	WRITE_BYTE(255);     // G
	WRITE_BYTE(128);     // B
	WRITE_BYTE(0);     // life * 10
	WRITE_BYTE(0); // decay
	MESSAGE_END();
	// Teh_Freak: World Lighting!

}

void CBarneyExtended::SetActivity(Activity newActivity)
{
	int	iSequence = ACTIVITY_NOT_AVAILABLE;
	
	if (newActivity == ACT_IDLE && IsTalking() )
		newActivity = ACT_SIGNAL3;
	
	if ( newActivity == ACT_SIGNAL3 && (LookupActivity ( ACT_SIGNAL3 ) == ACTIVITY_NOT_AVAILABLE))
		newActivity = ACT_IDLE;

	if (newActivity == ACT_RANGE_ATTACK1)
	{
		switch (pev->weapons)
		{ 
		case BARNEY_WEAPON_EAGLE: 
		case BARNEY_WEAPON_357: 
			iSequence = LookupSequence("shootgun_heavy"); 
			break;
		case BARNEY_WEAPON_MP5: 
			iSequence = LookupSequence("shootgun_mp5"); 
			break;
		case BARNEY_WEAPON_SHOTGUN: 
			iSequence = LookupSequence("shootgun_shotgun"); 
			break;
		default: 
			int i = RANDOM_LONG(0, 1);

			if (i)
				iSequence = LookupSequence("shootgun"); 
			else
				iSequence = LookupSequence("shootgun2");

			break;
		}
	}
	else
	{
		iSequence = LookupActivity(newActivity);
	}

	m_Activity = newActivity; // Go ahead and set this so it doesn't keep trying when the anim is not present

	// Set to the desired anim, or default anim if the desired is not present
	if ( iSequence > ACTIVITY_NOT_AVAILABLE )
	{
		if ( pev->sequence != iSequence || !m_fSequenceLoops )
		{
			pev->frame = 0;
		}

		pev->sequence		= iSequence;	// Set to the reset anim (if it's there)
		ResetSequenceInfo( );
		SetYawSpeed();
	}
	else
	{
		// Not available try to get default anim
		ALERT ( at_debug, "%s has no sequence for act:%d\n", STRING(pev->classname), newActivity );
		pev->sequence		= 0;	// Set to the reset anim (if it's there)
	}
}

void CBarneyExtended::TraceAttack( entvars_t *pevAttacker, float flDamage, Vector vecDir, TraceResult *ptr, int bitsDamageType)
{
	switch( ptr->iHitgroup)
	{
	case HITGROUP_CHEST:
	case HITGROUP_STOMACH:
		if (bitsDamageType & (DMG_BULLET | DMG_SLASH | DMG_BLAST))
		{
			switch (m_iArmorStrength)
			{
				case BARNEY_KEVLAR_STRENGTH_SUPER: flDamage = 0; break;
				case BARNEY_KEVLAR_STRENGTH_USELESS: break;
				default: flDamage = flDamage / 2; break;
			}

			if (!(m_iArmorStrength == BARNEY_KEVLAR_STRENGTH_USELESS))
				EMIT_SOUND_DYN(edict(), CHAN_ITEM, "player/bhit_kevlar-1.wav", 0.6, ATTN_NORM, 0, 100);
		}
		break;

	case 10:
		if (bitsDamageType & (DMG_BULLET | DMG_SLASH | DMG_CLUB))
		{
			if (!(m_iHelmet == BARNEY_HELMET_OFF))
			{
				switch (m_iHelmetStrength)
				{
				case BARNEY_HELMET_STRENGTH_STRONGER: flDamage = flDamage / 2; break;
				case BARNEY_HELMET_STRENGTH_SUPER: flDamage = 0; break;
				case BARNEY_HELMET_STRENGTH_USELESS: break;
				default: flDamage -= 20; break;
				}

				if (flDamage <= 0)
				{
					UTIL_Ricochet(ptr->vecEndPos, 1.0);
					flDamage = 0.01;
				}

				if (!(m_iHelmetStrength == BARNEY_HELMET_STRENGTH_USELESS))
					EMIT_SOUND_DYN(edict(), CHAN_ITEM, "player/bhit_helmet-1.wav", 0.6, ATTN_NORM, 0, 100);
			}
		}
		break;

	case HITGROUP_HEAD:
		if (bitsDamageType & (DMG_BULLET | DMG_SLASH | DMG_CLUB))
		{
			if (m_iType == BARNEY_TYPE_HEV || m_iType == BARNEY_TYPE_CLEANSUIT)
			{
				if (!(m_iHelmet == BARNEY_HELMET_OFF))
				{
					switch (m_iHelmetStrength)
					{
					case BARNEY_HELMET_STRENGTH_STRONGER: flDamage = flDamage / 2; break;
					case BARNEY_HELMET_STRENGTH_SUPER: flDamage = 0; break;
					case BARNEY_HELMET_STRENGTH_USELESS: break;
					default: flDamage -= 20; break;
					}

					if (flDamage <= 0)
					{
						UTIL_Ricochet(ptr->vecEndPos, 1.0);
						flDamage = 0.01;
					}

					if (!(m_iHelmetStrength == BARNEY_HELMET_STRENGTH_USELESS))
						EMIT_SOUND_DYN(edict(), CHAN_ITEM, "player/bhit_helmet-1.wav", 0.6, ATTN_NORM, 0, 100);
				}
			}
		}
		break;
	}

	CTalkMonster::TraceAttack( pevAttacker, flDamage, vecDir, ptr, bitsDamageType );
}

void CBarneyExtended::PainSound()
{
	if (gpGlobals->time < m_painTime)
		return;

	if (m_bMonstBeQuiet)
		return;

	m_painTime = gpGlobals->time + RANDOM_FLOAT(0.5, 0.75);
	
	if (m_iVoicePreset == BARNEY_VOICE_SCIENTIST)
	{
		switch (RANDOM_LONG(0, 4))
		{
			case 0: EMIT_SOUND_DYN(ENT(pev), CHAN_VOICE, "scientist/sci_pain1.wav", 1, ATTN_NORM, 0, GetVoicePitch()); break;
			case 1: EMIT_SOUND_DYN(ENT(pev), CHAN_VOICE, "scientist/sci_pain2.wav", 1, ATTN_NORM, 0, GetVoicePitch()); break;
			case 2: EMIT_SOUND_DYN(ENT(pev), CHAN_VOICE, "scientist/sci_pain3.wav", 1, ATTN_NORM, 0, GetVoicePitch()); break;
			case 3: EMIT_SOUND_DYN(ENT(pev), CHAN_VOICE, "scientist/sci_pain4.wav", 1, ATTN_NORM, 0, GetVoicePitch()); break;
			case 4: EMIT_SOUND_DYN(ENT(pev), CHAN_VOICE, "scientist/sci_pain5.wav", 1, ATTN_NORM, 0, GetVoicePitch()); break;
		}
	}
	else
	{
		switch (RANDOM_LONG(0, 2))
		{
			case 0: EMIT_SOUND_DYN(ENT(pev), CHAN_VOICE, "barney/ba_pain1.wav", 1, ATTN_NORM, 0, GetVoicePitch()); break;
			case 1: EMIT_SOUND_DYN(ENT(pev), CHAN_VOICE, "barney/ba_pain2.wav", 1, ATTN_NORM, 0, GetVoicePitch()); break;
			case 2: EMIT_SOUND_DYN(ENT(pev), CHAN_VOICE, "barney/ba_pain3.wav", 1, ATTN_NORM, 0, GetVoicePitch()); break;
		}
	}
}

void CBarneyExtended::DeathSound()
{
	if (m_bMonstBeQuiet)
		return;

	if (m_iVoicePreset == BARNEY_VOICE_SCIENTIST)
	{
		switch (RANDOM_LONG(0, 4))
		{
			case 0: EMIT_SOUND_DYN(ENT(pev), CHAN_VOICE, "scientist/sci_pain1.wav", 1, ATTN_NORM, 0, GetVoicePitch()); break;
			case 1: EMIT_SOUND_DYN(ENT(pev), CHAN_VOICE, "scientist/sci_pain2.wav", 1, ATTN_NORM, 0, GetVoicePitch()); break;
			case 2: EMIT_SOUND_DYN(ENT(pev), CHAN_VOICE, "scientist/sci_pain3.wav", 1, ATTN_NORM, 0, GetVoicePitch()); break;
			case 3: EMIT_SOUND_DYN(ENT(pev), CHAN_VOICE, "scientist/sci_pain4.wav", 1, ATTN_NORM, 0, GetVoicePitch()); break;
			case 4: EMIT_SOUND_DYN(ENT(pev), CHAN_VOICE, "scientist/sci_pain5.wav", 1, ATTN_NORM, 0, GetVoicePitch()); break;
		}
	}
	else
	{
		switch (RANDOM_LONG(0, 2))
		{
			case 0: EMIT_SOUND_DYN(ENT(pev), CHAN_VOICE, "barney/ba_die1.wav", 1, ATTN_NORM, 0, GetVoicePitch()); break;
			case 1: EMIT_SOUND_DYN(ENT(pev), CHAN_VOICE, "barney/ba_die2.wav", 1, ATTN_NORM, 0, GetVoicePitch()); break;
			case 2: EMIT_SOUND_DYN(ENT(pev), CHAN_VOICE, "barney/ba_die3.wav", 1, ATTN_NORM, 0, GetVoicePitch()); break;
		}
	}
}
