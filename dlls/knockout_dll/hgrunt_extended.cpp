
#include	"extdll.h"
#include	"plane.h"
#include	"util.h"
#include	"cbase.h"
#include	"monsters.h"
#include	"schedule.h"
#include	"defaultai.h"
#include	"animation.h"
#include	"squadmonster.h"
#include	"weapons.h"
#include	"talkmonster.h"
#include	"COFAllyMonster.h"
#include	"COFSquadTalkMonster.h"
#include	"soundent.h"
#include	"effects.h"
#include	"customentity.h"
#include	"scripted.h" //LRC
#include	"knockout_dll\game_settings.h"
#include	"hgrunt_ally.h"
#include	"hornet.h"
#include	"weapons/CSpore.h"
#include	"weapons/CShockBeam.h"
#include	"weapons/CPenguin.h"
#include	"explode.h"

extern DLL_GLOBAL int		g_iSkillLevel;



enum HGruntBodyHead
{
	HGRUNT_HEADRAND_MATCHWEAPON = -6,
	HGRUNT_HEADRAND_MATCHTYPE = -5,
	HGRUNT_HEADRAND_CLASSIC = -4,
	HGRUNT_HEADRAND_ASSASSIN = -3,
	HGRUNT_HEADRAND_REGULAR = -2,
	HGRUNT_HEADRAND_ALL = -1,
	HGRUNT_HEADBODY_GASMASK = 0,
	HGRUNT_HEADBODY_COMMANDER,
	HGRUNT_HEADBODY_MASKED,
	HGRUNT_HEADBODY_TOWER,
	HGRUNT_HEADBODY_MP,
	HGRUNT_HEADBODY_MAJOR,
	HGRUNT_HEADBODY_MEDIC,
	HGRUNT_HEADBODY_TORCH,
	HGRUNT_HEADBODY_CLASSICGASMASK,
	HGRUNT_HEADBODY_CLASSICCOMMANDER,
	HGRUNT_HEADBODY_CLASSICMASKED,
	HGRUNT_HEADBODY_CLASSICTOWER,
	HGRUNT_HEADBODY_ASSASSIN,
	HGRUNT_HEADBODY_NIGHTVISION,
};

enum HGruntBodyTorso
{
	HGRUNT_TORSORAND_MATCHWEAPON = -4,
	HGRUNT_TORSORAND_MATCHTYPE = -3,
	HGRUNT_TORSORAND_REGULAR = -2,
	HGRUNT_TORSORAND_ALL = -1,
	HGRUNT_TORSOBODY_REGULAR = 0,
	HGRUNT_TORSOBODY_MACHINEGUN,
	HGRUNT_TORSOBODY_NOPACK,
	HGRUNT_TORSOBODY_SHOTGUNNER,
	HGRUNT_TORSOBODY_MEDIC,
	HGRUNT_TORSOBODY_TORCH,
	HGRUNT_TORSOBODY_MP,
	HGRUNT_TORSOBODY_CLASSIC,
	HGRUNT_TORSOBODY_ASSASSIN,
};

enum HGruntWeaponBody
{
	HGRUNT_WEAPONBODY_MP5 = 0,
	HGRUNT_WEAPONBODY_SHOTGUN,
	HGRUNT_WEAPONBODY_M249,
	HGRUNT_WEAPONBODY_SNIPER,
	HGRUNT_WEAPONBODY_GLOCK,
	HGRUNT_WEAPONBODY_EAGLE,
	HGRUNT_WEAPONBODY_PYTHON,
	HGRUNT_WEAPONBODY_HIVEHAND,
	HGRUNT_WEAPONBODY_SHOCKRIFLE,
	HGRUNT_WEAPONBODY_SPORELAUNCHER,
	HGRUNT_WEAPONBODY_NONE,
	HGRUNT_WEAPONBODY_GLOCK_HOLSTERED,
	HGRUNT_WEAPONBODY_EAGLE_HOLSTERED,
	HGRUNT_WEAPONBODY_PYTHON_HOLSTERED,
};

enum HGruntPropBody
{
	HGRUNT_PROPBODY_OFF = 0,
	HGRUNT_PROPBODY_ON,
};


enum HGruntBodygroups
{
	HGRUNT_BODYGROUP_HEAD = 0,
	HGRUNT_BODYGROUP_WEAPONS,
	HGRUNT_BODYGROUP_PROP,
};

enum HGruntWeapons
{
	HGRUNT_WEAPONRAND_ALIEN = -3,
	HGRUNT_WEAPONRAND_GUN = -2,
	HGRUNT_WEAPONRAND_ALL = -1,
	HGRUNT_WEAPON_MP5 = 0,
	HGRUNT_WEAPON_SHOTGUN,
	HGRUNT_WEAPON_M249,
	HGRUNT_WEAPON_SNIPER,
	HGRUNT_WEAPON_GLOCK,
	HGRUNT_WEAPON_EAGLE,
	HGRUNT_WEAPON_PYTHON,
	HGRUNT_WEAPON_HIVEHAND,
	HGRUNT_WEAPON_SHOCKROACH,
	HGRUNT_WEAPON_SPORELAUNCHER,
	HGRUNT_WEAPON_NONE,
};

enum HGruntGrenades
{
	HGRUNT_GRENADE_NONE = 0,
	HGRUNT_GRENADE_LAUNCHER,
	HGRUNT_GRENADE_FRAG,
	HGRUNT_GRENADE_SPORE,
	HGRUNT_GRENADE_SNARK,
	HGRUNT_GRENADE_PENGUIN,

	//ALWAYS LAST, ADD MORE ABOVE THIS
	HGRUNT_GRENADE_SPORE_LAUNCHER,
};

enum HGruntTypes
{
	HGRUNT_TYPE_REGULAR = 0,
	HGRUNT_TYPE_MEDIC,
	HGRUNT_TYPE_TORCH,
	HGRUNT_TYPE_ASSASSIN,
	HGRUNT_TYPE_CLASSIC,
};

enum HGruntVoices
{
	HGRUNT_VOICE_MATCH = -1,
	HGRUNT_VOICE_REGULAR = 0,
	HGRUNT_VOICE_CLASSIC,
	HGRUNT_VOICE_ASSASSIN,
};


//=========================================================
// monster-specific schedule types
//=========================================================
enum
{
	SCHED_MEDIC_ALLY_HEAL_ALLY = SCHED_GRUNT_ELOF_FAIL + 1,
};


#define MEDIC_AE_HOLSTER_GUN	15
#define MEDIC_AE_EQUIP_NEEDLE	16
#define MEDIC_AE_HOLSTER_NEEDLE	17
#define MEDIC_AE_EQUIP_GUN		18

#define TORCH_AE_HOLSTER_TORCH		19 //17
#define TORCH_AE_HOLSTER_GUN		20 //18 
#define TORCH_AE_HOLSTER_BOTH		21 //19
#define TORCH_AE_ACTIVATE_TORCH		22 //20
#define TORCH_AE_DEACTIVATE_TORCH	23 //21

#define TORCH_BEAM_SPRITE			"sprites/xbeam3.spr"

#define bits_COND_GRUNT_NOFIRE	( bits_COND_SPECIAL1 )

class CHGruntExtended : public CHGruntAlly
{
public:
	void Spawn() override;
	void Precache() override;
	void HandleAnimEvent(MonsterEvent_t* pEvent) override;
	void SetActivity(Activity NewActivity) override;
	void KeyValue(KeyValueData* pkvd) override;
	void DeathSound() override;
	void PainSound() override;
	void GibMonster() override;
	void Shoot() override;
	void Shotgun() override;
	void ShootSaw() override;
	int Classify() override;
	BOOL CheckRangeAttack1(float flDot, float flDist) override;
	BOOL CheckRangeAttack2(float flDot, float flDist) override;
	Schedule_t* GetSchedule() override;
	Schedule_t* GetScheduleOfType(int Type) override;
	int TakeDamage(entvars_t* pevInflictor, entvars_t* pevAttacker, float flDamage, int bitsDamageType) override;
	void Killed(entvars_t* pevAttacker, int iGib) override;
	void MonsterThink() override;
	void StartTask(Task_t* pTask) override;
	void RunTask(Task_t* pTask) override;
	int ObjectCaps() override;
	BOOL HealMe(COFSquadTalkMonster* pTarget) override;
	void HealOff();
	void EXPORT HealerUse(CBaseEntity* pActivator, CBaseEntity* pCaller, USE_TYPE useType, float value);
	void HealerActivate(CBaseMonster* pTarget);
	int	Save(CSave& save) override;
	int Restore(CRestore& restore) override;
	static TYPEDESCRIPTION m_SaveData[];
	CUSTOM_SCHEDULES;

	void SetupWeapons();
	void SetupHead();
	void SetupOutfit();
	void SetupVoice();

	void ShootSniper();
	void ShootPistol();
	void ShootHivehand();
	void ShootShockroach();

	void ThrowGrenade();
	void ThrowSpore();
	void ThrowSnark();
	void ThrowPenguin();
	void LaunchGrenade();
	void LaunchSpore();

	bool m_bIsFriendly;
	int m_iGruntType;
	int m_iGrenadeType;
	bool m_bUsingPistol;
	string_t m_chzReloadSound;
	string_t m_chzReloadAnimation;
	int m_iSkintone;
	const char* m_chzOutfit;
	int m_iHead;
	int m_iNewWeapons;
	int m_bUsingCustomModel;

	bool m_bPlayerBullets;
	int m_iVoicePreset;
	bool m_bCanHolster;
	int m_bBodyGroup_Gun;
	int m_bBodyGroup_Holster;
	string_t m_chzGunSound;
	string_t m_chzWeaponClassname;
	float m_flLastShot;

	BOOL m_fGunHolstered;
	BOOL m_fTorchHolstered;
	BOOL m_fTorchActive;
	CBeam* m_pTorchBeam;

	float m_flLastUseTime;
	int m_iHealCharge;
	BOOL m_fUseHealing;
	BOOL m_fHealing;
	BOOL m_fHypoHolstered;
	BOOL m_fHealActive;
	BOOL m_fFollowChecking;
	BOOL m_fFollowChecked;
	float m_flFollowCheckTime;
	BOOL m_fHealAudioPlaying;
	float m_flLastRejectAudio;

	int iHivehandMuzzleFlash;
	int iShockroachMuzzleFlash;
	string_t m_stRoachmodel;
	bool m_bDropLiveRoach;

	bool m_bDroppedAlready;
};

TYPEDESCRIPTION	CHGruntExtended::m_SaveData[] =
{
	DEFINE_FIELD( CHGruntExtended, m_flLastShot, FIELD_TIME ),
	DEFINE_FIELD( CHGruntExtended, m_fGunHolstered, FIELD_BOOLEAN ),
	DEFINE_FIELD( CHGruntExtended, m_fTorchHolstered, FIELD_BOOLEAN ),
	DEFINE_FIELD( CHGruntExtended, m_fTorchActive, FIELD_BOOLEAN ),
	
	DEFINE_FIELD( CHGruntExtended, m_flFollowCheckTime, FIELD_FLOAT ),
	DEFINE_FIELD( CHGruntExtended, m_flLastRejectAudio, FIELD_FLOAT ),
	DEFINE_FIELD( CHGruntExtended, m_fFollowChecking, FIELD_BOOLEAN ),
	DEFINE_FIELD( CHGruntExtended, m_fFollowChecked, FIELD_BOOLEAN ),
	DEFINE_FIELD( CHGruntExtended, m_iHealCharge, FIELD_INTEGER ),
	DEFINE_FIELD( CHGruntExtended, m_fUseHealing, FIELD_BOOLEAN ),
	DEFINE_FIELD( CHGruntExtended, m_fHealing, FIELD_BOOLEAN ),
	DEFINE_FIELD( CHGruntExtended, m_flLastUseTime, FIELD_TIME ),
	DEFINE_FIELD( CHGruntExtended, m_fHypoHolstered, FIELD_BOOLEAN ),
	DEFINE_FIELD( CHGruntExtended, m_fHealActive, FIELD_BOOLEAN ),
	
	DEFINE_FIELD( CHGruntExtended, m_bIsFriendly, FIELD_BOOLEAN ),
	DEFINE_FIELD( CHGruntExtended, m_iGruntType, FIELD_INTEGER ),
	DEFINE_FIELD( CHGruntExtended, m_iGrenadeType, FIELD_INTEGER ),
	DEFINE_FIELD( CHGruntExtended, m_bUsingPistol, FIELD_BOOLEAN ),
	DEFINE_FIELD( CHGruntExtended, m_iHead, FIELD_INTEGER ),
	DEFINE_FIELD( CHGruntExtended, m_iNewWeapons, FIELD_INTEGER ),
	DEFINE_FIELD( CHGruntExtended, m_bUsingCustomModel, FIELD_BOOLEAN ),
	
	DEFINE_FIELD( CHGruntExtended, m_bPlayerBullets, FIELD_BOOLEAN ),
	DEFINE_FIELD( CHGruntExtended, m_iVoicePreset, FIELD_INTEGER ),
	DEFINE_FIELD( CHGruntExtended, m_bCanHolster, FIELD_BOOLEAN ),
	DEFINE_FIELD( CHGruntExtended, m_bBodyGroup_Gun, FIELD_INTEGER ),
	DEFINE_FIELD( CHGruntExtended, m_bBodyGroup_Holster, FIELD_INTEGER ),
	
	DEFINE_FIELD( CHGruntExtended, m_chzGunSound, FIELD_STRING ),
	DEFINE_FIELD( CHGruntExtended, m_chzWeaponClassname, FIELD_STRING ),
	DEFINE_FIELD( CHGruntExtended, m_chzReloadAnimation, FIELD_STRING ),
	DEFINE_FIELD( CHGruntExtended, m_chzReloadSound, FIELD_STRING ),
};

IMPLEMENT_SAVERESTORE( CHGruntExtended, CHGruntAlly );

void CHGruntExtended::KeyValue(KeyValueData* pkvd)
{
	if (FStrEq(pkvd->szKeyName, "FriendToSci"))
	{
		m_bFriendlyToScientists = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "PlayerBullets"))
	{
		m_bPlayerBullets = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "VoicePreset"))
	{
		m_iVoicePreset = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Skintone"))
	{
		m_iSkintone = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "GruntType"))
	{
		m_iGruntType = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "NewWeapons"))
	{
		m_iNewWeapons = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "UsingCustomModel"))
	{
		m_bUsingCustomModel = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "roachmodel"))
	{
		m_stRoachmodel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "roachdrop"))
	{
		m_bDropLiveRoach = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else
		COFSquadTalkMonster::KeyValue(pkvd);
}

LINK_ENTITY_TO_CLASS( monster_human_grunt_extended, CHGruntExtended );
LINK_ENTITY_TO_CLASS( monster_human_grunt_ally_extended, CHGruntExtended );



Task_t	tlMedicAllyNewHealTargetExtended[] =
{
	{ TASK_SET_FAIL_SCHEDULE, SCHED_TARGET_CHASE },
	{ TASK_MOVE_TO_TARGET_RANGE, 50 },
	{ TASK_FACE_IDEAL, 0 },
	{ TASK_GRUNT_SPEAK_SENTENCE, 0 },
};

Schedule_t	slMedicAllyNewHealTargetExtended[] =
{
	{
		tlMedicAllyNewHealTargetExtended,
		ARRAYSIZE( tlMedicAllyNewHealTargetExtended),
		0,
		0,
		"Draw Needle"
	},
};

Task_t	tlMedicAllyDrawNeedleExtended[] =
{
	{ TASK_STOP_MOVING, 0 },
	{ TASK_PLAY_SEQUENCE_FACE_TARGET, ACT_ARM },
	{ TASK_SET_FAIL_SCHEDULE, SCHED_TARGET_CHASE },
	{ TASK_MOVE_TO_TARGET_RANGE, 50 },
	{ TASK_FACE_IDEAL, 0, },
	{ TASK_GRUNT_SPEAK_SENTENCE, 0 }
};

Schedule_t	slMedicAllyDrawNeedleExtended[] =
{
	{
		tlMedicAllyDrawNeedleExtended,
		ARRAYSIZE( tlMedicAllyDrawNeedleExtended),
		0,
		0,
		"Draw Needle"
	},
};

Task_t	tlMedicAllyDrawGunExtended[] =
{
	{ TASK_PLAY_SEQUENCE, ACT_DISARM },
	{ TASK_WAIT_FOR_MOVEMENT, 0 },
};

Schedule_t	slMedicAllyDrawGunExtended[] =
{
	{
		tlMedicAllyDrawGunExtended,
		ARRAYSIZE( tlMedicAllyDrawGunExtended),
		0,
		0,
		"Draw Gun"
	},
};

Task_t	tlMedicAllyHealTargetExtended[] =
{
	{ TASK_MELEE_ATTACK2, 0 },
	{ TASK_WAIT, 0.2f },
	{ TASK_TLK_HEADRESET, 0 },
};

Schedule_t	slMedicAllyHealTargetExtended[] =
{
	{
		tlMedicAllyHealTargetExtended,
		ARRAYSIZE( tlMedicAllyHealTargetExtended),
		0,
		0,
		"Medic Ally Heal Target"
	},
};

DEFINE_CUSTOM_SCHEDULES( CHGruntExtended )
{
	slMedicAllyNewHealTargetExtended,
	slMedicAllyDrawNeedleExtended,
	slMedicAllyDrawGunExtended,
	slMedicAllyHealTargetExtended,
};

IMPLEMENT_CUSTOM_SCHEDULES( CHGruntExtended, CHGruntAlly );


int CHGruntExtended::ObjectCaps()
{
	//Allow healing the player by continuously using
	if (m_iGruntType == HGRUNT_TYPE_MEDIC)
		return FCAP_ACROSS_TRANSITION | FCAP_CONTINUOUS_USE;
	else
		return FCAP_ACROSS_TRANSITION | FCAP_IMPULSE_USE;
}

//=========================================================
// Precache - precaches all resources this monster needs
//=========================================================
void CHGruntExtended:: Precache()
{
	//ALERT(at_console, "(PRECACHE) HGRUNT MODEL: %s\n", STRING(pev->model));

	PRECACHE_MODEL(STRING(pev->model));

	PRECACHE_SOUND("fgrunt/death1.wav");
	PRECACHE_SOUND("fgrunt/death2.wav");
	PRECACHE_SOUND("fgrunt/death3.wav");
	PRECACHE_SOUND("fgrunt/death4.wav");
	PRECACHE_SOUND("fgrunt/death5.wav");
	PRECACHE_SOUND("fgrunt/death6.wav");

	PRECACHE_SOUND("fgrunt/pain1.wav");
	PRECACHE_SOUND("fgrunt/pain2.wav");
	PRECACHE_SOUND("fgrunt/pain3.wav");
	PRECACHE_SOUND("fgrunt/pain4.wav");
	PRECACHE_SOUND("fgrunt/pain5.wav");
	PRECACHE_SOUND("fgrunt/pain6.wav");

	PRECACHE_SOUND("hgrunt/gr_die1.wav");
	PRECACHE_SOUND("hgrunt/gr_die2.wav");
	PRECACHE_SOUND("hgrunt/gr_die3.wav");

	PRECACHE_SOUND("hgrunt/gr_pain1.wav");
	PRECACHE_SOUND("hgrunt/gr_pain2.wav");
	PRECACHE_SOUND("hgrunt/gr_pain3.wav");
	PRECACHE_SOUND("hgrunt/gr_pain4.wav");
	PRECACHE_SOUND("hgrunt/gr_pain5.wav");

	PRECACHE_SOUND("hgrunt/gr_reload1.wav");
	PRECACHE_SOUND("weapons/saw_reload.wav");
	PRECACHE_SOUND("weapons/desert_eagle_reload.wav");

	PRECACHE_SOUND("agrunt/ag_fire1.wav");
	PRECACHE_SOUND("agrunt/ag_fire2.wav");
	PRECACHE_SOUND("agrunt/ag_fire3.wav");

	PRECACHE_SOUND("zombie/claw_miss2.wav");// because we use the basemonster SWIPE animation event

	m_iBrassShell = PRECACHE_MODEL("models/shell.mdl");// brass shell
	m_iShotgunShell = PRECACHE_MODEL("models/shotgunshell.mdl");
	m_iSawShell = PRECACHE_MODEL("models/saw_shell.mdl");
	m_iSawLink = PRECACHE_MODEL("models/saw_link.mdl");

	iShockroachMuzzleFlash = PRECACHE_MODEL("sprites/muzzle_shock.spr");
	iHivehandMuzzleFlash = PRECACHE_MODEL("sprites/muz4.spr");

	UTIL_PrecacheOther("hornet");
	UTIL_PrecacheOther("monster_penguin");
	UTIL_PrecacheOther("monster_snark");
	UTIL_PrecacheOther("monster_shockroach");

	PRECACHE_MODEL( TORCH_BEAM_SPRITE );

	PRECACHE_SOUND("fgrunt/medic_give_shot.wav");
	PRECACHE_SOUND("fgrunt/medical.wav");
	
	TalkInit();
	COFSquadTalkMonster::Precache();
}

//=========================================================
// Spawn
//=========================================================
void CHGruntExtended :: Spawn()
{
	//Someone's cheeky...
	if (m_iGrenadeType == HGRUNT_GRENADE_SPORE_LAUNCHER)
		m_iGrenadeType = HGRUNT_GRENADE_SPORE;

	//quick fix
	if (pev->weapons)
	{
		ALERT(at_console, "Extended HGrunt has old style weapon %d! Fix this!\n", pev->weapons);
		
		if (FBitSet(pev->weapons, HGruntAllyWeaponFlag::MP5))
			m_iNewWeapons = HGRUNT_WEAPON_MP5;

		if (FBitSet(pev->weapons, HGruntAllyWeaponFlag::Shotgun))
			m_iNewWeapons = HGRUNT_WEAPON_SHOTGUN;

		if (FBitSet(pev->weapons, HGruntAllyWeaponFlag::Saw))
			m_iNewWeapons = HGRUNT_WEAPON_M249;

		if (FBitSet(pev->weapons, HGruntAllyWeaponFlag::HandGrenade))
			m_iGrenadeType = HGRUNT_GRENADE_FRAG;

		if (FBitSet(pev->weapons, HGruntAllyWeaponFlag::GrenadeLauncher))
			m_iGrenadeType = HGRUNT_GRENADE_LAUNCHER;

		pev->weapons = 0;
	}

	if (strncmp(STRING(pev->classname), "monster_human_grunt_ally_extended", 34) == 0)
		m_bIsFriendly = true;
	else
		m_bIsFriendly = false;

	if (!m_bIsFriendly)
	{
		m_bPlayerBullets = false;
		m_bFriendlyToScientists = false;

		if (m_iGruntType == HGRUNT_TYPE_MEDIC || m_iGruntType == HGRUNT_TYPE_TORCH)
			m_iGruntType = HGRUNT_TYPE_REGULAR;

		Remember(bits_MEMORY_SUSPICIOUS);
		Remember(bits_MEMORY_PROVOKED);
	}

	m_iHead = pev->body;
	pev->body = 0;

	SetupOutfit();
	SetupVoice();
	Precache();

	if (!(pev->model))
	{
		ALERT(at_console, "HGRUNT HAS BAD MODEL!!! %s\n", STRING(pev->model));
		pev->model = ALLOC_STRING("models/hgrunt.mdl"); //FUCK
		PRECACHE_MODEL("models/hgrunt.mdl");
	}

	SET_MODEL(ENT(pev), STRING(pev->model));

	SetupHead();
	SetupWeapons();

	//Snark/Penguin nades are disabled currently, too broken
	if (m_iGrenadeType > HGRUNT_GRENADE_SPORE)
	{
		//ALERT(at_console, "Extended HGrunt grenade type is invaild! Type is %d, fixing to FRAG (2)\n", m_iGrenadeType);
		m_iGrenadeType = HGRUNT_GRENADE_FRAG;
	}

	if (m_iNewWeapons == HGRUNT_WEAPON_SPORELAUNCHER && !(m_iGrenadeType == HGRUNT_GRENADE_SPORE))
	{
		//ALERT(at_console, "Fixing Extended HGrunt grenade type from %d to SPORE (3) for SporeLauncher\n", m_iGrenadeType);
		m_iGrenadeType = HGRUNT_GRENADE_SPORE;
	}

	//Is Shockroach and not Sporelauncher because Sporelauncher is too broken right now
	if (m_iNewWeapons > HGRUNT_WEAPON_SHOCKROACH)
	{
		//ALERT(at_console, "Extended HGrunt weapon type is invaild! Type is %d, fixing to MP5 (0)\n", m_iNewWeapons);
		m_iNewWeapons = HGRUNT_WEAPON_MP5;
		SetBodygroup(HGRUNT_BODYGROUP_WEAPONS, HGRUNT_WEAPONBODY_MP5);
	}

	UTIL_SetSize(pev, VEC_HUMAN_HULL_MIN, VEC_HUMAN_HULL_MAX);

	pev->solid			= SOLID_SLIDEBOX;
	pev->movetype		= MOVETYPE_STEP;
	m_bloodColor		= BLOOD_COLOR_RED;
	pev->effects		= 0;

	if (pev->health == 0)
	{
		if (m_bIsFriendly)
			pev->health = gSkillData.hgruntAllyHealth;
		else
			pev->health = gSkillData.hgruntHealth;
	}

	m_flFieldOfView		= 0.2;// indicates the width of this monster's forward view cone ( as a dotproduct result )
	m_MonsterState		= MONSTERSTATE_NONE;
	m_flNextGrenadeCheck = gpGlobals->time + 1;
	m_flNextPainTime	= gpGlobals->time;
	m_iSentence			= -1; //HGRUNT_SENT_NONE;

	m_afCapability		= bits_CAP_SQUAD | bits_CAP_TURN_HEAD | bits_CAP_DOORS_GROUP | bits_CAP_HEAR;

	m_fEnemyEluded		= FALSE;
	m_fFirstEncounter	= TRUE;// this is true when the grunt spawns, because he hasn't encountered an enemy yet.

	m_HackedGunPos = Vector ( 0, 0, 55 );

	m_fGunHolstered = false;
	m_fTorchHolstered = true;
	m_fTorchActive = false;

	m_flLastUseTime = 0;
	m_iHealCharge = gSkillData.medicAllyHeal;
	m_fHypoHolstered = true;
	m_fHealActive = false;
	m_fUseHealing = false;
	m_fHealing = false;
	m_fFollowChecked = false;
	m_fFollowChecking = false;

	if (!m_voicePitch)
	{
		if ( m_iHead == HGRUNT_HEADBODY_MASKED || m_iHead == HGRUNT_HEADBODY_TOWER )
			m_voicePitch = 90;
		else if ( m_iHead == HGRUNT_HEADBODY_MEDIC )
			m_voicePitch = 105;
		else if ( m_iHead == HGRUNT_HEADBODY_TORCH )
			m_voicePitch = 95;
		else if (m_iHead == HGRUNT_HEADBODY_CLASSICCOMMANDER || m_iHead == HGRUNT_HEADBODY_CLASSICTOWER ||
				m_iHead == HGRUNT_HEADBODY_CLASSICGASMASK)
			{
				if (RANDOM_LONG(0, 1))
					m_voicePitch = 109 + RANDOM_LONG(0, 7);
				else
					m_voicePitch = 100;
			}
		else
			m_voicePitch = 100;
	}

	COFSquadTalkMonster::g_talkWaitTime = 0;

	m_flMedicWaitTime = gpGlobals->time;
	
	m_flLastShot = gpGlobals->time;

	MonsterInit();

	if (m_bIsFriendly)
	{
		if (m_iGruntType == HGRUNT_TYPE_MEDIC)
			SetUse(&CHGruntExtended::HealerUse);
		else
			SetUse(&CHGruntAlly::FollowerUse);
	}

	m_bDroppedAlready = false;
}

void CHGruntExtended::SetupVoice()
{
	if (m_iVoicePreset == HGRUNT_VOICE_MATCH)
	{
		if (m_iGruntType == HGRUNT_TYPE_ASSASSIN)
			m_iVoicePreset = HGRUNT_VOICE_ASSASSIN;
		else if (m_bIsFriendly && !(m_iGruntType == HGRUNT_TYPE_CLASSIC))
			m_iVoicePreset = HGRUNT_VOICE_REGULAR;
		else
			m_iVoicePreset = HGRUNT_VOICE_CLASSIC;
	}

	const char* voset;
		
	switch (m_iVoicePreset)
	{
		case HGRUNT_VOICE_ASSASSIN: voset = "NULL"; break;
		case HGRUNT_VOICE_CLASSIC: voset = "HG"; break;
		default: voset = "FG"; break;
	}

	if (!m_iszSpeakAs)
		m_iszSpeakAs = ALLOC_STRING(voset);
}


//=========================================================
// PainSound
//=========================================================
void CHGruntExtended :: PainSound ()
{
	if (m_iVoicePreset == HGRUNT_VOICE_ASSASSIN)
		return;

	if (m_bMonstBeQuiet)
		return;

	if ( gpGlobals->time > m_flNextPainTime )
	{
		if (m_iVoicePreset == HGRUNT_VOICE_REGULAR)
		{
			switch (RANDOM_LONG(0, 7))
			{
				case 0: EMIT_SOUND(ENT(pev), CHAN_VOICE, "fgrunt/pain6.wav", 1, ATTN_NORM); break;
				case 1: EMIT_SOUND(ENT(pev), CHAN_VOICE, "fgrunt/pain5.wav", 1, ATTN_NORM); break;
				case 2: EMIT_SOUND(ENT(pev), CHAN_VOICE, "fgrunt/pain4.wav", 1, ATTN_NORM); break;
				case 3: EMIT_SOUND(ENT(pev), CHAN_VOICE, "fgrunt/pain3.wav", 1, ATTN_NORM); break;
				case 4: EMIT_SOUND(ENT(pev), CHAN_VOICE, "fgrunt/pain2.wav", 1, ATTN_NORM); break;
				case 5: EMIT_SOUND(ENT(pev), CHAN_VOICE, "fgrunt/pain1.wav", 1, ATTN_NORM); break;
				default: break;
			}
		}
		else
		{
			switch (RANDOM_LONG(0, 6))
			{
			case 0: EMIT_SOUND(ENT(pev), CHAN_VOICE, "hgrunt/gr_pain5.wav", 1, ATTN_NORM); break;
			case 1: EMIT_SOUND(ENT(pev), CHAN_VOICE, "hgrunt/gr_pain4.wav", 1, ATTN_NORM); break;
			case 2: EMIT_SOUND(ENT(pev), CHAN_VOICE, "hgrunt/gr_pain3.wav", 1, ATTN_NORM); break;
			case 3: EMIT_SOUND(ENT(pev), CHAN_VOICE, "hgrunt/gr_pain2.wav", 1, ATTN_NORM); break;
			case 4: EMIT_SOUND(ENT(pev), CHAN_VOICE, "hgrunt/gr_pain1.wav", 1, ATTN_NORM); break;
			default: break;
			}
		}

		m_flNextPainTime = gpGlobals->time + 1;
	}
}

//=========================================================
// DeathSound 
//=========================================================
void CHGruntExtended :: DeathSound ()
{
	if (m_iVoicePreset == HGRUNT_VOICE_ASSASSIN)
		return;

	if (m_bMonstBeQuiet)
		return;

	if (m_iVoicePreset == HGRUNT_VOICE_REGULAR)
	{
		switch (RANDOM_LONG(0, 5))
		{
		case 0: EMIT_SOUND(ENT(pev), CHAN_VOICE, "fgrunt/death1.wav", 1, ATTN_NORM); break;
		case 1: EMIT_SOUND(ENT(pev), CHAN_VOICE, "fgrunt/death2.wav", 1, ATTN_NORM); break;
		case 2: EMIT_SOUND(ENT(pev), CHAN_VOICE, "fgrunt/death3.wav", 1, ATTN_NORM); break;
		case 3: EMIT_SOUND(ENT(pev), CHAN_VOICE, "fgrunt/death4.wav", 1, ATTN_NORM); break;
		case 4: EMIT_SOUND(ENT(pev), CHAN_VOICE, "fgrunt/death5.wav", 1, ATTN_NORM); break;
		case 5: EMIT_SOUND(ENT(pev), CHAN_VOICE, "fgrunt/death6.wav", 1, ATTN_NORM); break;
		}
	}
	else
	{
		switch (RANDOM_LONG(0, 2))
		{
		case 0: EMIT_SOUND(ENT(pev), CHAN_VOICE, "hgrunt/gr_die1.wav", 1, ATTN_NORM); break;
		case 1: EMIT_SOUND(ENT(pev), CHAN_VOICE, "hgrunt/gr_die2.wav", 1, ATTN_NORM); break;
		case 2: EMIT_SOUND(ENT(pev), CHAN_VOICE, "hgrunt/gr_die3.wav", 1, ATTN_NORM); break;
		}
	}
}



//=========================================================
// HandleAnimEvent - catches the monster-specific messages
// that occur when tagged animation frames are played.
//=========================================================
void CHGruntExtended :: HandleAnimEvent( MonsterEvent_t *pEvent )
{
	Vector	vecShootDir;
	Vector	vecShootOrigin;

	switch( pEvent->event )
	{
		case HGRUNT_AE_DROP_GUN:
			{

			if (pev->spawnflags & SF_MONSTER_NO_WPN_DROP)
				break;

			Vector	vecGunPos;
			Vector	vecGunAngles;

			GetAttachment( 0, vecGunPos, vecGunAngles );

			// switch to body group with no gun.
			SetBodygroup( HGRUNT_BODYGROUP_WEAPONS, HGRUNT_WEAPONBODY_NONE );

			// now spawn a gun.
			if (!m_bDroppedAlready)
			{
				m_bDroppedAlready = true;
				if (m_iNewWeapons == HGRUNT_WEAPON_SHOCKROACH && m_bDropLiveRoach)
				{
					CBaseEntity* pGun = DropItem("monster_shockroach", vecGunPos + Vector(0, 0, 32), vecGunAngles);

					if (pGun)
					{
						if (m_stRoachmodel)
							pGun->pev->model = m_stRoachmodel;

						pGun->pev->velocity = Vector(RANDOM_FLOAT(-100, 100), RANDOM_FLOAT(-100, 100), RANDOM_FLOAT(200, 300));
						pGun->pev->avelocity = Vector(0, RANDOM_FLOAT(200, 400), 0);

						CBaseMonster* pShock = pGun->MyMonsterPointer();

						//LRC - hornets have the same allegiance as their creators
						pShock->m_iPlayerReact = m_iPlayerReact;
						pShock->m_iClass = m_iClass;
						if (m_afMemory & bits_MEMORY_PROVOKED) // if I'm mad at the player, so are my hornets
							pShock->Remember(bits_MEMORY_PROVOKED);
					}
				}
				else
				{
					DropItem(STRING(m_chzWeaponClassname), vecGunPos, vecGunAngles);
				}

				if (m_iGrenadeType == HGRUNT_GRENADE_LAUNCHER)
				{
					DropItem("ammo_ARgrenades", BodyTarget(pev->origin), vecGunAngles);
				}
			}

			m_iWeaponIdx = HGruntAllyWeapon::None;
			}
			break;

		case HGRUNT_AE_RELOAD:
			EMIT_SOUND( ENT(pev), CHAN_WEAPON, STRING(m_chzReloadSound), 1, ATTN_NORM );

			m_cAmmoLoaded = m_cClipSize;
			ClearConditions(bits_COND_NO_AMMO_LOADED);
			break;

		case HGRUNT_AE_GREN_TOSS:
		{
			if (m_iGrenadeType == HGRUNT_GRENADE_FRAG)
				ThrowGrenade();
			else if (m_iGrenadeType == HGRUNT_GRENADE_SPORE)
				ThrowSpore();
//			else if (m_iGrenadeType == HGRUNT_GRENADE_SNARK)
//				ThrowSnark();
//			else if (m_iGrenadeType == HGRUNT_GRENADE_PENGUIN)
//				ThrowPenguin();
		}
		break;

		case HGRUNT_AE_GREN_LAUNCH:
		{
			if (gpgs && gpgs->m_iszMP5SoundLauncher)
				EMIT_SOUND(ENT(pev), CHAN_WEAPON, STRING(gpgs->m_iszMP5SoundLauncher), 0.8, ATTN_NORM);
			else
				EMIT_SOUND(ENT(pev), CHAN_WEAPON, "weapons/glauncher.wav", 0.8, ATTN_NORM);
			
			//LRC: firing due to a script?
			if (m_pCine)
			{
				Vector vecToss;
				if (m_hTargetEnt != NULL && m_pCine->PreciseAttack())
					vecToss = VecCheckThrow( pev, GetGunPosition(), m_hTargetEnt->pev->origin, gSkillData.hgruntGrenadeSpeed, 0.5 );
				else
				{
					// just shoot diagonally up+forwards
					UTIL_MakeVectors(pev->angles);
					vecToss = (gpGlobals->v_forward*0.5 + gpGlobals->v_up*0.5).Normalize() * gSkillData.hgruntGrenadeSpeed;
				}
				CGrenade::ShootContact( pev, GetGunPosition(), vecToss );
			}
			else
			CGrenade::ShootContact( pev, GetGunPosition(), m_vecTossVelocity );

			m_fThrowGrenade = FALSE;
			if (g_iSkillLevel == SKILL_HARD)
				m_flNextGrenadeCheck = gpGlobals->time + RANDOM_FLOAT( 2, 5 );// wait a random amount of time before shooting again
			else
				m_flNextGrenadeCheck = gpGlobals->time + 6;// wait six seconds before even looking again to see if a grenade can be thrown.
		}
		break;

		case HGRUNT_AE_GREN_DROP:
		{
			UTIL_MakeVectors( pev->angles );
			CGrenade::ShootTimed( pev, pev->origin + gpGlobals->v_forward * 17 - gpGlobals->v_right * 27 + gpGlobals->v_up * 6, g_vecZero, 3 );
		}
		break;

		case HGRUNT_AE_BURST1:
		{
			if (m_iNewWeapons == HGRUNT_WEAPON_SHOTGUN)
				Shotgun();
			else if (m_iNewWeapons == HGRUNT_WEAPON_M249)
				ShootSaw();
			else if (m_iNewWeapons == HGRUNT_WEAPON_MP5)
				Shoot();
			else if (m_iNewWeapons == HGRUNT_WEAPON_HIVEHAND)
				ShootHivehand();
			else if (m_iNewWeapons == HGRUNT_WEAPON_SPORELAUNCHER)
				; //nothing
			else if (m_iNewWeapons == HGRUNT_WEAPON_SHOCKROACH)
				ShootShockroach();
			else if (m_iNewWeapons == HGRUNT_WEAPON_SNIPER)
				ShootSniper();
			else if (m_iNewWeapons == HGRUNT_WEAPON_GLOCK || m_iNewWeapons == HGRUNT_WEAPON_EAGLE || m_iNewWeapons == HGRUNT_WEAPON_PYTHON)
				ShootPistol();
			else
				ALERT(at_console, "EXTENDED HGRUNT TRIED TO FIRE INVAILD WEAPON! m_iNewWeapons is %d\n", m_iNewWeapons);
		
			CSoundEnt::InsertSound ( bits_SOUND_COMBAT, pev->origin, 384, 0.3 );

			m_flLastShot = gpGlobals->time;
		}
		break;

		case HGRUNT_AE_BURST2:
		case HGRUNT_AE_BURST3:
		{
			if (m_iNewWeapons == HGRUNT_WEAPON_SHOTGUN)
				Shotgun();
			else if (m_iNewWeapons == HGRUNT_WEAPON_M249)
				ShootSaw();
			else if (m_iNewWeapons == HGRUNT_WEAPON_MP5)
				Shoot();
			else if (m_iNewWeapons == HGRUNT_WEAPON_HIVEHAND)
				ShootHivehand();
			else if (m_iNewWeapons == HGRUNT_WEAPON_SPORELAUNCHER)
				; //nothing
			else if (m_iNewWeapons == HGRUNT_WEAPON_SHOCKROACH)
				ShootShockroach();
			else if (m_iNewWeapons == HGRUNT_WEAPON_SNIPER)
				ShootSniper();
			else if (m_iNewWeapons == HGRUNT_WEAPON_GLOCK || m_iNewWeapons == HGRUNT_WEAPON_EAGLE || m_iNewWeapons == HGRUNT_WEAPON_PYTHON)
				ShootPistol();
			else
				ALERT(at_console, "EXTENDED HGRUNT TRIED TO FIRE INVAILD WEAPON! m_iNewWeapons is %d\n", m_iNewWeapons);

			m_flLastShot = gpGlobals->time;
		}
		break;

		case MEDIC_AE_HOLSTER_GUN:
			SetBodygroup( HGRUNT_BODYGROUP_WEAPONS, m_bBodyGroup_Holster );
			m_fGunHolstered = true;
			break;

		case MEDIC_AE_EQUIP_NEEDLE:
			SetBodygroup( HGRUNT_BODYGROUP_PROP, HGRUNT_PROPBODY_ON );
			m_fHypoHolstered = false;
			break;

		case MEDIC_AE_HOLSTER_NEEDLE:
			SetBodygroup( HGRUNT_BODYGROUP_PROP, HGRUNT_PROPBODY_OFF );
			m_fHypoHolstered = true;
			break;

		case MEDIC_AE_EQUIP_GUN:
			SetBodygroup( HGRUNT_BODYGROUP_WEAPONS, m_bBodyGroup_Gun );
			m_fGunHolstered = false;
			break;

		case TORCH_AE_HOLSTER_TORCH:
			{
				SetBodygroup( HGRUNT_BODYGROUP_WEAPONS, m_bBodyGroup_Gun );
				SetBodygroup( HGRUNT_BODYGROUP_PROP, HGRUNT_PROPBODY_OFF );
				m_fGunHolstered = false;
				m_fTorchHolstered = true;
				break;
			}

		case TORCH_AE_HOLSTER_GUN:
			{
				SetBodygroup( HGRUNT_BODYGROUP_WEAPONS, m_bBodyGroup_Holster );
				SetBodygroup( HGRUNT_BODYGROUP_PROP, HGRUNT_PROPBODY_ON );
				m_fGunHolstered = true;
				m_fTorchHolstered = false;
				break;
			}

		case TORCH_AE_HOLSTER_BOTH:
			{
				SetBodygroup(HGRUNT_BODYGROUP_WEAPONS, m_bBodyGroup_Holster);
				SetBodygroup(HGRUNT_BODYGROUP_PROP, HGRUNT_PROPBODY_OFF);
				m_fGunHolstered = true;
				m_fTorchHolstered = true;
				break;
			}

		case TORCH_AE_ACTIVATE_TORCH:
			{
				m_fTorchActive = true;
				m_pTorchBeam = CBeam::BeamCreate( TORCH_BEAM_SPRITE, 5 );

				if( m_pTorchBeam )
				{
					Vector vecTorchPos, vecTorchAng;
					GetAttachment( 2, vecTorchPos, vecTorchAng );

					m_pTorchBeam->EntsInit( entindex(), entindex() );

					m_pTorchBeam->SetStartAttachment( 4 );
					m_pTorchBeam->SetEndAttachment( 3 );

					m_pTorchBeam->SetColor( 0, 0, 255 );
					m_pTorchBeam->SetBrightness( 255 );
					m_pTorchBeam->SetWidth( 5 );
					m_pTorchBeam->SetFlags( BEAM_FSHADEIN );
					m_pTorchBeam->SetScrollRate( 20 );

					m_pTorchBeam->pev->spawnflags |= SF_BEAM_SPARKEND;
					m_pTorchBeam->DoSparks( vecTorchPos, vecTorchPos );
				}
				break;
			}

		case TORCH_AE_DEACTIVATE_TORCH:
			{
				if( m_pTorchBeam )
				{
					m_fTorchActive = false;
					UTIL_Remove( m_pTorchBeam );
					m_pTorchBeam = nullptr;
				}
				break;
			}

		default:
			CHGruntAlly::HandleAnimEvent( pEvent );
			break;
	}
}

//=========================================================
// GibMonster - make gun fly through the air.
//=========================================================
void CHGruntExtended :: GibMonster ()
{
	if( m_hWaitMedic )
	{
		auto pMedic = m_hWaitMedic.Entity<COFSquadTalkMonster>();

		if( pMedic->pev->deadflag != DEAD_NO )
			m_hWaitMedic = nullptr;
		else
			pMedic->HealMe( nullptr );
	}

	Vector	vecGunPos;
	Vector	vecGunAngles;

	if (!(pev->spawnflags & SF_MONSTER_NO_WPN_DROP))
	{// throw a gun if the grunt has one
		GetAttachment( 0, vecGunPos, vecGunAngles );
		
		const char* dropwpn = STRING(m_chzWeaponClassname);

		bool livedrop = false;

		if (m_bDropLiveRoach && (m_iNewWeapons == HGRUNT_WEAPON_SHOCKROACH))
		{
			dropwpn = "monster_shockroach";
			livedrop = true;
		}

		CBaseEntity *pGun;
		pGun = DropItem(dropwpn, vecGunPos, vecGunAngles );
		
		if ( pGun )
		{
			pGun->pev->velocity = Vector (RANDOM_FLOAT(-100,100), RANDOM_FLOAT(-100,100), RANDOM_FLOAT(200,300));
			pGun->pev->avelocity = Vector ( 0, RANDOM_FLOAT( 200, 400 ), 0 );

			if (livedrop)
			{
				CBaseMonster* pShock = pGun->MyMonsterPointer();

				//LRC - hornets have the same allegiance as their creators
				pShock->m_iPlayerReact = m_iPlayerReact;
				pShock->m_iClass = m_iClass;
				if (m_afMemory & bits_MEMORY_PROVOKED) // if I'm mad at the player, so are my hornets
					pShock->Remember(bits_MEMORY_PROVOKED);
			}
		}
	
		if ( m_iGrenadeType == HGRUNT_GRENADE_LAUNCHER )
		{
			pGun = DropItem( "ammo_ARgrenades", vecGunPos, vecGunAngles );
			if ( pGun )
			{
				pGun->pev->velocity = Vector (RANDOM_FLOAT(-100,100), RANDOM_FLOAT(-100,100), RANDOM_FLOAT(200,300));
				pGun->pev->avelocity = Vector ( 0, RANDOM_FLOAT( 200, 400 ), 0 );
			}
		}

		m_iWeaponIdx = HGruntAllyWeapon::None;
	}

	CBaseMonster :: GibMonster();
}

//=========================================================
// Shoot
//=========================================================
void CHGruntExtended :: Shoot ()
{
	if (m_hEnemy == NULL)
	{
		return;
	}

	Vector vecShootOrigin = GetGunPosition();
	Vector vecShootDir = ShootAtEnemy( vecShootOrigin );

	UTIL_MakeVectors ( pev->angles );

	Vector	vecShellVelocity = gpGlobals->v_right * RANDOM_FLOAT(40,90) + gpGlobals->v_up * RANDOM_FLOAT(75,200) + gpGlobals->v_forward * RANDOM_FLOAT(-40, 40);
	EjectBrass ( vecShootOrigin - vecShootDir * 24, vecShellVelocity, pev->angles.y, m_iBrassShell, TE_BOUNCE_SHELL); 

	int bullettype;

	if (m_bPlayerBullets)
		bullettype = BULLET_PLAYER_MP5;
	else
		bullettype = BULLET_MONSTER_MP5;

	FireBullets(1, vecShootOrigin, vecShootDir, VECTOR_CONE_10DEGREES, 2048, bullettype ); // shoot +-5 degrees
	
	EMIT_SOUND(ENT(pev), CHAN_WEAPON, STRING(m_chzGunSound), 1, ATTN_NORM);

	pev->effects |= EF_MUZZLEFLASH;
	
	m_cAmmoLoaded--;// take away a bullet!

	Vector angDir = UTIL_VecToAngles( vecShootDir );
	SetBlending( 0, angDir.x );
}

//=========================================================
// Shoot
//=========================================================
void CHGruntExtended :: Shotgun ()
{
	if (m_hEnemy == NULL)
	{
		return;
	}

	Vector vecShootOrigin = GetGunPosition();
	Vector vecShootDir = ShootAtEnemy( vecShootOrigin );

	UTIL_MakeVectors ( pev->angles );

	Vector	vecShellVelocity = gpGlobals->v_right * RANDOM_FLOAT(40,90) + gpGlobals->v_up * RANDOM_FLOAT(75,200) + gpGlobals->v_forward * RANDOM_FLOAT(-40, 40);
	EjectBrass ( vecShootOrigin - vecShootDir * 24, vecShellVelocity, pev->angles.y, m_iShotgunShell, TE_BOUNCE_SHOTSHELL); 

	int bullettype;

	if (m_bPlayerBullets)
		bullettype = BULLET_PLAYER_BUCKSHOT;
	else
		bullettype = BULLET_MONSTER_BUCKSHOT;

	FireBullets(gSkillData.hgruntAllyShotgunPellets, vecShootOrigin, vecShootDir, VECTOR_CONE_15DEGREES, 2048, bullettype, 0 ); // shoot +-7.5 degrees
	
	EMIT_SOUND(ENT(pev), CHAN_WEAPON, STRING(m_chzGunSound), 1, ATTN_NORM );

	pev->effects |= EF_MUZZLEFLASH;
	
	m_cAmmoLoaded--;// take away a bullet!

	Vector angDir = UTIL_VecToAngles( vecShootDir );
	SetBlending( 0, angDir.x );
}

void CHGruntExtended::ShootSaw()
{
	if( m_hEnemy == NULL )
	{
		return;
	}

	Vector vecShootOrigin = GetGunPosition();
	Vector vecShootDir = ShootAtEnemy( vecShootOrigin );

	UTIL_MakeVectors( pev->angles );

	switch( RANDOM_LONG( 0, 1 ) )
	{
	case 0:
		{
			auto vecShellVelocity = gpGlobals->v_right * RANDOM_FLOAT( 75, 200 ) + gpGlobals->v_up * RANDOM_FLOAT( 150, 200 ) + gpGlobals->v_forward * 25.0;
			EjectBrass( vecShootOrigin - vecShootDir * 6, vecShellVelocity, pev->angles.y, m_iSawLink, TE_BOUNCE_SHELL );
			break;
		}

	case 1:
		{
			auto vecShellVelocity = gpGlobals->v_right * RANDOM_FLOAT( 100, 250 ) + gpGlobals->v_up * RANDOM_FLOAT( 100, 150 ) + gpGlobals->v_forward * 25.0;
			EjectBrass( vecShootOrigin - vecShootDir * 6, vecShellVelocity, pev->angles.y, m_iSawShell, TE_BOUNCE_SHELL );
			break;
		}
	}

	int bullettype;

	if (m_bPlayerBullets)
		bullettype = BULLET_PLAYER_556;
	else
		bullettype = BULLET_MONSTER_556;

	FireBullets( 1, vecShootOrigin, vecShootDir, VECTOR_CONE_5DEGREES, TRUE_WORLD_SIZE, bullettype, 2 ); // shoot +-5 degrees

	EMIT_SOUND_DYN(edict(), CHAN_WEAPON, STRING(m_chzGunSound), VOL_NORM, ATTN_NORM, 0, RANDOM_LONG(0, 15) + 94);

	pev->effects |= EF_MUZZLEFLASH;

	m_cAmmoLoaded--;// take away a bullet!

	Vector angDir = UTIL_VecToAngles( vecShootDir );
	SetBlending( 0, angDir.x );
}

//=========================================================
// Shoot
//=========================================================
void CHGruntExtended :: ShootSniper ()
{
	if (m_hEnemy == NULL)
	{
		return;
	}

	if( ( m_iNewWeapons == HGRUNT_WEAPON_SNIPER ) && gpGlobals->time - m_flLastShot <= 0.11 )
	{
		return;
	}

	Vector vecShootOrigin = GetGunPosition();
	Vector vecShootDir = ShootAtEnemy( vecShootOrigin );

	UTIL_MakeVectors ( pev->angles );

	FireBullets( 1, vecShootOrigin, vecShootDir, VECTOR_CONE_1DEGREES, 2048, BULLET_MONSTER_762 );

	EMIT_SOUND_DYN( edict(), CHAN_WEAPON, STRING(m_chzGunSound), VOL_NORM, ATTN_NORM, 0, 100);

	pev->effects |= EF_MUZZLEFLASH;
	
	m_cAmmoLoaded--;// take away a bullet!

	Vector angDir = UTIL_VecToAngles( vecShootDir );
	SetBlending( 0, angDir.x );
}

//=========================================================
// Shoot
//=========================================================
void CHGruntExtended :: ShootPistol ()
{
	//Limit fire rate
	if (m_hEnemy == NULL || gpGlobals->time - m_flLastShot <= 0.11 )
	{
		return;
	}

	Vector vecShootOrigin = GetGunPosition();
	Vector vecShootDir = ShootAtEnemy( vecShootOrigin );

	UTIL_MakeVectors ( pev->angles );

	int bullettype = BULLET_MONSTER_9MM;

	if ( m_bPlayerBullets && (m_iNewWeapons == HGRUNT_WEAPON_EAGLE) )
		bullettype = BULLET_PLAYER_EAGLE;
	else if (m_bPlayerBullets && (m_iNewWeapons == HGRUNT_WEAPON_GLOCK))
		bullettype = BULLET_PLAYER_9MM;
	else if (m_bPlayerBullets && (m_iNewWeapons == HGRUNT_WEAPON_PYTHON))
		bullettype = BULLET_PLAYER_357;
	else if (m_iNewWeapons == HGRUNT_WEAPON_EAGLE)
		bullettype = BULLET_MONSTER_EAGLE;
	else if (m_iNewWeapons == HGRUNT_WEAPON_PYTHON)
		bullettype = BULLET_MONSTER_357;
	
	FireBullets( 1, vecShootOrigin, vecShootDir, VECTOR_CONE_2DEGREES, 1024, bullettype); // shoot +-5 degrees

	const auto random = RANDOM_LONG(0, 20);

	const auto pitch = random <= 10 ? random + 95 : 100;

	EMIT_SOUND_DYN( edict(), CHAN_WEAPON, STRING(m_chzGunSound), VOL_NORM, ATTN_NORM, 0, pitch);

	pev->effects |= EF_MUZZLEFLASH;
	
	m_cAmmoLoaded--;// take away a bullet!

	Vector angDir = UTIL_VecToAngles( vecShootDir );
	SetBlending( 0, angDir.x );

	m_flLastShot = gpGlobals->time;
}

//=========================================================
// Shoot
//=========================================================
void CHGruntExtended::ShootHivehand()
{
	// m_vecEnemyLKP should be center of enemy body
	Vector vecArmPos, vecArmDir;
	Vector vecDirToEnemy;
	Vector angDir;

	if (HasConditions(bits_COND_SEE_ENEMY))
	{
		vecDirToEnemy = ((m_vecEnemyLKP)-pev->origin);
		angDir = UTIL_VecToAngles(vecDirToEnemy);
		vecDirToEnemy = vecDirToEnemy.Normalize();
	}
	else
	{
		angDir = pev->angles;
		UTIL_MakeAimVectors(angDir);
		vecDirToEnemy = gpGlobals->v_forward;
	}

	pev->effects = EF_MUZZLEFLASH;

	// make angles +-180
	if (angDir.x > 180)
	{
		angDir.x = angDir.x - 360;
	}

	SetBlending(0, angDir.x);
	GetAttachment(0, vecArmPos, vecArmDir);

	vecArmPos = vecArmPos + vecDirToEnemy * 32;
	MESSAGE_BEGIN(MSG_PVS, SVC_TEMPENTITY, vecArmPos);
	WRITE_BYTE(TE_SPRITE);
	WRITE_COORD(vecArmPos.x);	// pos
	WRITE_COORD(vecArmPos.y);
	WRITE_COORD(vecArmPos.z);
	WRITE_SHORT(iHivehandMuzzleFlash);		// model
	WRITE_BYTE(6);				// size * 10
	WRITE_BYTE(128);			// brightness
	MESSAGE_END();

	CBaseEntity* pHornet = CBaseEntity::Create("hornet", vecArmPos, UTIL_VecToAngles(vecDirToEnemy), edict());
	UTIL_MakeVectors(pHornet->pev->angles);
	pHornet->pev->velocity = gpGlobals->v_forward * 300;

	switch (RANDOM_LONG(0, 2))
	{
	case 0:	EMIT_SOUND_DYN(ENT(pev), CHAN_WEAPON, "agrunt/ag_fire1.wav", 1.0, ATTN_NORM, 0, 100);	break;
	case 1:	EMIT_SOUND_DYN(ENT(pev), CHAN_WEAPON, "agrunt/ag_fire2.wav", 1.0, ATTN_NORM, 0, 100);	break;
	case 2:	EMIT_SOUND_DYN(ENT(pev), CHAN_WEAPON, "agrunt/ag_fire3.wav", 1.0, ATTN_NORM, 0, 100);	break;
	}

	CBaseMonster* pHornetMonster = pHornet->MyMonsterPointer();

	//LRC - hornets have the same allegiance as their creators
	pHornetMonster->m_iPlayerReact = m_iPlayerReact;
	pHornetMonster->m_iClass = m_iClass;
	if (m_afMemory & bits_MEMORY_PROVOKED) // if I'm mad at the player, so are my hornets
		pHornetMonster->Remember(bits_MEMORY_PROVOKED);

	if (pHornetMonster)
	{
		if (m_pCine && m_pCine->PreciseAttack()) //LRC- are we doing a scripted action?
			pHornetMonster->m_hEnemy = m_hTargetEnt;
		else
			pHornetMonster->m_hEnemy = m_hEnemy;
	}
}

//=========================================================
// Shoot
//=========================================================
void CHGruntExtended::ShootShockroach()
{
	Vector	vecArmPos;
	Vector	vecArmAngles;

	GetAttachment(0, vecArmPos, vecArmAngles);

	MESSAGE_BEGIN(MSG_PVS, SVC_TEMPENTITY, vecArmPos);
	g_engfuncs.pfnWriteByte(TE_SPRITE);
	g_engfuncs.pfnWriteCoord(vecArmPos.x);
	g_engfuncs.pfnWriteCoord(vecArmPos.y);
	g_engfuncs.pfnWriteCoord(vecArmPos.z);
	g_engfuncs.pfnWriteShort(iShockroachMuzzleFlash);
	g_engfuncs.pfnWriteByte(4);
	g_engfuncs.pfnWriteByte(128);
	MESSAGE_END();

	Vector vecShootOrigin = GetGunPosition();
	Vector vecShootDir = ShootAtEnemy(vecShootOrigin);

	Vector angDir = UTIL_VecToAngles(vecShootDir);

	UTIL_MakeVectors(pev->angles);

	auto shootAngles = angDir;

	shootAngles.x = -shootAngles.x;

	auto pBeam = CShockBeam::CreateShockBeam(vecShootOrigin, shootAngles, this);

	pBeam->pev->velocity = vecShootDir * 2000;
	pBeam->pev->speed = 2000;

	SetBlending(0, angDir.x);

	m_flLastShot = gpGlobals->time;

	EMIT_SOUND(ENT(pev), CHAN_WEAPON, STRING(m_chzGunSound), 1, ATTN_NORM);

	CSoundEnt::InsertSound(bits_SOUND_COMBAT, pev->origin, 384, 0.3);
}

//=========================================================
// Classify - indicates this monster's place in the 
// relationship table.
//=========================================================
int	CHGruntExtended :: Classify ()
{
	if (m_iClass)
		return m_iClass;

	if (m_bIsFriendly)
		return CLASS_HUMAN_MILITARY_FRIENDLY;
	else
		return CLASS_HUMAN_MILITARY;
}

void CHGruntExtended::ThrowGrenade()
{
	UTIL_MakeVectors(pev->angles);
	// CGrenade::ShootTimed( pev, pev->origin + gpGlobals->v_forward * 34 + Vector (0, 0, 32), m_vecTossVelocity, 3.5 );
	CGrenade::ShootTimed(pev, GetGunPosition(), m_vecTossVelocity, 3.5);

	m_fThrowGrenade = FALSE;
	m_flNextGrenadeCheck = gpGlobals->time + 6;// wait six seconds before even looking again to see if a grenade can be thrown.
	// !!!LATER - when in a group, only try to throw grenade if ordered.
}

void CHGruntExtended::ThrowSpore()
{
	UTIL_MakeVectors(pev->angles);
	// CGrenade::ShootTimed( pev, pev->origin + gpGlobals->v_forward * 34 + Vector (0, 0, 32), m_vecTossVelocity, 3.5 );
	CSpore::CreateSpore(pev->origin + Vector(0, 0, 98), m_vecTossVelocity, this, CSpore::SporeType::GRENADE, true, false);

	m_fThrowGrenade = FALSE;
	m_flNextGrenadeCheck = gpGlobals->time + 6;// wait six seconds before even looking again to see if a grenade can be thrown.
	// !!!LATER - when in a group, only try to throw grenade if ordered.
}

void CHGruntExtended::ThrowSnark()
{
	UTIL_MakeVectors(pev->angles);
	TraceResult tr;
	Vector trace_origin;

	trace_origin = pev->origin;

	// find place to toss monster
	UTIL_TraceLine(trace_origin + gpGlobals->v_forward * 20, trace_origin + gpGlobals->v_forward * 64, dont_ignore_monsters, NULL, &tr);

	CBaseEntity* pSqueak = CBaseEntity::Create("monster_snark", tr.vecEndPos, pev->angles, edict());
	pSqueak->pev->velocity = gpGlobals->v_forward * 200 + pev->velocity;

	CBaseMonster* pShock = pSqueak->MyMonsterPointer();

	//LRC - hornets have the same allegiance as their creators
	pShock->m_iPlayerReact = m_iPlayerReact;
	pShock->m_iClass = m_iClass;
	if (m_afMemory & bits_MEMORY_PROVOKED) // if I'm mad at the player, so are my hornets
		pShock->Remember(bits_MEMORY_PROVOKED);

	// play hunt sound
	float flRndSound = RANDOM_FLOAT(0, 1);

	if (flRndSound <= 0.5)
		EMIT_SOUND_DYN(ENT(pev), CHAN_VOICE, "squeek/sqk_hunt2.wav", 1, ATTN_NORM, 0, 105);
	else
		EMIT_SOUND_DYN(ENT(pev), CHAN_VOICE, "squeek/sqk_hunt3.wav", 1, ATTN_NORM, 0, 105);
}

void CHGruntExtended::ThrowPenguin()
{
	UTIL_MakeVectors(pev->angles);
	TraceResult tr;
	Vector trace_origin;

	trace_origin = pev->origin;

	// find place to toss monster
	UTIL_TraceLine(trace_origin + gpGlobals->v_forward * 20, trace_origin + gpGlobals->v_forward * 64, dont_ignore_monsters, NULL, &tr);

	CBaseEntity* pSqueak = CBaseEntity::Create("monster_penguin", tr.vecEndPos, pev->angles, edict());
	pSqueak->pev->velocity = gpGlobals->v_forward * 200 + pev->velocity;

	CBaseMonster* pShock = pSqueak->MyMonsterPointer();

	//LRC - hornets have the same allegiance as their creators
	pShock->m_iPlayerReact = m_iPlayerReact;
	pShock->m_iClass = m_iClass;
	if (m_afMemory & bits_MEMORY_PROVOKED) // if I'm mad at the player, so are my hornets
		pShock->Remember(bits_MEMORY_PROVOKED);

	// play hunt sound
	float flRndSound = RANDOM_FLOAT(0, 1);

	if (flRndSound <= 0.5)
		EMIT_SOUND_DYN(ENT(pev), CHAN_VOICE, "squeek/sqk_hunt2.wav", 1, ATTN_NORM, 0, 105);
	else
		EMIT_SOUND_DYN(ENT(pev), CHAN_VOICE, "squeek/sqk_hunt3.wav", 1, ATTN_NORM, 0, 105);
}

//=========================================================
// SetActivity 
//=========================================================
void CHGruntExtended :: SetActivity ( Activity NewActivity )
{
	int	iSequence = ACTIVITY_NOT_AVAILABLE;
	void *pmodel = GET_MODEL_PTR( ENT(pev) );

	switch ( NewActivity)
	{
	case ACT_RANGE_ATTACK1:
		// grunt is either shooting standing or shooting crouched
		if (m_bUsingCustomModel == 2)
		{
			if (m_iNewWeapons == HGRUNT_WEAPON_SHOTGUN || m_iNewWeapons == HGRUNT_WEAPON_SPORELAUNCHER
				|| m_iNewWeapons == HGRUNT_WEAPON_HIVEHAND || m_iNewWeapons == HGRUNT_WEAPON_SHOCKROACH
				|| m_iNewWeapons == HGRUNT_WEAPON_SNIPER || m_bUsingPistol)
			{
				if (m_fStanding)
				{
					// get aimable sequence
					iSequence = LookupSequence("standing_shotgun");
				}
				else
				{
					// get crouching shoot
					iSequence = LookupSequence("crouching_shotgun");
				}
			}
			else
			{
				if (m_fStanding)
				{
					// get aimable sequence
					iSequence = LookupSequence("standing_mp5");
				}
				else
				{
					// get crouching shoot
					iSequence = LookupSequence("crouching_mp5");
				}
			}
		}
		else
		{
			if (m_iNewWeapons == HGRUNT_WEAPON_SHOTGUN || m_iNewWeapons == HGRUNT_WEAPON_SPORELAUNCHER || m_iNewWeapons == HGRUNT_WEAPON_SHOCKROACH)
			{
				if (m_fStanding)
				{
					// get aimable sequence
					iSequence = LookupSequence("standing_shotgun");
				}
				else
				{
					// get crouching shoot
					iSequence = LookupSequence("crouching_shotgun");
				}
			}
			else if (m_bUsingPistol)
			{
				if (m_fStanding)
				{
					// get aimable sequence
					iSequence = LookupSequence("standing_pistol");
				}
				else
				{
					// get crouching shoot
					iSequence = LookupSequence("crouching_pistol");
				}
			}
			else if (m_iNewWeapons == HGRUNT_WEAPON_M249)
			{
				if (m_fStanding)
				{
					// get aimable sequence
					iSequence = LookupSequence("standing_saw");
				}
				else
				{
					// get crouching shoot
					iSequence = LookupSequence("crouching_saw");
				}
			}
			else if (m_iNewWeapons == HGRUNT_WEAPON_SNIPER)
			{
				if (m_fStanding)
				{
					// get aimable sequence
					iSequence = LookupSequence("standing_m40a1");
				}
				else
				{
					// get crouching shoot
					iSequence = LookupSequence("crouching_m40a1");
				}
			}
			else
			{
				if (m_fStanding)
				{
					// get aimable sequence
					iSequence = LookupSequence("standing_mp5");
				}
				else
				{
					// get crouching shoot
					iSequence = LookupSequence("crouching_mp5");
				}
			}
		}
		break;

	case ACT_RANGE_ATTACK2:
		// grunt is going to a secondary long range attack. This may be a thrown 
		// grenade or fired grenade, we must determine which and pick proper sequence
		if ( m_iGrenadeType == HGRUNT_GRENADE_LAUNCHER )
		{
			// get toss anim
			iSequence = LookupSequence( "launchgrenade" );
		}
		else
		{
			// get launch anim
			iSequence = LookupSequence( "throwgrenade" );
		}
		break;

	case ACT_RUN:
		if (m_bUsingCustomModel == 2)
		{
			if (pev->health <= HGRUNT_LIMP_HEALTH)
			{
				// limp!
				iSequence = LookupActivity(ACT_RUN_HURT);
			}
			else
			{
				iSequence = LookupActivity(NewActivity);
			}
		}
		else
		{
			if (m_iGruntType == HGRUNT_TYPE_ASSASSIN)
			{
				iSequence = LookupSequence("assassin_run");
			}
			else if (pev->health <= HGRUNT_LIMP_HEALTH)
			{
				// limp!
				iSequence = LookupActivity(ACT_RUN_HURT);
			}
			else if (m_bUsingPistol)
			{
				iSequence = LookupSequence("pistol_run");
			}
			else
			{
				iSequence = LookupSequence("regular_run");
			}
		}
		break;

	case ACT_WALK:
		if ( pev->health <= HGRUNT_LIMP_HEALTH )
		{
			// limp!
			iSequence = LookupActivity ( ACT_WALK_HURT );
		}
		else if (m_bUsingCustomModel == 2)
		{
			iSequence = LookupActivity(NewActivity);
		}
		else if (m_bUsingPistol)
		{
			iSequence = LookupSequence( "pistol_walk1" );
		}
		else
		{
			iSequence = LookupSequence( "regular_walk1" );
		}
		break;

	case ACT_IDLE:
		{
			int j = RANDOM_LONG(0, 1);
			if (m_bUsingCustomModel == 2)
			{
				iSequence = LookupActivity(NewActivity);
			}
			else if (m_bUsingPistol)
			{
				if (j)
					iSequence = LookupSequence("pistol_idle1");
				else
					iSequence = LookupSequence("pistol_idle2");
			}
			else
			{
				if (j)
					iSequence = LookupSequence("regular_idle1");
				else
					iSequence = LookupSequence("regular_idle2");
			}
		}

		break;
		
	case ACT_STRAFE_LEFT:
		if (m_bUsingCustomModel == 2)
		{
			iSequence = LookupActivity(NewActivity);
		}
		else if (m_iGruntType == HGRUNT_TYPE_ASSASSIN)
		{
			iSequence = LookupSequence( "assassin_strafeleft" );
		}
		else
		{
			iSequence = LookupSequence( "regular_strafeleft" );
		}
		break;

	case ACT_STRAFE_RIGHT:
		if (m_bUsingCustomModel == 2)
		{
			iSequence = LookupActivity(NewActivity);
		}
		else if (m_iGruntType == HGRUNT_TYPE_ASSASSIN)
		{
			iSequence = LookupSequence( "assassin_straferight" );
		}
		else
		{
			iSequence = LookupSequence( "regular_straferight" );
		}
		break;

	case ACT_CROUCH:
		if (m_bUsingCustomModel == 2)
		{
			iSequence = LookupActivity(NewActivity);
		}
		else if ( m_bUsingPistol )
		{
			iSequence = LookupSequence("pistol_crouching_wait");
		}
		else
		{
			iSequence = LookupSequence("regular_crouching_wait");
		}
		break;

	case ACT_MELEE_ATTACK1:
		if (m_bUsingCustomModel == 2)
		{
			iSequence = LookupActivity(NewActivity);
		}
		else if (m_iGruntType == HGRUNT_TYPE_ASSASSIN)
		{
			int j = RANDOM_LONG(0, 1);
			
			switch (j)
			{
			case 0: iSequence = LookupSequence("assassin_frontkick"); break;
			case 1: iSequence = LookupSequence("assassin_frontkick2"); break;
			}
		}
		else
		{
			iSequence = LookupSequence( "regular_frontkick" );
		}
		break;

	case ACT_RELOAD:
		if (m_bUsingCustomModel == 2)
			iSequence = LookupActivity(NewActivity);
		else
			iSequence = LookupSequence( STRING(m_chzReloadAnimation) );
		break;

	default:
		iSequence = LookupActivity ( NewActivity );
		break;
	}
	
	m_Activity = NewActivity; // Go ahead and set this so it doesn't keep trying when the anim is not present

	// Set to the desired anim, or default anim if the desired is not present
	if ( iSequence > ACTIVITY_NOT_AVAILABLE )
	{
		if ( pev->sequence != iSequence || !m_fSequenceLoops )
		{
			pev->frame = 0;
		}

		pev->sequence		= iSequence;	// Set to the reset anim (if it's there)
		ResetSequenceInfo( );
		SetYawSpeed();
	}
	else
	{
		// Not available try to get default anim
		ALERT ( at_console, "%s has no sequence for act:%d\n", STRING(pev->classname), NewActivity );
		pev->sequence		= 0;	// Set to the reset anim (if it's there)
	}
}


void CHGruntExtended::SetupWeapons()
{
	if (m_iNewWeapons == HGRUNT_WEAPONRAND_ALL)
		m_iNewWeapons = RANDOM_LONG(HGRUNT_WEAPON_MP5, HGRUNT_WEAPON_SPORELAUNCHER);
	else if (m_iNewWeapons == HGRUNT_WEAPONRAND_GUN)
		m_iNewWeapons = RANDOM_LONG(HGRUNT_WEAPON_MP5, HGRUNT_WEAPON_PYTHON);
	else if (m_iNewWeapons == HGRUNT_WEAPONRAND_ALIEN)
		m_iNewWeapons = RANDOM_LONG(HGRUNT_WEAPON_HIVEHAND, HGRUNT_WEAPON_SPORELAUNCHER);

	m_bBodyGroup_Gun = HGRUNT_WEAPONBODY_NONE;
	m_bBodyGroup_Holster = HGRUNT_WEAPONBODY_NONE;
	m_bUsingPistol = false;
	m_bCanHolster = false;
	m_cClipSize = 1;
	m_chzReloadAnimation = ALLOC_STRING("reload_mp5");
	m_chzReloadSound = ALLOC_STRING("hgrunt/gr_reload1.wav");
	m_chzGunSound = ALLOC_STRING("weapons/desert_eagle_fire.wav");

	switch (m_iNewWeapons)
	{
		case HGRUNT_WEAPON_EAGLE:
			m_bCanHolster = true;
			m_bUsingPistol = true;
			m_cClipSize = EAGLE_MAX_CLIP;
			m_chzReloadAnimation = ALLOC_STRING("reload_pistol");
			m_chzReloadSound = ALLOC_STRING("weapons/desert_eagle_reload.wav");

			m_bBodyGroup_Gun = HGRUNT_WEAPONBODY_EAGLE;
			m_bBodyGroup_Holster = HGRUNT_WEAPONBODY_EAGLE_HOLSTERED;
			
			if (gpgs && gpgs->m_iszEagleSound)
				(m_chzGunSound) = (gpgs->m_iszEagleSound);

			(m_chzWeaponClassname) = ALLOC_STRING("weapon_eagle");
			break;

		case HGRUNT_WEAPON_PYTHON:
			m_bCanHolster = true;
			m_bUsingPistol = true;
			m_cClipSize = PYTHON_MAX_CLIP;
			m_chzReloadAnimation = ALLOC_STRING("reload_pistol");
			m_chzReloadSound = ALLOC_STRING("weapons/desert_eagle_reload.wav");

			m_bBodyGroup_Gun = HGRUNT_WEAPONBODY_PYTHON;
			m_bBodyGroup_Holster = HGRUNT_WEAPONBODY_PYTHON_HOLSTERED;
			
			if (gpgs && gpgs->m_isz357Sound)
				(m_chzGunSound) = (gpgs->m_isz357Sound);
			else
				(m_chzGunSound) = ALLOC_STRING("weapons/357_shot1.wav");

			(m_chzWeaponClassname) = ALLOC_STRING("weapon_357");
			break;

		case HGRUNT_WEAPON_GLOCK:
			m_bCanHolster = true;
			m_bUsingPistol = true;
			m_cClipSize = GLOCK_MAX_CLIP;
			m_chzReloadAnimation = ALLOC_STRING("reload_pistol");
			m_chzReloadSound = ALLOC_STRING("weapons/desert_eagle_reload.wav");

			m_bBodyGroup_Gun = HGRUNT_WEAPONBODY_GLOCK;
			m_bBodyGroup_Holster = HGRUNT_WEAPONBODY_GLOCK_HOLSTERED;

			if (gpgs && gpgs->m_iszGlockSound)
				(m_chzGunSound) = (gpgs->m_iszGlockSound);
			else
				(m_chzGunSound) = ALLOC_STRING("weapons/pl_gun3.wav");

			(m_chzWeaponClassname) = ALLOC_STRING("weapon_9mmhandgun");
			break;

		case HGRUNT_WEAPON_SHOTGUN:
			m_cClipSize = GRUNT_SHOTGUN_CLIP_SIZE;
			m_chzReloadAnimation = ALLOC_STRING("reload_m40a1");

			m_bBodyGroup_Gun = HGRUNT_WEAPONBODY_SHOTGUN;

			if (gpgs && gpgs->m_iszShotgunSoundPri)
				(m_chzGunSound) = (gpgs->m_iszShotgunSoundPri);
			else
				(m_chzGunSound) = ALLOC_STRING("weapons/sbarrel1.wav");

			(m_chzWeaponClassname) = ALLOC_STRING("weapon_shotgun");
			break;

		case HGRUNT_WEAPON_M249:
			m_cClipSize = GRUNT_SAW_CLIP_SIZE;

			m_bBodyGroup_Gun = HGRUNT_WEAPONBODY_M249;
			m_chzReloadSound = ALLOC_STRING("weapons/saw_reload.wav");

			if (gpgs && gpgs->m_iszM249Sound)
				(m_chzGunSound) = (gpgs->m_iszM249Sound);
			else
				(m_chzGunSound) = ALLOC_STRING("weapons/saw_fire1.wav");

			(m_chzWeaponClassname) = ALLOC_STRING("weapon_m249");
			break;

		case HGRUNT_WEAPON_SNIPER:
			m_chzReloadAnimation = ALLOC_STRING("reload_m40a1");

			m_bBodyGroup_Gun = HGRUNT_WEAPONBODY_SNIPER;

			if (gpgs && gpgs->m_iszSniperSound)
				(m_chzGunSound) = (gpgs->m_iszSniperSound);
			else
				(m_chzGunSound) = ALLOC_STRING("weapons/sniper_fire.wav");

			(m_chzWeaponClassname) = ALLOC_STRING("weapon_sniperrifle");
			break;

		case HGRUNT_WEAPON_HIVEHAND:
			m_bUsingPistol = true;
			m_chzReloadSound = ALLOC_STRING("common/null.wav");

			m_bBodyGroup_Gun = HGRUNT_WEAPONBODY_HIVEHAND;

			(m_chzGunSound) = ALLOC_STRING("agrunt/ag_fire1.wav");

			(m_chzWeaponClassname) = ALLOC_STRING("weapon_hornetgun");
			break;

		case HGRUNT_WEAPON_SHOCKROACH:
			m_bBodyGroup_Gun = HGRUNT_WEAPONBODY_SHOCKRIFLE;
			m_chzReloadSound = ALLOC_STRING("common/null.wav");

			(m_chzGunSound) = ALLOC_STRING("weapons/shock_fire.wav");

			(m_chzWeaponClassname) = ALLOC_STRING("weapon_hornetgun");
			break;

		case HGRUNT_WEAPON_SPORELAUNCHER:
			m_bBodyGroup_Gun = HGRUNT_WEAPONBODY_SPORELAUNCHER;
			m_chzReloadSound = ALLOC_STRING("common/null.wav");

			(m_chzGunSound) = ALLOC_STRING("weapons/splauncher_fire.wav");

			(m_chzWeaponClassname) = ALLOC_STRING("weapon_sporelauncher");
			break;

		case HGRUNT_WEAPON_MP5:
		default:
			m_cClipSize = GRUNT_MP5_CLIP_SIZE;

			m_bBodyGroup_Gun = HGRUNT_WEAPONBODY_MP5;

			if (gpgs && gpgs->m_iszMP5Sound)
				(m_chzGunSound) = (gpgs->m_iszMP5Sound);
			else
				(m_chzGunSound) = ALLOC_STRING("weapons/hks1.wav");

			(m_chzWeaponClassname) = ALLOC_STRING("weapon_9mmAR");
			break;
	}

	if (!m_bUsingCustomModel)
		SetBodygroup( HGRUNT_BODYGROUP_WEAPONS, m_bBodyGroup_Gun );

	PRECACHE_SOUND(STRING(m_chzGunSound));

	m_cAmmoLoaded = m_cClipSize;
}

void CHGruntExtended::SetupHead()
{
	if (m_bUsingCustomModel)
		return;

	if ( m_iSkintone == 0 )
	{
		int i = RANDOM_LONG(0, 2);

		if (i == 2)
			pev->skin = 1;
		else
			pev->skin = 0;
	}
	else
	{
		if (m_iSkintone == 2)
			pev->skin = 1;
		else
			pev->skin = 0;
	}

	if (m_iHead == HGRUNT_HEADRAND_MATCHWEAPON)
	{

		if (m_iGruntType == HGRUNT_TYPE_CLASSIC)
		{
			switch (m_iNewWeapons)
			{
			case HGRUNT_WEAPON_MP5:
			case HGRUNT_WEAPON_GLOCK:
			case HGRUNT_WEAPON_EAGLE:
			case HGRUNT_WEAPON_PYTHON:
			{
				m_iHead = HGRUNT_HEADBODY_CLASSICGASMASK;
			}
			break;

			case HGRUNT_WEAPON_SNIPER:
			case HGRUNT_WEAPON_SHOTGUN:
			{
				m_iHead = HGRUNT_HEADBODY_CLASSICMASKED;
			}
			break;

			case HGRUNT_WEAPON_SPORELAUNCHER:
			case HGRUNT_WEAPON_SHOCKROACH:
			case HGRUNT_WEAPON_HIVEHAND:
			case HGRUNT_WEAPON_M249:
			{
				m_iHead = HGRUNT_HEADBODY_CLASSICTOWER;
			}
			break;
			}
		}
		else if (m_iGruntType == HGRUNT_TYPE_ASSASSIN)
		{
			switch (m_iNewWeapons)
			{
			case HGRUNT_WEAPON_MP5:
			case HGRUNT_WEAPON_SHOTGUN:
			case HGRUNT_WEAPON_M249:
			case HGRUNT_WEAPON_GLOCK:
			case HGRUNT_WEAPON_EAGLE:
			case HGRUNT_WEAPON_PYTHON:
			{
				m_iHead = HGRUNT_HEADBODY_ASSASSIN;
			}
			break;

			case HGRUNT_WEAPON_SNIPER:
			case HGRUNT_WEAPON_SPORELAUNCHER:
			case HGRUNT_WEAPON_SHOCKROACH:
			case HGRUNT_WEAPON_HIVEHAND:
			{
				m_iHead = HGRUNT_HEADBODY_NIGHTVISION;
			}
			break;
			}
		}
		else
		{
			switch (m_iNewWeapons)
			{
			case HGRUNT_WEAPON_SNIPER:
			case HGRUNT_WEAPON_SHOTGUN:
			{
				m_iHead = HGRUNT_HEADBODY_MASKED;
			}
			break;

			case HGRUNT_WEAPON_M249:
			{
				int k = RANDOM_LONG(0, 3);
				if (k == 3)
					m_iHead = HGRUNT_HEADBODY_TORCH;
				else
					m_iHead = HGRUNT_HEADBODY_TOWER;
			}
			break;

			case HGRUNT_WEAPON_GLOCK:
			case HGRUNT_WEAPON_EAGLE:
			case HGRUNT_WEAPON_PYTHON:
			{
				if (strncmp(STRING(pev->model), "models/hgrunt2/medic.mdl", 25) == 0)
					m_iHead = HGRUNT_HEADBODY_MEDIC;
				else
					m_iHead = HGRUNT_HEADBODY_MP;
			}
			break;

			case HGRUNT_WEAPON_MP5:
			case HGRUNT_WEAPON_SPORELAUNCHER:
			case HGRUNT_WEAPON_SHOCKROACH:
			case HGRUNT_WEAPON_HIVEHAND:
			{
				m_iHead = HGRUNT_HEADBODY_GASMASK;
			}
			break;
			}
		}
	}
	else
	{
		if (m_iHead == HGRUNT_HEADRAND_MATCHTYPE && !m_bIsFriendly)
		{
			switch (m_iGruntType)
			{
			case HGRUNT_TYPE_REGULAR: m_iHead = HGRUNT_HEADRAND_REGULAR; break;
			case HGRUNT_TYPE_CLASSIC: m_iHead = HGRUNT_HEADRAND_CLASSIC; break;
			case HGRUNT_TYPE_ASSASSIN: m_iHead = HGRUNT_HEADRAND_ASSASSIN; break;
			}
		}
		else if (m_iHead == HGRUNT_HEADRAND_MATCHTYPE && m_bIsFriendly)
		{
			switch (m_iGruntType)
			{
			case HGRUNT_TYPE_REGULAR: m_iHead = HGRUNT_HEADRAND_REGULAR; break;
			case HGRUNT_TYPE_CLASSIC: m_iHead = HGRUNT_HEADRAND_CLASSIC; break;
			case HGRUNT_TYPE_ASSASSIN: m_iHead = HGRUNT_HEADRAND_ASSASSIN; break;
			case HGRUNT_TYPE_MEDIC: m_iHead = HGRUNT_HEADBODY_MEDIC; break;
			case HGRUNT_TYPE_TORCH: m_iHead = HGRUNT_HEADBODY_TORCH; break;
			}
		}

		if (m_iHead == HGRUNT_HEADRAND_ALL)
		{
			m_iHead = RANDOM_LONG(HGRUNT_HEADBODY_GASMASK, HGRUNT_HEADBODY_NIGHTVISION);
		}
		else if (m_iHead == HGRUNT_HEADRAND_REGULAR && m_bIsFriendly)
		{
			int o = RANDOM_LONG(0, 3);
			switch (o)
			{
			case 0: m_iHead = HGRUNT_HEADBODY_GASMASK; break;
			case 1: m_iHead = HGRUNT_HEADBODY_MASKED; break;
			case 2: m_iHead = HGRUNT_HEADBODY_TOWER; break;
			case 3: m_iHead = HGRUNT_HEADBODY_MP; break;
			}
		}
		else if (m_iHead == HGRUNT_HEADRAND_REGULAR)
		{
			int p = RANDOM_LONG(0, 5);
			switch (p)
			{
			case 0: m_iHead = HGRUNT_HEADBODY_GASMASK; break;
			case 1: m_iHead = HGRUNT_HEADBODY_MASKED; break;
			case 2: m_iHead = HGRUNT_HEADBODY_TOWER; break;
			case 3: m_iHead = HGRUNT_HEADBODY_MP; break;
			case 4: m_iHead = HGRUNT_HEADBODY_MEDIC; break;
			case 5: m_iHead = HGRUNT_HEADBODY_TORCH; break;
			}
		}
		else if (m_iHead == HGRUNT_HEADRAND_CLASSIC)
		{
			int q = RANDOM_LONG(0, 2);
			switch (q)
			{
			case 0: m_iHead = HGRUNT_HEADBODY_CLASSICGASMASK; break;
			case 1: m_iHead = HGRUNT_HEADBODY_CLASSICMASKED; break;
			case 2: m_iHead = HGRUNT_HEADBODY_CLASSICTOWER; break;
			}
		}
		else if (m_iHead == HGRUNT_HEADRAND_ASSASSIN)
		{
			m_iHead = RANDOM_LONG(HGRUNT_HEADBODY_ASSASSIN, HGRUNT_HEADBODY_NIGHTVISION);
		}
	}

	if (pev->spawnflags & SF_SQUADMONSTER_LEADER)
	{
		if (m_iGruntType == HGRUNT_TYPE_REGULAR || m_iGruntType == HGRUNT_TYPE_MEDIC || m_iGruntType == HGRUNT_TYPE_TORCH)
		{
			int n = RANDOM_LONG(0, 1);
			if (n)
				m_iHead = HGRUNT_HEADBODY_COMMANDER;
			else
				m_iHead = HGRUNT_HEADBODY_MAJOR;
		}
		else if (m_iGruntType == HGRUNT_TYPE_CLASSIC)
		{
			m_iHead = HGRUNT_HEADBODY_CLASSICCOMMANDER;
		}
	}
	
	//Remaps to fix special model head overflows
	if (m_iGruntType == HGRUNT_TYPE_MEDIC || m_iGruntType == HGRUNT_TYPE_TORCH)
	{
		if (m_iHead == HGRUNT_HEADBODY_CLASSICCOMMANDER)
			m_iHead = HGRUNT_HEADBODY_COMMANDER;

		if (m_iHead == HGRUNT_HEADBODY_ASSASSIN || m_iHead == HGRUNT_HEADBODY_NIGHTVISION || m_iHead == HGRUNT_HEADBODY_CLASSICMASKED)
			m_iHead = HGRUNT_HEADBODY_MASKED;

		if (m_iHead == HGRUNT_HEADBODY_CLASSICTOWER)
			m_iHead = HGRUNT_HEADBODY_TOWER;

		if (m_iHead == HGRUNT_HEADBODY_CLASSICGASMASK)
			m_iHead = HGRUNT_HEADBODY_GASMASK;
	}

	SetBodygroup(HGRUNT_BODYGROUP_HEAD, m_iHead);
}


void CHGruntExtended::SetupOutfit()
{
	if (m_bUsingCustomModel)
		return;

	bool bCustomModel = false;

	int outfitrand = 25565;

	if (strncmp (STRING(pev->model),"models/editormodels/_match_weapon.mdl", 38) == 0)
	{
		bCustomModel = true;
		if (m_iGruntType == HGRUNT_TYPE_CLASSIC)
		{
			outfitrand = HGRUNT_TORSOBODY_CLASSIC;
		}
		else if (m_iGruntType == HGRUNT_TYPE_ASSASSIN)
		{
			outfitrand = HGRUNT_TORSOBODY_ASSASSIN;
		}
		else
		{
			switch (m_iNewWeapons)
			{
			case HGRUNT_WEAPON_SNIPER:
			case HGRUNT_WEAPON_MP5:
			{
				int i = RANDOM_LONG(0, 3);
				if (i == 3)
					outfitrand = HGRUNT_TORSOBODY_NOPACK;
				else
					outfitrand = HGRUNT_TORSOBODY_REGULAR;
			}
			break;

			case HGRUNT_WEAPON_SHOTGUN:
			{
				outfitrand = HGRUNT_TORSOBODY_SHOTGUNNER;
			}
			break;

			case HGRUNT_WEAPON_M249:
			{
				outfitrand = HGRUNT_TORSOBODY_MACHINEGUN;
			}
			break;

			case HGRUNT_WEAPON_GLOCK:
			case HGRUNT_WEAPON_EAGLE:
			case HGRUNT_WEAPON_PYTHON:
			{
				int q = RANDOM_LONG(0, 1);

				if (q)
					outfitrand = HGRUNT_TORSOBODY_MEDIC;
				else
					outfitrand = HGRUNT_TORSOBODY_MP;
			}
			break;

			case HGRUNT_WEAPON_SPORELAUNCHER:
			case HGRUNT_WEAPON_SHOCKROACH:
			case HGRUNT_WEAPON_HIVEHAND:
			{
				outfitrand = HGRUNT_TORSOBODY_TORCH;
			}
			break;

			default:
			{
				ALERT(at_console, "Extended HGrunt has invaild weapon, can't match outfit!\n");
				outfitrand = HGRUNT_TORSOBODY_REGULAR;
			}
			break;
			}
		}
	}
	else
	{
		if ((strncmp(STRING(pev->model), "models/editormodels/_match_type.mdl", 36) == 0) && !m_bIsFriendly)
		{
			bCustomModel = true;
			switch (m_iGruntType)
			{
			case HGRUNT_TYPE_REGULAR: outfitrand = HGRUNT_TORSORAND_REGULAR; break;
			case HGRUNT_TYPE_CLASSIC: outfitrand = HGRUNT_TORSOBODY_CLASSIC; break;
			case HGRUNT_TYPE_ASSASSIN: outfitrand = HGRUNT_TORSOBODY_ASSASSIN; break;
			}
		}

		if ((strncmp(STRING(pev->model), "models/editormodels/_match_type.mdl", 36) == 0) && m_bIsFriendly)
		{
			bCustomModel = true;
			switch (m_iGruntType)
			{
			case HGRUNT_TYPE_REGULAR: outfitrand = HGRUNT_TORSORAND_REGULAR; break;
			case HGRUNT_TYPE_CLASSIC: outfitrand = HGRUNT_TORSOBODY_CLASSIC; break;
			case HGRUNT_TYPE_ASSASSIN: outfitrand = HGRUNT_TORSOBODY_ASSASSIN; break;
			case HGRUNT_TYPE_MEDIC: outfitrand = HGRUNT_TORSOBODY_MEDIC; break;
			case HGRUNT_TYPE_TORCH: outfitrand = HGRUNT_TORSOBODY_TORCH; break;
			}
		}

		if (strncmp(STRING(pev->model), "models/editormodels/_random_all_g.mdl", 38) == 0)
		{
			bCustomModel = true;
			outfitrand = RANDOM_LONG(HGRUNT_TORSOBODY_REGULAR, HGRUNT_TORSOBODY_ASSASSIN);
		}

		if ((strncmp(STRING(pev->model), "models/editormodels/_random_regular.mdl", 40) == 0) && m_bIsFriendly)
		{
			bCustomModel = true;
			int j = RANDOM_LONG(0, 4);
			if (j == 4)
				outfitrand = HGRUNT_TORSOBODY_MP;
			else
				outfitrand = RANDOM_LONG(HGRUNT_TORSOBODY_REGULAR, HGRUNT_TORSOBODY_SHOTGUNNER);
		}
		else if ((strncmp(STRING(pev->model), "models/editormodels/_random_regular.mdl", 40) == 0) && !m_bIsFriendly)
			outfitrand = RANDOM_LONG(HGRUNT_TORSOBODY_REGULAR, HGRUNT_TORSOBODY_MP);
		
	}

	string_t tempString;

	if (!(outfitrand == 25565))
	{
		switch (outfitrand)
		{
		case HGRUNT_TORSOBODY_REGULAR: tempString = ALLOC_STRING("models/hgrunt2/regular"); break;
		case HGRUNT_TORSOBODY_MACHINEGUN: tempString = ALLOC_STRING("models/hgrunt2/machinegunner"); break;
		case HGRUNT_TORSOBODY_NOPACK: tempString = ALLOC_STRING("models/hgrunt2/nobackpack"); break;
		case HGRUNT_TORSOBODY_SHOTGUNNER: tempString = ALLOC_STRING("models/hgrunt2/shotgunner"); break;
		case HGRUNT_TORSOBODY_MEDIC: tempString = ALLOC_STRING("models/hgrunt2/medic"); break;
		case HGRUNT_TORSOBODY_TORCH: tempString = ALLOC_STRING("models/hgrunt2/torch"); break;
		case HGRUNT_TORSOBODY_MP: tempString = ALLOC_STRING("models/hgrunt2/police"); break;
		case HGRUNT_TORSOBODY_CLASSIC: tempString = ALLOC_STRING("models/hgrunt2/classic"); break;
		case HGRUNT_TORSOBODY_ASSASSIN: tempString = ALLOC_STRING("models/hgrunt2/assassin"); break;
		default: 
			//ALERT(at_console, "HGRUNT OUTFIT RAND FAILED!!\n"); 
			tempString = ALLOC_STRING("models/hgrunt2/regular"); 
			break;
		}
	}

//	ALERT(at_console, "(SETUPOUTFIT) BEFORE MERGE CHECK: HGRUNT MODEL: pev->body %s outfitrand %d tempString %s bCustomModel %d \n", 
//		STRING(pev->model), outfitrand, STRING(tempString), bCustomModel);

	char szBuf2[64];
	char szBuf[64];

	if (bCustomModel)
	{
		strcpy(szBuf2, STRING(tempString));
		strcat(szBuf2, "_f");

		if (m_bIsFriendly)
			tempString = ALLOC_STRING(szBuf2);

		strcpy(szBuf, STRING(tempString));
		strcat(szBuf, ".mdl");
	}


//	ALERT(at_console, "(SETUPOUTFIT) AFTER MERGE CHECK: HGRUNT MODEL: pev->body %s outfitrand %d szBuf2 %s szBuf %s tempString %s CustomModel %d \n", 
//		STRING(pev->model), outfitrand, szBuf2, szBuf, STRING(tempString), bCustomModel);

	if (bCustomModel)
		pev->model = ALLOC_STRING(szBuf);

	if (m_iGruntType == HGRUNT_TYPE_TORCH)
		pev->model = ALLOC_STRING("models/hgrunt2/torch_special.mdl");
	else if (m_iGruntType == HGRUNT_TYPE_MEDIC)
		pev->model = ALLOC_STRING("models/hgrunt2/medic_special.mdl");
}



//=========================================================
// CheckRangeAttack1 - overridden for HGrunt, cause 
// FCanCheckAttacks() doesn't disqualify all attacks based
// on whether or not the enemy is occluded because unlike
// the base class, the HGrunt can attack when the enemy is
// occluded (throw grenade over wall, etc). We must 
// disqualify the machine gun attack if the enemy is occluded.
//=========================================================
BOOL CHGruntExtended::CheckRangeAttack1(float flDot, float flDist)
{
	if (m_fGunHolstered)
		return FALSE;

	//Sporelauncher uses a hacked grenade throw to launch
	if (m_iNewWeapons == HGRUNT_WEAPON_SPORELAUNCHER)
		return FALSE;

	const auto maxDistance = m_iNewWeapons & HGRUNT_WEAPON_SHOTGUN ? 640 : 1024;

	//Friendly fire is allowed
	if (!HasConditions(bits_COND_ENEMY_OCCLUDED) && flDist <= maxDistance && flDot >= 0.5 /*&& NoFriendlyFire()*/)
	{
		TraceResult	tr;

		auto pEnemy = m_hEnemy.Entity<CBaseEntity>();

		if (!pEnemy->IsPlayer() && flDist <= 64)
		{
			// kick nonclients, but don't shoot at them.
			return FALSE;
		}

		//TODO: kinda odd that this doesn't use GetGunPosition like the original
		Vector vecSrc = pev->origin + Vector(0, 0, 55);

		//Fire at last known position, adjusting for target origin being offset from entity origin
		const auto targetOrigin = pEnemy->BodyTarget(vecSrc);

		const auto targetPosition = targetOrigin - pEnemy->pev->origin + m_vecEnemyLKP;

		// verify that a bullet fired from the gun will hit the enemy before the world.
		UTIL_TraceLine(vecSrc, targetPosition, dont_ignore_monsters, ENT(pev), &tr);

		m_lastAttackCheck = tr.flFraction == 1.0 ? true : tr.pHit && GET_PRIVATE(tr.pHit) == pEnemy;

		return m_lastAttackCheck;
	}

	return FALSE;
}

//=========================================================
// CheckRangeAttack2 - this checks the Grunt's grenade
// attack. 
//=========================================================
BOOL CHGruntExtended :: CheckRangeAttack2 ( float flDot, float flDist )
{
	if (m_fGunHolstered || m_iGrenadeType == HGRUNT_GRENADE_NONE )
	{
		return FALSE;
	}
	
	// if the grunt isn't moving, it's ok to check.
	if ( m_flGroundSpeed != 0 )
	{
		m_fThrowGrenade = FALSE;
		return m_fThrowGrenade;
	}

	// assume things haven't changed too much since last time
	if (gpGlobals->time < m_flNextGrenadeCheck )
	{
		return m_fThrowGrenade;
	}

	if ( !FBitSet ( m_hEnemy->pev->flags, FL_ONGROUND ) && m_hEnemy->pev->waterlevel == 0 && m_vecEnemyLKP.z > pev->absmax.z  )
	{
		//!!!BUGBUG - we should make this check movetype and make sure it isn't FLY? Players who jump a lot are unlikely to 
		// be grenaded.
		// don't throw grenades at anything that isn't on the ground!
		m_fThrowGrenade = FALSE;
		return m_fThrowGrenade;
	}
	
	Vector vecTarget;

	if ( m_iGrenadeType == HGRUNT_GRENADE_LAUNCHER || m_iGrenadeType == HGRUNT_GRENADE_SPORE_LAUNCHER )
	{
		// find target
		// vecTarget = m_hEnemy->BodyTarget( pev->origin );
		vecTarget = m_vecEnemyLKP + (m_hEnemy->BodyTarget(pev->origin) - m_hEnemy->pev->origin);
		// estimate position
		if (HasConditions(bits_COND_SEE_ENEMY))
			vecTarget = vecTarget + ((vecTarget - pev->origin).Length() / gSkillData.hgruntAllyGrenadeSpeed) * m_hEnemy->pev->velocity;
	}
	else
	{
		// find feet
		if (RANDOM_LONG(0,1))
		{
			// magically know where they are
			vecTarget = Vector( m_hEnemy->pev->origin.x, m_hEnemy->pev->origin.y, m_hEnemy->pev->absmin.z );
		}
		else
		{
			// toss it to where you last saw them
			vecTarget = m_vecEnemyLKP;
		}
		// vecTarget = m_vecEnemyLKP + (m_hEnemy->BodyTarget( pev->origin ) - m_hEnemy->pev->origin);
		// estimate position
		// vecTarget = vecTarget + m_hEnemy->pev->velocity * 2;
	}

	// are any of my squad members near the intended grenade impact area?
	if ( InSquad() )
	{
		if (SquadMemberInRange( vecTarget, 256 ))
		{
			// crap, I might blow my own guy up. Don't throw a grenade and don't check again for a while.
			m_flNextGrenadeCheck = gpGlobals->time + 1; // one full second.
			m_fThrowGrenade = FALSE;
		}
	}
	
	if ( ( vecTarget - pev->origin ).Length2D() <= 256 )
	{
		// crap, I don't want to blow myself up
		m_flNextGrenadeCheck = gpGlobals->time + 1; // one full second.
		m_fThrowGrenade = FALSE;
		return m_fThrowGrenade;
	}

		
	if ( m_iGrenadeType == HGRUNT_GRENADE_LAUNCHER || m_iGrenadeType == HGRUNT_GRENADE_SPORE_LAUNCHER )
	{
		Vector vecToss = VecCheckThrow( pev, GetGunPosition(), vecTarget, gSkillData.hgruntAllyGrenadeSpeed, 0.5 );

		if ( vecToss != g_vecZero )
		{
			m_vecTossVelocity = vecToss;

			// throw a hand grenade
			m_fThrowGrenade = TRUE;
			// don't check again for a while.
			m_flNextGrenadeCheck = gpGlobals->time + 0.3; // 1/3 second.
		}
		else
		{
			// don't throw
			m_fThrowGrenade = FALSE;
			// don't check again for a while.
			m_flNextGrenadeCheck = gpGlobals->time + 1; // one full second.
		}
	}
	else
	{
		Vector vecToss = VecCheckToss( pev, GetGunPosition(), vecTarget, 0.5 );

		if ( vecToss != g_vecZero )
		{
			m_vecTossVelocity = vecToss;

			// throw a hand grenade
			m_fThrowGrenade = TRUE;
			// don't check again for a while.
			m_flNextGrenadeCheck = gpGlobals->time; // 1/3 second.
		}
		else
		{
			// don't throw
			m_fThrowGrenade = FALSE;
			// don't check again for a while.
			m_flNextGrenadeCheck = gpGlobals->time + 1; // one full second.
		}
	}

	return m_fThrowGrenade;
}


//=========================================================
// Get Schedule!
//=========================================================
Schedule_t *CHGruntExtended :: GetSchedule()
{

	// clear old sentence
	m_iSentence = -1; //HGRUNT_SENT_NONE;

	// flying? If PRONE, barnacle has me. IF not, it's assumed I am rapelling. 
	if ( pev->movetype == MOVETYPE_FLY && m_MonsterState != MONSTERSTATE_PRONE )
	{
		if (pev->flags & FL_ONGROUND)
		{
			// just landed
			pev->movetype = MOVETYPE_STEP;
			return GetScheduleOfType ( SCHED_GRUNT_REPEL_LAND );
		}
		else
		{
			// repel down a rope, 
			if ( m_MonsterState == MONSTERSTATE_COMBAT )
				return GetScheduleOfType ( SCHED_GRUNT_REPEL_ATTACK );
			else
				return GetScheduleOfType ( SCHED_GRUNT_REPEL );
		}
	}

	if( m_fHealing )
	{
		if( m_hTargetEnt )
		{
			auto pHealTarget = m_hTargetEnt.Entity<CBaseEntity>();

			if( ( pHealTarget->pev->origin - pev->origin ).Make2D().Length() <= 50.0
				&& ( !m_fUseHealing || gpGlobals->time - m_flLastUseTime <= 0.25 )
				&& m_iHealCharge
				&& pHealTarget->IsAlive()
				&& pHealTarget->pev->health != pHealTarget->pev->max_health )
			{
				return slMedicAllyHealTargetExtended;
			}
		}

		return slMedicAllyDrawGunExtended;
	}

	// grunts place HIGH priority on running away from danger sounds.
	if ( HasConditions(bits_COND_HEAR_SOUND) )
	{
		CSound *pSound;
		pSound = PBestSound();

		ASSERT( pSound != NULL );
		if ( pSound)
		{
			if (pSound->m_iType & bits_SOUND_DANGER)
			{
				// dangerous sound nearby!
				
				//!!!KELLY - currently, this is the grunt's signal that a grenade has landed nearby,
				// and the grunt should find cover from the blast
				// good place for "SHIT!" or some other colorful verbal indicator of dismay.
				// It's not safe to play a verbal order here "Scatter", etc cause 
				// this may only affect a single individual in a squad. 
				
				if (FOkToSpeak())
				{
					SENTENCEG_PlayRndSz( ENT(pev), m_szGrp[TLK_GREN], HGRUNT_SENTENCE_VOLUME, GRUNT_ATTN, 0, m_voicePitch);
					JustSpoke();
				}
				return GetScheduleOfType( SCHED_TAKE_COVER_FROM_BEST_SOUND );
			}
			/*
			if (!HasConditions( bits_COND_SEE_ENEMY ) && ( pSound->m_iType & (bits_SOUND_PLAYER | bits_SOUND_COMBAT) ))
			{
				MakeIdealYaw( pSound->m_vecOrigin );
			}
			*/
		}
	}
	switch	( m_MonsterState )
	{
	case MONSTERSTATE_COMBAT:
		{
// dead enemy
			if ( HasConditions( bits_COND_ENEMY_DEAD ) )
			{
				if( FOkToSpeak() )
				{
					PlaySentence( m_szGrp[TLK_KILL], 4, VOL_NORM, ATTN_NORM );
				}

				// call base class, all code to handle dead enemies is centralized there.
				return COFSquadTalkMonster :: GetSchedule();
			}

			if( m_hWaitMedic )
			{
				auto pMedic = m_hWaitMedic.Entity<COFSquadTalkMonster>();

				if( pMedic->pev->deadflag != DEAD_NO )
					m_hWaitMedic = nullptr;
				else
					pMedic->HealMe( nullptr );

				m_flMedicWaitTime = gpGlobals->time + 5.0;
			}

			if (!m_fTorchHolstered)
			{
				return COFSquadTalkMonster::GetSchedule();
			}

// new enemy
			//Do not fire until fired upon
			if ( HasAllConditions( bits_COND_NEW_ENEMY | bits_COND_LIGHT_DAMAGE ) )
			{
				if ( InSquad() )
				{
					MySquadLeader()->m_fEnemyEluded = FALSE;

					if ( !IsLeader() )
					{
						return GetScheduleOfType ( SCHED_TAKE_COVER_FROM_ENEMY );
					}
					else 
					{
						//!!!KELLY - the leader of a squad of grunts has just seen the player or a 
						// monster and has made it the squad's enemy. You
						// can check pev->flags for FL_CLIENT to determine whether this is the player
						// or a monster. He's going to immediately start
						// firing, though. If you'd like, we can make an alternate "first sight" 
						// schedule where the leader plays a handsign anim
						// that gives us enough time to hear a short sentence or spoken command
						// before he starts pluggin away.
						if (FOkToSpeak())// && RANDOM_LONG(0,1))
						{
							if ((m_hEnemy != NULL) && m_hEnemy->IsPlayer())
								// player
								SENTENCEG_PlayRndSz( ENT(pev), m_szGrp[TLK_ALERT], HGRUNT_SENTENCE_VOLUME, GRUNT_ATTN, 0, m_voicePitch);
							else if ((m_hEnemy != NULL) &&
									(m_hEnemy->Classify() != CLASS_PLAYER_ALLY) && 
									(m_hEnemy->Classify() != CLASS_HUMAN_PASSIVE) && 
									(m_hEnemy->Classify() != CLASS_MACHINE))
								// monster
								SENTENCEG_PlayRndSz( ENT(pev), m_szGrp[TLK_MONSTER], HGRUNT_SENTENCE_VOLUME, GRUNT_ATTN, 0, m_voicePitch);

							JustSpoke();
						}
						
						if ( HasConditions ( bits_COND_CAN_RANGE_ATTACK1 ) )
						{
							return GetScheduleOfType ( SCHED_GRUNT_SUPPRESS );
						}
						else
						{
							return GetScheduleOfType ( SCHED_GRUNT_ESTABLISH_LINE_OF_FIRE );
						}
					}
				}

				return GetScheduleOfType( SCHED_SMALL_FLINCH );
			}

			else if( HasConditions( bits_COND_HEAVY_DAMAGE ) )
				return GetScheduleOfType( SCHED_TAKE_COVER_FROM_ENEMY );
// no ammo
			//Only if the grunt has a weapon
			else if ( HasConditions ( bits_COND_NO_AMMO_LOADED ) )
			{
				//!!!KELLY - this individual just realized he's out of bullet ammo. 
				// He's going to try to find cover to run to and reload, but rarely, if 
				// none is available, he'll drop and reload in the open here. 
				return GetScheduleOfType ( SCHED_GRUNT_COVER_AND_RELOAD );
			}
			
// damaged just a little
			else if ( HasConditions( bits_COND_LIGHT_DAMAGE ) )
			{
				// if hurt:
				// 90% chance of taking cover
				// 10% chance of flinch.
				int iPercent = RANDOM_LONG(0,99);

				if ( iPercent <= 90 && m_hEnemy != NULL )
				{
					// only try to take cover if we actually have an enemy!

					//!!!KELLY - this grunt was hit and is going to run to cover.
					if (FOkToSpeak()) // && RANDOM_LONG(0,1))
					{
						//SENTENCEG_PlayRndSz( ENT(pev), m_szGrp[TLK_COVER], HGRUNT_SENTENCE_VOLUME, GRUNT_ATTN, 0, m_voicePitch);
						m_iSentence = 3; // HGRUNT_SENT_COVER;
						//JustSpoke();
					}
					return GetScheduleOfType( SCHED_TAKE_COVER_FROM_ENEMY );
				}
				else
				{
					return GetScheduleOfType( SCHED_SMALL_FLINCH );
				}
			}
// can kick
			else if ( HasConditions ( bits_COND_CAN_MELEE_ATTACK1 ) )
			{
				return GetScheduleOfType ( SCHED_MELEE_ATTACK1 );
			}
// can grenade launch

			else if ( ( m_iGrenadeType == HGRUNT_GRENADE_LAUNCHER ) && HasConditions ( bits_COND_CAN_RANGE_ATTACK2 ) && OccupySlot( bits_SLOTS_HGRUNT_GRENADE ) )
			{
				// shoot a grenade if you can
				return GetScheduleOfType( SCHED_RANGE_ATTACK2 );
			}
// can shoot
			else if ( HasConditions ( bits_COND_CAN_RANGE_ATTACK1 ) )
			{
				if ( InSquad() )
				{
					// if the enemy has eluded the squad and a squad member has just located the enemy
					// and the enemy does not see the squad member, issue a call to the squad to waste a 
					// little time and give the player a chance to turn.
					if ( MySquadLeader()->m_fEnemyEluded && !HasConditions ( bits_COND_ENEMY_FACING_ME ) )
					{
						MySquadLeader()->m_fEnemyEluded = FALSE;
						return GetScheduleOfType ( SCHED_GRUNT_FOUND_ENEMY );
					}
				}

				if ( OccupySlot ( bits_SLOTS_HGRUNT_ENGAGE ) )
				{
					// try to take an available ENGAGE slot
					return GetScheduleOfType( SCHED_RANGE_ATTACK1 );
				}
				else if ( HasConditions ( bits_COND_CAN_RANGE_ATTACK2 ) && OccupySlot( bits_SLOTS_HGRUNT_GRENADE ) )
				{
					// throw a grenade if can and no engage slots are available
					return GetScheduleOfType( SCHED_RANGE_ATTACK2 );
				}
				else
				{
					// hide!
					return GetScheduleOfType( SCHED_TAKE_COVER_FROM_ENEMY );
				}
			}
// can't see enemy
			else if ( HasConditions( bits_COND_ENEMY_OCCLUDED ) )
			{
				if ( HasConditions( bits_COND_CAN_RANGE_ATTACK2 ) && OccupySlot( bits_SLOTS_HGRUNT_GRENADE ) )
				{
					//!!!KELLY - this grunt is about to throw or fire a grenade at the player. Great place for "fire in the hole"  "frag out" etc
					if (FOkToSpeak())
					{
						SENTENCEG_PlayRndSz( ENT(pev), m_szGrp[TLK_THROW], HGRUNT_SENTENCE_VOLUME, GRUNT_ATTN, 0, m_voicePitch);
						JustSpoke();
					}
					return GetScheduleOfType( SCHED_RANGE_ATTACK2 );
				}
				else if ( OccupySlot( bits_SLOTS_HGRUNT_ENGAGE ) )
				{
					//!!!KELLY - grunt cannot see the enemy and has just decided to 
					// charge the enemy's position. 
					if (FOkToSpeak())// && RANDOM_LONG(0,1))
					{
						//SENTENCEG_PlayRndSz( ENT(pev), m_szGrp[TLK_CHARGE], HGRUNT_SENTENCE_VOLUME, GRUNT_ATTN, 0, m_voicePitch);
						m_iSentence = 5; // HGRUNT_SENT_CHARGE;
						//JustSpoke();
					}

					return GetScheduleOfType( SCHED_GRUNT_ESTABLISH_LINE_OF_FIRE );
				}
				else
				{
					//!!!KELLY - grunt is going to stay put for a couple seconds to see if
					// the enemy wanders back out into the open, or approaches the
					// grunt's covered position. Good place for a taunt, I guess?
					if (FOkToSpeak() && RANDOM_LONG(0,1))
					{
						SENTENCEG_PlayRndSz( ENT(pev), m_szGrp[TLK_TAUNT], HGRUNT_SENTENCE_VOLUME, GRUNT_ATTN, 0, m_voicePitch);
						JustSpoke();
					}
					return GetScheduleOfType( SCHED_STANDOFF );
				}
			}
			
			//Only if not following a player
			if( !m_hTargetEnt || !m_hTargetEnt->IsPlayer() )
			{
				if( HasConditions( bits_COND_SEE_ENEMY ) && !HasConditions( bits_COND_CAN_RANGE_ATTACK1 ) )
				{
					return GetScheduleOfType( SCHED_GRUNT_ESTABLISH_LINE_OF_FIRE );
				}
			}

			//Don't fall through to idle schedules
			break;
		}

		case MONSTERSTATE_ALERT:
		case MONSTERSTATE_IDLE:
			if( HasConditions( bits_COND_LIGHT_DAMAGE | bits_COND_HEAVY_DAMAGE ) )
			{
				// flinch if hurt
				return GetScheduleOfType( SCHED_SMALL_FLINCH );
			}

			//if we're not waiting on a medic and we're hurt, call out for a medic
			if( !m_hWaitMedic
				&& gpGlobals->time > m_flMedicWaitTime
				&& pev->health <= 20.0 )
			{
				auto pMedic = MySquadMedic();

				if( !pMedic )
				{
					pMedic = FindSquadMedic( 1024 );
				}

				if( pMedic )
				{
					if( pMedic->pev->deadflag == DEAD_NO )
					{
						ALERT( at_aiconsole, "Injured Grunt found Medic\n" );

						if( pMedic->HealMe( this ) )
						{
							ALERT( at_aiconsole, "Injured Grunt called for Medic\n" );
							
							SENTENCEG_PlayRndSz(ENT(pev), m_szGrp[TLK_MEDIC], HGRUNT_SENTENCE_VOLUME, GRUNT_ATTN, 0, m_voicePitch);

							JustSpoke();
							m_flMedicWaitTime = gpGlobals->time + 5.0;
						}
					}
				}
			}

			if( m_hEnemy == NULL && IsFollowing() )
			{
				if( !m_hTargetEnt->IsAlive() )
				{
					// UNDONE: Comment about the recently dead player here?
					StopFollowing( FALSE );
					break;
				}
				else
				{
					if( HasConditions( bits_COND_CLIENT_PUSH ) )
					{
						return GetScheduleOfType( SCHED_MOVE_AWAY_FOLLOW );
					}
					return GetScheduleOfType( SCHED_TARGET_FACE );
				}
			}

			if( HasConditions( bits_COND_CLIENT_PUSH ) )
			{
				return GetScheduleOfType( SCHED_MOVE_AWAY );
			}

			// try to say something about smells
			TrySmellTalk();
			break;
	}
	
	// no special cases here, call the base class
	return COFSquadTalkMonster:: GetSchedule();
}




//=========================================================
// TakeDamage - overridden for the grunt because the grunt
// needs to forget that he is in cover if he's hurt. (Obviously
// not in a safe place anymore).
//=========================================================
int CHGruntExtended :: TakeDamage( entvars_t *pevInflictor, entvars_t *pevAttacker, float flDamage, int bitsDamageType )
{
	// make sure friends talk about it if player hurts talkmonsters...
	int ret = COFSquadTalkMonster::TakeDamage( pevInflictor, pevAttacker, flDamage, bitsDamageType );

	if( pev->deadflag != DEAD_NO )
		return ret;

	Forget(bits_MEMORY_INCOVER);

	if( m_MonsterState != MONSTERSTATE_PRONE && ( pevAttacker->flags & FL_CLIENT ) )
	{
		m_flPlayerDamage += flDamage;

		if (m_bIsFriendly)
		{
			// This is a heurstic to determine if the player intended to harm me
			// If I have an enemy, we can't establish intent (may just be crossfire)
			if (m_hEnemy == NULL)
			{
				// If the player was facing directly at me, or I'm already suspicious, get mad
				if (gpGlobals->time - m_flLastHitByPlayer < 4.0 && m_iPlayerHits > 2
					&& ((m_afMemory & bits_MEMORY_SUSPICIOUS) || IsFacing(pevAttacker, pev->origin)))
				{
					// Alright, now I'm pissed!
					PlaySentence(m_szGrp[TLK_MAD], 4, VOL_NORM, ATTN_NORM);

					Remember(bits_MEMORY_PROVOKED);
					StopFollowing(TRUE);
					ALERT(at_console, "HGrunt Ally is now MAD!\n");
				}
				else
				{
					// Hey, be careful with that
					PlaySentence(m_szGrp[TLK_SHOT], 4, VOL_NORM, ATTN_NORM);
					Remember(bits_MEMORY_SUSPICIOUS);

					if (4.0 > gpGlobals->time - m_flLastHitByPlayer)
						++m_iPlayerHits;
					else
						m_iPlayerHits = 0;

					m_flLastHitByPlayer = gpGlobals->time;

					ALERT(at_console, "HGrunt Ally is now SUSPICIOUS!\n");
				}
			}
			else if (!m_hEnemy->IsPlayer())
			{
				PlaySentence(m_szGrp[TLK_SHOT], 4, VOL_NORM, ATTN_NORM);
			}
		}
	}

	return ret;
}

void CHGruntExtended::Killed( entvars_t* pevAttacker, int iGib )
{
	if( m_fTorchActive )
	{
		m_fTorchActive = false;
		UTIL_Remove( m_pTorchBeam );
		m_pTorchBeam = nullptr;
	}

	CHGruntAlly::Killed( pevAttacker, iGib );

	// switch to body group with no gun.
	SetBodygroup(HGRUNT_BODYGROUP_WEAPONS, HGRUNT_WEAPONBODY_NONE);

	if (pev->spawnflags & SF_MONSTER_NO_WPN_DROP)
		return;

	Vector	vecGunPos;
	Vector	vecGunAngles;

	GetAttachment(0, vecGunPos, vecGunAngles);

	// now spawn a gun.
	if (!m_bDroppedAlready)
	{
		m_bDroppedAlready = true;
		if (m_iNewWeapons == HGRUNT_WEAPON_SHOCKROACH && m_bDropLiveRoach)
		{
			CBaseEntity* pGun = DropItem("monster_shockroach", vecGunPos + Vector(0, 0, 32), vecGunAngles);

			if (pGun)
			{
				if (m_stRoachmodel)
					pGun->pev->model = m_stRoachmodel;

				pGun->pev->velocity = Vector(RANDOM_FLOAT(-100, 100), RANDOM_FLOAT(-100, 100), RANDOM_FLOAT(200, 300));
				pGun->pev->avelocity = Vector(0, RANDOM_FLOAT(200, 400), 0);

				CBaseMonster* pShock = pGun->MyMonsterPointer();

				//LRC - hornets have the same allegiance as their creators
				pShock->m_iPlayerReact = m_iPlayerReact;
				pShock->m_iClass = m_iClass;
				if (m_afMemory & bits_MEMORY_PROVOKED) // if I'm mad at the player, so are my hornets
					pShock->Remember(bits_MEMORY_PROVOKED);
			}
		}
		else
		{
			DropItem(STRING(m_chzWeaponClassname), vecGunPos, vecGunAngles);
		}

		if (m_iGrenadeType == HGRUNT_GRENADE_LAUNCHER)
		{
			DropItem("ammo_ARgrenades", BodyTarget(pev->origin), vecGunAngles);
		}
	}
}

void CHGruntExtended::MonsterThink()
{
	if( m_fTorchActive && m_pTorchBeam )
	{
		Vector vecTorchPos;
		Vector vecTorchAng;
		Vector vecEndPos;
		Vector vecEndAng;

		GetAttachment( 2, vecTorchPos, vecTorchAng );
		GetAttachment( 3, vecEndPos, vecEndAng );

		TraceResult tr;
		UTIL_TraceLine( vecTorchPos, ( vecEndPos - vecTorchPos ).Normalize() * 4 + vecTorchPos, ignore_monsters, edict(), &tr );
		
		if( tr.flFraction != 1.0 )
		{
			m_pTorchBeam->pev->spawnflags &= ~SF_BEAM_SPARKSTART;
			//TODO: looks like a bug to me, shouldn't be bitwise inverting
			m_pTorchBeam->pev->spawnflags |= ~SF_BEAM_SPARKEND;

			UTIL_DecalTrace( &tr, RANDOM_LONG( 0, 4 ) );
			m_pTorchBeam->DoSparks( tr.vecEndPos, tr.vecEndPos );
		}

		m_pTorchBeam->SetBrightness( RANDOM_LONG( 192, 255 ) );
	}

	if (m_iGruntType == HGRUNT_TYPE_MEDIC)
	{
		if (m_fFollowChecking && !m_fFollowChecked && gpGlobals->time - m_flFollowCheckTime > 0.5)
		{
			m_fFollowChecking = false;

			//TODO: not suited for multiplayer
			auto pPlayer = UTIL_FindEntityByClassname(nullptr, "player");

			FollowerUse(pPlayer, pPlayer, USE_TOGGLE, 0);
		}
	}

	COFSquadTalkMonster::MonsterThink();
}


BOOL CHGruntExtended::HealMe( COFSquadTalkMonster* pTarget )
{
	if( pTarget )
	{
		if( m_hTargetEnt && !m_hTargetEnt->IsPlayer() )
		{
			auto pCurrentTarget = m_hTargetEnt->MySquadTalkMonsterPointer();

			if( pCurrentTarget && pCurrentTarget->MySquadLeader() == MySquadLeader() )
			{
				return false;
			}

			if( pTarget->MySquadLeader() != MySquadLeader() )
			{
				return false;
			}
		}

		if( m_MonsterState != MONSTERSTATE_COMBAT && m_iHealCharge )
		{
			HealerActivate( pTarget );
			return true;
		}
	}
	else
	{
		if( m_hTargetEnt )
		{
			auto v14 = m_hTargetEnt->MySquadTalkMonsterPointer();
			if( v14 )
				v14->m_hWaitMedic = nullptr;
		}
		
		m_hTargetEnt = nullptr;

		if( m_movementGoal == MOVEGOAL_TARGETENT )
			RouteClear();

		ClearSchedule();
		ChangeSchedule( slMedicAllyDrawGunExtended );
	}

	return false;
}

void CHGruntExtended::HealOff()
{
	m_fHealing = false;

	if( m_movementGoal == MOVEGOAL_TARGETENT )
		RouteClear();

	m_hTargetEnt = nullptr;
	ClearSchedule();

	SetThink( nullptr );
	pev->nextthink = 0;
}

void CHGruntExtended::HealerActivate( CBaseMonster* pTarget )
{
	if( m_hTargetEnt )
	{
		auto pMonster = m_hTargetEnt->MySquadTalkMonsterPointer();

		if( pMonster )
			pMonster->m_hWaitMedic = nullptr;

		//TODO: could just change the type of pTarget since this is the only type passed in
		auto pSquadTarget = static_cast<COFSquadTalkMonster*>(pTarget);

		pSquadTarget->m_hWaitMedic = this;

		m_hTargetEnt = pTarget;

		m_fHealing = false;

		ClearSchedule();

		ChangeSchedule( slMedicAllyNewHealTargetExtended);
	}
	else if( m_iHealCharge > 0
		&& pTarget->IsAlive()
		&& pTarget->pev->max_health > pTarget->pev->health
		&& !m_fHealing )
	{
		if( m_hTargetEnt && m_hTargetEnt->IsPlayer() )
		{
			StopFollowing( false );
		}

		m_hTargetEnt = pTarget;

		auto pMonster = pTarget->MySquadTalkMonsterPointer();

		if( pMonster )
			pMonster->m_hWaitMedic = this;

		m_fHealing = true;

		ClearSchedule();

		SENTENCEG_PlayRndSz(ENT(pev), m_szGrp[TLK_HEAL], HGRUNT_SENTENCE_VOLUME, GRUNT_ATTN, 0, m_voicePitch);

		ChangeSchedule( slMedicAllyDrawNeedleExtended);
	}
}

void CHGruntExtended::HealerUse( CBaseEntity* pActivator, CBaseEntity* pCaller, USE_TYPE useType, float value )
{
	if( m_useTime > gpGlobals->time
		|| m_flLastUseTime > gpGlobals->time
		|| pActivator == m_hEnemy )
	{
		return;
	}

	if( m_fFollowChecked || m_fFollowChecking )
	{
		if( !m_fFollowChecked && m_fFollowChecking )
		{
			if( gpGlobals->time - m_flFollowCheckTime < 0.3 )
				return;

			m_fFollowChecked = true;
			m_fFollowChecking = false;
		}

		const auto newTarget = !m_fUseHealing && m_hTargetEnt && m_fHealing;

		if( newTarget )
		{
			if( pActivator->pev->health >= pActivator->pev->max_health )
				return;
			
			m_fHealing = false;

			auto pMonster = m_hTargetEnt->MySquadTalkMonsterPointer();

			if( pMonster )
				pMonster->m_hWaitMedic = nullptr;
		}

		if( m_iHealCharge > 0
			&& pActivator->IsAlive()
			&& pActivator->pev->max_health > pActivator->pev->health )
		{
			if( !m_fHealing )
			{
				if( m_hTargetEnt && m_hTargetEnt->IsPlayer() )
				{
					StopFollowing( false );
				}

				m_hTargetEnt = pActivator;

				m_fHealing = true;
				m_fUseHealing = true;

				ClearSchedule();

				m_fHealAudioPlaying = false;

				if( newTarget )
				{
					ChangeSchedule( slMedicAllyNewHealTargetExtended);
				}
				else
				{
					SENTENCEG_PlayRndSz( edict(), m_szGrp[TLK_HEAL], HGRUNT_SENTENCE_VOLUME, ATTN_NORM, 0, m_voicePitch );
					ChangeSchedule( slMedicAllyDrawNeedleExtended );
				}
			}

			if( m_fHealActive )
			{
				if( pActivator->TakeHealth( 2, DMG_GENERIC ) )
				{
					m_iHealCharge -= 2;
				}
			}
			else if( pActivator->TakeHealth( 1, DMG_GENERIC ) )
			{
				--m_iHealCharge;
			}
		}
		else
		{
			m_fFollowChecked = false;
			m_fFollowChecking = false;

			if( gpGlobals->time - m_flLastRejectAudio > 4.0 && m_iHealCharge <= 0 && !m_fHealing )
			{
				m_flLastRejectAudio = gpGlobals->time;
				SENTENCEG_PlayRndSz( edict(), m_szGrp[TLK_NOTHEAL], HGRUNT_SENTENCE_VOLUME, ATTN_NORM, 0, m_voicePitch );
			}
		}

		m_flLastUseTime = gpGlobals->time + 0.2;
		return;
	}

	m_fFollowChecking = true;
	m_flFollowCheckTime = gpGlobals->time;
}

//=========================================================
// start task
//=========================================================
void CHGruntExtended :: StartTask ( Task_t *pTask )
{
	m_iTaskStatus = TASKSTATUS_RUNNING;

	switch ( pTask->iTask )
	{

	case TASK_MELEE_ATTACK2:
		{
			if (!(m_iGruntType == HGRUNT_TYPE_MEDIC))
			{
				CHGruntAlly::StartTask(pTask);
				break;
			}

			m_IdealActivity = ACT_MELEE_ATTACK2;

			if( !m_fHealAudioPlaying )
			{
				EMIT_SOUND( edict(), CHAN_WEAPON, "fgrunt/medic_give_shot.wav", VOL_NORM, ATTN_NORM);
				m_fHealAudioPlaying = true;
			}
			break;
		}

	case TASK_WAIT_FOR_MOVEMENT:
		{
			if (!(m_iGruntType == HGRUNT_TYPE_MEDIC))
			{
				CHGruntAlly::StartTask(pTask);
				break;
			}

			if( !m_fHealing )
				return COFSquadTalkMonster::StartTask( pTask );

			if( m_hTargetEnt )
			{
				auto pTarget = m_hTargetEnt.Entity<CBaseEntity>();
				auto pTargetMonster = pTarget->MySquadTalkMonsterPointer();

				if( pTargetMonster )
					pTargetMonster->m_hWaitMedic = nullptr;

				m_fHealing = false;
				m_fUseHealing = false;

				STOP_SOUND( edict(), CHAN_WEAPON, "fgrunt/medic_give_shot.wav" );

				m_fFollowChecked = false;
				m_fFollowChecking = false;

				if( m_movementGoal == MOVEGOAL_TARGETENT )
					RouteClear();

				m_hTargetEnt = nullptr;

				m_fHealActive = false;

				return COFSquadTalkMonster::StartTask( pTask );
			}

			m_fHealing = false;
			m_fUseHealing = false;

			STOP_SOUND( edict(), CHAN_WEAPON, "fgrunt/medic_give_shot.wav" );

			m_fFollowChecked = false;
			m_fFollowChecking = false;

			if( m_movementGoal == MOVEGOAL_TARGETENT )
				RouteClear();

			m_IdealActivity = ACT_DISARM;
			m_fHealActive = false;
			break;
		}

	default: 
		CHGruntAlly :: StartTask( pTask );
		break;
	}
}

//=========================================================
// RunTask
//=========================================================
void CHGruntExtended :: RunTask ( Task_t *pTask )
{
	switch ( pTask->iTask )
	{
	case TASK_GRUNT_FACE_TOSS_DIR:
		{
			// project a point along the toss vector and turn to face that point.
			MakeIdealYaw( pev->origin + m_vecTossVelocity * 64 );
			ChangeYaw( pev->yaw_speed );

			if ( FacingIdeal() )
			{
				m_iTaskStatus = TASKSTATUS_COMPLETE;
			}
			break;
		}

	case TASK_MELEE_ATTACK2:
		{
			if (!(m_iGruntType == HGRUNT_TYPE_MEDIC))
			{
				CHGruntAlly::RunTask(pTask);
				break;
			}

			if( m_fSequenceFinished )
			{
				if( m_fUseHealing )
				{
					if( gpGlobals->time - m_flLastUseTime > 0.3 )
						m_Activity = ACT_RESET;
				}
				else
				{
					m_fHealActive = true;

					if( m_hTargetEnt )
					{
						auto pHealTarget = m_hTargetEnt.Entity<CBaseEntity>();

						const auto toHeal = V_min( 5, pHealTarget->pev->max_health - pHealTarget->pev->health );

						if( toHeal != 0 && pHealTarget->TakeHealth( toHeal, DMG_GENERIC ) )
						{
							m_iHealCharge -= toHeal;
						}
						else
						{
							m_Activity = ACT_RESET;
						}
					}
					else
					{
						m_Activity = m_fHealing ? ACT_MELEE_ATTACK2 : ACT_RESET;
					}
				}

				TaskComplete();
			}

			break;
		}
	default:
		{
			COFSquadTalkMonster:: RunTask( pTask );
			break;
		}
	}
}

//=========================================================
//=========================================================
Schedule_t* CHGruntExtended :: GetScheduleOfType ( int Type ) 
{
	switch	( Type )
	{

	case SCHED_MEDIC_ALLY_HEAL_ALLY:
		return slMedicAllyHealTargetExtended;

	default:
		{
			return CHGruntAlly :: GetScheduleOfType ( Type );
		}
	}
}
