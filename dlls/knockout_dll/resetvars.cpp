
#include "extdll.h"
#include "util.h"
#include "cbase.h"
#include "knockout_dll\resetvars.h"

void GlobalResetVars()
{
	CVAR_SET_FLOAT("__hud_crosshair_color", 0);
	CVAR_SET_FLOAT("__hud_use_hd_sprites", 0);
	CVAR_SET_FLOAT("__hud_max_hp", 100);
	CVAR_SET_FLOAT("__hud_max_bat", 100);
	CVAR_SET_FLOAT("__hud_style_numbers", 0);
	CVAR_SET_FLOAT("__hud_style_health", 0);
	CVAR_SET_FLOAT("__hud_style_suit", 0);
	CVAR_SET_FLOAT("__hud_style_flashlight", 0);

	CVAR_SET_FLOAT("__wpn_gauss_charge", 4);
	CVAR_SET_FLOAT("__wpn_revolver_mp", 0);
	CVAR_SET_FLOAT("__wpn_crossbow_boom", 0);

	CVAR_SET_STRING("__sound_mp5", "weapons/hks1.wav");
	CVAR_SET_STRING("__sound_mp5_launcher", "weapons/glauncher.wav");
	CVAR_SET_STRING("__sound_glock", "weapons/pl_gun3.wav");
	CVAR_SET_STRING("__sound_357", "weapons/357_shot1.wav");
	CVAR_SET_STRING("__sound_eagle", "weapons/desert_eagle_fire.wav");
	CVAR_SET_STRING("__sound_m249", "weapons/saw_fire1.wav");
	CVAR_SET_STRING("__sound_shotgun_primary", "weapons/sbarrel1.wav");
	CVAR_SET_STRING("__sound_shotgun_altfire", "weapons/dbarrel1.wav");
	CVAR_SET_STRING("__sound_sniper", "weapons/sniper_fire.wav");

	CVAR_SET_STRING("__sound_jump_string", "player/plyrjmp8.wav");
	CVAR_SET_FLOAT("__sound_jump", 0);
	
	CVAR_SET_FLOAT("__flip_displacer", 0);
	CVAR_SET_FLOAT("__flip_eagle", 0);
	CVAR_SET_FLOAT("__flip_grapple", 0);
	CVAR_SET_FLOAT("__flip_knife", 0);
	CVAR_SET_FLOAT("__flip_m249", 0);
	CVAR_SET_FLOAT("__flip_penguin", 0);
	CVAR_SET_FLOAT("__flip_pipewrench", 0);
	CVAR_SET_FLOAT("__flip_crossbow", 0);
	CVAR_SET_FLOAT("__flip_crowbar", 0);
	CVAR_SET_FLOAT("__flip_shockroach", 0);
	CVAR_SET_FLOAT("__flip_sniper", 0);
	CVAR_SET_FLOAT("__flip_sporelauncher", 0);
	CVAR_SET_FLOAT("__flip_egon", 0);
	CVAR_SET_FLOAT("__flip_gauss", 0);
	CVAR_SET_FLOAT("__flip_glock", 0);
	CVAR_SET_FLOAT("__flip_handgrenade", 0);
	CVAR_SET_FLOAT("__flip_hivehand", 0);
	CVAR_SET_FLOAT("__flip_mp5", 0);
	CVAR_SET_FLOAT("__flip_357", 0);
	CVAR_SET_FLOAT("__flip_satchel", 0);
	CVAR_SET_FLOAT("__flip_satchel_radio", 0);
	CVAR_SET_FLOAT("__flip_shotgun", 0);
	CVAR_SET_FLOAT("__flip_snark", 0);
	CVAR_SET_FLOAT("__flip_tripmine", 0);

	CVAR_SET_FLOAT("__anims_displacer", 16);
	CVAR_SET_FLOAT("__anims_eagle", 17);
	CVAR_SET_FLOAT("__anims_grapple", 18);
	CVAR_SET_FLOAT("__anims_knife", 19);
	CVAR_SET_FLOAT("__anims_m249", 20);
	CVAR_SET_FLOAT("__anims_penguin", 21);
	CVAR_SET_FLOAT("__anims_pipewrench", 22);
	CVAR_SET_FLOAT("__anims_crossbow", 5);
	CVAR_SET_FLOAT("__anims_crowbar", 2);
	CVAR_SET_FLOAT("__anims_shockrifle", 23);
	CVAR_SET_FLOAT("__anims_sniper", 24);
	CVAR_SET_FLOAT("__anims_sporelauncher", 25);
	CVAR_SET_FLOAT("__anims_egon", 9);
	CVAR_SET_FLOAT("__anims_gauss", 8);
	CVAR_SET_FLOAT("__anims_glock", 1);
	CVAR_SET_FLOAT("__anims_handgrenade", 11);
	CVAR_SET_FLOAT("__anims_hivehand", 10);
	CVAR_SET_FLOAT("__anims_mp5", 4);
	CVAR_SET_FLOAT("__anims_357", 3);
	CVAR_SET_FLOAT("__anims_satchel", 12);
	CVAR_SET_FLOAT("__anims_satchel_radio", 13);
	CVAR_SET_FLOAT("__anims_shotgun", 6);
	CVAR_SET_FLOAT("__anims_snark", 15);
	CVAR_SET_FLOAT("__anims_tripmine", 14);

	CVAR_SET_STRING("__model_satchel", "models/v_satchel.mdl");
	CVAR_SET_STRING("__model_satchel_radio", "models/v_satchel_radio.mdl");

	CVAR_SET_FLOAT("__hud_flashlight_behavour", 0);
}

#ifndef CLIENT_DLL

void CWorld::ResetVars()
{
	if (!m_iCrosshairColor)
		CVAR_SET_FLOAT("__hud_crosshair_color", 0);

	if (!m_iPlayerMaxHealth)
		CVAR_SET_FLOAT("__hud_max_hp", 100);

	if (!m_iPlayerMaxArmor)
		CVAR_SET_FLOAT("__hud_max_bat", 100);

	if (!m_flGaussMaxCharge)
		CVAR_SET_FLOAT("__wpn_gauss_charge", 4);

	if (!m_bReolverIsMP)
		CVAR_SET_FLOAT("__wpn_revolver_mp", 0);

	if (!m_iszMP5Sound)
		CVAR_SET_STRING("__sound_mp5", "weapons/hks1.wav");

	if (!m_iszMP5SoundLauncher)
		CVAR_SET_STRING("__sound_mp5_launcher", "weapons/glauncher.wav");

	if (!m_iszGlockSound)
		CVAR_SET_STRING("__sound_glock", "weapons/pl_gun3.wav");

	if (!m_isz357Sound)
		CVAR_SET_STRING("__sound_357", "weapons/357_shot1.wav");

	if (!m_iszEagleSound)
		CVAR_SET_STRING("__sound_eagle", "weapons/desert_eagle_fire.wav");

	if (!m_iszM249Sound)
		CVAR_SET_STRING("__sound_m249", "weapons/saw_fire1.wav");
		
	if (!m_iszShotgunSoundPri)
		CVAR_SET_STRING("__sound_shotgun_primary", "weapons/sbarrel1.wav");
		
	if (!m_iszShotgunSoundAlt)
		CVAR_SET_STRING("__sound_shotgun_altfire", "weapons/dbarrel1.wav");
		
	if (!m_iszSniperSound)
		CVAR_SET_STRING("__sound_sniper", "weapons/sniper_fire.wav");
		
	if (!m_iszJumpSound)
		CVAR_SET_STRING("__sound_jump_string", "player/plyrjmp8.wav");
		
	if (!m_bPlayJumpSound)
		CVAR_SET_FLOAT("__sound_jump", 0);

	if (!m_bDisplacerFlip)
		CVAR_SET_FLOAT("__flip_displacer", 0);

	if (!m_bEagleFlip)
		CVAR_SET_FLOAT("__flip_eagle", 0);

	if (!m_bGrappleFlip)
		CVAR_SET_FLOAT("__flip_grapple", 0);

	if (!m_bKnifeFlip)
		CVAR_SET_FLOAT("__flip_knife", 0);

	if (!m_bM249Flip)
		CVAR_SET_FLOAT("__flip_m249", 0);

	if (!m_bPenguinFlip)
		CVAR_SET_FLOAT("__flip_penguin", 0);

	if (!m_bPipeWrenchFlip)
		CVAR_SET_FLOAT("__flip_pipewrench", 0);

	if (!m_bCrossbowFlip)
		CVAR_SET_FLOAT("__flip_crossbow", 0);

	if (!m_bCrowbarFlip)
		CVAR_SET_FLOAT("__flip_crowbar", 0);

	if (!m_bShockroachFlip)
		CVAR_SET_FLOAT("__flip_shockroach", 0);

	if (!m_bSniperFlip)
		CVAR_SET_FLOAT("__flip_sniper", 0);

	if (!m_bSporelauncherFlip)
		CVAR_SET_FLOAT("__flip_sporelauncher", 0);

	if (!m_bEgonFlip)
		CVAR_SET_FLOAT("__flip_egon", 0);

	if (!m_bGaussFlip)
		CVAR_SET_FLOAT("__flip_gauss", 0);

	if (!m_bGlockFlip)
		CVAR_SET_FLOAT("__flip_glock", 0);

	if (!m_bHandGrenadeFlip)
		CVAR_SET_FLOAT("__flip_handgrenade", 0);

	if (!m_bHivehandFlip)
		CVAR_SET_FLOAT("__flip_hivehand", 0);

	if (!m_bMP5Flip)
		CVAR_SET_FLOAT("__flip_mp5", 0);

	if (!m_b357Flip)
		CVAR_SET_FLOAT("__flip_357", 0);

	if (!m_bSatchelFlip)
		CVAR_SET_FLOAT("__flip_satchel", 0);

	if (!m_bSatchelRadioFlip)
		CVAR_SET_FLOAT("__flip_satchel_radio", 0);

	if (!m_bShotgunFlip)
		CVAR_SET_FLOAT("__flip_shotgun", 0);

	if (!m_bSnarkFlip)
		CVAR_SET_FLOAT("__flip_snark", 0);

	if (!m_bTripmineFlip)
		CVAR_SET_FLOAT("__flip_tripmine", 0);

	if (!m_iDisplacerActivity)
		CVAR_SET_FLOAT("__anims_displacer", 16);

	if (!m_iEagleActivity)
		CVAR_SET_FLOAT("__anims_eagle", 17);

	if (!m_iGrappleActivity)
		CVAR_SET_FLOAT("__anims_grapple", 18);

	if (!m_iKnifeActivity)
		CVAR_SET_FLOAT("__anims_knife", 19);

	if (!m_iM249Activity)
		CVAR_SET_FLOAT("__anims_m249", 20);

	if (!m_iPenguinActivity)
		CVAR_SET_FLOAT("__anims_penguin", 21);

	if (!m_iPipeWrenchActivity)
		CVAR_SET_FLOAT("__anims_pipewrench", 22);

	if (!m_iCrossbowActivity)
		CVAR_SET_FLOAT("__anims_crossbow", 5);

	if (!m_iCrowbarActivity)
		CVAR_SET_FLOAT("__anims_crowbar", 2);

	if (!m_iShockroachActivity)
		CVAR_SET_FLOAT("__anims_shockrifle", 23);

	if (!m_iSniperActivity)
		CVAR_SET_FLOAT("__anims_sniper", 24);

	if (!m_iSporelauncherActivity)
		CVAR_SET_FLOAT("__anims_sporelauncher", 25);

	if (!m_iEgonActivity)
		CVAR_SET_FLOAT("__anims_egon", 9);

	if (!m_iGaussActivity)
		CVAR_SET_FLOAT("__anims_gauss", 8);

	if (!m_iGlockActivity)
		CVAR_SET_FLOAT("__anims_glock", 1);

	if (!m_iHandGrenadeActivity)
		CVAR_SET_FLOAT("__anims_handgrenade", 11);

	if (!m_iHivehandActivity)
		CVAR_SET_FLOAT("__anims_hivehand", 10);

	if (!m_iMP5Activity)
		CVAR_SET_FLOAT("__anims_mp5", 4);

	if (!m_i357Activity)
		CVAR_SET_FLOAT("__anims_357", 3);

	if (!m_iSatchelActivity)
		CVAR_SET_FLOAT("__anims_satchel", 12);

	if (!m_iSatchelRadioActivity)
		CVAR_SET_FLOAT("__anims_satchel_radio", 13);

	if (!m_iShotgunActivity)
		CVAR_SET_FLOAT("__anims_shotgun", 6);

	if (!m_iSnarkActivity)
		CVAR_SET_FLOAT("__anims_snark", 15);

	if (!m_iTripmineActivity)
		CVAR_SET_FLOAT("__anims_tripmine", 15);

	if (!m_iszSatchelView)
		CVAR_SET_STRING("__model_satchel", "models/v_satchel.mdl");

	if (!m_iszSatchelViewRadio)
		CVAR_SET_STRING("__model_satchel_radio", "models/v_satchel_radio.mdl");
	
	if (!m_iFlashlightBehavour)
		CVAR_SET_FLOAT("__hud_flashlight_behavour", 0);
}

#endif
