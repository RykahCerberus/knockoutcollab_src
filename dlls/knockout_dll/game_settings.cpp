
#include "extdll.h"
#include "util.h"
#include "cbase.h"
#include "game_settings.h"
#include "UserMessages.h"
#include "player.h"
#include "monsters.h"
#include "customentity.h"
#include "effects.h"
#include "cdll_dll.h"
#include "weapons.h"
#include "knockout_dll/game_settings.h"

void CWorld::PlayerSpawn(entvars_t * pevPlayer)
{
	if (pevPlayer)
	{
		m_pPlayer = (CBasePlayer*)CBaseEntity::Instance(pevPlayer);
	}
}

void CWorld::PostPlayerSpawn(entvars_t * pevPlayer)
{
	if (pevPlayer && m_pPlayer)
	{
		m_bPostSpawn = true;

		if (m_iPlayerMaxHealth)
		{
			m_pPlayer->pev->max_health = m_iPlayerMaxHealth;
			m_pPlayer->pev->health = m_pPlayer->pev->max_health;
		}
		else
		{
			m_pPlayer->pev->max_health = 100;
		}

		if (m_iPlayerMaxArmor)
		{
			m_pPlayer->m_flMaxArmor = m_iPlayerMaxArmor;
			m_pPlayer->pev->armorvalue = 0;
		}
		else
		{
			m_pPlayer->m_flMaxArmor = MAX_NORMAL_BATTERY;
		}

		CVAR_SET_FLOAT("__hud_max_hp", m_pPlayer->pev->max_health);
		CVAR_SET_FLOAT("__hud_max_bat", m_pPlayer->m_flMaxArmor);

		if (m_bIsCollab)
		{
			m_pPlayer->RemoveAllItems(false);

			m_pPlayer->pev->health = m_pPlayer->pev->max_health;
			m_pPlayer->pev->armorvalue = 0;
			m_pPlayer->pev->armortype = 0;
			m_pPlayer->m_iHasHelmet = false;
			m_pPlayer->StripAllKeycards();
			m_pPlayer->m_iArbitraryCounterPosition = 0;
			m_pPlayer->m_iArbitraryCounterValue = 0;
			m_pPlayer->m_bDrawArbitraryCounter = FALSE;
		}

		if (m_bStartWithSuit)
			m_pPlayer->GiveNamedItem("item_suit_quiet");

		if (m_bStripKeycards)
			m_pPlayer->StripAllKeycards();
	}
}


TYPEDESCRIPTION CWorld::m_SaveData[] = 
{
	DEFINE_FIELD(CWorld, m_bPostSpawn, FIELD_BOOLEAN),

	DEFINE_FIELD(CWorld, m_pPlayer, FIELD_POINTER),

	DEFINE_FIELD(CWorld, m_bIsCollab, FIELD_BOOLEAN),
	DEFINE_FIELD(CWorld, m_bStripKeycards, FIELD_BOOLEAN),
	DEFINE_FIELD(CWorld, m_bStartWithSuit, FIELD_BOOLEAN),
	DEFINE_FIELD(CWorld, m_iszSuitSoundType, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_flGaussMaxCharge, FIELD_FLOAT),
	DEFINE_FIELD(CWorld, m_bReolverIsMP, FIELD_BOOLEAN),
	DEFINE_FIELD(CWorld, m_iPlayerMaxHealth, FIELD_INTEGER),
	DEFINE_FIELD(CWorld, m_iPlayerMaxArmor, FIELD_INTEGER),
	DEFINE_FIELD(CWorld, m_bCSPainSounds, FIELD_BOOLEAN),
	DEFINE_FIELD(CWorld, m_bPlayJumpSound, FIELD_BOOLEAN),
	DEFINE_FIELD(CWorld, m_iszJumpSound, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_vecPlayerHudColor, FIELD_VECTOR),
	DEFINE_FIELD(CWorld, m_iCrosshairColor, FIELD_INTEGER),
	DEFINE_FIELD(CWorld, m_iHudColor, FIELD_INTEGER),
	DEFINE_FIELD(CWorld, m_bXenReturn, FIELD_BOOLEAN),
	DEFINE_FIELD(CWorld, m_flXenGravity, FIELD_FLOAT),
	DEFINE_FIELD(CWorld, m_flSlimeDamage, FIELD_FLOAT),
	DEFINE_FIELD(CWorld, m_flLavaDamage, FIELD_FLOAT),
	DEFINE_FIELD(CWorld, m_bCombinePowerupTimers, FIELD_BOOLEAN),
	DEFINE_FIELD(CWorld, m_iFlashlightBehavour, FIELD_INTEGER),

	DEFINE_FIELD(CWorld, m_iszMP5View, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszMP5World, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszMP5Thrown, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszGlockView, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszGlockWorld, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_isz357View, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_isz357World, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszCrossbowView, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszCrossbowWorld, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszCrossbowThrown, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszCrowbarView, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszCrowbarWorld, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszDisplacerView, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszDisplacerWorld, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszEagleView, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszEagleWorld, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszEgonView, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszEgonWorld, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszHandGrenadeView, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszHandGrenadeWorld, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszHandGrenadeThrown, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszGaussView, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszGaussWorld, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszGrappleView, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszGrappleWorld, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszHivehandView, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszHivehandWorld, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszHornetModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszKnifeView, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszKnifeWorld, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszM249View, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszM249World, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszPenguinView, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszPenguinWorld, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszPenguinThrown, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszPipewrenchView, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszPipewrenchWorld, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszRPGView, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszRPGWorld, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszRPGThrown, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszSatchelView, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszSatchelViewRadio, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszSatchelWorld, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszSatchelThrown, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszShockroachView, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszShockroachWorld, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszShockroachModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszShotgunView, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszShotgunWorld, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszSnarkView, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszSnarkWorld, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszSnarkThrown, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszSniperView, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszSniperWorld, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszSporelauncherView, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszSporelauncherWorld, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszSporelauncherThrown, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszTripmineModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszTripmineView, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszTripmineWorld, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszTripmineThrown, FIELD_STRING),

	DEFINE_FIELD(CWorld, m_iszMP5Sound, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszMP5SoundLauncher, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszMP5SoundNPC, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszGlockSound, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_isz357Sound, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszEagleSound, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszM249Sound, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszShotgunSoundPri, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszShotgunSoundAlt, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszShotgunSoundPump, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszShotgunSoundReload, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszSniperSound, FIELD_STRING),
		
	DEFINE_FIELD(CWorld, m_iszKeycardModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszBatteryModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszMedkitModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszAmmo357Model, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszAmmo9mmARModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszAmmo9mmBoxModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszAmmo9mmClipModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszAmmoARGrenadesModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszAmmoShotgunModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszAmmoCrossbowModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszAmmoGaussModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszAmmoRPGModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszAmmo556Model, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszAmmo762Model, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszAmmoSporeModel, FIELD_STRING),

	DEFINE_FIELD(CWorld, m_iszAgruntModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszApacheModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszBabyVoltigoreModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszBarnacleModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszBarneyModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszBigMommaModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszBlkApacheModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszBlkOspreyModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszBullsquidModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszCleanScientistModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszGargantuaModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszGManModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszGonomeModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszHAssassinModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszBabyHeadcrabModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszBabyHeadcrabModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszHGruntModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszHGruntAllyModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszHGruntTorchModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszHGruntMedicModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszHoundeyeModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszIcthyosaurModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszLeechModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszMAssassinModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszOspreyModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszOtisModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszPitdroneModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszRoachModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszScientistModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszShocktrooperModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszTentacleModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszTurretModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszMiniTurretModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszSentryModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszVoltigoreModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszZombieModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszZombieBarneyModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszZombieSoldierModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszVortigauntModel, FIELD_STRING),

	DEFINE_FIELD(CWorld, m_iMP5Activity, FIELD_INTEGER),
	DEFINE_FIELD(CWorld, m_bMP5Flip, FIELD_BOOLEAN),
	DEFINE_FIELD(CWorld, m_iGlockActivity, FIELD_INTEGER),
	DEFINE_FIELD(CWorld, m_bGlockFlip, FIELD_BOOLEAN),
	DEFINE_FIELD(CWorld, m_i357Activity, FIELD_INTEGER),
	DEFINE_FIELD(CWorld, m_b357Flip, FIELD_BOOLEAN),
	DEFINE_FIELD(CWorld, m_iCrossbowActivity, FIELD_INTEGER),
	DEFINE_FIELD(CWorld, m_bCrossbowFlip, FIELD_BOOLEAN),
	DEFINE_FIELD(CWorld, m_iCrowbarActivity, FIELD_INTEGER),
	DEFINE_FIELD(CWorld, m_bCrowbarFlip, FIELD_BOOLEAN),
	DEFINE_FIELD(CWorld, m_iDisplacerActivity, FIELD_INTEGER),
	DEFINE_FIELD(CWorld, m_bDisplacerFlip, FIELD_BOOLEAN),
	DEFINE_FIELD(CWorld, m_iEagleActivity, FIELD_INTEGER),
	DEFINE_FIELD(CWorld, m_bEagleFlip, FIELD_BOOLEAN),
	DEFINE_FIELD(CWorld, m_iEgonActivity, FIELD_INTEGER),
	DEFINE_FIELD(CWorld, m_bEgonFlip, FIELD_BOOLEAN),
	DEFINE_FIELD(CWorld, m_iHandGrenadeActivity, FIELD_INTEGER),
	DEFINE_FIELD(CWorld, m_bHandGrenadeFlip, FIELD_BOOLEAN),
	DEFINE_FIELD(CWorld, m_iGaussActivity, FIELD_INTEGER),
	DEFINE_FIELD(CWorld, m_bGaussFlip, FIELD_BOOLEAN),
	DEFINE_FIELD(CWorld, m_iGrappleActivity, FIELD_INTEGER),
	DEFINE_FIELD(CWorld, m_bGrappleFlip, FIELD_BOOLEAN),
	DEFINE_FIELD(CWorld, m_iHivehandActivity, FIELD_INTEGER),
	DEFINE_FIELD(CWorld, m_bHivehandFlip, FIELD_BOOLEAN),
	DEFINE_FIELD(CWorld, m_iKnifeActivity, FIELD_INTEGER),
	DEFINE_FIELD(CWorld, m_bKnifeFlip, FIELD_BOOLEAN),
	DEFINE_FIELD(CWorld, m_iM249Activity, FIELD_INTEGER),
	DEFINE_FIELD(CWorld, m_bM249Flip, FIELD_BOOLEAN),
	DEFINE_FIELD(CWorld, m_iPenguinActivity, FIELD_INTEGER),
	DEFINE_FIELD(CWorld, m_bPenguinFlip, FIELD_BOOLEAN),
	DEFINE_FIELD(CWorld, m_iPipeWrenchActivity, FIELD_INTEGER),
	DEFINE_FIELD(CWorld, m_bPipeWrenchFlip, FIELD_BOOLEAN),
	DEFINE_FIELD(CWorld, m_iRPGActivity, FIELD_INTEGER),
	DEFINE_FIELD(CWorld, m_bRPGFlip, FIELD_BOOLEAN),
	DEFINE_FIELD(CWorld, m_iSatchelActivity, FIELD_INTEGER),
	DEFINE_FIELD(CWorld, m_bSatchelFlip, FIELD_BOOLEAN),
	DEFINE_FIELD(CWorld, m_iSatchelRadioActivity, FIELD_INTEGER),
	DEFINE_FIELD(CWorld, m_bSatchelRadioFlip, FIELD_BOOLEAN),
	DEFINE_FIELD(CWorld, m_iShockroachActivity, FIELD_INTEGER),
	DEFINE_FIELD(CWorld, m_bShockroachFlip, FIELD_BOOLEAN),
	DEFINE_FIELD(CWorld, m_iShotgunActivity, FIELD_INTEGER),
	DEFINE_FIELD(CWorld, m_bShotgunFlip, FIELD_BOOLEAN),
	DEFINE_FIELD(CWorld, m_iSnarkActivity, FIELD_INTEGER),
	DEFINE_FIELD(CWorld, m_bSnarkFlip, FIELD_BOOLEAN),
	DEFINE_FIELD(CWorld, m_iSniperActivity, FIELD_INTEGER),
	DEFINE_FIELD(CWorld, m_bSniperFlip, FIELD_BOOLEAN),
	DEFINE_FIELD(CWorld, m_iSporelauncherActivity, FIELD_INTEGER),
	DEFINE_FIELD(CWorld, m_bSporelauncherFlip, FIELD_BOOLEAN),
	DEFINE_FIELD(CWorld, m_iTripmineActivity, FIELD_INTEGER),
	DEFINE_FIELD(CWorld, m_bTripmineFlip, FIELD_BOOLEAN),

	DEFINE_FIELD(CWorld, m_iszArmyModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszDogModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszKnightModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszHKnightModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszWizardModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszDemonModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszEnforcerModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszShalrathModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszTarbabyModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszRotfishModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszShamblerModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszOgerModel, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszQZombieModel, FIELD_STRING),
		
	DEFINE_FIELD(CWorld, m_iszFlashlightPostfix, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszArmorPostfix, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszHealthPostfix, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszNumberFontPostfix, FIELD_STRING),
	DEFINE_FIELD(CWorld, m_iszSelectionSpritePostfix, FIELD_STRING),
		
	//user stuff
	DEFINE_FIELD(CWorld, m_bLeapingJump, FIELD_BOOLEAN),
	DEFINE_FIELD(CWorld, m_bNoJump, FIELD_BOOLEAN),
};
IMPLEMENT_SAVERESTORE( CWorld, CBaseEntity );
