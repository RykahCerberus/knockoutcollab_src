/***
*
*	Copyright (c) 1996-2001, Valve LLC. All rights reserved.
*
*	This product contains software technology licensed from Id
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc.
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/

#include "extdll.h"
#include "util.h"
#include "cbase.h"

#define KEYVALUE_NULL_VALUE -25565

const int MAX_CHANGE_KEYVALUES = 64;

#define MAKINT(name) int m_iToPev_##name##;
#define SAVINT(name) DEFINE_FIELD(CTriggerChangeKeyValue, m_iToPev_##name##, FIELD_INTEGER)
#define KEYINT(name) \
	else if (FStrEq("to_" #name, pkvd->szKeyName)) { \
	m_iToPev_##name## = atoi( pkvd->szValue ); \
	pkvd->fHandled = true; } 
#define SNDINT(name) \
		if (!(m_iToPev_##name## == KEYVALUE_NULL_VALUE)) { \
			targetEnt->pev->##name## = m_iToPev_##name##; \
			ALERT(at_console, "Sending int "#name" to entity %s with value %d\n", STRING(pev->target), m_iToPev_##name##);}

#define MAKFLO(name) float m_flToPev_##name##;
#define SAVFLO(name) DEFINE_FIELD(CTriggerChangeKeyValue, m_flToPev_##name##, FIELD_FLOAT)
#define KEYFLO(name) \
	else if (FStrEq("to_" #name, pkvd->szKeyName)) { \
	m_flToPev_##name## = atof( pkvd->szValue ); \
	pkvd->fHandled = true; } 
#define SNDFLO(name) \
		if (!(m_flToPev_##name## == KEYVALUE_NULL_VALUE)) { \
			targetEnt->pev->##name## = m_flToPev_##name##; \
			ALERT(at_console, "Sending float "#name" to entity %s with value %d\n", STRING(pev->target), m_flToPev_##name##);}

#define MAKVEC(name) Vector m_vecToPev_##name##;
#define SAVVEC(name) DEFINE_FIELD(CTriggerChangeKeyValue, m_vecToPev_##name##, FIELD_VECTOR)
#define KEYVEC(name) \
	else if (FStrEq("to_" #name, pkvd->szKeyName)) { \
	int tempvec[3];\
	UTIL_StringToIntArray( tempvec, 3, pkvd->szValue ); \
	m_vecToPev_##name##.x = tempvec[0];\
	m_vecToPev_##name##.y = tempvec[1];\
	m_vecToPev_##name##.z = tempvec[2];\
	pkvd->fHandled = true; }
#define KEYVECDIR(name, dir) \
	else if (FStrEq("to_" #name "_" #dir, pkvd->szKeyName)) { \
	m_vecToPev_##name##.##dir## = atof( pkvd->szValue );\
	pkvd->fHandled = true; }
#define SNDVEC(name) \
		bool sendVec##name## = false;\
		if (!(m_vecToPev_##name##.x == KEYVALUE_NULL_VALUE))\
			{ targetEnt->pev->##name##.x = m_vecToPev_##name##.x; sendVec##name## = true;} \
		if (!(m_vecToPev_##name##.y == KEYVALUE_NULL_VALUE))\
			{ targetEnt->pev->##name##.y = m_vecToPev_##name##.y; sendVec##name## = true;} \
		if (!(m_vecToPev_##name##.z == KEYVALUE_NULL_VALUE))\
			{ targetEnt->pev->##name##.z = m_vecToPev_##name##.z; sendVec##name## = true;} \
		if (sendVec##name##)\
		ALERT(at_console, "Sending vector "#name" to entity %s with value x%d y%d z%d\n", STRING(pev->target), m_vecToPev_##name##.y, m_vecToPev_##name##.x, m_vecToPev_##name##.z);

#define MAKSTR(name) string_t m_strToPev_##name##;
#define SAVSTR(name) DEFINE_FIELD(CTriggerChangeKeyValue, m_strToPev_##name##, FIELD_STRING)
#define KEYSTR(name) \
	else if (FStrEq("to_" #name, pkvd->szKeyName)) { \
	m_strToPev_##name## = ALLOC_STRING( pkvd->szValue ); \
	pkvd->fHandled = true; } 
#define SNDSTR(name) \
		if (!FStringNull(m_strToPev_##name##)) { \
			targetEnt->pev->##name## = m_strToPev_##name##; \
			ALERT(at_console, "Sending string "#name" to entity %s with value %s\n", STRING(pev->target), STRING(m_strToPev_##name##));}

/**
*	@brief Changes up to ::MAX_CHANGE_KEYVALUES key values in entities with the targetname specified in this entity's target keyvalue
*	Can also change the target entity's target by specifying a changetarget value
*/
class CTriggerChangeKeyValue : public CBaseDelay
{
public:
	int ObjectCaps() override { return 0; }

	void KeyValue(KeyValueData* pkvd) override;
	void Spawn() override;
	void Precache() override;

	void Use(CBaseEntity* pActivator, CBaseEntity* pCaller, USE_TYPE useType, float value) override;

	int Save(CSave& save) override;
	int Restore(CRestore& restore) override;
	static TYPEDESCRIPTION m_SaveData[];
	

	string_t m_changeTargetName;
	string_t m_iKey[MAX_CHANGE_KEYVALUES];
	string_t m_iValue[MAX_CHANGE_KEYVALUES];

	int m_iChangeCount;

	MAKINT(skin);
	MAKINT(body);
	MAKINT(effects);
	MAKFLO(gravity);
	MAKFLO(friction);

	MAKINT(sequence);
	MAKFLO(frame);
	MAKFLO(animtime);
	MAKFLO(scale);
	MAKINT(rendermode);
	MAKFLO(renderamt);

	MAKVEC(rendercolor);
	MAKINT(renderfx);
	MAKFLO(health);
	MAKFLO(frags);
	MAKINT(weapons);
	MAKFLO(takedamage);
	
	MAKINT(impulse);
	MAKINT(spawnflags);
	MAKINT(flags);
	MAKINT(team);
	MAKFLO(max_health);
	MAKFLO(armortype);
	MAKFLO(armorvalue);

	MAKSTR(model);
	MAKSTR(target);
	MAKSTR(targetname);
	MAKSTR(netname);
	MAKSTR(message);
		
	MAKSTR(noise);
	MAKSTR(noise1);
	MAKSTR(noise2);
	MAKSTR(noise3);
	MAKFLO(speed);
	MAKFLO(maxspeed);
	MAKFLO(fov);
	
	MAKINT(iuser1);
	MAKINT(iuser2);
	MAKINT(iuser3);
	MAKINT(iuser4);
	MAKFLO(fuser1);
	MAKFLO(fuser2);
	MAKFLO(fuser3);
	MAKFLO(fuser4);
	MAKVEC(vuser1);
	MAKVEC(vuser2);
	MAKVEC(vuser3);
	MAKVEC(vuser4);
};

LINK_ENTITY_TO_CLASS(trigger_changekeyvalue, CTriggerChangeKeyValue);

TYPEDESCRIPTION	CTriggerChangeKeyValue::m_SaveData[] =
{
	DEFINE_ARRAY(CTriggerChangeKeyValue, m_iKey, FIELD_STRING, MAX_CHANGE_KEYVALUES),
	DEFINE_ARRAY(CTriggerChangeKeyValue, m_iValue, FIELD_STRING, MAX_CHANGE_KEYVALUES),
	DEFINE_FIELD(CTriggerChangeKeyValue, m_changeTargetName, FIELD_STRING),
	DEFINE_FIELD(CTriggerChangeKeyValue, m_iChangeCount, FIELD_INTEGER),

	SAVINT(skin),
	SAVINT(body),
	SAVINT(effects),
	SAVFLO(gravity),
	SAVFLO(friction),

	SAVINT(sequence),
	SAVFLO(frame),
	SAVFLO(animtime),
	SAVFLO(scale),
	SAVINT(rendermode),
	SAVFLO(renderamt),

	SAVVEC(rendercolor),
	SAVINT(renderfx),
	SAVFLO(health),
	SAVFLO(frags),
	SAVINT(weapons),
	SAVFLO(takedamage),
	
	SAVINT(impulse),
	SAVINT(spawnflags),
	SAVINT(flags),
	SAVINT(team),
	SAVFLO(max_health),
	SAVFLO(armortype),
	SAVFLO(armorvalue),

	SAVSTR(model),
	SAVSTR(target),
	SAVSTR(targetname),
	SAVSTR(netname),
	SAVSTR(message),
		
	SAVSTR(noise),
	SAVSTR(noise1),
	SAVSTR(noise2),
	SAVSTR(noise3),
	SAVFLO(speed),
	SAVFLO(maxspeed),
	SAVFLO(fov),

	SAVINT(iuser1),
	SAVFLO(fuser1),
	SAVVEC(vuser1),
	SAVINT(iuser2),
	SAVFLO(fuser2),
	SAVVEC(vuser2),
	SAVINT(iuser3),
	SAVFLO(fuser3),
	SAVVEC(vuser3),
	SAVINT(iuser4),
	SAVFLO(fuser4),
	SAVVEC(vuser4),
};

IMPLEMENT_SAVERESTORE(CTriggerChangeKeyValue, CBaseDelay);

void CTriggerChangeKeyValue::KeyValue(KeyValueData* pkvd)
{
	//Make sure base class keys are handled properly
	if (FStrEq("origin", pkvd->szKeyName)
		|| FStrEq("target", pkvd->szKeyName)
		|| FStrEq("targetname", pkvd->szKeyName)
		|| FStrEq("delay", pkvd->szKeyName)
		|| FStrEq("classname", pkvd->szKeyName))
	{
		CBaseDelay::KeyValue(pkvd);
	}
	else if (FStrEq("changetarget", pkvd->szKeyName))
	{
		m_changeTargetName = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = true;
	}
	else if ((FStrEq("NOVALUE", pkvd->szKeyName)) || (FStrEq("NOVALUE0", pkvd->szKeyName)) || (FStrEq("NOVALUE1", pkvd->szKeyName)) || (FStrEq("NOVALUE2", pkvd->szKeyName))
		|| (FStrEq("NOVALUE5", pkvd->szKeyName)) || (FStrEq("NOVALUE4", pkvd->szKeyName)))
	{
		//Don't bother handling the hammer message
		pkvd->fHandled = true;
	}
	else if (FStrEq("to_framerate", pkvd->szKeyName))
	{
		//Horribly busted, this is a catcher for it.
		pkvd->fHandled = true;
	}

	KEYINT(skin)
	KEYINT(body)
	KEYINT(effects)
	KEYFLO(gravity)
	KEYFLO(friction)

	KEYINT(sequence)
	KEYFLO(frame)
	KEYFLO(animtime)
	KEYFLO(scale)
	KEYINT(rendermode)
	KEYFLO(renderamt)
		
	KEYVEC(rendercolor)
	KEYINT(renderfx)
	KEYFLO(health)
	KEYFLO(frags)
	KEYINT(weapons)
	KEYFLO(takedamage)
	
	KEYINT(impulse)
	KEYINT(spawnflags)
	KEYINT(flags)
	KEYINT(team)
	KEYFLO(max_health)
	KEYFLO(armortype)
	KEYFLO(armorvalue)
		
	KEYSTR(model)
	KEYSTR(target)
	KEYSTR(targetname)
	KEYSTR(netname)
	KEYSTR(message)
		
	KEYSTR(noise)
	KEYSTR(noise1)
	KEYSTR(noise2)
	KEYSTR(noise3)
	KEYFLO(speed)
	KEYFLO(maxspeed)
	KEYFLO(fov)
		
	KEYINT(iuser1)
	KEYFLO(fuser1)
	KEYINT(iuser2)
	KEYFLO(fuser2)
	KEYINT(iuser3)
	KEYFLO(fuser3)
	KEYINT(iuser4)
	KEYFLO(fuser4)

	KEYVECDIR(vuser1, x)
	KEYVECDIR(vuser1, y)
	KEYVECDIR(vuser1, z)
	KEYVECDIR(vuser2, x)
	KEYVECDIR(vuser2, y)
	KEYVECDIR(vuser2, z)
	KEYVECDIR(vuser3, x)
	KEYVECDIR(vuser3, y)
	KEYVECDIR(vuser3, z)
	KEYVECDIR(vuser4, x)
	KEYVECDIR(vuser4, y)
	KEYVECDIR(vuser4, z)

	//TODO: this was missing an else and caused "changetarget" to be added here
	else if ( m_iChangeCount < MAX_CHANGE_KEYVALUES )
	{
		char temp[256];

		UTIL_StripToken(pkvd->szKeyName, temp);
		m_iKey[m_iChangeCount] = ALLOC_STRING(temp);

		UTIL_StripToken(pkvd->szValue, temp);
		m_iValue[m_iChangeCount] = ALLOC_STRING(temp);

		m_iChangeCount++;

		pkvd->fHandled = true;
	}
}

void CTriggerChangeKeyValue::Precache()
{
	if (!FStringNull(m_strToPev_model))
		PRECACHE_MODEL(STRING(m_strToPev_model));
	
	if (!FStringNull(m_strToPev_noise))
		PRECACHE_SOUND(STRING(m_strToPev_noise));
	
	if (!FStringNull(m_strToPev_noise1))
		PRECACHE_SOUND(STRING(m_strToPev_noise1));

	if (!FStringNull(m_strToPev_noise2))
		PRECACHE_SOUND(STRING(m_strToPev_noise2));

	if (!FStringNull(m_strToPev_noise3))
		PRECACHE_SOUND(STRING(m_strToPev_noise3));
}

void CTriggerChangeKeyValue::Spawn()
{
	Precache();

	//ALERT(at_console, "CTriggerChangeKeyValue::Spawn - My targetname is %s, my m_iChangeCount is %d, and my target is %s.\n", 
	//	STRING(pev->targetname), m_iChangeCount, STRING(pev->target));
}

void CTriggerChangeKeyValue::Use(CBaseEntity* pActivator, CBaseEntity* pCaller, USE_TYPE useType, float value)
{
	char tmpkey[128];
	char tmpvalue[128];

	CBaseEntity* targetEnt = nullptr;

	ALERT(at_console, "CTriggerChangeKeyValue::Use - My targetname is %s\n", STRING(pev->targetname));

	while ((targetEnt = UTIL_FindEntityByString2(targetEnt, "targetname", STRING(pev->target))) != nullptr)
	{
		ALERT(at_console, "CTriggerChangeKeyValue::Use - Firing at target %s\n", STRING(pev->target));

		for (int i = 0; i < m_iChangeCount; ++i)
		{
			//Original code used strcpy here (unsafe)
			strncpy(tmpkey, STRING(m_iKey[i]), sizeof(tmpkey) - 1);
			tmpkey[sizeof(tmpkey) - 1] = '\0';

			strncpy(tmpvalue, STRING(m_iValue[i]), sizeof(tmpvalue) - 1);
			tmpvalue[sizeof(tmpvalue) - 1] = '\0';

			//Original code dynamically allocated kvd here (unnecessary)
			KeyValueData kvd{};

			kvd.szKeyName = tmpkey;
			kvd.szValue = tmpvalue;
			kvd.fHandled = false;
			kvd.szClassName = STRING(targetEnt->pev->classname);

			ALERT(at_console, "CTriggerChangeKeyValue::Use - Modifying Key %s, setting to value %s\n", tmpkey, tmpvalue);

			DispatchKeyValue(targetEnt->pev->pContainingEntity, &kvd);
		}

		if (!FStringNull(m_changeTargetName))
		{
			targetEnt->pev->target = m_changeTargetName;
		}

	SNDINT(skin)
	SNDINT(body)
	SNDINT(effects)
	SNDFLO(gravity)
	SNDFLO(friction)

	SNDINT(sequence)
	SNDFLO(frame)
	SNDFLO(animtime)
	SNDFLO(scale)
	SNDINT(rendermode)
	SNDFLO(renderamt)
			
	SNDVEC(rendercolor)
	SNDINT(renderfx)
	SNDFLO(health)
	SNDFLO(frags)
	SNDINT(weapons)
	SNDFLO(takedamage)

		//Not handled with a Marco for a tiny bit of behavour tweaking
		if (!FStringNull(m_strToPev_model))
		{
			targetEnt->pev->model = m_strToPev_model;
			SET_MODEL(ENT(targetEnt->pev), STRING(m_strToPev_model));
		}
	
	SNDSTR(target)
	SNDSTR(targetname)
	SNDSTR(netname)
	SNDSTR(message)
		
	SNDSTR(noise)
	SNDSTR(noise1)
	SNDSTR(noise2)
	SNDSTR(noise3)
	SNDFLO(speed)
	SNDFLO(maxspeed)
	SNDFLO(fov)

	SNDINT(iuser1)
	SNDFLO(fuser1)
	SNDVEC(vuser1)
	SNDINT(iuser2)
	SNDFLO(fuser2)
	SNDVEC(vuser2)
	SNDINT(iuser3)
	SNDFLO(fuser3)
	SNDVEC(vuser3)
	SNDINT(iuser4)
	SNDFLO(fuser4)
	SNDVEC(vuser4)
	}
}
