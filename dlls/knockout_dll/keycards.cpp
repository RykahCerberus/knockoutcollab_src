
#include "extdll.h"
#include "util.h"
#include "cbase.h"
#include "weapons.h"
#include "player.h"
#include "skill.h"
#include "items.h"
#include "gamerules.h"
#include "UserMessages.h"
#include "keycards.h"
#include "animation.h"

class CItemKeycard : public CItem
{
	void Spawn() override
	{
		HandleClassname();

		Precache();

		if (pev->model)
			SET_MODEL(ENT(pev), STRING(pev->model));
		else if (gpgs && gpgs->m_iszKeycardModel)
			SET_MODEL(ENT(pev), STRING(gpgs->m_iszKeycardModel));
		else
			SET_MODEL(ENT(pev), "models/keycard.mdl");

		CItem::Spawn();
	}
	void Precache() override
	{
		if (pev->model)
			PRECACHE_MODEL((char*)STRING(pev->model));
		else if (gpgs && gpgs->m_iszKeycardModel)
			PRECACHE_MODEL((char*)STRING(gpgs->m_iszKeycardModel));
		else
			PRECACHE_MODEL("models/keycard.mdl");

		if (pev->noise)
			PRECACHE_SOUND((char*)STRING(pev->noise));
		else
			PRECACHE_SOUND("items/cardkey_pickup.wav");
	}
	BOOL MyTouch(CBasePlayer* pPlayer) override
	{
		if (!pPlayer->HasKeycardType(KeycardTypes(pev->skin)))
		{
			if (pev->noise)
				EMIT_SOUND(pPlayer->edict(), CHAN_ITEM, STRING(pev->noise), 1, ATTN_NORM); //LRC
			else
				EMIT_SOUND(pPlayer->edict(), CHAN_ITEM, "items/cardkey_pickup.wav", 1, ATTN_NORM);

			const char* name = STRING(pev->classname); 

			switch (pev->skin)
			{
			case KEYCARD_TYPE_RED: name = "item_keycard_red"; break;
			case KEYCARD_TYPE_BLUE: name = "item_keycard_blue"; break;
			case KEYCARD_TYPE_YELLOW: name = "item_keycard_yellow"; break;
			case KEYCARD_TYPE_GREEN: name = "item_keycard_green"; break;
			case KEYCARD_TYPE_PURPLE: name = "item_keycard_purple"; break;
			case KEYCARD_TYPE_ORANGE: name = "item_keycard_orange"; break;
			case KEYCARD_TYPE_PINK: name = "item_keycard_pink"; break;
			case KEYCARD_TYPE_TEAL: name = "item_keycard_teal"; break;
			case KEYCARD_TYPE_LIME: name = "item_keycard_lime"; break;
			case KEYCARD_TYPE_WHITE: name = "item_keycard_white"; break;
			case KEYCARD_TYPE_BLACK: name = "item_keycard_black"; break;
			default: ALERT(at_console, "Invaild keycard picked up?! type is %d\n", pev->skin); break;
			}

			if (pev->netname)
				name = STRING(pev->netname);

			MESSAGE_BEGIN(MSG_ONE, gmsgItemPickup, NULL, pPlayer->pev);
				WRITE_STRING(name);
			MESSAGE_END();

			pPlayer->GiveKeycard(KeycardTypes(pev->skin));

			return TRUE;
		}
		return FALSE;
	}
	void HandleClassname()
	{
		if (strncmp(STRING(pev->classname), "item_keycard_red", 17) == 0)
			pev->skin = KEYCARD_TYPE_RED;
		else if (strncmp(STRING(pev->classname), "item_keycard_blue", 18) == 0)
			pev->skin = KEYCARD_TYPE_BLUE;
		else if (strncmp(STRING(pev->classname), "item_keycard_yellow", 20) == 0)
			pev->skin = KEYCARD_TYPE_YELLOW;
		else if (strncmp(STRING(pev->classname), "item_keycard_green", 19) == 0)
			pev->skin = KEYCARD_TYPE_GREEN;
		else if (strncmp(STRING(pev->classname), "item_keycard_purple", 20) == 0)
			pev->skin = KEYCARD_TYPE_PURPLE;
		else if (strncmp(STRING(pev->classname), "item_keycard_orange", 20) == 0)
			pev->skin = KEYCARD_TYPE_ORANGE;
		else if (strncmp(STRING(pev->classname), "item_keycard_black", 19) == 0)
			pev->skin = KEYCARD_TYPE_BLACK;
		else if (strncmp(STRING(pev->classname), "item_keycard_white", 19) == 0)
			pev->skin = KEYCARD_TYPE_WHITE;
		else if (strncmp(STRING(pev->classname), "item_keycard_pink", 18) == 0)
			pev->skin = KEYCARD_TYPE_PINK;
		else if (strncmp(STRING(pev->classname), "item_keycard_lime", 18) == 0)
			pev->skin = KEYCARD_TYPE_LIME;
		else if (strncmp(STRING(pev->classname), "item_keycard_teal", 18) == 0)
			pev->skin = KEYCARD_TYPE_TEAL;
	}
};

LINK_ENTITY_TO_CLASS(item_keycard, CItemKeycard);

LINK_ENTITY_TO_CLASS(item_keycard_red, CItemKeycard);
LINK_ENTITY_TO_CLASS(item_keycard_blue, CItemKeycard);
LINK_ENTITY_TO_CLASS(item_keycard_yellow, CItemKeycard);
LINK_ENTITY_TO_CLASS(item_keycard_green, CItemKeycard);
LINK_ENTITY_TO_CLASS(item_keycard_purple, CItemKeycard);
LINK_ENTITY_TO_CLASS(item_keycard_orange, CItemKeycard);
LINK_ENTITY_TO_CLASS(item_keycard_black, CItemKeycard);
LINK_ENTITY_TO_CLASS(item_keycard_white, CItemKeycard);
LINK_ENTITY_TO_CLASS(item_keycard_pink, CItemKeycard);
LINK_ENTITY_TO_CLASS(item_keycard_lime, CItemKeycard);
LINK_ENTITY_TO_CLASS(item_keycard_teal, CItemKeycard);


char* GetKeycardName(int card, bool localized)
{
	char* name = "INVAILD KEYCARD";
	char* Lname = "INVAILD KEYCARD";

	switch (card)
	{
	case KEYCARD_TYPE_RED:
		name = "Red";
		Lname = "#Keycard_Red";
		break;

	case KEYCARD_TYPE_BLUE:
		name = "Blue";
		Lname = "#Keycard_Blue";
		break;

	case KEYCARD_TYPE_YELLOW:
		name = "Yellow";
		Lname = "#Keycard_Yellow";
		break;

	case KEYCARD_TYPE_GREEN:
		name = "Green";
		Lname = "#Keycard_Green";
		break;

	case KEYCARD_TYPE_PURPLE:
		name = "Purple";
		Lname = "#Keycard_Purple";
		break;

	case KEYCARD_TYPE_ORANGE:
		name = "Orange";
		Lname = "#Keycard_Orange";
		break;

	case KEYCARD_TYPE_PINK:
		name = "Pink";
		Lname = "#Keycard_Pink";
		break;

	case KEYCARD_TYPE_TEAL:
		name = "Teal";
		Lname = "#Keycard_Teal";
		break;

	case KEYCARD_TYPE_LIME:
		name = "Lime";
		Lname = "#Keycard_Lime";
		break;

	case KEYCARD_TYPE_WHITE:
		name = "White";
		Lname = "#Keycard_White";
		break;

	case KEYCARD_TYPE_BLACK:
		name = "Black";
		Lname = "#Keycard_Black";
		break;
	}

	if (localized)
		return Lname;
	else
		return name;
}
