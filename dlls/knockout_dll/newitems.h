/***
*
*	Copyright (c) 1996-2001, Valve LLC. All rights reserved.
*
*	This product contains software technology licensed from Id
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc.
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/
#ifndef NEWITEMS_H
#define NEWITEMS_H

#include "weapons.h"

class CCustomAmmoClip : public CBasePlayerAmmo
{
public:
	int m_iMaxCarry;
	const char* m_chzEntToGive;
	void Spawn() override;
	void Precache() override;
	const char* HandleAmmoType();
	BOOL AddAmmo(CBaseEntity* pOther) override;
	static CCustomAmmoClip *CreatePickup(CBaseEntity* pVictim, int type, int ammount, char* model = "models/w_9mmARclip.mdl", char* sound = "items/9mmclip1.wav");

	string_t m_strTarget;
	void	KeyValue( KeyValueData* pkvd) override;

	int		Save(CSave& save) override;
	int		Restore(CRestore& restore) override;
	static	TYPEDESCRIPTION m_SaveData[];
};

#endif
