
#include "extdll.h"
#include "util.h"
#include "cbase.h"
#include "game_settings.h"
#include "UserMessages.h"
#include "player.h"
#include "monsters.h"
#include "customentity.h"
#include "effects.h"
#include "cdll_dll.h"
#include "weapons.h"
#include "knockout_dll/game_settings.h"

void CWorld::KeyValueGameSettings( KeyValueData *pkvd )
{
	if ( FStrEq(pkvd->szKeyName, "IsKnockoutCollab") )
	{
		m_bIsCollab = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "StripKeycards"))
	{
		m_bStripKeycards = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	
	else if ( FStrEq(pkvd->szKeyName, "KeepSuitOnSpawn") )
	{
		m_bStartWithSuit = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "SuitSoundPrefix") )
	{
		m_iszSuitSoundType = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "GaussChargeTime"))
	{
		m_flGaussMaxCharge = atof(pkvd->szValue);
		CVAR_SET_FLOAT("__wpn_gauss_charge", atof(pkvd->szValue));
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "RevolverIsMP"))
	{
		m_bReolverIsMP = atoi(pkvd->szValue);
		CVAR_SET_FLOAT("__wpn_revolver_mp", m_bReolverIsMP);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "PlayerArmorMax") )
	{
		m_iPlayerMaxArmor = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "PlayerMaxHealth") )
	{
		m_iPlayerMaxHealth = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "CSPainSounds"))
	{
		m_bCSPainSounds = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "JumpSound"))
	{
		m_iszJumpSound = ALLOC_STRING(pkvd->szValue);
		CVAR_SET_STRING("__sound_jump_string", STRING(m_iszJumpSound));
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "PlayJumpSound"))
	{
		m_bPlayJumpSound = atoi(pkvd->szValue);
		CVAR_SET_FLOAT("__sound_jump", m_bPlayJumpSound);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "CSPainSounds"))
	{
		m_bCSPainSounds = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}

	else if (FStrEq(pkvd->szKeyName, "PlayerHudColor"))
	{
		int color[3];

		UTIL_StringToIntArray( color, 3, pkvd->szValue );
		m_vecPlayerHudColor.x = color[0];
		m_vecPlayerHudColor.y = color[1];
		m_vecPlayerHudColor.z = color[2];

		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "CrosshairColor") )
	{
		m_iCrosshairColor = atoi(pkvd->szValue);
		CVAR_SET_FLOAT("__hud_crosshair_color", m_iCrosshairColor);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "HudColor"))
	{
		m_iHudColor = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	
	else if ( FStrEq(pkvd->szKeyName, "XenReturn") )
	{
		m_bXenReturn = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "XenGravity") )
	{
		m_flXenGravity = atof(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	
	else if ( FStrEq(pkvd->szKeyName, "LavaDamage") )
	{
		m_flLavaDamage = atof(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "SlimeDamage"))
	{
		m_flSlimeDamage = atof(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "PowerupTimerCombine"))
	{
		m_bCombinePowerupTimers = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	
	else if (FStrEq(pkvd->szKeyName, "FlashlightType"))
	{
		m_iFlashlightBehavour = atoi(pkvd->szValue);
		CVAR_SET_FLOAT("__hud_flashlight_behavour", m_iFlashlightBehavour);
		pkvd->fHandled = TRUE;
	}
	
	else if (FStrEq(pkvd->szKeyName, "Flashlight_Icon"))
	{
		m_iszFlashlightPostfix = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Battery_Icon"))
	{
		m_iszArmorPostfix = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Health_Icon"))
	{
		m_iszHealthPostfix = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Numbers_Icon"))
	{
		m_iszNumberFontPostfix = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Selection_Icon"))
	{
		m_iszSelectionSpritePostfix = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}

	else
		KeyValueWeapons( pkvd );
}

void CWorld::KeyValueWeapons(KeyValueData* pkvd)
{
	if ( FStrEq(pkvd->szKeyName, "MP5_View") )
	{
		m_iszMP5View = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "MP5_World") )
	{
		m_iszMP5World = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "MP5_Thrown") )
	{
		m_iszMP5Thrown = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "MP5_Sound") )
	{
		m_iszMP5Sound = ALLOC_STRING(pkvd->szValue);
		CVAR_SET_STRING("__sound_mp5", STRING(m_iszMP5Sound));
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "MP5_SoundLaunch") )
	{
		m_iszMP5SoundLauncher = ALLOC_STRING(pkvd->szValue);
		CVAR_SET_STRING("__sound_mp5_launcher", STRING(m_iszMP5SoundLauncher));
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "MP5_SoundNPC"))
	{
		m_iszMP5SoundNPC = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}

	else if ( FStrEq(pkvd->szKeyName, "Glock_View") )
	{
		m_iszGlockView = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Glock_World") )
	{
		m_iszGlockWorld = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Glock_Sound") )
	{
		m_iszGlockSound = ALLOC_STRING(pkvd->szValue);
		CVAR_SET_STRING("__sound_glock", STRING(m_iszGlockSound));
		pkvd->fHandled = TRUE;
	}

	else if ( FStrEq(pkvd->szKeyName, "357_View") )
	{
		m_isz357View = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "357_World") )
	{
		m_isz357World = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "357_Sound") )
	{
		m_isz357Sound = ALLOC_STRING(pkvd->szValue);
		CVAR_SET_STRING("__sound_357", STRING(m_isz357Sound));
		pkvd->fHandled = TRUE;
	}

	else if ( FStrEq(pkvd->szKeyName, "Crossbow_View") )
	{
		m_iszCrossbowView = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Crossbow_World") )
	{
		m_iszCrossbowWorld = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Crossbow_Thrown") )
	{
		m_iszCrossbowThrown = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}

	else if ( FStrEq(pkvd->szKeyName, "Crowbar_View") )
	{
		m_iszCrowbarView = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Crowbar_World") )
	{
		m_iszCrowbarWorld = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}

	else if ( FStrEq(pkvd->szKeyName, "Displacer_View") )
	{
		m_iszDisplacerView = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Displacer_World") )
	{
		m_iszDisplacerWorld = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}

	else if (FStrEq(pkvd->szKeyName, "Eagle_View"))
	{
		m_iszEagleView = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Eagle_World"))
	{
		m_iszEagleWorld = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Eagle_Sound"))
	{
		m_iszEagleSound = ALLOC_STRING(pkvd->szValue);
		CVAR_SET_STRING("__sound_eagle", STRING(m_iszEagleSound));
		pkvd->fHandled = TRUE;
	}

	else if ( FStrEq(pkvd->szKeyName, "Egon_View") )
	{
		m_iszEgonView = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Egon_World") )
	{
		m_iszEgonWorld = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}

	else if ( FStrEq(pkvd->szKeyName, "HandGrenade_View") )
	{
		m_iszHandGrenadeView = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "HandGrenade_World") )
	{
		m_iszHandGrenadeWorld = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "HandGrenade_Thrown") )
	{
		m_iszHandGrenadeThrown = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}

	else if ( FStrEq(pkvd->szKeyName, "Gauss_View") )
	{
		m_iszGaussView = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Gauss_World") )
	{
		m_iszGaussWorld = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}

	else if ( FStrEq(pkvd->szKeyName, "Grapple_View") )
	{
		m_iszGrappleView = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Grapple_World") )
	{
		m_iszGrappleWorld = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}

	else if ( FStrEq(pkvd->szKeyName, "Hivehand_View") )
	{
		m_iszHivehandView = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Hivehand_World") )
	{
		m_iszHivehandWorld = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}

	else if ( FStrEq(pkvd->szKeyName, "Knife_View") )
	{
		m_iszKnifeView = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Knife_World") )
	{
		m_iszKnifeWorld = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}

	else if (FStrEq(pkvd->szKeyName, "M249_View"))
	{
		m_iszM249View = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "M249_World"))
	{
		m_iszM249World = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "M249_Sound"))
	{
		m_iszM249Sound = ALLOC_STRING(pkvd->szValue);
		CVAR_SET_STRING("__sound_m249", STRING(m_iszM249Sound));
		pkvd->fHandled = TRUE;
	}

	else if ( FStrEq(pkvd->szKeyName, "Penguin_View") )
	{
		m_iszPenguinView = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Penguin_World") )
	{
		m_iszPenguinWorld = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Penguin_Thrown") )
	{
		m_iszPenguinThrown = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}

	else if ( FStrEq(pkvd->szKeyName, "Pipewrench_View") )
	{
		m_iszPipewrenchView = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Pipewrench_World") )
	{
		m_iszPipewrenchWorld = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}

	else if ( FStrEq(pkvd->szKeyName, "RPG_View") )
	{
		m_iszRPGView = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "RPG_World") )
	{
		m_iszRPGWorld = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "RPG_Thrown") )
	{
		m_iszRPGThrown = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}

	else if ( FStrEq(pkvd->szKeyName, "Satchel_View") )
	{
		m_iszSatchelView = ALLOC_STRING(pkvd->szValue);
		CVAR_SET_STRING("__model_satchel", STRING(m_iszSatchelView));
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Satchel_ViewRadio") )
	{
		m_iszSatchelViewRadio = ALLOC_STRING(pkvd->szValue);
		CVAR_SET_STRING("__model_satchel_radio", STRING(m_iszSatchelViewRadio));
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Satchel_World") )
	{
		m_iszSatchelWorld = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Satchel_Thrown") )
	{
		m_iszSatchelThrown = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}

	else if ( FStrEq(pkvd->szKeyName, "Shockroach_View") )
	{
		m_iszShockroachView = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Shockroach_World") )
	{
		m_iszShockroachWorld = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}

	else if (FStrEq(pkvd->szKeyName, "Shotgun_View"))
	{
		m_iszShotgunView = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Shotgun_World"))
	{
		m_iszShotgunWorld = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Shotgun_SoundPri"))
	{
		m_iszShotgunSoundPri = ALLOC_STRING(pkvd->szValue);
		CVAR_SET_STRING("__sound_shotgun_primary", STRING(m_iszShotgunSoundPri));
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Shotgun_SoundAlt"))
	{
		m_iszShotgunSoundAlt = ALLOC_STRING(pkvd->szValue);
		CVAR_SET_STRING("__sound_shotgun_altfire", STRING(m_iszShotgunSoundAlt));
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Shotgun_SoundPump"))
	{
		m_iszShotgunSoundPump = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Shotgun_SoundReload"))
	{
		m_iszShotgunSoundReload = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}

	else if ( FStrEq(pkvd->szKeyName, "Snark_View") )
	{
		m_iszSnarkView = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Snark_World") )
	{
		m_iszSnarkWorld = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Snark_Thrown") )
	{
		m_iszSnarkThrown = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}

	else if ( FStrEq(pkvd->szKeyName, "Sniper_View") )
	{
		m_iszSniperView = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Sniper_World") )
	{
		m_iszSniperWorld = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Sniper_Sound"))
	{
		m_iszSniperSound = ALLOC_STRING(pkvd->szValue);
		CVAR_SET_STRING("__sound_sniper", STRING(m_iszSniperSound));
		pkvd->fHandled = TRUE;
	}

	else if ( FStrEq(pkvd->szKeyName, "Sporelauncher_View") )
	{
		m_iszSporelauncherView = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Sporelauncher_World") )
	{
		m_iszSporelauncherWorld = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Sporelauncher_Thrown") )
	{
		m_iszSporelauncherThrown = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	
	else if ( FStrEq(pkvd->szKeyName, "Tripmine_Model") )
	{
		m_iszTripmineModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Tripmine_View") )
	{
		m_iszTripmineView = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Tripmine_World") )
	{
		m_iszTripmineWorld = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Tripmine_Thrown") )
	{
		m_iszTripmineThrown = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}

	else
		KeyValueWeapons2(pkvd);
}

void CWorld::KeyValueItems(KeyValueData* pkvd)
{
	if ( FStrEq(pkvd->szKeyName, "Keycard_Model") )
	{
		m_iszKeycardModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Battery_Model") )
	{
		m_iszBatteryModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Medkit_Model") )
	{
		m_iszMedkitModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Ammo357_Model") )
	{
		m_iszAmmo357Model = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Ammo9mmAR_Model") )
	{
		m_iszAmmo9mmARModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Ammo9mmBox_Model") )
	{
		m_iszAmmo9mmBoxModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Ammo9mmClip_Model") )
	{
		m_iszAmmo9mmClipModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "AmmoARgrenades_Model") )
	{
		m_iszAmmoARGrenadesModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "AmmoShotgun_Model") )
	{
		m_iszAmmoShotgunModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "AmmoCrossbow_Model") )
	{
		m_iszAmmoCrossbowModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "AmmoGauss_Model") )
	{
		m_iszAmmoGaussModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "AmmoRPG_Model") )
	{
		m_iszAmmoRPGModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Ammo556_Model") )
	{
		m_iszAmmo556Model = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Ammo762_Model") )
	{
		m_iszAmmo762Model = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "AmmoSpore_Model") )
	{
		m_iszAmmoSporeModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else
		KeyValueNPCs(pkvd);
}


void CWorld::KeyValueNPCs(KeyValueData* pkvd)
{
	if ( FStrEq(pkvd->szKeyName, "Hornet_Model") )
	{
		m_iszHornetModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Shockroach_Model") )
	{
		m_iszShockroachModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Agrunt_Model") )
	{
		m_iszAgruntModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Apache_Model") )
	{
		m_iszApacheModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "BabyVoltigore_Model") )
	{
		m_iszBabyVoltigoreModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Barnacle_Model") )
	{
		m_iszBarnacleModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Barney_Model") )
	{
		m_iszBarneyModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "BigMomma_Model") )
	{
		m_iszBigMommaModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "BlkApache_Model") )
	{
		m_iszBlkApacheModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "BlkOsprey_Model") )
	{
		m_iszBlkOspreyModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Bullsquid_Model") )
	{
		m_iszBullsquidModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "CleanScientist_Model") )
	{
		m_iszCleanScientistModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Gargantua_Model") )
	{
		m_iszGargantuaModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "GMan_Model") )
	{
		m_iszGManModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Gonome_Model") )
	{
		m_iszGonomeModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "HAssassin_Model") )
	{
		m_iszHAssassinModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Headcrab_Model") )
	{
		m_iszHeadcrabModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "BabyHeadcrab_Model") )
	{
		m_iszBabyHeadcrabModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "HGrunt_Model") )
	{
		m_iszHGruntModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "HGrunt_Ally_Model") )
	{
		m_iszHGruntAllyModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "HGrunt_Medic_Model") )
	{
		m_iszHGruntMedicModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "HGrunt_Torch_Model") )
	{
		m_iszHGruntTorchModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Houndeye_Model") )
	{
		m_iszHoundeyeModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Icthyosaur_Model") )
	{
		m_iszIcthyosaurModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Leech_Model") )
	{
		m_iszLeechModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "MAssassin_Model") )
	{
		m_iszMAssassinModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Osprey_Model") )
	{
		m_iszOspreyModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Otis_Model") )
	{
		m_iszOtisModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Pitdrone_Model") )
	{
		m_iszPitdroneModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Roach_Model") )
	{
		m_iszRoachModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Scientist_Model") )
	{
		m_iszScientistModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Shocktrooper_Model") )
	{
		m_iszShocktrooperModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Tentacle_Model") )
	{
		m_iszTentacleModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Turret_Model") )
	{
		m_iszTurretModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "MiniTurret_Model") )
	{
		m_iszMiniTurretModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Sentry_Model") )
	{
		m_iszSentryModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Voltigore_Model") )
	{
		m_iszVoltigoreModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Zombie_Model") )
	{
		m_iszZombieModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Zombie_Barney_Model") )
	{
		m_iszZombieBarneyModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Zombie_Soldier_Model") )
	{
		m_iszZombieSoldierModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Vortigaunt_Model") )
	{
		m_iszVortigauntModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	
	else if ( FStrEq(pkvd->szKeyName, "Army_Model") )
	{
		m_iszArmyModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Dog_Model") )
	{
		m_iszDogModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Knight_Model") )
	{
		m_iszKnightModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "HKnight_Model") )
	{
		m_iszHKnightModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Wizard_Model") )
	{
		m_iszWizardModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Demon_Model") )
	{
		m_iszDemonModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Enforcer_Model") )
	{
		m_iszEnforcerModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Shalrath_Model") )
	{
		m_iszShalrathModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Tarbaby_Model") )
	{
		m_iszTarbabyModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Fish_Model") )
	{
		m_iszRotfishModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Shambler_Model") )
	{
		m_iszShamblerModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "Ogre_Model") )
	{
		m_iszOgerModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if ( FStrEq(pkvd->szKeyName, "QZombie_Model") )
	{
		m_iszQZombieModel = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else
		CWorld::KeyValueUserRequests( pkvd );
}

void CWorld::KeyValueWeapons2(KeyValueData* pkvd)
{
	if (FStrEq(pkvd->szKeyName, "MP5_Acts"))
	{
		m_iMP5Activity = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "MP5_Flip"))
	{
		m_bMP5Flip = atoi(pkvd->szValue);
		CVAR_SET_FLOAT("__flip_mp5", atoi(pkvd->szValue));
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Glock_Acts"))
	{
		m_iGlockActivity = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Glock_Flip"))
	{
		m_bGlockFlip = atoi(pkvd->szValue);
		CVAR_SET_FLOAT("__flip_glock", atoi(pkvd->szValue));
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "357_Acts"))
	{
		m_i357Activity = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "357_Flip"))
	{
		m_b357Flip = atoi(pkvd->szValue);
		CVAR_SET_FLOAT("__flip_357", atoi(pkvd->szValue));
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Crossbow_Acts"))
	{
		m_iCrossbowActivity = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Crossbow_Flip"))
	{
		m_bCrossbowFlip = atoi(pkvd->szValue);
		CVAR_SET_FLOAT("__flip_crossbow", atoi(pkvd->szValue));
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Crowbar_Acts"))
	{
		m_iCrowbarActivity = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Crowbar_Flip"))
	{
		m_bCrowbarFlip = atoi(pkvd->szValue);
		CVAR_SET_FLOAT("__flip_crowbar", atoi(pkvd->szValue));
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Displacer_Acts"))
	{
		m_iDisplacerActivity = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Displacer_Flip"))
	{
		m_bDisplacerFlip = atoi(pkvd->szValue);
		CVAR_SET_FLOAT("__flip_displacer", atoi(pkvd->szValue));
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Eagle_Acts"))
	{
		m_iEagleActivity = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Eagle_Flip"))
	{
		m_bEagleFlip = atoi(pkvd->szValue);
		CVAR_SET_FLOAT("__flip_eagle", atoi(pkvd->szValue));
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Egon_Acts"))
	{
		m_iEgonActivity = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Egon_Flip"))
	{
		m_bEgonFlip = atoi(pkvd->szValue);
		CVAR_SET_FLOAT("__flip_egon", atoi(pkvd->szValue));
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "HandGrenade_Acts"))
	{
		m_iHandGrenadeActivity = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "HandGrenade_Flip"))
	{
		m_bHandGrenadeFlip = atoi(pkvd->szValue);
		CVAR_SET_FLOAT("__flip_handgrenade", atoi(pkvd->szValue));
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Gauss_Acts"))
	{
		m_iGaussActivity = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Gauss_Flip"))
	{
		m_bGaussFlip = atoi(pkvd->szValue);
		CVAR_SET_FLOAT("__flip_gauss", atoi(pkvd->szValue));
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Grapple_Acts"))
	{
		m_iGrappleActivity = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Grapple_Flip"))
	{
		m_bGrappleFlip = atoi(pkvd->szValue);
		CVAR_SET_FLOAT("__flip_grapple", atoi(pkvd->szValue));
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Hivehand_Acts"))
	{
		m_iHivehandActivity = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Hivehand_Flip"))
	{
		m_bHivehandFlip = atoi(pkvd->szValue);
		CVAR_SET_FLOAT("__flip_hivehand", atoi(pkvd->szValue));
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Knife_Acts"))
	{
		m_iKnifeActivity = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Knife_Flip"))
	{
		m_bKnifeFlip = atoi(pkvd->szValue);
		CVAR_SET_FLOAT("__flip_knife", atoi(pkvd->szValue));
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "M249_Acts"))
	{
		m_iM249Activity = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "M249_Flip"))
	{
		m_bM249Flip = atoi(pkvd->szValue);
		CVAR_SET_FLOAT("__flip_m249", atoi(pkvd->szValue));
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Penguin_Acts"))
	{
		m_iPenguinActivity = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Penguin_Flip"))
	{
		m_bPenguinFlip = atoi(pkvd->szValue);
		CVAR_SET_FLOAT("__flip_penguin", atoi(pkvd->szValue));
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Pipewrench_Acts"))
	{
		m_iPipeWrenchActivity = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Pipewrench_Flip"))
	{
		m_bPipeWrenchFlip = atoi(pkvd->szValue);
		CVAR_SET_FLOAT("__flip_pipewrench", atoi(pkvd->szValue));
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "RPG_Acts"))
	{
		m_iRPGActivity = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "RPG_Flip"))
	{
		m_bRPGFlip = atoi(pkvd->szValue);
		CVAR_SET_FLOAT("__flip_rpg", atoi(pkvd->szValue));
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Satchel_Acts"))
	{
		m_iSatchelActivity = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Satchel_Flip"))
	{
		m_bSatchelFlip = atoi(pkvd->szValue);
		CVAR_SET_FLOAT("__flip_satchel", atoi(pkvd->szValue));
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Satchel_ActsRadio"))
	{
		m_iSatchelRadioActivity = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Satchel_FlipRadio"))
	{
		m_bSatchelRadioFlip = atoi(pkvd->szValue);
		CVAR_SET_FLOAT("__flip_radio", atoi(pkvd->szValue));
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Shockroach_Acts"))
	{
		m_iShockroachActivity = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Shockroach_Flip"))
	{
		m_bShockroachFlip = atoi(pkvd->szValue);
		CVAR_SET_FLOAT("__flip_shockroach", atoi(pkvd->szValue));
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Shotgun_Acts"))
	{
		m_iShotgunActivity = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Shotgun_Flip"))
	{
		m_bShotgunFlip = atoi(pkvd->szValue);
		CVAR_SET_FLOAT("__flip_shotgun", atoi(pkvd->szValue));
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Snark_Acts"))
	{
		m_iSnarkActivity = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Snark_Flip"))
	{
		m_bSnarkFlip = atoi(pkvd->szValue);
		CVAR_SET_FLOAT("__flip_snark", atoi(pkvd->szValue));
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Sniper_Acts"))
	{
		m_iSniperActivity = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Sniper_Flip"))
	{
		m_bSniperFlip = atoi(pkvd->szValue);
		CVAR_SET_FLOAT("__flip_sniper", atoi(pkvd->szValue));
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Sporelauncher_Acts"))
	{
		m_iSporelauncherActivity = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Sporelauncher_Flip"))
	{
		m_bSporelauncherFlip = atoi(pkvd->szValue);
		CVAR_SET_FLOAT("__flip_sporelauncher", atoi(pkvd->szValue));
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Tripmine_Acts"))
	{
		m_iTripmineActivity = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "Tripmine_Flip"))
	{
		m_bTripmineFlip = atoi(pkvd->szValue);
		CVAR_SET_FLOAT("__flip_tripmine", atoi(pkvd->szValue));
		pkvd->fHandled = TRUE;
	}
	else
		KeyValueItems(pkvd);
}


void CWorld::KeyValueUserRequests(KeyValueData* pkvd)
{
	if (FStrEq(pkvd->szKeyName, "LeapingJump"))
	{
		m_bLeapingJump = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "NoJump"))
	{
		m_bNoJump = atoi(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else
		CBaseEntity::KeyValue(pkvd);
}
