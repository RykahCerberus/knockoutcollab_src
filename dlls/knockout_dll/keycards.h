/***
*
*	Copyright (c) 1996-2001, Valve LLC. All rights reserved.
*
*	This product contains software technology licensed from Id
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc.
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/
#ifndef KOC_KEYCARDS_H
#define KOC_KEYCARDS_H

enum KeycardTypes
{
	KEYCARD_TYPE_RED = 0,
	KEYCARD_TYPE_BLUE,
	KEYCARD_TYPE_YELLOW,
	KEYCARD_TYPE_GREEN,
	KEYCARD_TYPE_PURPLE,
	KEYCARD_TYPE_ORANGE,
	KEYCARD_TYPE_PINK,
	KEYCARD_TYPE_TEAL,
	KEYCARD_TYPE_LIME,
	KEYCARD_TYPE_WHITE,
	KEYCARD_TYPE_BLACK,

	//MUST ALWAYS BE LAST, AND UPDATE THIS
	KEYCARD_TYPE_LAST = KEYCARD_TYPE_BLACK,
	KEYCARD_TYPE_FIRST = KEYCARD_TYPE_RED,
};

char* GetKeycardName(int card, bool localized);

#endif
