
#include "extdll.h"
#include "util.h"
#include "cbase.h"
#include "game_settings.h"
#include "UserMessages.h"
#include "player.h"
#include "monsters.h"
#include "customentity.h"
#include "effects.h"
#include "cdll_dll.h"
#include "weapons.h"
#include "knockout_dll/game_settings.h"
#include <string>


void CWorld::PrecacheOverrides()
{
	if (m_iszJumpSound)
		PRECACHE_SOUND(STRING(m_iszJumpSound));

	if (m_iszMP5View)
		PRECACHE_MODEL(STRING(m_iszMP5View));

	if (m_iszMP5World)
		PRECACHE_MODEL(STRING(m_iszMP5World));

	if (m_iszMP5Thrown)
		PRECACHE_MODEL(STRING(m_iszMP5Thrown));
	
	if (m_iszMP5Sound)
	{
		PRECACHE_SOUND(STRING(m_iszMP5Sound));

		std::string critsound;
		critsound.append(STRING(m_iszMP5Sound));
		critsound.resize(critsound.size() - 4); //this trims the .wav
		critsound.append("_crit.wav");
		//PRECACHE_SOUND(critsound.c_str());
	}
	
	if (m_iszMP5SoundLauncher)
	{
		PRECACHE_SOUND(STRING(m_iszMP5SoundLauncher));

		std::string critsound2;
		critsound2.append(STRING(m_iszMP5SoundLauncher));
		critsound2.resize(critsound2.size() -4); //this trims the .wav
		critsound2.append("_crit.wav");
		//PRECACHE_SOUND(critsound2.c_str());
	}


	if (m_iszGlockView)
		PRECACHE_MODEL(STRING(m_iszGlockView));

	if (m_iszGlockWorld)
		PRECACHE_MODEL(STRING(m_iszGlockWorld));

	if (m_iszGlockSound)
	{
		PRECACHE_SOUND(STRING(m_iszGlockSound));

		std::string critsound3;
		critsound3.append(STRING(m_iszGlockSound));
		critsound3.resize(critsound3.size() -4); //this trims the .wav
		critsound3.append("_crit.wav");
		//PRECACHE_SOUND(critsound3.c_str());
	}

	if (m_isz357View)
		PRECACHE_MODEL(STRING(m_isz357View));

	if (m_isz357World)
		PRECACHE_MODEL(STRING(m_isz357World));
	
	if (m_isz357Sound)
	{
		PRECACHE_SOUND(STRING(m_isz357Sound));

		std::string critsound4;
		critsound4.append(STRING(m_isz357Sound));
		critsound4.resize(critsound4.size() -4); //this trims the .wav
		critsound4.append("_crit.wav");
		//PRECACHE_SOUND(critsound4.c_str());
	}
	
	if (m_iszCrossbowView)
		PRECACHE_MODEL(STRING(m_iszCrossbowView));

	if (m_iszCrossbowWorld)
		PRECACHE_MODEL(STRING(m_iszCrossbowWorld));

	if (m_iszCrossbowThrown)
		PRECACHE_MODEL(STRING(m_iszCrossbowThrown));
	
	
	if (m_iszCrowbarView)
		PRECACHE_MODEL(STRING(m_iszCrowbarView));

	if (m_iszCrowbarWorld)
		PRECACHE_MODEL(STRING(m_iszCrowbarWorld));
	
	
	if (m_iszDisplacerView)
		PRECACHE_MODEL(STRING(m_iszDisplacerView));

	if (m_iszDisplacerWorld)
		PRECACHE_MODEL(STRING(m_iszDisplacerWorld));
	

	if (m_iszEagleView)
		PRECACHE_MODEL(STRING(m_iszEagleView));

	if (m_iszEagleWorld)
		PRECACHE_MODEL(STRING(m_iszEagleWorld));

	if (m_iszEagleSound)
	{
		PRECACHE_SOUND(STRING(m_iszEagleSound));

		std::string critsound5;
		critsound5.append(STRING(m_iszEagleSound));
		critsound5.resize(critsound5.size() -4); //this trims the .wav
		critsound5.append("_crit.wav");
		//PRECACHE_SOUND(critsound5.c_str());
	}

	
	if (m_iszEgonView)
		PRECACHE_MODEL(STRING(m_iszEgonView));

	if (m_iszEgonWorld)
		PRECACHE_MODEL(STRING(m_iszEgonWorld));
	
	
	if (m_iszHandGrenadeView)
		PRECACHE_MODEL(STRING(m_iszHandGrenadeView));

	if (m_iszHandGrenadeWorld)
		PRECACHE_MODEL(STRING(m_iszHandGrenadeWorld));

	if (m_iszHandGrenadeThrown)
		PRECACHE_MODEL(STRING(m_iszHandGrenadeThrown));
	
	
	if (m_iszGaussView)
		PRECACHE_MODEL(STRING(m_iszGaussView));

	if (m_iszGaussWorld)
		PRECACHE_MODEL(STRING(m_iszGaussWorld));
	
	
	if (m_iszGrappleView)
		PRECACHE_MODEL(STRING(m_iszGrappleView));

	if (m_iszGrappleWorld)
		PRECACHE_MODEL(STRING(m_iszGrappleWorld));
	
	
	if (m_iszHivehandView)
		PRECACHE_MODEL(STRING(m_iszHivehandView));

	if (m_iszHivehandWorld)
		PRECACHE_MODEL(STRING(m_iszHivehandWorld));
	
	
	if (m_iszM249View)
		PRECACHE_MODEL(STRING(m_iszM249View));

	if (m_iszM249World)
		PRECACHE_MODEL(STRING(m_iszM249World));

	if (m_iszM249Sound)
	{
		PRECACHE_SOUND(STRING(m_iszM249Sound));

		std::string critsound6;
		critsound6.append(STRING(m_iszM249Sound));
		critsound6.resize(critsound6.size() -4); //this trims the .wav
		critsound6.append("_crit.wav");
		//PRECACHE_SOUND(critsound6.c_str());
	}
	
	if (m_iszPenguinView)
		PRECACHE_MODEL(STRING(m_iszPenguinView));

	if (m_iszPenguinWorld)
		PRECACHE_MODEL(STRING(m_iszPenguinWorld));

	if (m_iszPenguinThrown)
		PRECACHE_MODEL(STRING(m_iszPenguinThrown));
	
	
	if (m_iszPipewrenchView)
		PRECACHE_MODEL(STRING(m_iszPipewrenchView));

	if (m_iszPipewrenchWorld)
		PRECACHE_MODEL(STRING(m_iszPipewrenchWorld));
	
	
	if (m_iszRPGView)
		PRECACHE_MODEL(STRING(m_iszRPGView));

	if (m_iszRPGWorld)
		PRECACHE_MODEL(STRING(m_iszRPGWorld));

	if (m_iszRPGThrown)
		PRECACHE_MODEL(STRING(m_iszRPGThrown));
	
	
	if (m_iszSatchelView)
		PRECACHE_MODEL(STRING(m_iszSatchelView));
	
	if (m_iszSatchelViewRadio)
		PRECACHE_MODEL(STRING(m_iszSatchelViewRadio));

	if (m_iszSatchelWorld)
		PRECACHE_MODEL(STRING(m_iszSatchelWorld));

	if (m_iszSatchelThrown)
		PRECACHE_MODEL(STRING(m_iszSatchelThrown));
	
	
	if (m_iszShockroachView)
		PRECACHE_MODEL(STRING(m_iszShockroachView));

	if (m_iszShockroachWorld)
		PRECACHE_MODEL(STRING(m_iszShockroachWorld));
	
	
	if (m_iszShotgunView)
		PRECACHE_MODEL(STRING(m_iszShotgunView));

	if (m_iszShotgunWorld)
		PRECACHE_MODEL(STRING(m_iszShotgunWorld));

	if (m_iszShotgunSoundPri)
	{
		PRECACHE_SOUND(STRING(m_iszShotgunSoundPri));

		std::string critsound7;
		critsound7.append(STRING(m_iszShotgunSoundPri));
		critsound7.resize(critsound7.size() -4); //this trims the .wav
		critsound7.append("_crit.wav");
		//PRECACHE_SOUND(critsound7.c_str());
	}

	if (m_iszShotgunSoundAlt)
	{
		PRECACHE_SOUND(STRING(m_iszShotgunSoundAlt));

		std::string critsound8;
		critsound8.append(STRING(m_iszShotgunSoundAlt));
		critsound8.resize(critsound8.size() -4); //this trims the .wav
		critsound8.append("_crit.wav");
		//PRECACHE_SOUND(critsound8.c_str());
	}

	if (m_iszShotgunSoundPump)
		PRECACHE_SOUND(STRING(m_iszShotgunSoundPump));

	if (m_iszShotgunSoundReload)
		PRECACHE_SOUND(STRING(m_iszShotgunSoundReload));
	
	
	if (m_iszSnarkView)
		PRECACHE_MODEL(STRING(m_iszSnarkView));

	if (m_iszSnarkWorld)
		PRECACHE_MODEL(STRING(m_iszSnarkWorld));

	if (m_iszSnarkThrown)
		PRECACHE_MODEL(STRING(m_iszSnarkThrown));
	
	
	if (m_iszSniperView)
		PRECACHE_MODEL(STRING(m_iszSniperView));

	if (m_iszSniperWorld)
		PRECACHE_MODEL(STRING(m_iszSniperWorld));

	if (m_iszSniperSound)
	{
		PRECACHE_SOUND(STRING(m_iszSniperSound));

		std::string critsound9;
		critsound9.append(STRING(m_iszSniperSound));
		critsound9.resize(critsound9.size() -4); //this trims the .wav
		critsound9.append("_crit.wav");
		//PRECACHE_SOUND(critsound9.c_str());
	}
	
	
	if (m_iszKnifeView)
		PRECACHE_MODEL(STRING(m_iszKnifeView));

	if (m_iszKnifeWorld)
		PRECACHE_MODEL(STRING(m_iszKnifeWorld));
	
	
	if (m_iszSporelauncherView)
		PRECACHE_MODEL(STRING(m_iszSporelauncherView));

	if (m_iszSporelauncherWorld)
		PRECACHE_MODEL(STRING(m_iszSporelauncherWorld));

	if (m_iszSporelauncherThrown)
		PRECACHE_MODEL(STRING(m_iszSporelauncherThrown));
	
	
	if (m_iszTripmineModel)
		PRECACHE_MODEL(STRING(m_iszTripmineModel));

	if (m_iszTripmineView)
		PRECACHE_MODEL(STRING(m_iszTripmineView));

	if (m_iszTripmineWorld)
		PRECACHE_MODEL(STRING(m_iszTripmineWorld));

	if (m_iszTripmineThrown)
		PRECACHE_MODEL(STRING(m_iszTripmineThrown));
	
	if (m_iszKeycardModel)
		PRECACHE_MODEL(STRING(m_iszKeycardModel));

	if (m_iszBatteryModel)
		PRECACHE_MODEL(STRING(m_iszBatteryModel));

	if (m_iszMedkitModel)
		PRECACHE_MODEL(STRING(m_iszMedkitModel));

	if (m_iszAmmo357Model)
		PRECACHE_MODEL(STRING(m_iszAmmo357Model));

	if (m_iszAmmo9mmARModel)
		PRECACHE_MODEL(STRING(m_iszAmmo9mmARModel));

	if (m_iszAmmo9mmBoxModel)
		PRECACHE_MODEL(STRING(m_iszAmmo9mmBoxModel));

	if (m_iszAmmo9mmClipModel)
		PRECACHE_MODEL(STRING(m_iszAmmo9mmClipModel));

	if (m_iszAmmoARGrenadesModel)
		PRECACHE_MODEL(STRING(m_iszAmmoARGrenadesModel));

	if (m_iszAmmoShotgunModel)
		PRECACHE_MODEL(STRING(m_iszAmmoShotgunModel));

	if (m_iszAmmoCrossbowModel)
		PRECACHE_MODEL(STRING(m_iszAmmoCrossbowModel));

	if (m_iszAmmoGaussModel)
		PRECACHE_MODEL(STRING(m_iszAmmoGaussModel));

	if (m_iszAmmoRPGModel)
		PRECACHE_MODEL(STRING(m_iszAmmoRPGModel));

	if (m_iszAmmo556Model)
		PRECACHE_MODEL(STRING(m_iszAmmo556Model));

	if (m_iszAmmo762Model)
		PRECACHE_MODEL(STRING(m_iszAmmo762Model));

	if (m_iszAmmoSporeModel)
		PRECACHE_MODEL(STRING(m_iszAmmoSporeModel));


	if (m_iszHornetModel)
		PRECACHE_MODEL(STRING(m_iszHornetModel));

	if (m_iszShockroachModel)
		PRECACHE_MODEL(STRING(m_iszShockroachModel));

	if (m_iszAgruntModel)
		PRECACHE_MODEL(STRING(m_iszAgruntModel));

	if (m_iszApacheModel)
		PRECACHE_MODEL(STRING(m_iszApacheModel));

	if (m_iszBabyVoltigoreModel)
		PRECACHE_MODEL(STRING(m_iszBabyVoltigoreModel));

	if (m_iszBarnacleModel)
		PRECACHE_MODEL(STRING(m_iszBarnacleModel));

	if (m_iszBarneyModel)
		PRECACHE_MODEL(STRING(m_iszBarneyModel));

	if (m_iszBigMommaModel)
		PRECACHE_MODEL(STRING(m_iszBigMommaModel));

	if (m_iszBlkApacheModel)
		PRECACHE_MODEL(STRING(m_iszBlkApacheModel));

	if (m_iszBlkOspreyModel)
		PRECACHE_MODEL(STRING(m_iszBlkOspreyModel));

	if (m_iszBullsquidModel)
		PRECACHE_MODEL(STRING(m_iszBullsquidModel));

	if (m_iszCleanScientistModel)
		PRECACHE_MODEL(STRING(m_iszCleanScientistModel));

	if (m_iszGargantuaModel)
		PRECACHE_MODEL(STRING(m_iszGargantuaModel));

	if (m_iszGManModel)
		PRECACHE_MODEL(STRING(m_iszGManModel));

	if (m_iszGonomeModel)
		PRECACHE_MODEL(STRING(m_iszGonomeModel));

	if (m_iszHAssassinModel)
		PRECACHE_MODEL(STRING(m_iszHAssassinModel));

	if (m_iszHeadcrabModel)
		PRECACHE_MODEL(STRING(m_iszHeadcrabModel));

	if (m_iszBabyHeadcrabModel)
		PRECACHE_MODEL(STRING(m_iszBabyHeadcrabModel));

	if (m_iszHGruntModel)
		PRECACHE_MODEL(STRING(m_iszHGruntModel));

	if (m_iszHGruntAllyModel)
		PRECACHE_MODEL(STRING(m_iszHGruntAllyModel));

	if (m_iszHGruntMedicModel)
		PRECACHE_MODEL(STRING(m_iszHGruntMedicModel));

	if (m_iszHGruntTorchModel)
		PRECACHE_MODEL(STRING(m_iszHGruntTorchModel));

	if (m_iszHoundeyeModel)
		PRECACHE_MODEL(STRING(m_iszHoundeyeModel));

	if (m_iszIcthyosaurModel)
		PRECACHE_MODEL(STRING(m_iszIcthyosaurModel));

	if (m_iszLeechModel)
		PRECACHE_MODEL(STRING(m_iszLeechModel));

	if (m_iszMAssassinModel)
		PRECACHE_MODEL(STRING(m_iszMAssassinModel));

	if (m_iszOspreyModel)
		PRECACHE_MODEL(STRING(m_iszOspreyModel));

	if (m_iszOtisModel)
		PRECACHE_MODEL(STRING(m_iszOtisModel));

	if (m_iszPitdroneModel)
		PRECACHE_MODEL(STRING(m_iszPitdroneModel));

	if (m_iszRoachModel)
		PRECACHE_MODEL(STRING(m_iszRoachModel));

	if (m_iszScientistModel)
		PRECACHE_MODEL(STRING(m_iszScientistModel));

	if (m_iszShocktrooperModel)
		PRECACHE_MODEL(STRING(m_iszShocktrooperModel));

	if (m_iszTentacleModel)
		PRECACHE_MODEL(STRING(m_iszTentacleModel));

	if (m_iszTurretModel)
		PRECACHE_MODEL(STRING(m_iszTurretModel));

	if (m_iszMiniTurretModel)
		PRECACHE_MODEL(STRING(m_iszMiniTurretModel));

	if (m_iszSentryModel)
		PRECACHE_MODEL(STRING(m_iszSentryModel));

	if (m_iszVoltigoreModel)
		PRECACHE_MODEL(STRING(m_iszVoltigoreModel));

	if (m_iszZombieModel)
		PRECACHE_MODEL(STRING(m_iszZombieModel));

	if (m_iszZombieBarneyModel)
		PRECACHE_MODEL(STRING(m_iszZombieBarneyModel));

	if (m_iszZombieSoldierModel)
		PRECACHE_MODEL(STRING(m_iszZombieSoldierModel));

	if (m_iszVortigauntModel)
		PRECACHE_MODEL(STRING(m_iszVortigauntModel));
	

	if (m_iszArmyModel)
		PRECACHE_MODEL(STRING(m_iszArmyModel));

	if (m_iszDogModel)
		PRECACHE_MODEL(STRING(m_iszDogModel));

	if (m_iszKnightModel)
		PRECACHE_MODEL(STRING(m_iszKnightModel));

	if (m_iszHKnightModel)
		PRECACHE_MODEL(STRING(m_iszHKnightModel));

	if (m_iszWizardModel)
		PRECACHE_MODEL(STRING(m_iszWizardModel));

	if (m_iszDemonModel)
		PRECACHE_MODEL(STRING(m_iszDemonModel));

	if (m_iszEnforcerModel)
		PRECACHE_MODEL(STRING(m_iszEnforcerModel));

	if (m_iszShalrathModel)
		PRECACHE_MODEL(STRING(m_iszShalrathModel));

	if (m_iszTarbabyModel)
		PRECACHE_MODEL(STRING(m_iszTarbabyModel));

	if (m_iszRotfishModel)
		PRECACHE_MODEL(STRING(m_iszRotfishModel));

	if (m_iszShamblerModel)
		PRECACHE_MODEL(STRING(m_iszShamblerModel));

	if (m_iszOgerModel)
		PRECACHE_MODEL(STRING(m_iszOgerModel));

	if (m_iszQZombieModel)
		PRECACHE_MODEL(STRING(m_iszQZombieModel));
}
