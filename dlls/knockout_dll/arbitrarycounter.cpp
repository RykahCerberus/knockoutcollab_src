
#include "extdll.h"
#include "util.h"
#include "cbase.h"
#include "player.h"
#include "saverestore.h"
#include "UserMessages.h"

class CTargetHudCounterModify : public CPointEntity
{
public:
	void			Spawn() override;
	void			Use(CBaseEntity* pActivator, CBaseEntity* pCaller, USE_TYPE useType, float value) override;
};

LINK_ENTITY_TO_CLASS(hud_counter_modify, CTargetHudCounterModify);

void CTargetHudCounterModify::Spawn()
{
	pev->solid = SOLID_NOT;
	pev->movetype = MOVETYPE_NONE;

	SetUse(&CTargetHudCounterModify::Use);
}

void CTargetHudCounterModify::Use(CBaseEntity* pActivator, CBaseEntity* pCaller, USE_TYPE useType, float value)
{
	edict_t* pentPlayer;
	CBasePlayer* pPlayer = NULL;
	pentPlayer = FIND_CLIENT_IN_PVS(edict());

	if (!FNullEnt(pentPlayer))
	{
		pPlayer = GetClassPtr((CBasePlayer*)VARS(pentPlayer));

		if (pPlayer)
		{
			if (pev->frags == 1)
				pPlayer->m_bDrawArbitraryCounter = true;
			else if (pev->frags == 2)
				pPlayer->m_bDrawArbitraryCounter = false;

			if (pev->armorvalue == 2)
				pPlayer->m_iArbitraryCounterValue = pev->health;
			else if (pev->armorvalue == 1)
				pPlayer->m_iArbitraryCounterValue = (pPlayer->m_iArbitraryCounterValue - pev->health);
			else
				pPlayer->m_iArbitraryCounterValue = (pPlayer->m_iArbitraryCounterValue + pev->health);

			if (pev->armortype > 6 || pev->armortype < 0)
				pev->armortype = 0;

			pPlayer->m_iArbitraryCounterPosition = pev->armortype;
		}
		else
		{
			ALERT(at_console, "Hud Counter Modify - Unable to get player!\n");
		}
	}
}


class CTargetHudCounterCompare : public CPointEntity
{
public:
	void			Spawn() override;
	void			Use(CBaseEntity* pActivator, CBaseEntity* pCaller, USE_TYPE useType, float value) override;
};

LINK_ENTITY_TO_CLASS(hud_counter_compare, CTargetHudCounterCompare);

void CTargetHudCounterCompare::Spawn()
{
	pev->solid = SOLID_NOT;
	pev->movetype = MOVETYPE_NONE;

	SetUse(&CTargetHudCounterCompare::Use);
}

void CTargetHudCounterCompare::Use(CBaseEntity* pActivator, CBaseEntity* pCaller, USE_TYPE useType, float value)
{
	edict_t* pentPlayer;
	CBasePlayer* pPlayer = NULL;
	pentPlayer = FIND_CLIENT_IN_PVS(edict());

	if (!FNullEnt(pentPlayer))
	{
		pPlayer = GetClassPtr((CBasePlayer*)VARS(pentPlayer));

		if (pPlayer)
		{
			bool passed = false;

			if (pev->max_health == 1)
			{
				if (pPlayer->m_iArbitraryCounterValue < pev->weapons)
					passed = true;
			}
			else if (pev->max_health == 2)
			{
				if (pPlayer->m_iArbitraryCounterValue == pev->weapons)
					passed = true;
			}
			else
			{
				if (pPlayer->m_iArbitraryCounterValue > pev->weapons)
					passed = true;
			}


			if (passed)
				FireTargets(STRING(pev->target), this, this, (USE_TYPE)pev->team, 0);
		}
		else
		{
			ALERT(at_console, "Hud Counter Compare - Unable to get player!\n");
		}
	}
}
