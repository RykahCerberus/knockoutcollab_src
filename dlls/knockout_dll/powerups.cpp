//=========== (C) Copyright 1996-2002, Valve, L.L.C. All rights reserved. ===========
//
// The copyright to the contents herein is the property of Valve, L.L.C.
// The contents may be used and/or copied only with the written permission of
// Valve, L.L.C., or in accordance with the terms and conditions stipulated in
// the agreement/contract under which the contents have been supplied.
//
// Purpose: Quake world items
//
// $Workfile:     $
// $Date:         $
//
//-----------------------------------------------------------------------------
// $Log: $
//
// $NoKeywords: $
//=============================================================================

#include "extdll.h"
#include "util.h"
#include "cbase.h"
#include "player.h"
#include "shake.h"
#include "../engine/studio.h"
#include "weapons.h"
#include "effects.h"
#include "hltv.h"
#include "ctf/CItemCTF.h"
#include "gamerules.h"
#include "UserMessages.h"
#include "knockout_dll/powerups.h"
#include "items.h"

extern unsigned short g_usPowerUp;

TYPEDESCRIPTION	CQuakeItem::m_SaveData[] =
{
	DEFINE_FIELD(CQuakeItem, m_chzCustomPickupMessage, FIELD_STRING ), //	AJH master entity for Lockable weapons
	DEFINE_FIELD(CQuakeItem, m_flRespawnTime, FIELD_TIME ),
	DEFINE_FIELD(CQuakeItem, m_flDuration, FIELD_FLOAT ),
	DEFINE_FIELD(CQuakeItem, m_iStrength, FIELD_INTEGER ),
	DEFINE_FIELD(CQuakeItem, m_iDoRespawn, FIELD_INTEGER),
	DEFINE_FIELD(CQuakeItem, m_iRespawnTime, FIELD_INTEGER),
	DEFINE_FIELD(CQuakeItem, m_iPowerupBit, FIELD_INTEGER),

	DEFINE_FIELD(CQuakeItem, invincible_finished, FIELD_FLOAT ),
	DEFINE_FIELD(CQuakeItem, radsuit_finished, FIELD_FLOAT ),
	DEFINE_FIELD(CQuakeItem, invisible_finished, FIELD_FLOAT ),
	DEFINE_FIELD(CQuakeItem, super_damage_finished, FIELD_FLOAT ),
	DEFINE_FIELD(CQuakeItem, op4ammo_finished, FIELD_FLOAT ),
	DEFINE_FIELD(CQuakeItem, op4armor_finished, FIELD_FLOAT),
	DEFINE_FIELD(CQuakeItem, op4health_finished, FIELD_FLOAT),
	DEFINE_FIELD(CQuakeItem, op4accel_finished, FIELD_FLOAT),
	DEFINE_FIELD(CQuakeItem, op4jump_finished, FIELD_FLOAT),
	DEFINE_FIELD(CQuakeItem, marksman_finished, FIELD_FLOAT),
	DEFINE_FIELD(CQuakeItem, berserk_finished, FIELD_FLOAT),
};
IMPLEMENT_SAVERESTORE(CQuakeItem, CItem);

void CQuakeItem::KeyValue( KeyValueData *pkvd )
{
	if (FStrEq(pkvd->szKeyName, "PowDuration"))
	{
		m_flDuration = atof(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "PowMessage"))
	{
		m_chzCustomPickupMessage = ALLOC_STRING(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "PowStrength"))
	{
		m_iStrength = atof(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "PowRespawnTime"))
	{
		m_iRespawnTime = atof(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else if (FStrEq(pkvd->szKeyName, "PowDoRespawn"))
	{
		m_iDoRespawn = atof(pkvd->szValue);
		pkvd->fHandled = TRUE;
	}
	else
		CBaseEntity::KeyValue( pkvd );
}

//-----------------------------------------------------------------------------
// Purpose: Spawn and drop to the floor
//-----------------------------------------------------------------------------

void CQuakeItem :: SetObjectCollisionBox()
{
	pev->absmin = pev->origin + Vector(-32, -32, 0);
	pev->absmax = pev->origin + Vector(32, 32, 56); 
}

void CQuakeItem::Precache()
{
	PRECACHE_SOUND("items/itembk2.wav");

	if (pev->noise)
		PRECACHE_SOUND(STRING(pev->noise));
	else
		PRECACHE_SOUND("items/dsgetpow.wav");
}

void CQuakeItem::Spawn()
{
	if (!(pev->spawnflags & SF_WEAPON_DONT_FALL))
		pev->movetype = MOVETYPE_TOSS;
	else
		pev->movetype = MOVETYPE_NONE;

	pev->solid = SOLID_TRIGGER;
	
	if (!m_flDuration)
		m_flDuration = 30;

	if (!m_iStrength)
		m_iStrength = 4;

	SetTouch(&CQuakeItem::ItemTouch);

	if (DROP_TO_FLOOR(ENT(pev)) == 0)
	{
		ALERT(at_error, "Item %s fell out of level at %f,%f,%f", STRING( pev->classname ), pev->origin.x, pev->origin.y, pev->origin.z);
		UTIL_Remove( this );
		return;
	}

	//UTIL_SetOrigin( pev, pev->origin + Vector(0,0,16) );

	if (!m_flRespawnTime)
		m_flRespawnTime = 20;


	if( !FStringNull( pev->model ) )
	{
		SET_MODEL( edict(), STRING( pev->model ) );

		pev->sequence = LookupSequence( "idle" );

		if( pev->sequence != -1 )
		{
			ResetSequenceInfo();
			pev->frame = 0;
		}
	}

	if (!pev->noise)
		pev->noise = MAKE_STRING("items/dsgetpow.wav");
}

//-----------------------------------------------------------------------------
// Purpose: Bring the item back
//-----------------------------------------------------------------------------
void CQuakeItem::Materialize()
{
	// Become visible and touchable
	pev->effects &= ~EF_NODRAW;
	SetTouch( &CQuakeItem::ItemTouch );

	// Play respawn sound
	EMIT_SOUND( ENT(pev), CHAN_WEAPON, "items/itembk2.wav", 1, ATTN_NORM );
}

//-----------------------------------------------------------------------------
// Purpose: Setup the item's respawn in the time set
//-----------------------------------------------------------------------------
void CQuakeItem::Respawn( float flTime )
{
	pev->effects |= EF_NODRAW;
	SetTouch( NULL );

	// Come back in time
	SetThink ( &CQuakeItem::Materialize );
	pev->nextthink = gpGlobals->time + flTime;
}

extern int gEvilImpulse101;

//-----------------------------------------------------------------------------
// Purpose: Touch function that calls the virtual touch function
//-----------------------------------------------------------------------------
void CQuakeItem::ItemTouch( CBaseEntity *pOther )
{
	// if it's not a player, ignore
	if ( !pOther->IsPlayer() )
		return;

	CBasePlayer *pPlayer = ToPlayer(pOther);

//	if (!UTIL_IsMasterTriggered(m_sMaster, pPlayer))
//		return;	

	// ok, a player is touching this item, but can he have it?
	if ( !g_pGameRules->CanHaveItem( pPlayer, (CItem*)this ) )
		return;

	//ALERT(at_console, "CScriptItem - My Target is: %d\n", STRING(m_strTarget));

	if (m_strTarget)
	{
		//ALERT(at_console, "CScriptItem - I'm firing my target!\n");
		FireTargets(STRING(m_strTarget), this, this, USE_TOGGLE, 0);
	}
	else
	{
		//ALERT(at_console, "CScriptItem - Not firing target.\n");
	}

	// Call the virtual touch function
	if ( MyTouch( pPlayer ) )
	{
		SUB_UseTargets( pOther, USE_TOGGLE, 0 );

		if ((m_iDoRespawn == -1 && gpGlobals->deathmatch) || m_iDoRespawn == 0)
			Respawn(m_flRespawnTime);

		if ((m_iDoRespawn == -1 && !gpGlobals->deathmatch) || m_iDoRespawn == 1)
			UTIL_Remove(this);
	}
	else if (gEvilImpulse101)
	{
		UTIL_Remove(this);
	}
}
//===============================================================================
// POWERUPS
//===============================================================================
// Powerup Touch
BOOL CItemPowerup::MyTouch( CBasePlayer *pPlayer )
{
	if (pPlayer->pev->health <= 0)
		return FALSE;

	EMIT_SOUND( ENT(pev), CHAN_ITEM, STRING(pev->noise), 1, ATTN_NORM );

	pPlayer->m_iCurrentPowerups |= m_iPowerupBit;
	
	int iPowerUp = 0;

	// Invincibility
	if (invincible_finished)
	{
		int i = gpGlobals->time + invincible_finished;

		if (gpgs && gpgs->m_bCombinePowerupTimers && pPlayer->m_flPowInvincibleTimer)
			i = invincible_finished + pPlayer->m_flPowInvincibleTimer;

		pPlayer->m_flPowInvincibleTimer = i;
	}
	
	// Quad Damage
	if (super_damage_finished)
	{
		int i = gpGlobals->time + super_damage_finished;

		if (gpgs && gpgs->m_bCombinePowerupTimers && pPlayer->m_flPowSuperDamageTimer)
		{
			i = super_damage_finished + pPlayer->m_flPowSuperDamageTimer;

			//If we combine powerup timers, let's also combine the best bonus dmg with the combo'd timer.
			if (pPlayer->m_iBonusDamageMultiplier < m_iStrength)
				pPlayer->m_iBonusDamageMultiplier = m_iStrength;
		}
		else
		{
			pPlayer->m_iBonusDamageMultiplier = m_iStrength;
		}

		pPlayer->SetGunSounds(true);

		pPlayer->m_flPowSuperDamageTimer = i;
	}

	// Radiation suit
	if (radsuit_finished)
	{
		int i = gpGlobals->time + radsuit_finished;

		if (gpgs && gpgs->m_bCombinePowerupTimers && pPlayer->m_flPowRadsuitTimer)
			i = radsuit_finished + pPlayer->m_flPowRadsuitTimer;

		pPlayer->m_flPowRadsuitTimer = i;
	}

	// Invisibility
	if (invisible_finished)
	{
		int i = gpGlobals->time + invisible_finished;

		if (gpgs && gpgs->m_bCombinePowerupTimers && pPlayer->m_flPowInvisibleTimer)
			i = invisible_finished + pPlayer->m_flPowInvisibleTimer;

		pPlayer->m_flPowInvisibleTimer = i;
	}

	// OP4 Backpack
	if (op4ammo_finished)
	{
		int i = gpGlobals->time + op4ammo_finished;

		if (gpgs && gpgs->m_bCombinePowerupTimers && pPlayer->m_flPowBackpackTimer)
			i = op4ammo_finished + pPlayer->m_flPowBackpackTimer;

		pPlayer->m_flPowBackpackTimer = i;
	}

	// OP4 Regeneration
	if (op4health_finished)
	{
		int i = gpGlobals->time + op4health_finished;

		if (gpgs && gpgs->m_bCombinePowerupTimers && pPlayer->m_flPowRegenTimer)
			i = op4health_finished + pPlayer->m_flPowRegenTimer;

		pPlayer->m_flPowRegenTimer = i;
	}

	// OP4 PortHEV
	if (op4armor_finished)
	{
		int i = gpGlobals->time + op4armor_finished;

		if (gpgs && gpgs->m_bCombinePowerupTimers && pPlayer->m_flPowPortHEVTimer)
			i = op4armor_finished + pPlayer->m_flPowPortHEVTimer;

		pPlayer->m_flPowPortHEVTimer = i;
	}

	// OP4 Accellerator
	if (op4accel_finished)
	{
		int i = gpGlobals->time + op4accel_finished;

		if (gpgs && gpgs->m_bCombinePowerupTimers && pPlayer->m_flPowAccelleratorTimer)
			i = op4accel_finished + pPlayer->m_flPowAccelleratorTimer;

		pPlayer->m_flPowAccelleratorTimer = i;
	}

	// OP4 LongJump
	if (op4jump_finished)
	{
		int i = gpGlobals->time + op4jump_finished;

		if (gpgs && gpgs->m_bCombinePowerupTimers && pPlayer->m_flPowLongJumpTimer)
			i = op4jump_finished + pPlayer->m_flPowLongJumpTimer;

		pPlayer->m_flPowLongJumpTimer = i;
		g_engfuncs.pfnSetPhysicsKeyValue( pPlayer->edict(), "slj", "1" );
		pPlayer->m_fLongJump = true;

		EMIT_SOUND_DYN(pPlayer->edict(), CHAN_STATIC, "!HEV_A1", 1.0, ATTN_NORM, 0, 100);
	}

	// Marksman
	if (marksman_finished)
	{
		int i = gpGlobals->time + marksman_finished;

		if (gpgs && gpgs->m_bCombinePowerupTimers && pPlayer->m_flPowMarksmanTimer)
			i = marksman_finished + pPlayer->m_flPowMarksmanTimer;

		pPlayer->m_flPowMarksmanTimer = i;
	}

	// Berserk
	if (berserk_finished)
	{
		int i = gpGlobals->time + berserk_finished;

		if (gpgs && gpgs->m_bCombinePowerupTimers && pPlayer->m_flPowBerserkTimer)
			i = berserk_finished + pPlayer->m_flPowBerserkTimer;

		pPlayer->m_flPowBerserkTimer = i;

		if (m_iStrength)
		{
			char* item = "weapon_knife";

			if (m_iStrength == 2)
				item = "weapon_crowbar";

			if (m_iStrength == 3)
				item = "weapon_pipewrench";

			gEvilImpulse101 = TRUE;
			pPlayer->GiveNamedItem(item);
			gEvilImpulse101 = FALSE;

			pPlayer->SelectItem(item);
		}
		else
		{
			pPlayer->SelectItem("weapon_pipewrench");
			pPlayer->SelectItem("weapon_crowbar");
			pPlayer->SelectItem("weapon_knife");
		}
	}
	
	// tell director about it
	MESSAGE_BEGIN( MSG_SPEC, SVC_DIRECTOR );
		WRITE_BYTE ( 9 );	// command length in bytes
		WRITE_BYTE ( DRC_CMD_EVENT );	// powerup pickup
		WRITE_SHORT( ENTINDEX(pPlayer->edict()) );	// player is primary target
		WRITE_SHORT( ENTINDEX(this->edict()) );	// powerup as second target
		WRITE_LONG( 9 );   // highst prio in game
	MESSAGE_END();

	PLAYBACK_EVENT_FULL( FEV_GLOBAL | FEV_RELIABLE, 
	pPlayer->edict(), g_usPowerUp, 0, (float *)&g_vecZero, (float *)&g_vecZero, 
	(float)iPowerUp, 0.0, pPlayer->entindex(), pPlayer->pev->team, 0, 0 );

	return TRUE;
}


//===============
// Pentagram
class CItemPowerupInvincible : public CItemPowerup
{
public:
	void Spawn( void );
	void Precache( void );
};
LINK_ENTITY_TO_CLASS(artifact_invulnerability, CItemPowerupInvincible);

// Spawn
void CItemPowerupInvincible::Spawn( void )
{
	Precache();
	CQuakeItem::Spawn();

	if (m_iRespawnTime)
		m_flRespawnTime = m_iRespawnTime;
	else
		m_flRespawnTime = 300;

	invincible_finished = m_flDuration;

	if (pev->model)
		SET_MODEL(ENT(pev), STRING(pev->model));
	else
		SET_MODEL(ENT(pev), "models/w_pow_invuln.mdl");

	if (m_chzCustomPickupMessage)
		pev->netname = MAKE_STRING(m_chzCustomPickupMessage);
	else
		pev->netname = MAKE_STRING("Pentagram of Protection");

	//pev->noise = MAKE_STRING("items/protect.wav");
	m_iPowerupBit = POWERUP_INVULNERABILITY;
}

// Precache
void CItemPowerupInvincible::Precache( void )
{
	if (pev->model)
		PRECACHE_MODEL(STRING(pev->model));
	else
		PRECACHE_MODEL("models/w_pow_invuln.mdl");

	CQuakeItem::Precache();
}

//===============
// Radiation Suit
class CItemPowerupRadsuit : public CItemPowerup
{
public:
	void Spawn( void );
	void Precache( void );
};
LINK_ENTITY_TO_CLASS(artifact_envirosuit, CItemPowerupRadsuit);

// Spawn
void CItemPowerupRadsuit::Spawn( void )
{
	Precache();
	CQuakeItem::Spawn();

	if (m_iRespawnTime)
		m_flRespawnTime = m_iRespawnTime;
	else
		m_flRespawnTime = 60;

	radsuit_finished = m_flDuration;

	if (pev->model)
		SET_MODEL(ENT(pev), STRING(pev->model));
	else
		SET_MODEL(ENT(pev), "models/w_pow_suit.mdl");

	if (m_chzCustomPickupMessage)
		pev->netname = MAKE_STRING(m_chzCustomPickupMessage);
	else
		pev->netname = MAKE_STRING("Environmental Protection Helmet");

	//pev->noise = MAKE_STRING("items/suit.wav");
	m_iPowerupBit = POWERUP_ENVSUIT;
}

// Precache
void CItemPowerupRadsuit::Precache( void )
{
	if (pev->model)
		PRECACHE_MODEL(STRING(pev->model));
	else
		PRECACHE_MODEL("models/w_pow_suit.mdl");

	CQuakeItem::Precache();
}

//===============
// Ring of Invisibility
class CItemPowerupInvisibility : public CItemPowerup
{
public:
	void Spawn( void );
	void Precache( void );
};
LINK_ENTITY_TO_CLASS(artifact_invisibility, CItemPowerupInvisibility);

// Spawn
void CItemPowerupInvisibility::Spawn( void )
{
	Precache();
	CQuakeItem::Spawn();

	if (m_iRespawnTime)
		m_flRespawnTime = m_iRespawnTime;
	else
		m_flRespawnTime = 300;

	invisible_finished = m_flDuration;

	if (pev->model)
		SET_MODEL(ENT(pev), STRING(pev->model));
	else
		SET_MODEL(ENT(pev), "models/w_pow_invis.mdl");

	if (m_chzCustomPickupMessage)
		pev->netname = MAKE_STRING(m_chzCustomPickupMessage);
	else
		pev->netname = MAKE_STRING("Ring of Shadows");

	//pev->noise = MAKE_STRING("items/inv1.wav");
	m_iPowerupBit = POWERUP_INVISIBILITY;

	pev->renderfx = kRenderFxGlowShell;
	pev->rendercolor = Vector( 128, 128, 128 );	// RGB
	pev->renderamt = 25;	// Shell size

	pev->rendermode = kRenderTransColor;
	pev->renderamt = 30;
}

// Precache
void CItemPowerupInvisibility::Precache( void )
{
	if (pev->model)
		PRECACHE_MODEL(STRING(pev->model));
	else
		PRECACHE_MODEL("models/w_pow_invis.mdl");

	CQuakeItem::Precache();
}

//===============
// Quad Damage
class CItemPowerupQuad : public CItemPowerup
{
public:
	void Spawn( void );
	void Precache( void );
};
LINK_ENTITY_TO_CLASS(artifact_super_damage, CItemPowerupQuad);

// Spawn
void CItemPowerupQuad::Spawn( void )
{
	Precache();
	CQuakeItem::Spawn();

	if (m_iRespawnTime)
		m_flRespawnTime = m_iRespawnTime;
	else
		m_flRespawnTime = 60;

	super_damage_finished = m_flDuration;

	if (pev->model)
		SET_MODEL(ENT(pev), STRING(pev->model));
	else
		SET_MODEL(ENT(pev), "models/w_pow_quad.mdl");

	//pev->netname = MAKE_STRING("Quad Damage");
	if (m_chzCustomPickupMessage)
		pev->netname = MAKE_STRING(m_chzCustomPickupMessage);
	else
		pev->netname = MAKE_STRING("Bonus Damage");

	//pev->noise = MAKE_STRING("items/damage.wav");
	m_iPowerupBit = POWERUP_QUADDMG;
}

// Precache
void CItemPowerupQuad::Precache( void )
{
	if (pev->model)
		PRECACHE_MODEL(STRING(pev->model));
	else
		PRECACHE_MODEL("models/w_pow_quad.mdl");

	CQuakeItem::Precache();
}

//===============
// OP4 PortHEV
class CItemPowerupOP4HEV : public CItemPowerup
{
public:
	void Spawn(void);
	void Precache(void);
};
LINK_ENTITY_TO_CLASS(artifact_portablehev, CItemPowerupOP4HEV);

// Spawn
void CItemPowerupOP4HEV::Spawn(void)
{
	Precache();
	CQuakeItem::Spawn();

	if (m_iRespawnTime)
		m_flRespawnTime = m_iRespawnTime;
	else
		m_flRespawnTime = 150;

	op4armor_finished = m_flDuration;

	if (pev->model)
		SET_MODEL(ENT(pev), STRING(pev->model));
	else
		SET_MODEL(ENT(pev), "models/w_porthev.mdl");

	if (m_chzCustomPickupMessage)
		pev->netname = MAKE_STRING(m_chzCustomPickupMessage);
	else
		pev->netname = MAKE_STRING("Shield of Strengthening");

	//pev->noise = MAKE_STRING("items/suitchargeok1.wav");
	m_iPowerupBit = POWERUP_HEVREGEN;
}

// Precache
void CItemPowerupOP4HEV::Precache(void)
{
	if (pev->model)
		PRECACHE_MODEL(STRING(pev->model));
	else
		PRECACHE_MODEL("models/w_porthev.mdl");

	CQuakeItem::Precache();

	PRECACHE_SOUND("ctf/pow_armor_charge.wav");
}

//===============
// OP4 Regen
class CItemPowerupOP4Regen : public CItemPowerup
{
public:
	void Spawn(void);
	void Precache(void);
};
LINK_ENTITY_TO_CLASS(artifact_regeneration, CItemPowerupOP4Regen);

// Spawn
void CItemPowerupOP4Regen::Spawn(void)
{
	Precache();
	CQuakeItem::Spawn();

	if (m_iRespawnTime)
		m_flRespawnTime = m_iRespawnTime;
	else
		m_flRespawnTime = 150;

	op4health_finished = m_flDuration;

	if (pev->model)
		SET_MODEL(ENT(pev), STRING(pev->model));
	else
		SET_MODEL(ENT(pev), "models/w_health.mdl");

	if (m_chzCustomPickupMessage)
		pev->netname = MAKE_STRING(m_chzCustomPickupMessage);
	else
		pev->netname = MAKE_STRING("Charm of Regeneration");

	//pev->noise = MAKE_STRING("items/pickup_regeneration_start.wav");
	m_iPowerupBit = POWERUP_HEALTHREGEN;
}

// Precache
void CItemPowerupOP4Regen::Precache(void)
{
	if (pev->model)
		PRECACHE_MODEL(STRING(pev->model));
	else
		PRECACHE_MODEL("models/w_health.mdl");

	CQuakeItem::Precache();

	PRECACHE_SOUND("ctf/pow_health_charge.wav");
}

//===============
// OP4 Backpack
class CItemPowerupOP4Ammo : public CItemPowerup
{
public:
	void Spawn(void);
	void Precache(void);
};
LINK_ENTITY_TO_CLASS(artifact_backpack, CItemPowerupOP4Ammo);

// Spawn
void CItemPowerupOP4Ammo::Spawn(void)
{
	Precache();
	CQuakeItem::Spawn();

	if (m_iRespawnTime)
		m_flRespawnTime = m_iRespawnTime;
	else
		m_flRespawnTime = 150;

	op4ammo_finished = m_flDuration;

	if (pev->model)
		SET_MODEL(ENT(pev), STRING(pev->model));
	else
		SET_MODEL(ENT(pev), "models/w_backpack.mdl");

	if (m_chzCustomPickupMessage)
		pev->netname = MAKE_STRING(m_chzCustomPickupMessage);
	else
		pev->netname = MAKE_STRING("Rune of Ammunition");

	//pev->noise = MAKE_STRING("items/pickup_backpack_start.wav");
	m_iPowerupBit = POWERUP_AMMOREGEN;
}

// Precache
void CItemPowerupOP4Ammo::Precache(void)
{
	if (pev->model)
		PRECACHE_MODEL(STRING(pev->model));
	else
		PRECACHE_MODEL("models/w_backpack.mdl");

	CQuakeItem::Precache();

	PRECACHE_SOUND("ctf/pow_backpack.wav");
}

//===============
// OP4 Accellerator
class CItemPowerupOP4Accel : public CItemPowerup
{
public:
	void Spawn(void);
	void Precache(void);
};
LINK_ENTITY_TO_CLASS(artifact_accelerator, CItemPowerupOP4Accel);

// Spawn
void CItemPowerupOP4Accel::Spawn(void)
{
	Precache();
	CQuakeItem::Spawn();

	if (m_iRespawnTime)
		m_flRespawnTime = m_iRespawnTime;
	else
		m_flRespawnTime = 150;

	op4accel_finished = m_flDuration;

	if (pev->model)
		SET_MODEL(ENT(pev), STRING(pev->model));
	else
		SET_MODEL(ENT(pev), "models/w_pow_firerate.mdl");

	if (m_chzCustomPickupMessage)
		pev->netname = MAKE_STRING(m_chzCustomPickupMessage);
	else
		pev->netname = MAKE_STRING("Gun Accellerator");

	//pev->noise = MAKE_STRING("items/pickup_firerate_start.wav");
	m_iPowerupBit = POWERUP_ACCELERATOR;
}

// Precache
void CItemPowerupOP4Accel::Precache(void)
{
	if (pev->model)
		PRECACHE_MODEL(STRING(pev->model));
	else
		PRECACHE_MODEL("models/w_pow_firerate.mdl");

	CQuakeItem::Precache();
}

//===============
// OP4 LongJump
class CItemPowerupOP4Jump : public CItemPowerup
{
public:
	void Spawn(void);
	void Precache(void);
};
LINK_ENTITY_TO_CLASS(artifact_longjump, CItemPowerupOP4Jump);

// Spawn
void CItemPowerupOP4Jump::Spawn(void)
{
	Precache();
	CQuakeItem::Spawn();

	if (m_iRespawnTime)
		m_flRespawnTime = m_iRespawnTime;
	else
		m_flRespawnTime = 150;

	op4jump_finished = m_flDuration;

	if (pev->model)
		SET_MODEL(ENT(pev), STRING(pev->model));
	else
		SET_MODEL(ENT(pev), "models/w_jumppack.mdl");

	if (m_chzCustomPickupMessage)
		pev->netname = MAKE_STRING(m_chzCustomPickupMessage);
	else
		pev->netname = MAKE_STRING("Temporary Long-Jump");

	//pev->noise = MAKE_STRING("items/ammopickup1.wav");
	m_iPowerupBit = POWERUP_LONGJUMP;
}

// Precache
void CItemPowerupOP4Jump::Precache(void)
{
	if (pev->model)
		PRECACHE_MODEL(STRING(pev->model));
	else
		PRECACHE_MODEL("models/w_jumppack.mdl");

	CQuakeItem::Precache();
}


//===============
// Marksman
class CItemPowerupMarksman : public CItemPowerup
{
public:
	void Spawn(void);
	void Precache(void);
};
LINK_ENTITY_TO_CLASS(artifact_marksman, CItemPowerupMarksman);

// Spawn
void CItemPowerupMarksman::Spawn(void)
{
	Precache();
	CQuakeItem::Spawn();

	if (m_iRespawnTime)
		m_flRespawnTime = m_iRespawnTime;
	else
		m_flRespawnTime = 150;

	marksman_finished = m_flDuration;

	if (pev->model)
		SET_MODEL(ENT(pev), STRING(pev->model));
	else
		SET_MODEL(ENT(pev), "models/w_pow_marksman.mdl");

	if (m_chzCustomPickupMessage)
		pev->netname = MAKE_STRING(m_chzCustomPickupMessage);
	else
		pev->netname = MAKE_STRING("Operator Manual");

	//pev->noise = MAKE_STRING("items/pickup_marksman_start.wav");
	m_iPowerupBit = POWERUP_MARKSMAN;
}

// Precache
void CItemPowerupMarksman::Precache(void)
{
	if (pev->model)
		PRECACHE_MODEL(STRING(pev->model));
	else
		PRECACHE_MODEL("models/w_pow_marksman.mdl");

	CQuakeItem::Precache();
}


//===============
// Marksman
class CItemPowerupBerserk : public CItemPowerup
{
public:
	void Spawn(void);
	void Precache(void);
};
LINK_ENTITY_TO_CLASS(artifact_berserk, CItemPowerupBerserk);

// Spawn
void CItemPowerupBerserk::Spawn(void)
{
	Precache();
	CQuakeItem::Spawn();

	if (m_iRespawnTime)
		m_flRespawnTime = m_iRespawnTime;
	else
		m_flRespawnTime = 150;

	berserk_finished = m_flDuration;

	if (pev->model)
		SET_MODEL(ENT(pev), STRING(pev->model));
	else
		SET_MODEL(ENT(pev), "models/w_accelerator.mdl");

	if (m_chzCustomPickupMessage)
		pev->netname = MAKE_STRING(m_chzCustomPickupMessage);
	else
		pev->netname = MAKE_STRING("Berserk Pack");

	//pev->noise = MAKE_STRING("items/pickup_berserk_start.wav");
	m_iPowerupBit = POWERUP_BERSERK;
}

// Precache
void CItemPowerupBerserk::Precache(void)
{
	if (pev->model)
		PRECACHE_MODEL(STRING(pev->model));
	else
		PRECACHE_MODEL("models/w_accelerator.mdl");

	CQuakeItem::Precache();
}
