/***
*
*	Copyright (c) 1996-2001, Valve LLC. All rights reserved.
*
*	This product contains software technology licensed from Id
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc.
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/

#include "extdll.h"
#include "util.h"
#include "cbase.h"
#include "shake.h"
#include "UserMessages.h"

int gmsgShake = 0;
int gmsgFade = 0;
int gmsgSelAmmo = 0;
int gmsgFlashlight = 0;
int gmsgFlashBattery = 0;
int gmsgResetHUD = 0;
int gmsgInitHUD = 0;
int gmsgSetFog = 0; //LRC
int gmsgKeyedDLight = 0;//LRC
int gmsgKeyedELight = 0;//LRC
int gmsgSetSky = 0;		//LRC
int gmsgHUDColor = 0;	//LRC
int gmsgAddShine = 0;   // LRC
int gmsgParticle = 0; // LRC
int gmsgClampView = 0; //LRC 1.8
int gmsgPlayMP3 = 0; //Killar
int gmsgShowGameTitle = 0;
int gmsgCurWeapon = 0;
int gmsgHealth = 0;
int gmsgDamage = 0;
int gmsgBattery = 0;
int gmsgTrain = 0;
int gmsgLogo = 0;
int gmsgWeaponList = 0;
int gmsgAmmoX = 0;
int gmsgHudText = 0;
int gmsgDeathMsg = 0;
int gmsgScoreInfo = 0;
int gmsgTeamInfo = 0;
int gmsgTeamScore = 0;
int gmsgGameMode = 0;
int gmsgMOTD = 0;
int gmsgServerName = 0;
int gmsgAmmoPickup = 0;
int gmsgWeapPickup = 0;
int gmsgItemPickup = 0;
int gmsgHideWeapon = 0;
int gmsgSetCurWeap = 0;
int gmsgSayText = 0;
int gmsgTextMsg = 0;
int gmsgSetFOV = 0;
int gmsgShowMenu = 0;
int gmsgGeigerRange = 0;
int gmsgTeamNames = 0;
int gmsgStatusIcon = 0; //LRC
int gmsgStatusText = 0;
int gmsgStatusValue = 0;
int gmsgCamData = 0; // for trigger_viewset
int gmsgRainData = 0;
int gmsgInventory = 0; //AJH Inventory system

int gmsgSpectator = 0;
int gmsgPlayerBrowse = 0;
int gmsgFlagIcon = 0;
int gmsgFlagTimer = 0;
int gmsgPlayerIcon = 0;
int gmsgVGUIMenu = 0;
int gmsgAllowSpec = 0;
int gmsgSetMenuTeam = 0;
int gmsgCTFScore = 0;
int gmsgStatsInfo = 0;
int gmsgStatsPlayer = 0;
int gmsgTeamFull = 0;
int gmsgOldWeapon = 0;
int gmsgCustomIcon = 0;

int gmsgQItems = 0;
int gmsgTempEntity = 0;

int gmsgFlashIcon = 0;
int gmsgHealthIcon = 0;
int gmsgArmorIcon = 0;
int gmsgNumberFont = 0;
int gmsgSelSprite = 0;

void LinkUserMessages()
{
	// Already taken care of?
	if (gmsgSelAmmo)
	{
		return;
	}

	gmsgSelAmmo = REG_USER_MSG("SelAmmo", sizeof(SelAmmo));
	gmsgCurWeapon = REG_USER_MSG("CurWeapon", 6);
	gmsgGeigerRange = REG_USER_MSG("Geiger", 1);
	gmsgFlashlight = REG_USER_MSG("Flashlight", 2);
	gmsgFlashBattery = REG_USER_MSG("FlashBat", 1);
	gmsgHealth = REG_USER_MSG("Health", 2);
	gmsgDamage = REG_USER_MSG("Damage", 12);
	gmsgBattery = REG_USER_MSG("Battery", 4);
	gmsgTrain = REG_USER_MSG("Train", 1);
	//gmsgHudText = REG_USER_MSG( "HudTextPro", -1 );
	gmsgHudText = REG_USER_MSG("HudText", -1); // we don't use the message but 3rd party addons may!
	gmsgSayText = REG_USER_MSG("SayText", -1);
	gmsgTextMsg = REG_USER_MSG("TextMsg", -1);
	gmsgWeaponList = REG_USER_MSG("WeaponList", -1);
	gmsgResetHUD = REG_USER_MSG("ResetHUD", 1);		// called every respawn
	gmsgInitHUD = REG_USER_MSG("InitHUD", -1);		// called every time a new player joins the server

	gmsgSetFog = REG_USER_MSG("SetFog", 9); //LRC
	gmsgKeyedDLight = REG_USER_MSG("KeyedDLight", -1);	//LRC
	gmsgKeyedELight = REG_USER_MSG("KeyedELight", -1);	//LRC
	gmsgSetSky = REG_USER_MSG("SetSky", 8);			//LRC
	gmsgHUDColor = REG_USER_MSG("HUDColor", 4);		//LRC
	gmsgParticle = REG_USER_MSG("Particle", -1);		//LRC
	gmsgAddShine = REG_USER_MSG("AddShine", -1);      //LRC
	gmsgClampView = REG_USER_MSG("ClampView", 10);	//LRC 1.8

	gmsgShowGameTitle = REG_USER_MSG("GameTitle", 1);
	gmsgDeathMsg = REG_USER_MSG("DeathMsg", -1);
	gmsgScoreInfo = REG_USER_MSG("ScoreInfo", 5);
	gmsgTeamInfo = REG_USER_MSG("TeamInfo", -1);  // sets the name of a player's team
	gmsgTeamScore = REG_USER_MSG("TeamScore", -1);  // sets the score of a team on the scoreboard
	gmsgGameMode = REG_USER_MSG("GameMode", 1);
	gmsgMOTD = REG_USER_MSG("MOTD", -1);
	gmsgServerName = REG_USER_MSG("ServerName", -1);
	gmsgAmmoPickup = REG_USER_MSG("AmmoPickup", 5);
	gmsgWeapPickup = REG_USER_MSG("WeapPickup", 1);
	gmsgItemPickup = REG_USER_MSG("ItemPickup", -1);
	gmsgHideWeapon = REG_USER_MSG("HideWeapon", 1);
	gmsgSetFOV = REG_USER_MSG("SetFOV", 1);
	gmsgShowMenu = REG_USER_MSG("ShowMenu", -1);
	gmsgShake = REG_USER_MSG("ScreenShake", sizeof(ScreenShake));
	gmsgFade = REG_USER_MSG("ScreenFade", sizeof(ScreenFade));
	gmsgAmmoX = REG_USER_MSG("AmmoX", 5);
	gmsgTeamNames = REG_USER_MSG("TeamNames", -1);
	gmsgStatusIcon = REG_USER_MSG("StatusIcon", -1);

	gmsgStatusText = REG_USER_MSG("StatusText", -1);
	gmsgStatusValue = REG_USER_MSG("StatusValue", 3);
	gmsgCamData = REG_USER_MSG("CamData", -1);
	gmsgPlayMP3 = REG_USER_MSG("PlayMP3", -1);	//Killar
	gmsgRainData = REG_USER_MSG("RainData", 16);
	gmsgInventory = REG_USER_MSG("Inventory", -1);	//AJH Inventory system

	gmsgSpectator = g_engfuncs.pfnRegUserMsg("Spectator", 2);
	gmsgStatusIcon = g_engfuncs.pfnRegUserMsg("StatusIcon", -1);
	gmsgPlayerBrowse = g_engfuncs.pfnRegUserMsg("PlyrBrowse", -1);
	gmsgFlagIcon = g_engfuncs.pfnRegUserMsg("FlagIcon", -1);
	gmsgFlagTimer = g_engfuncs.pfnRegUserMsg("FlagTimer", -1);
	gmsgPlayerIcon = g_engfuncs.pfnRegUserMsg("PlayerIcon", -1);
	gmsgVGUIMenu = g_engfuncs.pfnRegUserMsg("VGUIMenu", -1);
	gmsgAllowSpec = g_engfuncs.pfnRegUserMsg("AllowSpec", 1);
	gmsgSetMenuTeam = g_engfuncs.pfnRegUserMsg("SetMenuTeam", 1);
	gmsgCTFScore = g_engfuncs.pfnRegUserMsg("CTFScore", 2);
	gmsgStatsInfo = g_engfuncs.pfnRegUserMsg("StatsInfo", -1);
	gmsgStatsPlayer = g_engfuncs.pfnRegUserMsg("StatsPlayer", 31);
	gmsgTeamFull = g_engfuncs.pfnRegUserMsg("TeamFull", 1);
	gmsgOldWeapon = g_engfuncs.pfnRegUserMsg("OldWeapon", 1);
	gmsgCustomIcon = g_engfuncs.pfnRegUserMsg("CustomIcon", -1);

	gmsgQItems = REG_USER_MSG( "QItems", -1 );
	gmsgTempEntity = REG_USER_MSG("TempEntity", -1);
	
	gmsgFlashIcon = REG_USER_MSG("FlashIcon", -1);
	gmsgHealthIcon = REG_USER_MSG("HealthIcon", -1);
	gmsgArmorIcon = REG_USER_MSG("ArmorIcon", -1);
	gmsgNumberFont = REG_USER_MSG("NumberFont", -1);
	gmsgSelSprite = REG_USER_MSG("SelSprite", -1);
}
