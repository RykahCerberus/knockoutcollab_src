/***
*
*	Copyright (c) 1996-2002, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/
//
// flashlight.cpp
//
// implementation of CHudFlashlight class
//

#include "hud.h"
#include "cl_util.h"
#include "parsemsg.h"

#include <string.h>
#include <stdio.h>



DECLARE_MESSAGE(m_Flash, FlashBat)
DECLARE_MESSAGE(m_Flash, Flashlight)

#define BAT_NAME "sprites/%d_Flashlight.spr"

int CHudFlashlight::Init()
{
	m_fFade = 0;
	m_fOn = 0;

	gHUD.setNightVisionState( false );

	HOOK_MESSAGE(Flashlight);
	HOOK_MESSAGE(FlashBat);

	m_iFlags |= HUD_ACTIVE;

	gHUD.AddHudElem(this);

	return 1;
};

void CHudFlashlight::Reset()
{
	m_fFade = 0;
	m_fOn = 0;

	gHUD.setNightVisionState( false );
}

int CHudFlashlight::VidInit()
{
	int HUD_flash_empty = gHUD.GetSpriteIndex( "flash_empty" );
	int HUD_flash_full = gHUD.GetSpriteIndex( "flash_full" );
	int HUD_flash_beam = gHUD.GetSpriteIndex( "flash_beam" );

	m_hSprite1 = gHUD.GetSprite(HUD_flash_empty);
	m_hSprite2 = gHUD.GetSprite(HUD_flash_full);
	m_hBeam = gHUD.GetSprite(HUD_flash_beam);
	m_prc1 = &gHUD.GetSpriteRect(HUD_flash_empty);
	m_prc2 = &gHUD.GetSpriteRect(HUD_flash_full);
	m_prcBeam = &gHUD.GetSpriteRect(HUD_flash_beam);
	m_iWidth = m_prc2->right - m_prc2->left;

	m_nvSprite = LoadSprite( "sprites/of_nv_b.spr" );

	return 1;
};

int CHudFlashlight:: MsgFunc_FlashBat(const char *pszName,  int iSize, void *pbuf )
{
	BEGIN_READ( pbuf, iSize );
	int x = READ_BYTE();
	m_iBat = x;
	m_flBat = ((float)x)/100.0;

	return 1;
}

int CHudFlashlight:: MsgFunc_Flashlight(const char *pszName,  int iSize, void *pbuf )
{
	BEGIN_READ( pbuf, iSize );
	m_fOn = READ_BYTE();
	
	if (gHUD.flashlight_type->value)
		gHUD.setNightVisionState( m_fOn != 0 );

	int x = READ_BYTE();
	m_iBat = x;
	m_flBat = ((float)x)/100.0;

	return 1;
}

int CHudFlashlight::Draw(float flTime)
{
	if ( gHUD.m_iHideHUDDisplay & ( HIDEHUD_FLASHLIGHT | HIDEHUD_ALL ) )
		return 1;

	int r, g, b, x, y, a;
	wrect_t rc;
	
	HSPRITE sprite1;
	HSPRITE sprite2;
	HSPRITE spritebeam;
	wrect_t *wreSprite1;
	wrect_t *wreSprite2;
	wrect_t *wreSpriteBeam;
	int	  width;		// width of the battery innards

	int HUD_flash_empty;
	int HUD_flash_full;
	int HUD_flash_beam;
	char flashstring_beam[64];
	char flashstring_full[64];
	char flashstring_empty[64];

	if ((gHUD.m_chzFlashlightIconPostfix != NULL) && (gHUD.m_chzFlashlightIconPostfix[0] == '\0'))
	{
		HUD_flash_beam = gHUD.GetSpriteIndex("flash_beam");
		HUD_flash_full = gHUD.GetSpriteIndex("flash_full");
		HUD_flash_empty = gHUD.GetSpriteIndex("flash_empty");
	}
	else
	{
		sprintf(flashstring_beam, "flash_beam_%s", gHUD.m_chzFlashlightIconPostfix);
		sprintf(flashstring_full, "flash_full_%s", gHUD.m_chzFlashlightIconPostfix);
		sprintf(flashstring_empty, "flash_empty_%s", gHUD.m_chzFlashlightIconPostfix);

		//Nice job.
		if (strncmp(flashstring_beam, "flash_beam_", 12) == 0)
			sprintf(flashstring_beam, "flash_beam");
		
		if (strncmp(flashstring_full, "flash_full_", 12) == 0)
			sprintf(flashstring_full, "flash_full");

		if (strncmp(flashstring_empty, "flash_empty_", 13) == 0)
			sprintf(flashstring_empty, "flash_empty");


		HUD_flash_beam = gHUD.GetSpriteIndex(flashstring_beam);
		HUD_flash_full = gHUD.GetSpriteIndex(flashstring_full);
		HUD_flash_empty = gHUD.GetSpriteIndex(flashstring_empty);
	}

	m_hSprite1 = gHUD.GetSprite(HUD_flash_empty);
	m_hSprite2 = gHUD.GetSprite(HUD_flash_full);
	m_hBeam = gHUD.GetSprite(HUD_flash_beam);
	m_prc1 = &gHUD.GetSpriteRect(HUD_flash_empty);
	m_prc2 = &gHUD.GetSpriteRect(HUD_flash_full);
	m_prcBeam = &gHUD.GetSpriteRect(HUD_flash_beam);
	m_iWidth = m_prc2->right - m_prc2->left;

	width = m_iWidth; 
	sprite1 = m_hSprite1; sprite2 = m_hSprite2; spritebeam = m_hBeam;
	wreSprite1 = m_prc1; wreSprite2 = m_prc2; wreSpriteBeam = m_prcBeam;

	if (!(gHUD.m_iWeaponBits & (1<<(WEAPON_SUIT)) ))
		return 1;

	if (m_fOn)
		a = 225;
	else
		a = MIN_ALPHA;
	
	if( m_flBat < 0.20 )
	{
		UnpackRGB( r, g, b, RGB_REDISH );
	}
	else
	{
		if( gHUD.isNightVisionOn() )
		{
			gHUD.getNightVisionHudItemColor( r, g, b );
		}
		else
		{
			r = giR;
			g = giG;
			b = giB;
		}
	}

	ScaleColors(r, g, b, a);

	y = (wreSprite1->bottom - wreSprite2->top)/2;
	x = ScreenWidth - width - width /2 ;

	// Draw the flashlight casing
	SPR_Set(sprite1, r, g, b );
	SPR_DrawAdditive( 0,  x, y, wreSprite1);

	if ( m_fOn )
	{  // draw the flashlight beam
		x = ScreenWidth - width /2;

		SPR_Set(spritebeam, r, g, b );
		SPR_DrawAdditive( 0, x, y, wreSpriteBeam);

		if (gHUD.flashlight_type->value)
			drawNightVision();
	}

	// draw the flashlight energy level
	x = ScreenWidth - width - width /2 ;
	int iOffset = width * (1.0 - m_flBat);
	if (iOffset < width)
	{
		rc = *wreSprite2;
		rc.left += iOffset;

		SPR_Set(sprite2, r, g, b );
		SPR_DrawAdditive( 0, x + iOffset, y, &rc);
	}


	return 1;
}

void CHudFlashlight::drawNightVision()
{
	static int lastFrame = 0;

	auto frameIndex = rand() % gEngfuncs.pfnSPR_Frames( m_nvSprite );

	if( frameIndex == lastFrame )
		frameIndex = ( frameIndex + 1 ) % gEngfuncs.pfnSPR_Frames( m_nvSprite );

	lastFrame = frameIndex;

	if( m_nvSprite )
	{
		const auto width = gEngfuncs.pfnSPR_Width( m_nvSprite, 0 );
		const auto height = gEngfuncs.pfnSPR_Height( m_nvSprite, 0 );

		gEngfuncs.pfnSPR_Set( m_nvSprite, 0, 170, 0 );

		wrect_t drawingRect;

		for( auto x = 0; x < gHUD.m_scrinfo.iWidth; x += width )
		{
			drawingRect.left = 0;
			drawingRect.right = x + width >= gHUD.m_scrinfo.iWidth ? gHUD.m_scrinfo.iWidth - x : width;

			for( auto y = 0; y < gHUD.m_scrinfo.iHeight; y += height )
			{
				drawingRect.top = 0;
				drawingRect.bottom = y + height >= gHUD.m_scrinfo.iHeight ? gHUD.m_scrinfo.iHeight - y : height;

				gEngfuncs.pfnSPR_DrawAdditive( frameIndex, x, y, &drawingRect );
			}
		}
	}
}

