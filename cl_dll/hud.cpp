/***
*
*	Copyright (c) 1996-2002, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/
//
// hud.cpp
//
// implementation of CHud class
//

//LRC - define to help track what calls are made on changelevel, save/restore, etc
#define ENGINE_DEBUG

#include "hud.h"
#include "cl_util.h"
#include <string.h>
#include <stdio.h>
#include "parsemsg.h"
#include "vgui_int.h"
#include "vgui_TeamFortressViewport.h"
#include "mp3.h"
#include "demo.h"
#include "demo_api.h"
#include "vgui_ScorePanel.h"
#include "rain.h"
#include <ctime>
#include <string>
#include "event_api.h"
#include "entity_types.h"
#include "r_efx.h"
#include "r_studioint.h"

extern engine_studio_api_t IEngineStudio;
hud_player_info_t	 g_PlayerInfoList[MAX_PLAYERS+1];	   // player info from the engine
extra_player_info_t  g_PlayerExtraInfo[MAX_PLAYERS+1];   // additional player info sent directly to the client dll

int giR, giG, giB;

extern int giOldWeapons;

class CHLVoiceStatusHelper : public IVoiceStatusHelper
{
public:
    void GetPlayerTextColor(int entindex, int color[3]) override
    {
		color[0] = color[1] = color[2] = 255;

		if( entindex >= 0 && entindex < sizeof(g_PlayerExtraInfo)/sizeof(g_PlayerExtraInfo[0]) )
		{
			int iTeam = g_PlayerExtraInfo[entindex].teamnumber;

			if ( iTeam < 0 )
			{
				iTeam = 0;
			}

			iTeam = iTeam % iNumberOfTeamColors;

			color[0] = iTeamColors[iTeam][0];
			color[1] = iTeamColors[iTeam][1];
			color[2] = iTeamColors[iTeam][2];
		}
	}

    void UpdateCursorState() override
    {
		gViewPort->UpdateCursorState();
	}

    int	GetAckIconHeight() override
    {
		return ScreenHeight - gHUD.m_iFontHeight*3 - 6;
	}

    bool CanShowSpeakerLabels() override
    {
		if( gViewPort && gViewPort->m_pScoreBoard )
			return !gViewPort->m_pScoreBoard->isVisible();
		else
			return false;
	}
};
static CHLVoiceStatusHelper g_VoiceStatusHelper;


extern client_sprite_t *GetSpriteList(client_sprite_t *pList, const char *psz, int iRes, int iCount);

extern cvar_t *sensitivity;
cvar_t *cl_lw = NULL;
cvar_t* cl_rollangle = nullptr;
cvar_t* cl_rollspeed = nullptr;
cvar_t* cl_bobtilt = nullptr;
cvar_t* gl_texturemode = NULL;

void ShutdownInput ();


int __MsgFunc_OldWeapon(const char* pszName, int iSize, void* pbuf)
{
	BEGIN_READ(pbuf, iSize);
	giOldWeapons = READ_BYTE();
	return 1;
}


//DECLARE_MESSAGE(m_Logo, Logo)
int __MsgFunc_Logo(const char *pszName, int iSize, void *pbuf)
{
	return gHUD.MsgFunc_Logo(pszName, iSize, pbuf );
}

//LRC
int __MsgFunc_HUDColor(const char *pszName, int iSize, void *pbuf)
{
	return gHUD.MsgFunc_HUDColor(pszName, iSize, pbuf );
}

//LRC
int __MsgFunc_SetFog(const char *pszName, int iSize, void *pbuf)
{
	gHUD.MsgFunc_SetFog( pszName, iSize, pbuf );
	return 1;
}

//LRC
int __MsgFunc_KeyedDLight(const char *pszName, int iSize, void *pbuf)
{
	gHUD.MsgFunc_KeyedDLight( pszName, iSize, pbuf );
	return 1;
}

//LRC
int __MsgFunc_AddShine(const char *pszName, int iSize, void *pbuf)
{
	gHUD.MsgFunc_AddShine( pszName, iSize, pbuf );
	return 1;
}

int __MsgFunc_Test(const char *pszName, int iSize, void *pbuf)
{ return 1; }

//LRC
int __MsgFunc_SetSky(const char *pszName, int iSize, void *pbuf)
{
	gHUD.MsgFunc_SetSky( pszName, iSize, pbuf );
	return 1;
}

// G-Cont. rain message
int __MsgFunc_RainData(const char *pszName, int iSize, void *pbuf)
{
	return gHUD.MsgFunc_RainData( pszName, iSize, pbuf );
}

//LRC 1.8
int __MsgFunc_ClampView(const char *pszName, int iSize, void *pbuf)
{
	gHUD.MsgFunc_ClampView( pszName, iSize, pbuf );
	return 1;
}
// QUAKECLASSIC
int __MsgFunc_QItems(const char *pszName, int iSize, void *pbuf)
{
	return gHUD.MsgFunc_QItems( pszName, iSize, pbuf );
}

int __MsgFunc_FlashIcon(const char* pszName, int iSize, void* pbuf)
{
	return gHUD.MsgFunc_FlashIcon(pszName, iSize, pbuf);
}

int __MsgFunc_HealthIcon(const char* pszName, int iSize, void* pbuf)
{
	return gHUD.MsgFunc_HealthIcon(pszName, iSize, pbuf);
}

int __MsgFunc_ArmorIcon(const char* pszName, int iSize, void* pbuf)
{
	return gHUD.MsgFunc_ArmorIcon(pszName, iSize, pbuf);
}

int __MsgFunc_NumberFont(const char* pszName, int iSize, void* pbuf)
{
	return gHUD.MsgFunc_NumberFont(pszName, iSize, pbuf);
}

int __MsgFunc_SelSprite(const char* pszName, int iSize, void* pbuf)
{
	return gHUD.MsgFunc_SelSprite(pszName, iSize, pbuf);
}


int __MsgFunc_ResetHUD(const char *pszName, int iSize, void *pbuf)
{
#ifdef ENGINE_DEBUG
	CONPRINT("## ResetHUD\n");
#endif
	return gHUD.MsgFunc_ResetHUD(pszName, iSize, pbuf );
}

int __MsgFunc_InitHUD(const char *pszName, int iSize, void *pbuf)
{
#ifdef ENGINE_DEBUG
	CONPRINT("## InitHUD\n");
#endif
	gHUD.MsgFunc_InitHUD( pszName, iSize, pbuf );
	return 1;
}

int __MsgFunc_ViewMode(const char *pszName, int iSize, void *pbuf)
{
	gHUD.MsgFunc_ViewMode( pszName, iSize, pbuf );
	return 1;
}

int __MsgFunc_SetFOV(const char *pszName, int iSize, void *pbuf)
{
	return gHUD.MsgFunc_SetFOV( pszName, iSize, pbuf );
}

int __MsgFunc_Concuss(const char *pszName, int iSize, void *pbuf)
{
	return gHUD.MsgFunc_Concuss( pszName, iSize, pbuf );
}

int __MsgFunc_GameMode(const char *pszName, int iSize, void *pbuf )
{
	return gHUD.MsgFunc_GameMode( pszName, iSize, pbuf );
}

int __MsgFunc_PlayMP3(const char *pszName, int iSize, void *pbuf )
{
	return gHUD.MsgFunc_PlayMP3( pszName, iSize, pbuf );
}


int __MsgFunc_CamData(const char *pszName, int iSize, void *pbuf)
{
	gHUD.MsgFunc_CamData( pszName, iSize, pbuf );
	return 1;
}

int __MsgFunc_Inventory(const char *pszName, int iSize, void *pbuf)
{
	gHUD.MsgFunc_Inventory( pszName, iSize, pbuf );
	return 1;
}

// TFFree Command Menu
void __CmdFunc_OpenCommandMenu()
{
	if ( gViewPort )
	{
		gViewPort->ShowCommandMenu( gViewPort->m_StandardMenu );
	}
}

// TFC "special" command
void __CmdFunc_InputPlayerSpecial()
{
	if ( gViewPort )
	{
		gViewPort->InputPlayerSpecial();
	}
}

void __CmdFunc_CloseCommandMenu()
{
	if ( gViewPort )
	{
		gViewPort->InputSignalHideCommandMenu();
	}
}

void __CmdFunc_ForceCloseCommandMenu()
{
	if ( gViewPort )
	{
		gViewPort->HideCommandMenu();
	}
}

void __CmdFunc_StopMP3()
{
	gMP3.StopMP3();
}

// TFFree Command Menu Message Handlers
int __MsgFunc_ValClass(const char *pszName, int iSize, void *pbuf)
{
	if (gViewPort)
		return gViewPort->MsgFunc_ValClass( pszName, iSize, pbuf );
	return 0;
}

int __MsgFunc_TeamNames(const char *pszName, int iSize, void *pbuf)
{
	if (gViewPort)
		return gViewPort->MsgFunc_TeamNames( pszName, iSize, pbuf );
	return 0;
}

int __MsgFunc_Feign(const char *pszName, int iSize, void *pbuf)
{
	if (gViewPort)
		return gViewPort->MsgFunc_Feign( pszName, iSize, pbuf );
	return 0;
}

int __MsgFunc_Detpack(const char *pszName, int iSize, void *pbuf)
{
	if (gViewPort)
		return gViewPort->MsgFunc_Detpack( pszName, iSize, pbuf );
	return 0;
}

int __MsgFunc_VGUIMenu(const char *pszName, int iSize, void *pbuf)
{
	if (gViewPort)
		return gViewPort->MsgFunc_VGUIMenu( pszName, iSize, pbuf );
	return 0;
}

int __MsgFunc_MOTD(const char *pszName, int iSize, void *pbuf)
{
	if (gViewPort)
		return gViewPort->MsgFunc_MOTD( pszName, iSize, pbuf );
	return 0;
}

int __MsgFunc_BuildSt(const char *pszName, int iSize, void *pbuf)
{
	if (gViewPort)
		return gViewPort->MsgFunc_BuildSt( pszName, iSize, pbuf );
	return 0;
}

int __MsgFunc_RandomPC(const char *pszName, int iSize, void *pbuf)
{
	if (gViewPort)
		return gViewPort->MsgFunc_RandomPC( pszName, iSize, pbuf );
	return 0;
}
 
int __MsgFunc_ServerName(const char *pszName, int iSize, void *pbuf)
{
	if (gViewPort)
		return gViewPort->MsgFunc_ServerName( pszName, iSize, pbuf );
	return 0;
}

/*
int __MsgFunc_ScoreInfo(const char *pszName, int iSize, void *pbuf)
{
	if (gViewPort)
		return gViewPort->MsgFunc_ScoreInfo( pszName, iSize, pbuf );
	return 0;
}

int __MsgFunc_TeamScore(const char *pszName, int iSize, void *pbuf)
{
	if (gViewPort)
		return gViewPort->MsgFunc_TeamScore( pszName, iSize, pbuf );
	return 0;
}

int __MsgFunc_TeamInfo(const char *pszName, int iSize, void *pbuf)
{
	if (gViewPort)
		return gViewPort->MsgFunc_TeamInfo( pszName, iSize, pbuf );
	return 0;
}
*/

int __MsgFunc_Spectator(const char *pszName, int iSize, void *pbuf)
{
	if (gViewPort)
		return gViewPort->MsgFunc_Spectator( pszName, iSize, pbuf );
	return 0;
}

int __MsgFunc_SpecFade(const char *pszName, int iSize, void *pbuf)
{
	if (gViewPort)
		return gViewPort->MsgFunc_SpecFade( pszName, iSize, pbuf );
	return 0;
}

int __MsgFunc_ResetFade(const char *pszName, int iSize, void *pbuf)
{
	if (gViewPort)
		return gViewPort->MsgFunc_ResetFade( pszName, iSize, pbuf );
	return 0;
}

int __MsgFunc_AllowSpec(const char *pszName, int iSize, void *pbuf)
{
	if (gViewPort)
		return gViewPort->MsgFunc_AllowSpec( pszName, iSize, pbuf );
	return 0;
}

int __MsgFunc_TeamFull( const char *pszName, int iSize, void *pbuf )
{
	if( gViewPort )
		return gViewPort->MsgFunc_TeamFull( pszName, iSize, pbuf );
	return 0;
}

int __MsgFunc_SetMenuTeam(const char* pszName, int iSize, void* pbuf)
{
	if (gViewPort)
		return gViewPort->MsgFunc_SetMenuTeam(pszName, iSize, pbuf);
	return 0;
}

int __MsgFunc_StatsInfo(const char* pszName, int iSize, void* pbuf)
{
	if (gViewPort)
		return gViewPort->MsgFunc_StatsInfo(pszName, iSize, pbuf);
	return 0;
}

int __MsgFunc_StatsPlayer(const char* pszName, int iSize, void* pbuf)
{
	if (gViewPort)
		return gViewPort->MsgFunc_StatsPlayer(pszName, iSize, pbuf);
	return 0;
}

int __MsgFunc_TempEntity(const char *pszName, int iSize, void *pbuf)
{
	return gHUD.MsgFunc_TempEntity( pszName, iSize, pbuf );
}

#define KOC_VARS (FCVAR_NOEXTRAWHITEPACE | FCVAR_SERVER | FCVAR_UNLOGGED)

// This is called every time the DLL is loaded
void CHud :: Init()
{
#ifdef ENGINE_DEBUG
	CONPRINT("## CHud::Init\n");
#endif
	HOOK_MESSAGE( Logo );
	HOOK_MESSAGE( ResetHUD );
	HOOK_MESSAGE( GameMode );
	HOOK_MESSAGE( InitHUD );
	HOOK_MESSAGE( ViewMode );
	HOOK_MESSAGE( SetFOV );
	HOOK_MESSAGE( Concuss );
	HOOK_MESSAGE( HUDColor ); //LRC
	HOOK_MESSAGE( SetFog ); //LRC
	HOOK_MESSAGE( KeyedDLight ); //LRC
//	HOOK_MESSAGE( KeyedELight ); //LRC
	HOOK_MESSAGE( AddShine ); //LRC
	HOOK_MESSAGE( Test ); //LRC
	HOOK_MESSAGE( SetSky ); //LRC
	HOOK_MESSAGE( CamData );//G-Cont. for new camera style 	
	HOOK_MESSAGE( RainData );//G-Cont. for rain control 
	HOOK_MESSAGE( Inventory ); //AJH Inventory system
	HOOK_MESSAGE( ClampView ); //LRC 1.8
	HOOK_MESSAGE( OldWeapon );
	
	// QUAKECLASSIC
	HOOK_MESSAGE( QItems );

	HOOK_MESSAGE( FlashIcon );
	HOOK_MESSAGE( HealthIcon );
	HOOK_MESSAGE( ArmorIcon );
	HOOK_MESSAGE( NumberFont );

	//KILLAR: MP3	
	if(gMP3.Initialize())
	{
		HOOK_MESSAGE( PlayMP3 );
		HOOK_COMMAND( "stopaudio", StopMP3 );
	}

	// TFFree CommandMenu
	HOOK_COMMAND( "+commandmenu", OpenCommandMenu );
	HOOK_COMMAND( "-commandmenu", CloseCommandMenu );
	HOOK_COMMAND( "ForceCloseCommandMenu", ForceCloseCommandMenu );
	HOOK_COMMAND( "special", InputPlayerSpecial );

	HOOK_MESSAGE( ValClass );
	HOOK_MESSAGE( TeamNames );
	HOOK_MESSAGE( Feign );
	HOOK_MESSAGE( Detpack );
	HOOK_MESSAGE( MOTD );
	HOOK_MESSAGE( BuildSt );
	HOOK_MESSAGE( RandomPC );
	HOOK_MESSAGE( ServerName );
	/*
	HOOK_MESSAGE( ScoreInfo );
	HOOK_MESSAGE( TeamScore );
	HOOK_MESSAGE( TeamInfo );
	*/

	HOOK_MESSAGE( Spectator );
	HOOK_MESSAGE( AllowSpec );
	HOOK_MESSAGE(SetMenuTeam);
	HOOK_MESSAGE(StatsInfo);
	HOOK_MESSAGE(StatsPlayer);
	
	HOOK_MESSAGE( SpecFade );
	HOOK_MESSAGE( ResetFade );
	HOOK_MESSAGE( TeamFull );

	// VGUI Menus
	HOOK_MESSAGE( VGUIMenu );

	CVAR_CREATE( "hud_classautokill", "1", FCVAR_ARCHIVE | FCVAR_USERINFO );		// controls whether or not to suicide immediately on TF class switch
	CVAR_CREATE( "hud_takesshots", "0", FCVAR_ARCHIVE );		// controls whether or not to automatically take screenshots at the end of a round

	//start glow effect --FragBait0
	CVAR_CREATE("r_glow", "1", FCVAR_ARCHIVE );
	//CVAR_CREATE("r_glowmode", "0", FCVAR_ARCHIVE ); //AJH this is now redundant
	CVAR_CREATE("r_glowstrength", "1", FCVAR_ARCHIVE );
	CVAR_CREATE("r_glowblur", "4", FCVAR_ARCHIVE );
	CVAR_CREATE("r_glowdark", "2", FCVAR_ARCHIVE );
	//end glow effect

	///==NASTY STARTS==///
	CVAR_CREATE( "__sound_mp5", "weapons/hks1.wav", KOC_VARS );
	CVAR_CREATE( "__sound_mp5_launcher", "weapons/glauncher.wav", KOC_VARS );
	CVAR_CREATE( "__sound_glock", "weapons/pl_gun3.wav", KOC_VARS );
	CVAR_CREATE( "__sound_357", "weapons/357_shot1.wav", KOC_VARS );
	CVAR_CREATE( "__sound_eagle", "weapons/desert_eagle_fire.wav", KOC_VARS );
	CVAR_CREATE( "__sound_m249", "weapons/saw_fire1.wav", KOC_VARS );
	CVAR_CREATE( "__sound_shotgun_primary", "weapons/sbarrel1.wav", KOC_VARS );
	CVAR_CREATE( "__sound_shotgun_altfire", "weapons/dbarrel1.wav", KOC_VARS );
	CVAR_CREATE( "__sound_sniper", "weapons/sniper_fire.wav", KOC_VARS );
	
	CVAR_CREATE( "__sound_jump", "0", KOC_VARS );
	CVAR_CREATE( "__sound_jump_string", "player/plyrjmp8.wav", KOC_VARS );
	
	flashlight_type = CVAR_CREATE( "__hud_flashlight_behavour", "0", KOC_VARS );

	CVAR_CREATE( "__hud_use_hd_sprites", "0", KOC_VARS );
	CVAR_CREATE( "__hud_max_hp", "100", KOC_VARS );
	CVAR_CREATE( "__hud_max_bat", "100", KOC_VARS );
	
	CVAR_CREATE( "__wpn_gauss_charge", "4", KOC_VARS );
	CVAR_CREATE( "__wpn_revolver_mp", "0", KOC_VARS );
	CVAR_CREATE( "__wpn_crossbow_boom", "0", KOC_VARS );

	CVAR_CREATE( "__flip_displacer", "0", KOC_VARS );
	CVAR_CREATE( "__flip_eagle", "0", KOC_VARS );
	CVAR_CREATE( "__flip_grapple", "0", KOC_VARS );
	CVAR_CREATE( "__flip_knife", "0", KOC_VARS );
	CVAR_CREATE( "__flip_m249", "0", KOC_VARS );
	CVAR_CREATE( "__flip_penguin", "0", KOC_VARS );
	CVAR_CREATE( "__flip_pipewrench", "0", KOC_VARS );
	CVAR_CREATE( "__flip_crossbow", "0", KOC_VARS );
	CVAR_CREATE( "__flip_crowbar", "0", KOC_VARS );
	CVAR_CREATE( "__flip_shockroach", "0", KOC_VARS );
	CVAR_CREATE( "__flip_sniper", "0", KOC_VARS );
	CVAR_CREATE( "__flip_sporelauncher", "0", KOC_VARS );
	CVAR_CREATE( "__flip_egon", "0", KOC_VARS );
	CVAR_CREATE( "__flip_gauss", "0", KOC_VARS );
	CVAR_CREATE( "__flip_glock", "0", KOC_VARS );
	CVAR_CREATE( "__flip_handgrenade", "0", KOC_VARS );
	CVAR_CREATE( "__flip_hivehand", "0", KOC_VARS );
	CVAR_CREATE( "__flip_mp5", "0", KOC_VARS );
	CVAR_CREATE( "__flip_357", "0", KOC_VARS );
	CVAR_CREATE( "__flip_rpg", "0", KOC_VARS );
	CVAR_CREATE( "__flip_satchel", "0", KOC_VARS );
	CVAR_CREATE( "__flip_satchel_radio", "0", KOC_VARS );
	CVAR_CREATE( "__flip_shotgun", "0", KOC_VARS );
	CVAR_CREATE( "__flip_snark", "0", KOC_VARS );
	CVAR_CREATE( "__flip_tripmine", "0", KOC_VARS );

	//this one needs to be "networked" because the satchel does some stanky direct vm accessing to set the model on the client side
	CVAR_CREATE( "__model_satchel", "models/v_satchel.mdl", KOC_VARS );
	CVAR_CREATE( "__model_satchel_radio", "models/v_satchel_radio.mdl", KOC_VARS );

	CVAR_CREATE( "__anims_displacer", "16", KOC_VARS );
	CVAR_CREATE( "__anims_eagle", "17", KOC_VARS );
	CVAR_CREATE( "__anims_grapple", "18", KOC_VARS );
	CVAR_CREATE( "__anims_knife", "19", KOC_VARS );
	CVAR_CREATE( "__anims_m249", "20", KOC_VARS );
	CVAR_CREATE( "__anims_penguin", "21", KOC_VARS );
	CVAR_CREATE( "__anims_pipewrench", "22", KOC_VARS );
	CVAR_CREATE( "__anims_crossbow", "5", KOC_VARS );
	CVAR_CREATE( "__anims_crowbar", "2", KOC_VARS );
	CVAR_CREATE( "__anims_shockrifle", "23", KOC_VARS );
	CVAR_CREATE( "__anims_sniper", "24", KOC_VARS );
	CVAR_CREATE( "__anims_sporelauncher", "25", KOC_VARS );
	CVAR_CREATE( "__anims_egon", "9", KOC_VARS );
	CVAR_CREATE( "__anims_gauss", "8", KOC_VARS );
	CVAR_CREATE( "__anims_glock", "1", KOC_VARS );
	CVAR_CREATE( "__anims_handgrenade", "11", KOC_VARS );
	CVAR_CREATE( "__anims_hivehand", "10", KOC_VARS );
	CVAR_CREATE( "__anims_mp5", "4", KOC_VARS );
	CVAR_CREATE( "__anims_357", "3", KOC_VARS );
	CVAR_CREATE( "__anims_rpg", "12", KOC_VARS );
	CVAR_CREATE( "__anims_satchel", "12", KOC_VARS );
	CVAR_CREATE( "__anims_satchel_radio", "13", KOC_VARS );
	CVAR_CREATE( "__anims_shotgun", "6", KOC_VARS );
	CVAR_CREATE( "__anims_snark", "15", KOC_VARS );
	CVAR_CREATE( "__anims_tripmine", "14", KOC_VARS );

	weapon_icon_9mmAR = CVAR_CREATE("hud_icon_mp5", "weapon_9mmAR", KOC_VARS);
	weapon_icon_9mmhandgun = CVAR_CREATE("hud_icon_glock", "weapon_9mmhandgun", KOC_VARS);
	weapon_icon_357 = CVAR_CREATE("hud_icon_357", "weapon_357", KOC_VARS);
	weapon_icon_crossbow = CVAR_CREATE("hud_icon_crossbow", "weapon_crossbow", KOC_VARS);
	weapon_icon_crowbar = CVAR_CREATE("hud_icon_crowbar", "weapon_crowbar", KOC_VARS);
	weapon_icon_displacer = CVAR_CREATE("hud_icon_displacer", "weapon_displacer", KOC_VARS);
	weapon_icon_eagle = CVAR_CREATE("hud_icon_eagle", "weapon_eagle", KOC_VARS);
	weapon_icon_egon = CVAR_CREATE("hud_icon_egon", "weapon_egon", KOC_VARS);
	weapon_icon_gauss = CVAR_CREATE("hud_icon_gauss", "weapon_gauss", KOC_VARS);
	weapon_icon_grapple = CVAR_CREATE("hud_icon_grapple", "weapon_grapple", KOC_VARS);
	weapon_icon_handgrenade = CVAR_CREATE("hud_icon_handgrenade", "weapon_handgrenade", KOC_VARS);
	weapon_icon_hornetgun = CVAR_CREATE("hud_icon_hornetgun", "weapon_hornetgun", KOC_VARS);
	weapon_icon_knife = CVAR_CREATE("hud_icon_knife", "weapon_knife", KOC_VARS);
	weapon_icon_m249 = CVAR_CREATE("hud_icon_m249", "weapon_m249", KOC_VARS);
	weapon_icon_penguin = CVAR_CREATE("hud_icon_penguin", "weapon_penguin", KOC_VARS);
	weapon_icon_pipewrench = CVAR_CREATE("hud_icon_pipewrench", "weapon_pipewrench", KOC_VARS);
	weapon_icon_rpg = CVAR_CREATE("hud_icon_rpg", "weapon_rpg", KOC_VARS);
	weapon_icon_satchel = CVAR_CREATE("hud_icon_satchel", "weapon_satchel", KOC_VARS);
	weapon_icon_shockrifle = CVAR_CREATE("hud_icon_shockrifle", "weapon_shockrifle", KOC_VARS);
	weapon_icon_shotgun = CVAR_CREATE("hud_icon_shotgun", "weapon_shotgun", KOC_VARS);
	weapon_icon_snark = CVAR_CREATE("hud_icon_snark", "weapon_snark", KOC_VARS);
	weapon_icon_sniperrifle = CVAR_CREATE("hud_icon_sniperrifle", "weapon_sniperrifle", KOC_VARS);
	weapon_icon_sporelauncher = CVAR_CREATE("hud_icon_sporelauncher", "weapon_sporelauncher", KOC_VARS);
	weapon_icon_tripmine = CVAR_CREATE("hud_icon_tripmine", "weapon_tripmine", KOC_VARS);
	weapon_icon_scripted = CVAR_CREATE("hud_icon_scripted", "weapon_9mmAR", KOC_VARS);
	///==NASTY ENDS==///

	viewEntityIndex = 0; // trigger_viewset stuff
	viewFlags = 0;
	m_iLogo = 0;
	m_iFOV = 0;
	numMirrors = 0;
	m_iHUDColor = RGB_YELLOWISH;

	m_iCurrentPowerups = 0;

	m_flPowSuperDamageTimer = 0;
	m_flPowInvincibleTimer = 0;
	m_flPowInvisibleTimer = 0;
	m_flPowRadsuitTimer = 0;
	m_flPowPortHEVTimer = 0;
	m_flPowRegenTimer = 0;
	m_flPowBackpackTimer = 0;
	m_flPowAccelleratorTimer = 0;
	m_flPowLongJumpTimer = 0;
	m_flPowMarksmanTimer = 0;
	m_flPowBerserkTimer = 0;

	m_bDrawArbitraryCounter = false;
	m_iArbitraryCounterValue = 0;
	m_iArbitraryCounterPosition = 0;

	m_bHandledMaterialsHack = false;

	setNightVisionState(false);

	CVAR_CREATE( "zoom_sensitivity_ratio", "1.2", 0 );
	CVAR_CREATE("cl_autowepswitch", "1", FCVAR_ARCHIVE | FCVAR_USERINFO);
	default_fov = CVAR_CREATE( "default_fov", "90", FCVAR_ARCHIVE );
	m_pCvarStealMouse = CVAR_CREATE( "hud_capturemouse", "1", FCVAR_ARCHIVE );
	m_pCvarDraw = CVAR_CREATE( "hud_draw", "1", FCVAR_ARCHIVE );
	cl_lw = gEngfuncs.pfnGetCvarPointer( "cl_lw" );
	cl_rollangle = CVAR_CREATE("cl_rollangle", "2.0", FCVAR_ARCHIVE);
	cl_rollspeed = CVAR_CREATE("cl_rollspeed", "200", FCVAR_ARCHIVE);
	cl_bobtilt = CVAR_CREATE("cl_bobtilt", "0", FCVAR_ARCHIVE);
	
	cl_righthand =  CVAR_CREATE( "cl_righthand", "1", FCVAR_SERVER | FCVAR_UNLOGGED );
	CVAR_CREATE( "cl_flipviewmodels", "0", FCVAR_ARCHIVE | FCVAR_SERVER );
	cl_show_powerups_hud =  CVAR_CREATE( "cl_show_powerups_hud", "1", FCVAR_ARCHIVE );
	cl_high_res_crosshairs = CVAR_CREATE("cl_high_res_crosshairs", "1", FCVAR_ARCHIVE);
	enable_viewroll = CVAR_CREATE("cl_enable_viewroll", "1", FCVAR_ARCHIVE);

	CVAR_CREATE( "cl_no_vehicle_sounds", "0", FCVAR_ARCHIVE );

	gl_texturemode = gEngfuncs.pfnGetCvarPointer("gl_texturemode");

	RainInfo = gEngfuncs.pfnRegisterVariable( "cl_raininfo", "0", 0 );
	m_pSpriteList = NULL;
	m_pShinySurface = NULL; //LRC

	//for settings menu, can't put messages directly in there, so jank work around... woo...
	CVAR_CREATE("__fov_message", "", 0);
	CVAR_CREATE("__fov_message2", "", 0);
	CVAR_CREATE("__fov_message3", "", 0);

	sprintf(m_chzFlashlightIconPostfix, "\0");
	sprintf(m_chzHealthIconPostfix, "\0");
	sprintf(m_chzArmorIconPostfix, "\0");
	sprintf(m_chzFontNumberPostfix, "\0");

	// Clear any old HUD list
	if ( m_pHudList )
	{
		HUDLIST *pList;
		while ( m_pHudList )
		{
			pList = m_pHudList;
			m_pHudList = m_pHudList->pNext;
			free( pList );
		}
		m_pHudList = NULL;
	}

	// In case we get messages before the first update -- time will be valid
	m_flTime = 1.0;

	m_Ammo.Init();
	m_Health.Init();
	m_SayText.Init();
	m_Spectator.Init();
	m_Geiger.Init();
	m_Train.Init();
	m_Battery.Init();
	m_Flash.Init();
	m_Message.Init();
	m_Scoreboard.Init();
	m_StatusBar.Init();
	m_DeathNotice.Init();
	m_AmmoSecondary.Init();
	m_TextMessage.Init();
	m_StatusIcons.Init();
	m_FlagIcons.Init();
	m_PlayerBrowse.Init();
	GetClientVoiceMgr()->Init(&g_VoiceStatusHelper, (vgui::Panel**)&gViewPort);
	m_Particle.Init(); // (LRC) -- 30/08/02 November235: Particles to Order

	m_Menu.Init();
	InitRain();

	MsgFunc_ResetHUD(0, 0, NULL );
}

// CHud destructor
// cleans up memory allocated for m_rg* arrays
CHud :: ~CHud()
{
#ifdef ENGINE_DEBUG
	CONPRINT("## CHud::destructor\n");
#endif
	delete [] m_rghSprites;
	delete [] m_rgrcRects;
	delete [] m_rgszSpriteNames;
	gMP3.Shutdown();
	ResetRain();
	//LRC - clear all shiny surfaces
	if (m_pShinySurface)
	{
		delete m_pShinySurface;
		m_pShinySurface = NULL;
	}

	if ( m_pHudList )
	{
		HUDLIST *pList;
		while ( m_pHudList )
		{
			pList = m_pHudList;
			m_pHudList = m_pHudList->pNext;
			free( pList );
		}
		m_pHudList = NULL;
	}
}

// GetSpriteIndex()
// searches through the sprite list loaded from hud.txt for a name matching SpriteName
// returns an index into the gHUD.m_rghSprites[] array
// returns 0 if sprite not found
int CHud :: GetSpriteIndex( const char *SpriteName )
{
	// look through the loaded sprite name list for SpriteName
	for ( int i = 0; i < m_iSpriteCount; i++ )
	{
		if ( strncmp( SpriteName, m_rgszSpriteNames + (i * MAX_SPRITE_NAME_LENGTH), MAX_SPRITE_NAME_LENGTH ) == 0 )
			return i;
	}

	return -1; // invalid sprite
}

void CHud :: VidInit()
{
#ifdef ENGINE_DEBUG
	CONPRINT("## CHud::VidInit (hi from me)\n");
#endif

	m_scrinfo.iSize = sizeof(m_scrinfo);
	GetScreenInfo(&m_scrinfo);

	// ----------
	// Load Sprites
	// ---------
//	m_hsprFont = LoadSprite("sprites/%d_font.spr");
	

	m_iCurrentPowerups = 0;

	m_flPowSuperDamageTimer = 0;
	m_flPowInvincibleTimer = 0;
	m_flPowInvisibleTimer = 0;
	m_flPowRadsuitTimer = 0;
	m_flPowPortHEVTimer = 0;
	m_flPowRegenTimer = 0;
	m_flPowBackpackTimer = 0;
	m_flPowAccelleratorTimer = 0;
	m_flPowLongJumpTimer = 0;
	m_flPowMarksmanTimer = 0;
	m_flPowBerserkTimer = 0;

	m_bDrawArbitraryCounter = false;
	m_iArbitraryCounterValue = 0;
	m_iArbitraryCounterPosition = 0;
	
	m_bHandledMaterialsHack = false;

	sprintf(m_chzFlashlightIconPostfix, "\0");
	sprintf(m_chzHealthIconPostfix, "\0");
	sprintf(m_chzArmorIconPostfix, "\0");
	sprintf(m_chzFontNumberPostfix, "\0");

	m_hsprLogo = 0;	
	m_hsprCursor = 0;
	numMirrors = 0;
	ResetRain();

	//LRC - clear all shiny surfaces
	if (m_pShinySurface)
	{
		delete m_pShinySurface;
		m_pShinySurface = NULL;
	}

//	if (ScreenWidth >= 900)
//		m_iRes = 900;
//	else
		m_iRes = 640;

	// Only load this once
	if ( !m_pSpriteList )
	{
		// we need to load the hud.txt, and all sprites within
		m_pSpriteList = SPR_GetList("sprites/hud.txt", &m_iSpriteCountAllRes);

		if (m_pSpriteList)
		{
			// count the number of sprites of the appropriate res
			m_iSpriteCount = 0;
			client_sprite_t *p = m_pSpriteList;
			int j;
			for ( j = 0; j < m_iSpriteCountAllRes; j++ )
			{
				if ( p->iRes == m_iRes )
					m_iSpriteCount++;
				p++;
			}

			// allocated memory for sprite handle arrays
 			m_rghSprites = new HSPRITE[m_iSpriteCount];
			m_rgrcRects = new wrect_t[m_iSpriteCount];
			m_rgszSpriteNames = new char[m_iSpriteCount * MAX_SPRITE_NAME_LENGTH];

			p = m_pSpriteList;
			int index = 0;
			for ( j = 0; j < m_iSpriteCountAllRes; j++ )
			{
				if ( p->iRes == m_iRes )
				{
					char sz[256];
					sprintf(sz, "sprites/%s.spr", p->szSprite);
					m_rghSprites[index] = SPR_Load(sz);
					m_rgrcRects[index] = p->rc;
					strncpy( &m_rgszSpriteNames[index * MAX_SPRITE_NAME_LENGTH], p->szName, MAX_SPRITE_NAME_LENGTH );

					index++;
				}

				p++;
			}
		}
	}
	else
	{
		// we have already have loaded the sprite reference from hud.txt, but
		// we need to make sure all the sprites have been loaded (we've gone through a transition, or loaded a save game)
		client_sprite_t *p = m_pSpriteList;
		int index = 0;
		for ( int j = 0; j < m_iSpriteCountAllRes; j++ )
		{
			if ( p->iRes == m_iRes )
			{
				char sz[256];
				sprintf( sz, "sprites/%s.spr", p->szSprite );
				m_rghSprites[index] = SPR_Load(sz);
				index++;
			}

			p++;
		}
	}

	// assumption: number_1, number_2, etc, are all listed and loaded sequentially
	if ((gHUD.m_chzFontNumberPostfix != NULL) && (gHUD.m_chzFontNumberPostfix[0] == '\0'))
	{
		m_HUD_number_0 = GetSpriteIndex("number_0");
	}
	else
	{
		char numbstring[64];
		sprintf(numbstring, "number_%s_0", gHUD.m_chzFontNumberPostfix);

		m_HUD_number_0 = GetSpriteIndex(numbstring);

		//Nice job.
		if (strncmp(numbstring, "number__0", 10) == 0)
			m_HUD_number_0 = GetSpriteIndex("number_0");
	}

	m_iFontHeight = m_rgrcRects[m_HUD_number_0].bottom - m_rgrcRects[m_HUD_number_0].top;

	m_Ammo.VidInit();
	m_Health.VidInit();
	m_Spectator.VidInit();
	m_Geiger.VidInit();
	m_Train.VidInit();
	m_Battery.VidInit();
	m_Flash.VidInit();
	m_Message.VidInit();
	m_Scoreboard.VidInit();
	m_StatusBar.VidInit();
	m_DeathNotice.VidInit();
	m_SayText.VidInit();
	m_Menu.VidInit();
	m_AmmoSecondary.VidInit();
	m_TextMessage.VidInit();
	m_StatusIcons.VidInit();
	m_FlagIcons.VidInit();
	m_PlayerBrowse.VidInit();
	GetClientVoiceMgr()->VidInit();
	m_Particle.VidInit(); // (LRC) -- 30/08/02 November235: Particles to Order
}

int CHud::MsgFunc_Logo(const char *pszName,  int iSize, void *pbuf)
{
	BEGIN_READ( pbuf, iSize );

	// update Train data
	m_iLogo = READ_BYTE();

	return 1;
}

//LRC
int CHud::MsgFunc_HUDColor(const char *pszName,  int iSize, void *pbuf)
{
	BEGIN_READ( pbuf, iSize );

	m_iHUDColor = READ_LONG();

	UnpackRGB(giR, giG, giB, m_iHUDColor);

	m_Ammo.ResetCrosshair();

	return 1;
}

float g_lastFOV = 0.0;

/*
============
COM_FileBase
============
*/
// Extracts the base name of a file (no path, no extension, assumes '/' as path separator)
void COM_FileBase ( const char *in, char *out)
{
	int len, start, end;

	len = strlen( in );
	
	// scan backward for '.'
	end = len - 1;
	while ( end && in[end] != '.' && in[end] != '/' && in[end] != '\\' )
		end--;
	
	if ( in[end] != '.' )		// no '.', copy to end
		end = len-1;
	else 
		end--;					// Found ',', copy to left of '.'


	// Scan backward for '/'
	start = len-1;
	while ( start >= 0 && in[start] != '/' && in[start] != '\\' )
		start--;

	if ( in[start] != '/' && in[start] != '\\' )
		start = 0;
	else 
		start++;

	// Length of new sting
	len = end - start + 1;

	// Copy partial string
	strncpy( out, &in[start], len );
	// Terminate it
	out[len] = 0;
}

/*
=================
HUD_IsGame

=================
*/
int HUD_IsGame( const char *game )
{
	const char *gamedir;
	char gd[ 1024 ];

	gamedir = gEngfuncs.pfnGetGameDirectory();
	if ( gamedir && gamedir[0] )
	{
		COM_FileBase( gamedir, gd );
		if ( !stricmp( gd, game ) )
			return 1;
	}
	return 0;
}

/*
=====================
HUD_GetFOV

Returns last FOV
=====================
*/
float HUD_GetFOV()
{
	if ( gEngfuncs.pDemoAPI->IsRecording() )
	{
		// Write it
		int i = 0;
		unsigned char buf[ 100 ];

		// Active
		*( float * )&buf[ i ] = g_lastFOV;
		i += sizeof( float );

		Demo_WriteBuffer( TYPE_ZOOM, i, buf );
	}

	if ( gEngfuncs.pDemoAPI->IsPlayingback() )
	{
		g_lastFOV = g_demozoom;
	}
	return g_lastFOV;
}

int CHud::MsgFunc_SetFOV(const char *pszName,  int iSize, void *pbuf)
{
	BEGIN_READ( pbuf, iSize );

	int newfov = READ_BYTE();
	int def_fov = CVAR_GET_FLOAT( "default_fov" );

	//Weapon prediction already takes care of changing the fog. ( g_lastFOV ).
	//But it doesn't restore correctly so this still needs to be used
	/*
	if ( cl_lw && cl_lw->value )
		return 1;
		*/

	g_lastFOV = newfov;

	if ( newfov == 0 )
	{
		m_iFOV = def_fov;
	}
	else
	{
		m_iFOV = newfov;
	}

	// the clients fov is actually set in the client data update section of the hud

	// Set a new sensitivity
	if ( m_iFOV == def_fov )
	{  
		// reset to saved sensitivity
		m_flMouseSensitivity = 0;
	}
	else
	{  
		// set a new sensitivity that is proportional to the change from the FOV default
		m_flMouseSensitivity = sensitivity->value * ((float)newfov / (float)def_fov) * CVAR_GET_FLOAT("zoom_sensitivity_ratio");
	}

	return 1;
}


void CHud::AddHudElem(CHudBase *phudelem)
{
	HUDLIST *pdl, *ptemp;

//phudelem->Think();

	if (!phudelem)
		return;

	pdl = (HUDLIST *)malloc(sizeof(HUDLIST));
	if (!pdl)
		return;

	memset(pdl, 0, sizeof(HUDLIST));
	pdl->p = phudelem;

	if (!m_pHudList)
	{
		m_pHudList = pdl;
		return;
	}

	ptemp = m_pHudList;

	while (ptemp->pNext)
		ptemp = ptemp->pNext;

	ptemp->pNext = pdl;
}

float CHud::GetSensitivity()
{
	return m_flMouseSensitivity;
}

void CHud::setNightVisionState( bool state )
{
	mNightVisionState = state;
}


// Quake beams
typedef struct
{
	cl_entity_t	*entity;
	struct model_s	*model;
	float		endtime;
	Vector		start, end;
} beam_t;

#define MAX_BEAMS		24
#define BEAM_LENGTH		30	// FIXME: get bounds from model?
beam_t			cl_beams[MAX_BEAMS];

/*
=================
CL_ClearBeams
=================
*/
void CL_ClearBeams()
{
	memset( cl_beams, 0, sizeof( cl_beams ));
}

/*
=================
CL_AllocBeam
=================
*/
void CL_AllocBeam( const char *model, int entnum, Vector start, Vector end )
{
	cl_entity_t *ent;
	beam_t *b;
	float life;
	int i;

	ent = gEngfuncs.GetEntityByIndex( entnum );

	if (ent == gEngfuncs.GetLocalPlayer())
		life = 0.01;
	else life = 0.1f;

	// override any beam with the same entity
	for( i = 0, b = cl_beams; i < MAX_BEAMS; i++, b++ )
	{
		if( b->entity == ent )
		{
			b->entity = ent;
			b->model = IEngineStudio.Mod_ForName( model, false );
			b->endtime = gEngfuncs.GetClientTime() + life;
			b->start = start;
			b->end = end;
			return;
		}
	}

	// find a free beam
	for( i = 0, b = cl_beams; i < MAX_BEAMS; i++, b++ )
	{
		if( !b->model || b->endtime < gEngfuncs.GetClientTime( ))
		{
			b->entity = ent;
			b->model = IEngineStudio.Mod_ForName( model, false );
			b->endtime = gEngfuncs.GetClientTime() + life;
			b->start = start;
			b->end = end;
			return;
		}
	}

	gEngfuncs.Con_Printf( "beam list overflow!\n" );	
}

/*
=================
CL_UpdateBeams
=================
*/
void CL_UpdateBeams()
{
	int		i;
	float		d;
	beam_t		*b;
	Vector		dist, org, angles;
	TEMPENTITY	*ent;

	// update lightning
	for (i = 0, b = cl_beams; i< MAX_BEAMS ; i++, b++)
	{
		if (!b->model || b->endtime < gEngfuncs.GetClientTime())
			continue;

		VectorSubtract( b->end, b->start, dist );

		// calculate pitch and yaw
		VectorAngles( dist, angles );
		d = VectorNormalize( dist );

		// add new entities for the lightning
		org = b->start;

		while (d > 0)
		{
			ent = gEngfuncs.pEfxAPI->CL_TempEntAlloc( org, b->model );
			if( !ent ) return;

			ent->entity.angles[0] = angles[0];
			ent->entity.angles[1] = angles[1];
			ent->entity.angles[2] = gEngfuncs.pfnRandomFloat( 0, 360 );
			//ent->entity.curstate.effects = EF_FULLBRIGHT;
			ent->die = gEngfuncs.GetClientTime(); // die at next frame so we can realloc it again

			VectorMA( org, BEAM_LENGTH, dist, org );
			d -= BEAM_LENGTH;
		}
	}
	
}

/*
=================
CL_ParseBeam
=================
*/
void HUD_ParseBeam( const char *modelname )
{
	int	ent;
	Vector	start, end;

	ent = READ_SHORT ();
	
	start.x = READ_COORD ();
	start.y = READ_COORD ();
	start.z = READ_COORD ();
	
	end.x = READ_COORD ();
	end.y = READ_COORD ();
	end.z = READ_COORD ();
 
	CL_AllocBeam( modelname, ent, start, end );
}

int CHud :: MsgFunc_TempEntity( const char *pszName, int iSize, void *pbuf )
{
	BEGIN_READ( pbuf, iSize );
	int type = READ_BYTE();

	Vector pos;
	dlight_t *dl;
	int colorStart, colorLength;
	TEMPENTITY *pTemp;

	if (type != TE_LIGHTNING1 && type != TE_LIGHTNING2 && type != TE_LIGHTNING3)
	{
		// all quake temp ents begin from pos
		pos.x = READ_COORD ();
		pos.y = READ_COORD ();
		pos.z = READ_COORD ();
	}

	switch (type)
	{
	case TE_SPIKE:
	case TE_SUPERSPIKE:
		gEngfuncs.pEfxAPI->R_RunParticleEffect( pos, (0,0,0), 0, (type == TE_SPIKE) ? 10 : 20 );

		if(gEngfuncs.pfnRandomLong( 0, 5 ) % 5)
		{
			gEngfuncs.pfnPlaySoundByNameAtLocation( "quake/weapons/tink1.wav", 1.0, pos );
		}
		else
		{
			char soundpath[32];
			sprintf( soundpath, "quake/weapons/ric%i.wav", gEngfuncs.pfnRandomLong( 1, 3 ));
			gEngfuncs.pfnPlaySoundByNameAtLocation( soundpath, 1.0, pos );
		}
		break;
	case TE_GUNSHOT:
		gEngfuncs.pEfxAPI->R_RunParticleEffect( pos, (0, 0, 0), 0, 20 );
		break;
	case TE_EXPLOSION:
		dl = gEngfuncs.pEfxAPI->CL_AllocDlight (0);
		dl->origin = pos;
		dl->radius = 350;
		dl->color.r = dl->color.g = dl->color.b = 250;
		dl->die = gEngfuncs.GetClientTime() + 0.5;
		dl->decay = 300;
		pTemp = gEngfuncs.pEfxAPI->R_DefaultSprite( pos, gEngfuncs.pEventAPI->EV_FindModelIndex( "sprites/s_explod.spr" ), 0 );
		if (!pTemp) return 1;
		//pTemp->entity.curstate.effects = EF_FULLBRIGHT;
		pTemp->entity.curstate.rendermode = kRenderTransAlpha;
		pTemp->entity.curstate.renderamt = 255;
		gEngfuncs.pfnPlaySoundByNameAtLocation( "quake/weapons/r_exp3.wav", 1.0, pos );
		gEngfuncs.pEfxAPI->R_ParticleExplosion( pos );
		break;
	case TE_TAREXPLOSION:
		pTemp = gEngfuncs.pEfxAPI->R_DefaultSprite( pos, gEngfuncs.pEventAPI->EV_FindModelIndex( "sprites/s_explod.spr" ), 0 );
		if (!pTemp) return 1;
		//pTemp->entity.curstate.effects = EF_FULLBRIGHT;
		pTemp->entity.curstate.rendermode = kRenderTransAlpha;
		pTemp->entity.curstate.renderamt = 255;
		gEngfuncs.pfnPlaySoundByNameAtLocation( "quake/weapons/r_exp3.wav", 1.0, pos );
		gEngfuncs.pEfxAPI->R_BlobExplosion( pos );
		break;
	case TE_LIGHTNING1:
		HUD_ParseBeam( "models/quake/bolt.mdl" );
		break;
	case TE_LIGHTNING2:
		HUD_ParseBeam( "models/quake/bolt2.mdl" );
		break;
	case TE_WIZSPIKE:
		gEngfuncs.pEfxAPI->R_RunParticleEffect( pos, (0, 0, 0), 20, 30 );
		gEngfuncs.pfnPlaySoundByNameAtLocation( "quake/wizard/hit.wav", 1.0, pos );
                    {
			int decalIndex = gEngfuncs.pEfxAPI->Draw_DecalIndex( gEngfuncs.pEfxAPI->Draw_DecalIndexFromName( "{spit1" ));
			gEngfuncs.pEfxAPI->R_DecalShoot( decalIndex, 0, 0, pos, 0 );
		}
		break;
	case TE_KNIGHTSPIKE:
		gEngfuncs.pEfxAPI->R_RunParticleEffect( pos, (0, 0, 0), 226, 20 );
		gEngfuncs.pfnPlaySoundByNameAtLocation( "quake/hknight/hit.wav", 1.0, pos );
		break;
	case TE_LIGHTNING3:
		HUD_ParseBeam( "models/quake/bolt3.mdl" );
		break;
	case TE_LAVASPLASH:
		gEngfuncs.pEfxAPI->R_LavaSplash( pos );
		break;
	case TE_TELEPORT:
		gEngfuncs.pEfxAPI->R_TeleportSplash( pos );
		break;
	case TE_EXPLOSION2:
		colorStart = READ_BYTE ();
		colorLength = READ_BYTE ();
		dl = gEngfuncs.pEfxAPI->CL_AllocDlight (0);
		dl->origin = pos;
		dl->radius = 350;
		dl->color.r = dl->color.g = dl->color.b = 250;
		dl->die = gEngfuncs.GetClientTime() + 0.5;
		dl->decay = 300;
		gEngfuncs.pfnPlaySoundByNameAtLocation( "quake/weapons/r_exp3.wav", 1.0, pos );
		gEngfuncs.pEfxAPI->R_ParticleExplosion2( pos, colorStart, colorLength );
		break;
	case TE_BEAM: // grappling hook beam
		HUD_ParseBeam( "models/quake/beam.mdl" );
		break;
	}
	return 1;
}
