/***
*
*	Copyright (c) 1996-2002, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/
//
// battery.cpp
//
// implementation of CHudBattery class
//

#include "hud.h"
#include "cl_util.h"
#include "parsemsg.h"

#include <string.h>
#include <stdio.h>

#include "knockout_dll/powerups.h"

DECLARE_MESSAGE(m_Battery, Battery)

int CHudBattery::Init()
{
	m_iBat = 0;
	m_fFade = 0;
	m_iFlags = 0;

	HOOK_MESSAGE(Battery);

	gHUD.AddHudElem(this);

	return 1;
};


int CHudBattery::VidInit()
{
	m_hSprite1 = m_hSprite2 = 0;  // delaying get sprite handles until we know the sprites are loaded

	m_prc1 = &gHUD.GetSpriteRect( gHUD.GetSpriteIndex( "suit_empty" ) );
	m_prc2 = &gHUD.GetSpriteRect( gHUD.GetSpriteIndex( "suit_full" ) );
	m_iHeight = m_prc2->bottom - m_prc1->top;

	m_fFade = 0;

	VidInitPowerups();

	return 1;
};

int CHudBattery:: MsgFunc_Battery(const char *pszName,  int iSize, void *pbuf )
{
	m_iFlags |= HUD_ACTIVE;
	
	BEGIN_READ( pbuf, iSize );
	int x = READ_LONG();

	if ( x != m_iBat )
	{
		m_fFade = FADE_TIME;
		m_iBat = x;
	}

	return 1;
}

int CHudBattery::Draw(float flTime)
{
	DrawPowerups(flTime);
	DrawCounter(flTime);

	if ( gHUD.m_iHideHUDDisplay & HIDEHUD_HEALTH )
		return 1;
	
	wrect_t *suitfullspr;
	int hightint;
	char suitfullstring[64];
	char suitemptystring[64];

	int r, g, b, x, y, a;
	wrect_t rc;

	if ((gHUD.m_chzArmorIconPostfix != NULL) && (gHUD.m_chzArmorIconPostfix[0] == '\0'))
	{
		sprintf(suitfullstring, "suit_full");
		sprintf(suitemptystring, "suit_empty");
	}
	else
	{
		sprintf(suitfullstring, "suit_full_%s", gHUD.m_chzArmorIconPostfix);
		sprintf(suitemptystring, "suit_empty_%s", gHUD.m_chzArmorIconPostfix);

		//Nice job.
		if (strncmp(suitemptystring, "suit_empty_", 12) == 0)
			sprintf(suitemptystring, "suit_empty");

		if (strncmp(suitemptystring, "suit_full_", 11) == 0)
			sprintf(suitemptystring, "suit_full");
	}

	m_prc1 = &gHUD.GetSpriteRect(gHUD.GetSpriteIndex(suitemptystring));
	m_prc2 = &gHUD.GetSpriteRect(gHUD.GetSpriteIndex(suitfullstring));
	m_iHeight = m_prc2->bottom - m_prc1->top;

	suitfullspr = m_prc1; 
	suitfullspr = m_prc2; 
	hightint = m_iHeight; 

	rc = *suitfullspr;

	m_iBatMax = CVAR_GET_FLOAT("__hud_max_bat");

	float fScale = 0.0;
	
	if ( m_iBatMax > 0 )
		fScale = 1.0 / (float)m_iBatMax;

	rc.top  += hightint * ((float)(m_iBatMax-(V_min(m_iBatMax,m_iBat))) * fScale); // battery can go from 0 to m_iBatMax so * fScale goes from 0 to 1

	if( gHUD.isNightVisionOn() )
	{
		gHUD.getNightVisionHudItemColor( r, g, b );
	}
	else
	{
		r = giR;
		g = giG;
		b = giB;
	}

	if (gHUD.m_iCurrentPowerups & POWERUP_INVULNERABILITY)
		UnpackRGB(r, g, b, 0x00FF0000);

	if (!(gHUD.m_iWeaponBits & (1<<(WEAPON_SUIT)) ))
		return 1;

	// Has health changed? Flash the health #
	if (m_fFade)
	{
		if (m_fFade > FADE_TIME)
			m_fFade = FADE_TIME;

		m_fFade -= (gHUD.m_flTimeDelta * 20);
		if (m_fFade <= 0)
		{
			a = 128;
			m_fFade = 0;
		}

		// Fade the health number back to dim

		a = MIN_ALPHA +  (m_fFade/FADE_TIME) * 128;

	}
	else
		a = MIN_ALPHA;

	ScaleColors(r, g, b, a );
	
	int iOffset = (suitfullspr->bottom - suitfullspr->top)/6;

	int fontnum;

	fontnum = gHUD.m_iFontHeight; 

	y = ScreenHeight - fontnum - fontnum / 2;
	x = ScreenWidth/4;

	// make sure we have the right sprite handles
	//if ( !m_hSprite1 )
		m_hSprite1 = gHUD.GetSprite( gHUD.GetSpriteIndex( suitemptystring ) );
	//if ( !m_hSprite2 )
		m_hSprite2 = gHUD.GetSprite( gHUD.GetSpriteIndex( suitfullstring ) );

	SPR_Set(m_hSprite1, r, g, b );
	SPR_DrawAdditive( 0,  x, y - iOffset, suitfullspr);

	if (rc.bottom > rc.top)
	{
		SPR_Set(m_hSprite2, r, g, b );
		SPR_DrawAdditive( 0, x, y - iOffset + (rc.top - suitfullspr->top), &rc);
	}

	x += (suitfullspr->right - suitfullspr->left);

	if (gHUD.m_iCurrentPowerups & POWERUP_INVULNERABILITY)
		x = gHUD.DrawHudNumber(x, y, DHN_DRAWZERO, 666, r, g, b);
	else
		x = gHUD.DrawHudNumber(x, y, DHN_DRAWZERO, m_iBat, r, g, b);

	return 1;
}

void CHudBattery::VidInitPowerups()
{
	m_hSprite_EnvSuit = m_hSprite_QuadDMG = m_hSprite_Invuln = m_hSprite_Invis = 
	m_hSprite_PortHEV = m_hSprite_Regen = m_hSprite_Backpack = m_hSprite_Accel = 
	m_hSprite_LJ = m_hSprite_Marks = m_hSprite_Zerk = 0;

	m_prc_EnvSuit = &gHUD.GetSpriteRect(gHUD.GetSpriteIndex("artifact_envirosuit"));
	m_prc_QuadDMG = &gHUD.GetSpriteRect(gHUD.GetSpriteIndex("artifact_super_damage"));
	m_prc_Invuln = &gHUD.GetSpriteRect(gHUD.GetSpriteIndex("artifact_invulnerability"));
	m_prc_Invis = &gHUD.GetSpriteRect(gHUD.GetSpriteIndex("artifact_invisibility"));
	m_prc_PortHEV = &gHUD.GetSpriteRect(gHUD.GetSpriteIndex("artifact_portablehev"));
	m_prc_Regen = &gHUD.GetSpriteRect(gHUD.GetSpriteIndex("artifact_regeneration"));
	m_prc_Backpack = &gHUD.GetSpriteRect(gHUD.GetSpriteIndex("artifact_backpack"));
	m_prc_Accel = &gHUD.GetSpriteRect(gHUD.GetSpriteIndex("artifact_accelerator"));
	m_prc_LJ = &gHUD.GetSpriteRect(gHUD.GetSpriteIndex("artifact_longjump"));
	m_prc_Marks = &gHUD.GetSpriteRect(gHUD.GetSpriteIndex("artifact_marksman"));
	m_prc_Zerk = &gHUD.GetSpriteRect(gHUD.GetSpriteIndex("artifact_berserk"));

	m_bAlreadyDrawing = false;
};

int CHudBattery::DrawPowerups(float flTime)
{
	if (!gHUD.cl_show_powerups_hud->value)
		return 1;

	int y = ScreenHeight - 64;

	if (gHUD.m_iCurrentPowerups & POWERUP_ENVSUIT)
	{
		wrect_t* sprite = m_prc_EnvSuit;
		const char* spriteString = "artifact_envirosuit";
		m_hSprite_EnvSuit = gHUD.GetSprite(gHUD.GetSpriteIndex(spriteString));

		wrect_t rc = *sprite;

		y += (rc.top ) - rc.bottom - 5;

		int r, g, b;

		//255, 255, 255
		UnpackRGB(r, g, b, 0x00FFFFFF);

		int numb = gHUD.m_flPowRadsuitTimer;

		if (numb > 999)
			numb = 999;

		gEngfuncs.pfnSPR_Set(m_hSprite_EnvSuit, r, g, b);
		gEngfuncs.pfnSPR_DrawAdditive(0, 5, y, &rc);
		gHUD.DrawHudNumber(
			rc.right - rc.left + 10,
			y + ((rc.bottom - rc.top) / 2) - 5,
			DHN_DRAWZERO,
			numb, r, g, b);
	}

	if (gHUD.m_iCurrentPowerups & POWERUP_QUADDMG)
	{
		wrect_t* sprite = m_prc_QuadDMG;
		const char* spriteString = "artifact_super_damage";
		m_hSprite_QuadDMG = gHUD.GetSprite(gHUD.GetSpriteIndex(spriteString));

		wrect_t rc = *sprite;

		y += (rc.top) - rc.bottom - 5;

		int r, g, b;

		UnpackRGB(r, g, b, RGB_BLUEISH);

		int numb = gHUD.m_flPowSuperDamageTimer;

		if (numb > 999)
			numb = 999;

		gEngfuncs.pfnSPR_Set(m_hSprite_QuadDMG, r, g, b);
		gEngfuncs.pfnSPR_DrawAdditive(0, 5, y, &rc);
		gHUD.DrawHudNumber(
			rc.right - rc.left + 10,
			y + ((rc.bottom - rc.top) / 2) - 5,
			DHN_DRAWZERO,
			numb, r, g, b);
	}

	if (gHUD.m_iCurrentPowerups & POWERUP_INVULNERABILITY)
	{
		wrect_t* sprite = m_prc_Invuln;
		const char* spriteString = "artifact_invulnerability";
		m_hSprite_Invuln = gHUD.GetSprite(gHUD.GetSpriteIndex(spriteString));

		wrect_t rc = *sprite;

		y += (rc.top) - rc.bottom - 5;

		int r, g, b;

		//255, 0, 0
		UnpackRGB(r, g, b, 0x00FF0000);

		int numb = gHUD.m_flPowInvincibleTimer;

		if (numb > 999)
			numb = 999;

		gEngfuncs.pfnSPR_Set(m_hSprite_Invuln, r, g, b);
		gEngfuncs.pfnSPR_DrawAdditive(0, 5, y, &rc);
		gHUD.DrawHudNumber(
			rc.right - rc.left + 10,
			y + ((rc.bottom - rc.top) / 2) - 5,
			DHN_DRAWZERO,
			numb, r, g, b);
	}

	if (gHUD.m_iCurrentPowerups & POWERUP_INVISIBILITY)
	{
		wrect_t* sprite = m_prc_Invis;
		const char* spriteString = "artifact_invisibility";
		m_hSprite_Invis = gHUD.GetSprite(gHUD.GetSpriteIndex(spriteString));

		wrect_t rc = *sprite;

		y += (rc.top) - rc.bottom - 5;

		int r, g, b;

		//160, 160, 160
		UnpackRGB(r, g, b, 0x00A0A0A0);

		int numb = gHUD.m_flPowInvisibleTimer;

		if (numb > 999)
			numb = 999;

		gEngfuncs.pfnSPR_Set(m_hSprite_Invis, r, g, b);
		gEngfuncs.pfnSPR_DrawAdditive(0, 5, y, &rc);
		gHUD.DrawHudNumber(
			rc.right - rc.left + 10,
			y + ((rc.bottom - rc.top) / 2) - 5,
			DHN_DRAWZERO,
			numb, r, g, b);
	}

	if (gHUD.m_iCurrentPowerups & POWERUP_HEVREGEN)
	{
		wrect_t* sprite = m_prc_PortHEV;
		const char* spriteString = "artifact_portablehev";
		m_hSprite_PortHEV = gHUD.GetSprite(gHUD.GetSpriteIndex(spriteString));

		wrect_t rc = *sprite;

		y += (rc.top) - rc.bottom - 5;

		int r, g, b;

		//255, 151, 0
		UnpackRGB(r, g, b, 0x00FF9700);

		int numb = gHUD.m_flPowPortHEVTimer;

		if (numb > 999)
			numb = 999;

		gEngfuncs.pfnSPR_Set(m_hSprite_PortHEV, r, g, b);
		gEngfuncs.pfnSPR_DrawAdditive(0, 5, y, &rc);
		gHUD.DrawHudNumber(
			rc.right - rc.left + 10,
			y + ((rc.bottom - rc.top) / 2) - 5,
			DHN_DRAWZERO,
			numb, r, g, b);
	}

	if (gHUD.m_iCurrentPowerups & POWERUP_HEALTHREGEN)
	{
		wrect_t* sprite = m_prc_Regen;
		const char* spriteString = "artifact_regeneration";
		m_hSprite_Regen = gHUD.GetSprite(gHUD.GetSpriteIndex(spriteString));

		wrect_t rc = *sprite;

		y += (rc.top) - rc.bottom - 5;

		int r, g, b;

		//156, 255, 24
		UnpackRGB(r, g, b, 0x009CFF18);

		int numb = gHUD.m_flPowRegenTimer;

		if (numb > 999)
			numb = 999;

		gEngfuncs.pfnSPR_Set(m_hSprite_Regen, r, g, b);
		gEngfuncs.pfnSPR_DrawAdditive(0, 5, y, &rc);
		gHUD.DrawHudNumber(
			rc.right - rc.left + 10,
			y + ((rc.bottom - rc.top) / 2) - 5,
			DHN_DRAWZERO,
			numb, r, g, b);
	}

	if (gHUD.m_iCurrentPowerups & POWERUP_AMMOREGEN)
	{
		wrect_t* sprite = m_prc_Backpack;
		const char* spriteString = "artifact_backpack";
		m_hSprite_Backpack = gHUD.GetSprite(gHUD.GetSpriteIndex(spriteString));

		wrect_t rc = *sprite;

		y += (rc.top) - rc.bottom - 5;

		int r, g, b;

		//255, 255, 0
		UnpackRGB(r, g, b, 0x00FFFF00);

		int numb = gHUD.m_flPowBackpackTimer;

		if (numb > 999)
			numb = 999;

		gEngfuncs.pfnSPR_Set(m_hSprite_Backpack, r, g, b);
		gEngfuncs.pfnSPR_DrawAdditive(0, 5, y, &rc);
		gHUD.DrawHudNumber(
			rc.right - rc.left + 10,
			y + ((rc.bottom - rc.top) / 2) - 5,
			DHN_DRAWZERO,
			numb, r, g, b);
	}

	if (gHUD.m_iCurrentPowerups & POWERUP_LONGJUMP)
	{
		wrect_t* sprite = m_prc_LJ;
		const char* spriteString = "artifact_longjump";
		m_hSprite_LJ = gHUD.GetSprite(gHUD.GetSpriteIndex(spriteString));

		wrect_t rc = *sprite;

		y += (rc.top) - rc.bottom - 5;

		int r, g, b;

		UnpackRGB(r, g, b, RGB_YELLOWISH);

		int numb = gHUD.m_flPowLongJumpTimer;

		if (numb > 999)
			numb = 999;

		gEngfuncs.pfnSPR_Set(m_hSprite_LJ, r, g, b);
		gEngfuncs.pfnSPR_DrawAdditive(0, 5, y, &rc);
		gHUD.DrawHudNumber(
			rc.right - rc.left + 10,
			y + ((rc.bottom - rc.top) / 2) - 5,
			DHN_DRAWZERO,
			numb, r, g, b);
	}

	if (gHUD.m_iCurrentPowerups & POWERUP_ACCELERATOR)
	{
		wrect_t* sprite = m_prc_Accel;
		const char* spriteString = "artifact_accelerator";
		m_hSprite_Accel = gHUD.GetSprite(gHUD.GetSpriteIndex(spriteString));

		wrect_t rc = *sprite;

		y += (rc.top) - rc.bottom - 5;

		int r, g, b;

		//255, 0, 110
		UnpackRGB(r, g, b, 0x00FF006E);

		int numb = gHUD.m_flPowAccelleratorTimer;

		if (numb > 999)
			numb = 999;

		gEngfuncs.pfnSPR_Set(m_hSprite_Accel, r, g, b);
		gEngfuncs.pfnSPR_DrawAdditive(0, 5, y, &rc);
		gHUD.DrawHudNumber(
			rc.right - rc.left + 10,
			y + ((rc.bottom - rc.top) / 2) - 5,
			DHN_DRAWZERO,
			numb, r, g, b);
	}

	if (gHUD.m_iCurrentPowerups & POWERUP_MARKSMAN)
	{
		wrect_t* sprite = m_prc_Marks;
		const char* spriteString = "artifact_marksman";
		m_hSprite_Marks = gHUD.GetSprite(gHUD.GetSpriteIndex(spriteString));

		wrect_t rc = *sprite;

		y += (rc.top) - rc.bottom - 5;

		int r, g, b;

		UnpackRGB(r, g, b, RGB_GREENISH);

		int numb = gHUD.m_flPowMarksmanTimer;

		if (numb > 999)
			numb = 999;

		gEngfuncs.pfnSPR_Set(m_hSprite_Marks, r, g, b);
		gEngfuncs.pfnSPR_DrawAdditive(0, 5, y, &rc);
		gHUD.DrawHudNumber(
			rc.right - rc.left + 10,
			y + ((rc.bottom - rc.top) / 2) - 5,
			DHN_DRAWZERO,
			numb, r, g, b);
	}

	if (gHUD.m_iCurrentPowerups & POWERUP_BERSERK)
	{
		wrect_t* sprite = m_prc_Zerk;
		const char* spriteString = "artifact_berserk";
		m_hSprite_Zerk = gHUD.GetSprite(gHUD.GetSpriteIndex(spriteString));

		wrect_t rc = *sprite;

		y += (rc.top) - rc.bottom - 5;

		int r, g, b;

		UnpackRGB(r, g, b, RGB_REDISH);

		int numb = gHUD.m_flPowBerserkTimer;

		if (numb > 999)
			numb = 999;

		gEngfuncs.pfnSPR_Set(m_hSprite_Zerk, r, g, b);
		gEngfuncs.pfnSPR_DrawAdditive(0, 5, y, &rc);
		gHUD.DrawHudNumber(
			rc.right - rc.left + 10,
			y + ((rc.bottom - rc.top) / 2) - 5,
			DHN_DRAWZERO,
			numb, r, g, b);
	}

	return 1;
}

int CHudBattery::DrawCounter(float flTime)
{
	if (!gHUD.m_bDrawArbitraryCounter)
		return 1;

	// draw secondary ammo icons above normal ammo readout
	int x, y, r, g, b, AmmoWidth, hudnum, fonthight;
	r = giR;
	g = giG;
	b = giB;
	ScaleColors(r, g, b, 255);

	hudnum = gHUD.m_HUD_number_0;
	fonthight = gHUD.m_iFontHeight;

	AmmoWidth = gHUD.GetSpriteRect(hudnum).right - gHUD.GetSpriteRect(hudnum).left;

	y = ScreenHeight - (fonthight * 8);  // this is one font height higher than the weapon ammo values
	x = ScreenWidth - AmmoWidth;

	x -= AmmoWidth;
	y -= (gHUD.GetSpriteRect(hudnum).top - gHUD.GetSpriteRect(hudnum).bottom);

	// half a char gap between the ammo number and the previous pic
	x -= (AmmoWidth / 2);

	// draw the number, right-aligned
	x -= (gHUD.GetNumWidth(gHUD.m_iArbitraryCounterValue, DHN_DRAWZERO ) * AmmoWidth);
	gHUD.DrawHudNumber(x, y, DHN_DRAWZERO, gHUD.m_iArbitraryCounterValue, r, g, b);

	return 1;
}
